using System;

namespace MSSQLServerAuditor.DataAccess.Entity.Attributes
{
	[AttributeUsage(AttributeTargets.Property)]
	public class UniqueKeyAttribute : Attribute
	{
	}
}
