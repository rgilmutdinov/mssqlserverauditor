using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;
using MSSQLServerAuditor.DataAccess.Entity.Repositories.Interfaces;
using MSSQLServerAuditor.DataAccess.Extensions;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;
using MSSQLServerAuditor.DataAccess.Utils;

namespace MSSQLServerAuditor.DataAccess.Entity.Repositories
{
	public class CruRepository<T> : ICruRepository<T>
		where T : class, IEntity
	{
		protected readonly IAmbientDbContextLocator _ambientDbContextLocator;

		protected StorageContext DbContext
		{
			get
			{
				StorageContext dbContext = this._ambientDbContextLocator.Get<StorageContext>();

				if (dbContext == null)
				{
					throw new InvalidOperationException(
						"No ambient DbContext of type StorageContext found. This means that this repository method " +
						"has been called outside of the scope of a DbContextScope. A repository must only be accessed " +
						"within the scope of a DbContextScope, which takes care of creating the DbContext instances " +
						"that the repositories need and making them available as ambient contexts. " +
						"This is what ensures that, for any given DbContext-derived type, the same instance is used " +
						"throughout the duration of a business transaction. To fix this issue, use IDbContextScopeFactory " +
						"in your top-level business logic service method to create a DbContextScope that wraps the entire " +
						"business transaction that your service method implements. Then access this repository within that scope. " +
						"Refer to the comments in the IDbContextScope.cs file for more details."
					);
				}

				return dbContext;
			}
		}

		public CruRepository(IAmbientDbContextLocator ambientDbContextLocator)
		{
			if (ambientDbContextLocator == null)
			{
				throw new ArgumentNullException("ambientDbContextLocator");
			}

			this._ambientDbContextLocator = ambientDbContextLocator;
		}

		public int Count => DbSet.Count();

		public virtual IQueryable<T> All()
		{
			return DbSet.AsQueryable();
		}

		public virtual IQueryable<T> Get(
			Expression<Func<T, bool>>                 filter  = null,
			Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
			string                                    includeProperties = ""
		)
		{
			IQueryable<T> query = DbSet;

			if (filter != null)
			{
				query = query.Where(filter);
			}

			if (!string.IsNullOrWhiteSpace(includeProperties))
			{
				query = includeProperties
					.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
					.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
			}

			return orderBy?.Invoke(query).AsQueryable() ?? query.AsQueryable();
		}

		public virtual IQueryable<T> GetIncluding(
			Expression<Func<T, bool>>                 filter  = null,
			Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
			params Expression<Func<T, object>>[]      includes
		)
		{
			IQueryable<T> query = DbSet;

			if (filter != null)
			{
				query = query.Where(filter);
			}

			if (includes != null)
			{
				query = includes.Aggregate(query,
					(current, include) => current.Include(include));
			}

			return orderBy?.Invoke(query).AsQueryable() ?? query.AsQueryable();
		}

		public virtual T GetById(long id)
		{
			return DbSet.Find(id);
		}

		public virtual IQueryable<T> Filter(Expression<Func<T, bool>> predicate)
		{
			return DbSet.Where(predicate).AsQueryable();
		}

		public virtual IQueryable<T> Filter(Expression<Func<T, bool>> filter, out int total, int index = 0, int size = 10)
		{
			int skipCount = index * size;

			DbSet<T> dbSet = DbSet;

			IQueryable<T> result = filter != null
				? dbSet.Where(filter).AsQueryable()
				: dbSet.AsQueryable();

			result = skipCount == 0
				? result.Take(size)
				: result.Skip(skipCount).Take(size);

			total = result.Count();

			return result.AsQueryable();
		}

		public bool Contains(Expression<Func<T, bool>> predicate)
		{
			return DbSet.Count(predicate) > 0;
		}

		public virtual T Find(params object[] keys)
		{
			return DbSet.Find(keys);
		}

		public virtual T Find(Expression<Func<T, bool>> predicate)
		{
			return DbSet.FirstOrDefault(predicate);
		}

		public virtual T FindByUniqueKey(T specification)
		{
			Expression<Func<T, bool>> uniquePredicate =
				GetUniquePredicate(specification);

			return DbSet.FirstOrDefault(uniquePredicate);
		}

		public virtual IQueryable<T> QueryByUniqueKey(T specification)
		{
			Expression<Func<T, bool>> uniquePredicate =
				GetUniquePredicate(specification);

			return DbSet.Where(uniquePredicate);
		}

		public virtual T Create(T entity)
		{
			T entry = DbSet.Add(entity);

			return entry;
		}

		public virtual void Update(T entity)
		{
			DbEntityEntry<T> entry = DbContext.Entry(entity);

			DbSet.Attach(entity);

			entry.State = EntityState.Modified;
		}

		public virtual T InsertOrUpdate(T entity)
		{
			DbContext.InsertOrUpdate(entity);

			return entity;
		}

		public virtual T UpdateProperties(
			T entity,
			params Expression<Func<T, object>>[] properties
		)
		{
			DbSet.Attach(entity);

			DbEntityEntry<T> dbEntry = DbContext.Entry(entity);

			foreach (Expression<Func<T, object>> prop in properties)
			{
				dbEntry.Property(prop).IsModified = true;
			}

			return dbEntry.Entity;
		}

		protected DbSet<T> DbSet => DbContext.Set<T>();

		private Expression<Func<T, bool>> GetUniquePredicate(T entity)
		{
			PropertyInfo[] properties = ReflectionUtils
				.GetPropertiesWithAttribute<T, UniqueKeyAttribute>();

			if (properties.Length == 0)
			{
				properties = ReflectionUtils.GetPropertiesWithAttribute<T, KeyAttribute>();
			}

			// build where condition for unique key
			ParameterExpression parameter = Expression.Parameter(typeof(T));
			IEnumerable<BinaryExpression> matches = properties.Select(
				p => Expression.Equal(
					Expression.Property(parameter, p),
					Expression.Constant(p.GetValue(entity, null), p.PropertyType)
				)
			);

			Expression<Func<T, bool>> match = Expression.Lambda<Func<T, bool>>(
				matches.Aggregate(Expression.AndAlso),
				parameter
			);

			return match;
		}
	}
}
