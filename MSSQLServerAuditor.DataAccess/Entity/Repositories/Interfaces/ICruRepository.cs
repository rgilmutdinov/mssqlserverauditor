﻿using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Repositories.Interfaces
{
	public interface ICruRepository<T> : IReadRepository<T>, ICreateUpdateRepository<T>
		where T : class, IEntity
	{
	}
}
