﻿using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Repositories.Interfaces
{
	public interface ICrudRepository<T> : ICruRepository<T>, IDeleteRepository<T>
		where T : class, IEntity
	{
	}
}
