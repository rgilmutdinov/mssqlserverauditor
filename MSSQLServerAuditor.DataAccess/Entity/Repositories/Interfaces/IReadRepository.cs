﻿using System;
using System.Linq;
using System.Linq.Expressions;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Repositories.Interfaces
{
	public interface IReadRepository<T> where T : class, IEntity
	{
		int Count { get; }

		IQueryable<T> All();

		IQueryable<T> Get(
			Expression<Func<T, bool>> filter = null,
			Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
			string includeProperties = "");

		IQueryable<T> GetIncluding(
			Expression<Func<T, bool>> filter = null,
			Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
			params Expression<Func<T, object>>[] includes);

		T GetById(long id);

		IQueryable<T> Filter(Expression<Func<T, bool>> predicate);

		IQueryable<T> Filter(Expression<Func<T, bool>> filter, out int total, int index = 0, int size = 10);

		bool Contains(Expression<Func<T, bool>> predicate);

		T Find(params object[] keys);

		T Find(Expression<Func<T, bool>> predicate);

		T FindByUniqueKey(T specification);
	}
}
