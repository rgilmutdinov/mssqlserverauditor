using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.DataAccess.Entity.Repositories
{
	public class CrudRepository<T> : CruRepository<T>
		where T : class, IEntity
	{
		public CrudRepository(IAmbientDbContextLocator ambientDbContextLocator)
			: base(ambientDbContextLocator)
		{
		}

		public virtual T Delete(long id)
		{
			T entityToDelete = DbSet.Find(id);

			return Delete(entityToDelete);
		}

		public virtual T Delete(T entity)
		{
			if (DbContext.Entry(entity).State == EntityState.Detached)
			{
				DbSet.Attach(entity);
			}

			return DbSet.Remove(entity);
		}

		public virtual List<T> Delete(Expression<Func<T, bool>> predicate)
		{
			List<T>       deleted          = new List<T>();
			IQueryable<T> entitiesToDelete = Filter(predicate);

			foreach (T entity in entitiesToDelete)
			{
				DbSet<T> dbSet = DbSet;

				if (DbContext.Entry(entity).State == EntityState.Detached)
				{
					dbSet.Attach(entity);
				}

				deleted.Add(dbSet.Remove(entity));
			}

			return deleted;
		}
	}
}
