using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models.Extensions
{
	public static class EntityExtensions
	{
		public static long GetIdSafe(this IEntity entity)
		{
			if (entity == null)
			{
				return 0;
			}

			return entity.Id;
		}
	}
}
