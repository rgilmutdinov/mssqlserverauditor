using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBGroupServer : AuditableEntity
	{
		public const string TableName              = "d_ConnectionGroup_ServerInstance";

		public const string FieldId                = "d_ConnectionGroup_ServerInstance_id";
		public const string FieldConnectionGroupId = "d_ConnectionGroup_id";
		public const string FieldServerInstanceId  = "d_ServerInstance_id";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldConnectionGroupId)]
		public long? ConnectionGroupId { get; set; }

		[UniqueKey]
		[Column(FieldServerInstanceId)]
		public long ServerInstanceId { get; set; }

		[ForeignKey(nameof(ConnectionGroupId))]
		public virtual DBConnectionGroup ConnectionGroup { get; set; }

		[ForeignKey(nameof(ServerInstanceId))]
		public virtual DBServerInstance ServerInstance { get; set; }
	}
}
