using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBNodeAttribute : AuditableEntity
	{
		public const string TableName           = "d_NodeInstance_Attribute";

		public const string FieldId             = "d_NodeInstance_Attribute_id";
		public const string FieldNodeInstanceId = "d_NodeInstance_id";
		public const string FieldName           = "AttributeName";
		public const string FieldValue          = "AttributeValue";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldNodeInstanceId)]
		public long? NodeInstanceId { get; set; }

		[UniqueKey]
		[Column(FieldName)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Name { get; set; }

		[Column(FieldValue)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Value { get; set; }

		[ForeignKey(nameof(NodeInstanceId))]
		public virtual DBNodeInstance NodeInstance { get; set; }
	}
}
