﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.Common;

namespace MSSQLServerAuditor.DataAccess.Entity.Models.Contract
{
	public abstract class AuditableEntity : IEntity
	{
		public const string FieldDateCreated = "date_created";
		public const string FieldDateUpdated = "date_updated";

		public AuditableEntity()
		{
			DateCreated = DefaultValues.Date.Minimum;
		}

		[Column(FieldDateCreated)]
		public DateTime DateCreated { get; set; }

		[Column(FieldDateUpdated)]
		public DateTime? DateUpdated { get; set; }

		public abstract long Id { get; set; }

		public bool IsNew => Id == 0;
	}
}
