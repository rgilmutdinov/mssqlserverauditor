﻿namespace MSSQLServerAuditor.DataAccess.Entity.Models.Contract
{
	public interface IEntity
	{
		long Id { get; set; }
	}
}
