using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBQuery : AuditableEntity
	{
		public const string TableName                = "d_Query";

		public const string FieldId                  = "d_Query_id";
		public const string FieldNodeInstanceId      = "d_NodeInstance_id";
		public const string FieldTemplatQueryId      = "d_TemplateNodeQuery_id";
		public const string FieldServerInstanceId    = "d_ServerInstance_id";
		public const string FieldDefaultDatabaseName = "DefaultDatabaseName";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldNodeInstanceId)]
		public long NodeInstanceId { get; set; }

		[UniqueKey]
		[Column(FieldTemplatQueryId)]
		public long? TemplateQueryId { get; set; }

		[UniqueKey]
		[Column(FieldServerInstanceId)]
		public long? ServerInstanceId { get; set; }

		[Column(FieldDefaultDatabaseName)]
		[StringLength(Defaults.Size.TextMedium)]
		public string DefaultDatabaseName { get; set; }

		[ForeignKey(nameof(NodeInstanceId))]
		public virtual DBNodeInstance NodeInstance { get; set; }

		[ForeignKey(nameof(TemplateQueryId))]
		public virtual DBTemplateQuery TemplateQuery { get; set; }

		[ForeignKey(nameof(ServerInstanceId))]
		public virtual DBServerInstance ServerInstance { get; set; }

		public virtual ICollection<DBQueryParameter> QueryParameters { get; set; }

		public virtual ICollection<DBMetaResult>     MetaResults     { get; set; }

		public virtual ICollection<DBMetaSubResult>  MetaSubResults  { get; set; }

		public virtual ICollection<DBMetaEtlResult>  MetaEtlResults  { get; set; }
	}
}
