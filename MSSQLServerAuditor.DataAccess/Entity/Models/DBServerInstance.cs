using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public abstract class DBServerInstance : AuditableEntity
	{
		public const string TableName             = "d_ServerInstance";

		public const string FieldId               = "d_ServerInstance_id";
		public const string FieldLoginId          = "d_Login_id";
		public const string FieldConnectionTypeId = "d_ConnectionType_id";
		public const string FieldConnectionName   = "ConnectionName";
		public const string FieldConnectionAlias  = "ConnectionAlias";
		public const string FieldIsDynamic        = "IsDynamicConnection";
		public const string FieldIsActive         = "IsActive";
		public const string FieldIsDeleted        = "IsDeleted";

		public DBServerInstance()
		{
			IsActive  = false;
			IsDeleted = false;
			IsDynamic = false;
		}

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldLoginId)]
		public long? LoginId { get; set; }

		[UniqueKey]
		[Column(FieldConnectionName)]
		[StringLength(Defaults.Size.TextLong)]
		public string ConnectionName { get; set; }

		[Column(FieldConnectionAlias)]
		[StringLength(Defaults.Size.TextLong)]
		public string ConnectionAlias { get; set; }

		[UniqueKey]
		[Column(FieldConnectionTypeId)]
		public long ConnectionTypeId { get; set; }

		[UniqueKey]
		[Column(FieldIsDynamic)]
		public bool? IsDynamic { get; set; }

		[Column(FieldIsDeleted)]
		public bool IsDeleted { get; set; }

		[Column(FieldIsActive)]
		public bool IsActive { get; set; }

		[ForeignKey(nameof(LoginId))]
		public virtual DBLogin Login { get; set; }

		[ForeignKey(nameof(ConnectionTypeId))]
		public virtual DBConnectionType ConnectionType { get; set; }

		public virtual ICollection<DBQuery>      Queries     { get; set; }
		public virtual ICollection<DBQueryGroup> QueryGroups { get; set; }

		public virtual ICollection<DBGroupServer> GroupServers { get; set; }
	}

	[Table(TableName)]
	public class DBServerInstanceMssql : DBServerInstance
	{
		public new const string TableName = "d_ServerInstance_mssql";

		public const string FieldIsOdbc           = "IsOdbc";
		public const string FieldInstanceName     = "InstanceName";
		public const string FieldEnvironment      = "ServerEnvironment";
		public const string FieldHost             = "ServerHostName";
		public const string FieldDomain           = "ServerDomainName";
		public const string FieldVersion          = "InstanceVersion";
		public const string FieldConnectionString = "ConnectionString";

		[Column(FieldInstanceName)]
		[StringLength(Defaults.Size.TextLong)]
		public string InstanceName { get; set; }

		// connection environment (optional)
		[Column(FieldEnvironment)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Environment { get; set; }

		// connection host name (optional)
		[Column(FieldHost)]
		[StringLength(Defaults.Size.TextLarge)]
		public string Host { get; set; }

		// connection server domain name (optional)
		[Column(FieldDomain)]
		[StringLength(Defaults.Size.TextLarge)]
		public string Domain { get; set; }

		[Column(FieldVersion)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Version { get; set; }

		[Column(FieldIsOdbc)]
		public bool? IsOdbc { get; set; }

		[Column(FieldConnectionString)]
		[StringLength(Defaults.Size.TextXLarge)]
		public string ConnectionString { get; set; }
	}

	[Table(TableName)]
	public class DBServerInstanceSqlite : DBServerInstance
	{
		public new const string TableName = "d_ServerInstance_sqlite";

		public const string FieldDatabaseFilePath = "DatabaseFilePath";

		[Column(FieldDatabaseFilePath)]
		[StringLength(Defaults.Size.TextXLarge)]
		public string DatabaseFilePath { get; set; }
	}

	[Table(TableName)]
	public class DBServerInstanceInternal : DBServerInstance
	{
		public new const string TableName = "d_ServerInstance_internal";

		public const string FieldConnectionGroupSourceId = "d_ConnectionGroup_id_Source";
		public const string FieldServerInstanceSourceId  = "d_ServerInstance_id_Source";
		public const string FieldTemplateSourceId        = "d_Template_id_Source";

		[Column(FieldServerInstanceSourceId)]
		public long ServerInstanceSourceId { get; set; }

		[Column(FieldConnectionGroupSourceId)]
		public long ConnectionGroupSourceId { get; set; }

		[Column(FieldTemplateSourceId)]
		public long TemplateSourceId { get; set; }

		[ForeignKey(nameof(ConnectionGroupSourceId))]
		public virtual DBConnectionGroup ConnectionGroupSource { get; set; }

		[ForeignKey(nameof(ServerInstanceSourceId))]
		public virtual DBServerInstance ServerInstanceSource { get; set; }

		[ForeignKey(nameof(TemplateSourceId))]
		public virtual DBTemplate TemplateSource { get; set; }
	}

	[Table(TableName)]
	public class DBServerInstanceEventLog : DBServerInstance
	{
		public new const string TableName = "d_ServerInstance_eventlog";

		public const string FieldMachineName = "MachineName";

		[Column(FieldMachineName)]
		[StringLength(Defaults.Size.TextLong)]
		public string MachineName { get; set; }
	}

	[Table(TableName)]
	public class DBServerInstanceTdsql : DBServerInstance
	{
		public new const string TableName = "d_ServerInstance_tdsql";

		public const string FieldIsOdbc           = "IsOdbc";
		public const string FieldInstanceName     = "InstanceName";
		public const string FieldEnvironment      = "ServerEnvironment";
		public const string FieldHost             = "ServerHostName";
		public const string FieldDomain           = "ServerDomainName";
		public const string FieldVersion          = "InstanceVersion";
		public const string FieldConnectionString = "ConnectionString";

		[Column(FieldInstanceName)]
		[StringLength(Defaults.Size.TextLong)]
		public string InstanceName { get; set; }

		// connection environment (optional)
		[Column(FieldEnvironment)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Environment { get; set; }

		// connection host name (optional)
		[Column(FieldHost)]
		[StringLength(Defaults.Size.TextLarge)]
		public string Host { get; set; }

		// connection server domain name (optional)
		[Column(FieldDomain)]
		[StringLength(Defaults.Size.TextLarge)]
		public string Domain { get; set; }

		[Column(FieldVersion)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Version { get; set; }

		[Column(FieldIsOdbc)]
		public bool? IsOdbc { get; set; }

		[Column(FieldConnectionString)]
		[StringLength(Defaults.Size.TextXLarge)]
		public string ConnectionString { get; set; }
	}

	[Table(TableName)]
	public class DBServerInstanceWmi : DBServerInstance
	{
		public new const string TableName = "d_ServerInstance_wmi";

		public const string FieldAuthentication = "Authentication";
		public const string FieldImpersonation  = "Impersonation";
		public const string FieldHost           = "Host";
		public const string FieldAuthority      = "Authority";

		[Column(FieldAuthentication)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Authentication { get; set; }

		[Column(FieldImpersonation)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Impersonation { get; set; }

		[Column(FieldHost)]
		[StringLength(Defaults.Size.TextLarge)]
		public string Host { get; set; }

		[Column(FieldAuthority)]
		[StringLength(Defaults.Size.TextLarge)]
		public string Authority { get; set; }
	}

	[Table(TableName)]
	public class DBServerInstanceLdap : DBServerInstance
	{
		public new const string TableName = "d_ServerInstance_ldap";

		public const string FieldConnectionPath = "ConnectionPath";

		[Column(FieldConnectionPath)]
		[StringLength(Defaults.Size.TextLong)]
		public string ConnectionPath { get; set; }
	}

	[Table(TableName)]
	public class DBServerInstanceNetworkInformation : DBServerInstance
	{
		public new const string TableName = "d_ServerInstance_networkinformation";

		public const string FieldHost     = "Host";
		public const string FieldPort     = "Port";
		public const string FieldProtocol = "Protocol";
		public const string FieldTimeout  = "Timeout";

		[Column(FieldHost)]
		[StringLength(Defaults.Size.TextLong)]
		public string Host { get; set; }

		[Column(FieldPort)]
		public int? Port { get; set; }

		[Column(FieldProtocol)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Protocol { get; set; }

		[Column(FieldTimeout)]
		public int? Timeout { get; set; }
	}

	[Table(TableName)]
	public class DBServerInstanceSystem : DBServerInstance
	{
		public new const string TableName = "d_ServerInstance_system";

		public const string FieldDummy = "Dummy";

		[Column(FieldDummy)]
		[StringLength(Defaults.Size.TextLong)]
		public string Dummy { get; set; }
	}
}
