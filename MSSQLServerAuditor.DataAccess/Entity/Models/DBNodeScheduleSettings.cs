using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBNodeScheduleSettings : AuditableEntity
	{
		public const string TableName                    = "d_NodeInstanceSchedule_UserSettings";
		public const string FieldId                      = "d_NodeInstanceSchedule_UserSettings_id";
		public const string FieldNodeInstanceId          = "d_NodeInstance_id";
		public const string FieldUserId                  = "UserId";
		public const string FieldUserName                = "UserName";
		public const string FieldSendMessage             = "SendMessage";
		public const string FieldSendMessageNonEmptyOnly = "SendMessageNonEmptyOnly";
		public const string FieldMessageRecipients       = "SendMessageRecipients";
		public const string FieldMessageLanguage         = "SendMessageLanguage";
		public const string FieldIsEnabled               = "Enabled";
		public const string FieldCronExpression          = "CronExpression";
		public const string FieldTargetMachine           = "TargetMachine";
		public const string FieldServiceOnly             = "ServiceOnly";
		public const string FieldUpdateHierarchically    = "UpdateHierarchically";
		public const string FieldUpdateDeep              = "UpdateDeep";
		public const string FieldLastRan                 = "LastRan";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldNodeInstanceId)]
		public long NodeInstanceId { get; set; }

		[UniqueKey]
		[Column(FieldUserId)]
		[Required]
		[StringLength(Defaults.Size.TextMedium)]
		public string UId { get; set; }

		[Column(FieldUserName)]
		[StringLength(Defaults.Size.TextMedium)]
		public string Name { get; set; }

		[Column(FieldSendMessage)]
		public bool? SendMessage { get; set; }

		[Column(FieldIsEnabled)]
		public bool? IsEnabled { get; set; }

		[Column(FieldCronExpression)]
		[StringLength(Defaults.Size.TextSmall)]
		public string CronExpression { get; set; }

		[Column(FieldTargetMachine)]
		[StringLength(Defaults.Size.TextSmall)]
		public string TargetMachine { get; set; }

		[Column(FieldServiceOnly)]
		public bool ServiceOnly { get; set; }

		[Column(FieldLastRan)]
		public DateTime? LastRan { get; set; }

		[Column(FieldSendMessageNonEmptyOnly)]
		public bool SendMessageNonEmptyOnly { get; set; }

		[Column(FieldMessageRecipients)]
		[StringLength(Defaults.Size.TextMedium)]
		public string MessageRecipients { get; set; }

		[Column(FieldMessageLanguage)]
		[StringLength(Defaults.Size.TextSmall)]
		public string MessageLanguage { get; set; }

		[Column(FieldUpdateHierarchically)]
		public bool UpdateHierarchically { get; set; }

		[Column(FieldUpdateDeep)]
		public int UpdateDeep { get; set; }

		[ForeignKey(nameof(NodeInstanceId))]
		public virtual DBNodeInstance NodeInstance { get; set; }
	}
}
