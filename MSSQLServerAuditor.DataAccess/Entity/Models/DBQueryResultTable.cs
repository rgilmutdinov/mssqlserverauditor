﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBQueryResultTable : AuditableEntity
	{
		public const string TableName = "d_Query_ResultTable";

		public const string FieldId          = "d_Query_ResultTable_id";
		public const string FieldResultTable = "TableName";
		public const string FieldRevision    = "Revision";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldResultTable)]
		[StringLength(Defaults.Size.TextLarge)]
		public string Name { get; set; }

		[Column(FieldRevision)]
		public long? Revision { get; set; }

		public virtual ICollection<DBQueryResults> QueryResults { get; set; }
	}
}
