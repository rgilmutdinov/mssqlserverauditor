using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBTemplate : AuditableEntity
	{
		public const string TableName       = "d_Template";

		public const string FieldId         = "d_Template_id";
		public const string FieldName       = "TemplateUserName";
		public const string FieldUId        = "TemplateUserId";
		public const string FieldDirectory  = "TemplateUserDir";
		public const string FieldIsExternal = "IsExternal";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldName)]
		[StringLength(Defaults.Size.TextLarge)]
		public string Name { get; set; }

		[UniqueKey]
		[Column(FieldUId)]
		[StringLength(Defaults.Size.TextSmall)]
		public string UId { get; set; }

		[UniqueKey]
		[Column(FieldDirectory)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Directory { get; set; }

		[UniqueKey]
		[Column(FieldIsExternal)]
		public bool IsExternal { get; set; }

		public virtual ICollection<DBTemplateNode> TemplateNodes { get; set; }
	}
}
