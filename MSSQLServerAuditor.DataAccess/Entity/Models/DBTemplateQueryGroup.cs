using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBTemplateQueryGroup : AuditableEntity
	{
		public const string TableName                 = "d_TemplateNodeQueryGroup";

		public const string FieldId                   = "d_TemplateNodeQueryGroup_id";
		public const string FieldTemplateNodeParentId = "d_TemplateNodeParent_id";
		public const string FieldGroupId              = "TemplateNodeQueryGroupId";
		public const string FieldName                 = "TemplateNodeQueryGroupName";
		public const string FieldDefaultDatabaseField = "DefaultDatabaseField";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldTemplateNodeParentId)]
		[Required]
		public long TemplateNodeParentId { get; set; }

		[UniqueKey]
		[Column(FieldGroupId)]
		[StringLength(Defaults.Size.TextSmall)]
		public string GroupId { get; set; }

		[UniqueKey]
		[Column(FieldName)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Name { get; set; }

		[Column(FieldDefaultDatabaseField)]
		[StringLength(Defaults.Size.TextSmall)]
		public string DefaultDatabaseField { get; set; }

		[ForeignKey(nameof(TemplateNodeParentId))]
		public virtual DBTemplateNode TemplateNodeParent { get; set; }

		public virtual ICollection<DBTemplateQueryGroupParameter> TemplateQueryGroupParameters { get; set; }

		public virtual ICollection<DBQueryGroup> QueryGroups { get; set; }
	}
}
