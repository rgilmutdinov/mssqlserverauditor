using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBTemplateQuery : AuditableEntity
	{
		public const string TableName           = "d_TemplateNodeQuery";

		public const string FieldId             = "d_TemplateNodeQuery_id";
		public const string FieldTemplateNodeId = "d_TemplateNode_id";
		public const string FieldUId            = "TemplateNodeQueryUserId";
		public const string FieldName           = "TemplateNodeQueryUserName";
		public const string FieldHierarchy      = "TemplateNodeQueryUserHierarchy";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldTemplateNodeId)]
		public long? TemplateNodeId { get; set; }

		[UniqueKey]
		[Column(FieldUId)]
		[StringLength(Defaults.Size.TextSmall)]
		public string UId { get; set; }

		[UniqueKey]
		[Column(FieldName)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Name { get; set; }

		[UniqueKey]
		[Column(FieldHierarchy)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Hierarchy { get; set; }

		[ForeignKey(nameof(TemplateNodeId))]
		public virtual DBTemplateNode TemplateNode { get; set; }

		public virtual ICollection<DBTemplateQueryParameter> TemplateQueryParameters { get; set; }

		public virtual ICollection<DBQuery> Queries { get; set; }
	}
}
