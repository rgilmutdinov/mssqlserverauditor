using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBConnectionType : AuditableEntity
	{
		public const string TableName = "d_ConnectionType";

		public const string FieldId   = "d_ConnectionType_id";
		public const string FieldName = "ConnectionType";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldName)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Name { get; set; }

		public virtual ICollection<DBServerInstance> ServerInstances { get; set; }

		public virtual ICollection<DBConnectionGroup> ConnectionGroups { get; set; }
	}
}
