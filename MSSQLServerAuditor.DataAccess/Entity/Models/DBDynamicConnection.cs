using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBDynamicConnection : AuditableEntity
	{
		public const string TableName             = "d_DynamicConnection";

		public const string FieldId               = "d_DynamicConnection_id";
		public const string FieldQueryParentId    = "d_Query_parent_id";
		public const string FieldQueryId          = "d_Query_id";
		public const string FieldServerInstanceId = "d_ServerInstance_id";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldQueryParentId)]
		public long? QueryParentId { get; set; }

		[UniqueKey]
		[Column(FieldQueryId)]
		public long? QueryId { get; set; }

		[UniqueKey]
		[Column(FieldServerInstanceId)]
		public long ServerId { get; set; }

		[ForeignKey(nameof(QueryId))]
		public virtual DBQuery Query { get; set; }

		[ForeignKey(nameof(QueryParentId))]
		public virtual DBQuery QueryParent { get; set; }

		[ForeignKey(nameof(ServerId))]
		public virtual DBServerInstance ServerInstance { get; set; }
	}
}
