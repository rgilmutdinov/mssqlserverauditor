using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBQueryGroupParameter : AuditableEntity
	{
		public const string TableName                          = "d_Query_GroupParameter";

		public const string FieldId                            = "d_Query_GroupParameter_id";
		public const string FieldQueryGroupId                  = "d_Query_Group_id";
		public const string FieldTemplateQueryGroupParameterId = "d_TemplateNodeQueryGroupParameter_id";
		public const string FieldValue                         = "ParameterValue";
		public const string FieldIsEnabled                     = "IsParameterEnabled";

		public DBQueryGroupParameter()
		{
			IsEnabled = true;
		}

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldQueryGroupId)]
		public long? QueryGroupId { get; set; }

		[UniqueKey]
		[Column(FieldTemplateQueryGroupParameterId)]
		public long? TemplateQueryGroupParameterId { get; set; }

		[Column(FieldValue)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Value { get; set; }

		[Column(FieldIsEnabled)]
		public bool? IsEnabled { get; set; }

		[ForeignKey(nameof(QueryGroupId))]
		public virtual DBQueryGroup QueryGroup { get; set; }

		[ForeignKey(nameof(TemplateQueryGroupParameterId))]
		public virtual DBTemplateQueryGroupParameter TemplateQueryGroupParameter { get; set; }
	}
}
