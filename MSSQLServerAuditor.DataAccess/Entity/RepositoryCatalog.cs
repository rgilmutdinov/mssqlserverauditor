using System;
using System.Collections.Generic;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;
using MSSQLServerAuditor.DataAccess.Entity.Repositories;
using MSSQLServerAuditor.DataAccess.Scope.Implementations;

namespace MSSQLServerAuditor.DataAccess.Entity
{
	public class RepositoryCatalog
	{
		private readonly Dictionary<Type, object> _repositoryDictionary;
		private readonly Dictionary<Type, object> _repositoryLocks;
		private readonly RepositoryFactory        _factory;

		public RepositoryCatalog(AmbientDbContextLocator contextLocator)
		{
			Check.NotNull(contextLocator, nameof(contextLocator));

			this._factory              = new RepositoryFactory(contextLocator);
			this._repositoryDictionary = new Dictionary<Type, object>();
			this._repositoryLocks      = new Dictionary<Type, object>();

			CreateRepository<DBLogin>();
			CreateRepository<DBConnectionType>();
			CreateRepository<DBServerInstance>();
			CreateRepository<DBLastConnection>();
			CreateRepository<DBConnectionGroup>();
			CreateRepository<DBDynamicConnection>();
			CreateRepository<DBGroupServer>();
			CreateRepository<DBNodeInstance>();
			CreateRepository<DBNodeAttribute>();
			CreateRepository<DBMetaResult>();
			CreateRepository<DBMetaSubResult>();
			CreateRepository<DBMetaEtlResult>();
			CreateRepository<DBQuery>();
			CreateRepository<DBQueryResults>();
			CreateRepository<DBQueryResultTable>();
			CreateRepository<DBQueryParameter>();
			CreateRepository<DBQueryGroup>();
			CreateRepository<DBQueryGroupParameter>();
			CreateRepository<DBTemplate>();
			CreateRepository<DBTemplateNode>();
			CreateRepository<DBTemplateQuery>();
			CreateRepository<DBTemplateQueryParameter>();
			CreateRepository<DBTemplateQueryGroup>();
			CreateRepository<DBTemplateQueryGroupParameter>();
			CreateRepository<DBNodeSettings>();
			CreateRepository<DBNodeScheduleSettings>();
		}

		private void CreateRepository<TEntity>()
			where TEntity : class, IEntity
		{
			CrudRepository<TEntity> repository = this._factory
				.CreateCrudRepository<TEntity>();

			RegisterRepository(repository);
		}

		private void RegisterRepository<TEntity>(CrudRepository<TEntity> value)
			where TEntity : class, IEntity
		{
			Type entityType = typeof(TEntity);

			this._repositoryDictionary.Add(entityType, value);
			this._repositoryLocks.Add(entityType, new object());
		}

		public CrudRepository<TEntity> Get<TEntity>()
			where TEntity : class, IEntity
		{
			return (CrudRepository<TEntity>) this._repositoryDictionary[typeof(TEntity)];
		}

		public object GetLock<TEntity>()
			where TEntity : class, IEntity
		{
			return this._repositoryLocks[typeof (TEntity)];
		}
	}
}
