﻿using System.Data;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.DataAccess.Extensions
{
	public static class ContextScopeFactoryExtensions
	{
		public static IDbContextScope<StorageContext> CreateSerialized(
			this IDbContextScopeFactory<StorageContext> contextScopeFactory)
		{
			return contextScopeFactory.CreateWithTransaction(IsolationLevel.Serializable);
		}
	}
}
