using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;
using MSSQLServerAuditor.DataAccess.Utils;

namespace MSSQLServerAuditor.DataAccess.Extensions
{
	public static class AddOrUpdateExtensions
	{
		private static readonly ConcurrentDictionary<Type, PropertyInfo[]> PrimaryKeyCache =
			new ConcurrentDictionary<Type, PropertyInfo[]>();

		private static PropertyInfo[] PrimaryKeys<TEntity>() where TEntity : class
		{
			Type entityType = typeof (TEntity);

			return PrimaryKeyCache.GetOrAdd(entityType, type =>
			{
				return type.GetProperties()
					.Where(p => Attribute.IsDefined(p, typeof (KeyAttribute)) || "Id".EqualsIgnoreCase(p.Name))
					.ToArray();
			});
		}

		public static void InsertOrUpdate<TEntity>(
			this DbContext                    context,
			params TEntity[]                  entities)
			where TEntity : class
		{
			PropertyInfo[] primaryKeys = PrimaryKeys<TEntity>();
			PropertyInfo[] properties  = ReflectionUtils
				.GetPropertiesWithAttribute<TEntity, UniqueKeyAttribute>();

			if (properties.Length == 0)
			{
				properties = ReflectionUtils.GetPropertiesWithAttribute<TEntity, KeyAttribute>();
			}

			for (int i = 0; i < entities.Length; i++)
			{
				// build where condition for "identifiers"
				ParameterExpression parameter = Expression.Parameter(typeof(TEntity));
				IEnumerable<BinaryExpression> matches = properties.Select(
					p => Expression.Equal(
						Expression.Property(parameter, p),
						Expression.Constant(p.GetValue(entities[i], null), p.PropertyType)));

				Expression<Func<TEntity, bool>> match = Expression.Lambda<Func<TEntity, bool>>(
					matches.Aggregate(Expression.AndAlso),
					parameter);

				// match "identifiers" for current item
				TEntity current = context.Set<TEntity>().SingleOrDefault(match);

				if (current != null)
				{
					// update primary keys
					foreach (PropertyInfo k in primaryKeys)
					{
						k.SetValue(entities[i], k.GetValue(current, null), null);
					}

					// update all the values
					context.Entry(current).CurrentValues.SetValues(entities[i]);

					// ignore date created field for date tracking entities
					if (current is AuditableEntity)
					{
						AuditableEntity dte = current as AuditableEntity;
						context.Entry(dte).Property(e => e.DateCreated).IsModified = false;
					}

					// replace updated item
					entities[i] = current;
				}
				else
				{
					// add new item
					entities[i] = context.Set<TEntity>().Add(entities[i]);
				}
			}
		}

		public static void AndProperty<TEntity, TProperty>(
			this DbEntityEntry<TEntity>          entry,
			Expression<Func<TEntity, TProperty>> property,
			TProperty                            value
		) where TEntity : class
		{
			DbPropertyEntry<TEntity, TProperty> propEntry =
				entry.Property(property);

			propEntry.CurrentValue = value;
			propEntry.IsModified   = true;
		}
	}
}
