using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using Perceiveit.Data;

namespace MSSQLServerAuditor.DataAccess
{
	public class Storage
	{
		public Storage(DBDatabase dbDatabase, ICommandExecutor commandExecutor, string dbName)
		{
			this.DbDatabase      = dbDatabase;
			this.CommandExecutor = commandExecutor;
			this.DbName          = dbName;
		}

		public DBDatabase       DbDatabase      { get; private set; }
		public ICommandExecutor CommandExecutor { get; private set; }
		public string           DbName          { get; private set; }
	}
}
