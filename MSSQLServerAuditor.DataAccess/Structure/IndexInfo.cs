﻿using System.Collections.Generic;
using System.Linq;

namespace MSSQLServerAuditor.DataAccess.Structure
{
	public class IndexInfo
	{
		public static readonly IndexInfo Empty = new IndexInfo(string.Empty, false);

		public IndexInfo(string name, bool isUnique, params string[] fieldNames)
		{
			this.Name     = name;
			this.IsUnique = isUnique;

			this.FieldNames = fieldNames.Length > 0 
				? fieldNames.ToList() 
				: new List<string>();
		}

		public List<string> FieldNames { get; private set; }
		public string       Name       { get; private set; }
		public bool         IsUnique   { get; private set; }

		public bool IsCompound
		{
			get { return FieldNames.Count > 1; }	
		}

		public bool IsEmpty
		{
			get { return this == Empty || FieldNames.Count == 0; }
		}
	}
}
