﻿/* 
 * Copyright (C) 2014 Mehdi El Gueddari
 * http://mehdi.me
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

using System;
using System.Data;
using System.Data.Entity;
using MSSQLServerAuditor.DataAccess.Scope.Enums;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.DataAccess.Scope.Implementations
{
	public class DbContextScopeFactory<TDbContext> : IDbContextScopeFactory<TDbContext> where TDbContext : DbContext
	{
		private readonly IDbContextFactory<TDbContext> _dbContextFactory;

		public DbContextScopeFactory(IDbContextFactory<TDbContext> dbContextFactory = null)
		{
			_dbContextFactory = dbContextFactory;
		}

		public IDbContextScope<TDbContext> Create(DbContextScopeOption joiningOption = DbContextScopeOption.JoinExisting)
		{
			return new DbContextScope<TDbContext>(
				joiningOption: joiningOption, 
				readOnly: false, 
				isolationLevel: null, 
				dbContextFactory: _dbContextFactory);
		}

		public IDbContextReadOnlyScope<TDbContext> CreateReadOnly(DbContextScopeOption joiningOption = DbContextScopeOption.JoinExisting)
		{
			return new DbContextReadOnlyScope<TDbContext>(
				joiningOption: joiningOption, 
				isolationLevel: null, 
				dbContextFactory: _dbContextFactory);
		}

		public IDbContextScope<TDbContext> CreateWithTransaction(IsolationLevel isolationLevel)
		{
			return new DbContextScope<TDbContext>(
				joiningOption: DbContextScopeOption.ForceCreateNew, 
				readOnly: false, 
				isolationLevel: isolationLevel, 
				dbContextFactory: _dbContextFactory);
		}

		public IDbContextReadOnlyScope<TDbContext> CreateReadOnlyWithTransaction(IsolationLevel isolationLevel)
		{
			return new DbContextReadOnlyScope<TDbContext>(
				joiningOption: DbContextScopeOption.ForceCreateNew, 
				isolationLevel: isolationLevel, 
				dbContextFactory: _dbContextFactory);
		}

		public IDisposable SuppressAmbientContext()
		{
			return new AmbientContextSuppressor<TDbContext>();
		}
	}
}