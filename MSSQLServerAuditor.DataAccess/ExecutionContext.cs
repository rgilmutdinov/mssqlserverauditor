﻿using System.Data.Common;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Extensions;
using Perceiveit.Data;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess
{
	public class ExecutionContext
	{
		private readonly DBDatabase _database;

		public DbConnection  Connection  { get; }
		public DbTransaction Transaction { get; }

		public ExecutionContext(
			DBDatabase    database, 
			DbConnection  connection, 
			DbTransaction transaction = null)
		{
			Check.NotNull(database,   nameof(database));
			Check.NotNull(connection, nameof(connection));

			this._database = database;

			Connection     = connection;
			Transaction    = transaction;
		}

		public DbCommand CreateCommand(
			string                      query,
			params DbCommandParameter[] parameters)
		{
			DbCommand command = Connection.CreateCommand();
			command.CommandText = query;
			if (Transaction != null)
			{
				command.Transaction = Transaction;
			}

			foreach (DbCommandParameter parameter in parameters)
			{
				command.AddParameter(parameter.Value, parameter.Field);
			}

			return command;
		}

		public string GenerateSql(DBQuery query)
		{
			return query.ToSQLString(this._database);
		}

		public string DatabaseName => this._database.Name;
	}
}
