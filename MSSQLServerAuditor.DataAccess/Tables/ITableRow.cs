﻿using System.Collections.Generic;
using MSSQLServerAuditor.DataAccess.Structure;

namespace MSSQLServerAuditor.DataAccess.Tables
{
	public interface ITableRow
	{
		TableInfo TableInfo { get; }

		/// <summary>
		/// Values of the row
		/// </summary>
		Dictionary<string, object> Values { get; }

		/// <summary>
		/// Copy values to new row
		/// </summary>
		/// <param name="dest">Destination row</param>
		bool CopyValues(ITableRow dest);

		T GetValue<T>(string name, T defaultValue = default(T));

		void SetValue(string name, object value);
	}
}
