﻿using System;
using System.Data.Common;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Extensions;
using NLog;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class ExecuteNonQueryCommand : CommandBase<int>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IStorageConnectionFactory _connectionFactory;
		private readonly string                    _sqlQuery;
		private readonly DbCommandParameter[]      _cmdParameters;

		public ExecuteNonQueryCommand(
			IStorageConnectionFactory   connectionFactory,
			string                      sqlQuery,
			params DbCommandParameter[] cmdParameters)
		{
			this._connectionFactory = connectionFactory;
			this._sqlQuery          = sqlQuery;
			this._cmdParameters     = cmdParameters;
		}

		public override int Execute()
		{
			using (DbConnection connection = this._connectionFactory.CreateOpenedConnection())
			{
				using (DbCommand command = connection.CreateCommand())
				{
					command.CommandText = this._sqlQuery;
					foreach (DbCommandParameter parameter in this._cmdParameters)
					{
						command.AddParameter(parameter.Value, parameter.Field);
					}

					try
					{
						return command.ExecuteNonQuery();
					}
					catch (Exception exc)
					{
						Log.Error(exc, "Error executing non query command");

						throw;
					}
				}
			}
		}
	}
}
