﻿using System.Collections.Generic;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Tables;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class SelectRowsCommand : DatabaseCommand<List<ITableRow>>
	{
		private readonly DBSelectQuery            _selectQuery;
		private readonly TableInfo                _tableInfo;
		private readonly List<DbCommandParameter> _parameters;

		public SelectRowsCommand(
			ExecutionContext         context, 
			DBSelectQuery            selectQuery, 
			TableInfo                tableInfo,
			List<DbCommandParameter> parameters) : base(context)
		{
			this._selectQuery = selectQuery;
			this._tableInfo   = tableInfo;
			this._parameters  = parameters;
		}

		public override List<ITableRow> Execute()
		{
			string query = ExecutionContext.GenerateSql(this._selectQuery);

			return ExecuteReadRows(
				this._tableInfo, 
				query, 
				this._parameters.ToArray());
		}
	}
}