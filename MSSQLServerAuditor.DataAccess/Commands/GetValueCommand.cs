﻿using System.Collections.Generic;
using MSSQLServerAuditor.DataAccess.Structure;
using Perceiveit.Data;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class GetValueCommand<TResult> : DatabaseCommand<TResult>
	{
		private readonly TableInfo            _tableInfo;
		private readonly string               _selectField;
		private readonly DbCommandParameter[] _cmdParams;

		public GetValueCommand(
			ExecutionContext         context,
			TableInfo                tableInfo,
			string                   selectField, 
			List<DbCommandParameter> cmdParams) : base(context)
		{
			this._tableInfo   = tableInfo;
			this._selectField = selectField;
			this._cmdParams   = cmdParams.ToArray();
		}

		public override TResult Execute()
		{
			List<DBComparison> clauses = new List<DBComparison>();
			foreach (DbCommandParameter cmdParam in this._cmdParams)
			{
				string fieldName = cmdParam.Field.Name;
				clauses.Add(DBComparison.Compare(
					DBField.Field(fieldName), 
					Compare.Equals, 
					DBParam.Param(fieldName)));
			}

			DBSelectQuery selectQuery = DBQuery
				.Select()
				.Field(this._selectField)
				.From(this._tableInfo.Name)
				.WhereAll(clauses.ToArray())
				.TopN(1);

			string sqlQuery = ExecutionContext.GenerateSql(selectQuery);
			return (TResult) ExecuteScalar(sqlQuery, this._cmdParams);
		}
	}
}
