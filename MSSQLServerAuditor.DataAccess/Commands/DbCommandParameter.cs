﻿using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.DataAccess.Structure;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class DbCommandParameter
	{
		public DbCommandParameter(object value, FieldInfo field)
		{
			Check.NotNull(field, nameof(field));

			Value = value;
			Field = field;
		}

		public object    Value { get; private set; }
		public FieldInfo Field { get; private set; }
	}
}
