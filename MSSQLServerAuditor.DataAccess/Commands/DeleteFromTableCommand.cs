﻿using System.Collections.Generic;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class DeleteFromTableCommand : DatabaseCommand<long>
	{
		private readonly DBDeleteQuery            _deleteQuery;
		private readonly List<DbCommandParameter> _parameters;

		public DeleteFromTableCommand(
			ExecutionContext         context, 
			DBDeleteQuery            deleteQuery,
			List<DbCommandParameter> parameters) : base(context)
		{
			this._deleteQuery = deleteQuery;
			this._parameters  = parameters;
		}

		public override long Execute()
		{
			string sqlDelete = ExecutionContext.GenerateSql(this._deleteQuery);

			return ExecuteNonQuery(sqlDelete, this._parameters.ToArray());
		}
	}
}
