﻿namespace MSSQLServerAuditor.DataAccess.Commands.Interfaces
{
	internal interface ICommand<out TResult>
	{
		TResult Execute(ICommandExecutor cmdExecutor);
	}
}
