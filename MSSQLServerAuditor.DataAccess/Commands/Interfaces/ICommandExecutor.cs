﻿namespace MSSQLServerAuditor.DataAccess.Commands.Interfaces
{
	public interface ICommandExecutor
	{
		TResult Execute<TResult>(CommandBase<TResult> command);
	}
}