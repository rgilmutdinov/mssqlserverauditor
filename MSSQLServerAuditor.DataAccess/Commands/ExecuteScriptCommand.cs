﻿namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class ExecuteScriptCommand : DatabaseCommand<int>
	{
		private readonly string               _sqlScript;
		private readonly DbCommandParameter[] _parameters;

		public ExecuteScriptCommand(
			ExecutionContext            context,
			string                      sqlScript,
			params DbCommandParameter[] parameters)
			: base(context)
		{
			this._sqlScript  = sqlScript;
			this._parameters = parameters;
		}

		public override int Execute()
		{
			return ExecuteNonQuery(
				this._sqlScript, 
				this._parameters);
		}
	}
}
