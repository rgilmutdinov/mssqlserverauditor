using System;
using System.Data.Common;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class SelectReaderCommand : CommandBase<long>
	{
		private readonly DbConnection         _connection;
		private readonly DbParameter[]        _parameters;
		private readonly string               _query;
		private readonly Action<DbDataReader> _selectAction;

		public SelectReaderCommand(
			DbConnection         connection,
			string               query,
			Action<DbDataReader> selectAction,
			params DbParameter[] parameters
		)
		{
			this._connection   = connection;
			this._query        = query;
			this._selectAction = selectAction;
			this._parameters   = parameters;
		}

		public override long Execute()
		{
			long rows = 0L;

			using (DbCommand command = this.GetCommand(this._query))
			{
				if (this._parameters != null)
				{
					foreach (DbParameter parameter in this._parameters)
					{
						command.Parameters.Add(parameter);
					}
				}

				using (DbDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						rows++;

						this._selectAction(reader);
					}

					reader.Close();
				}
			}

			return rows;
		}

		private DbCommand GetCommand(string sql)
		{
			DbCommand dbCommand   = this._connection.CreateCommand();
			dbCommand.CommandText = sql;

			return dbCommand;
		}
	}
}
