using System;
using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Tables;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class UpdateRowCommand : DatabaseCommand<long>
	{
		private readonly List<ITableRow> _rows;
		private readonly TableInfo       _tableInfo;
		private readonly FieldInfo       _fieldId;

		private readonly Dictionary<string, FieldInfo> _fieldMap;

		public UpdateRowCommand(
			ExecutionContext context,
			TableInfo        tableInfo,
			string           fieldId
		) : base(context)
		{
			Check.NotNull(tableInfo, nameof(tableInfo));

			this._tableInfo = tableInfo;
			this._fieldId   = this._tableInfo.Fields.Find(fi => fi.Name == fieldId);

			if (this._fieldId == null)
			{
				throw new ArgumentException(
					$"Field '{fieldId}' does not exist in table definition"
				);
			}

			this._rows     = new List<ITableRow>();
			this._fieldMap = tableInfo.Fields.ToDictionary(
				fi => fi.Name,
				fi => fi
			);
		}

		public override long Execute()
		{
			long          totalRows   = 0L;
			DBUpdateQuery updateQuery = DBQuery.Update(this._tableInfo.Name);

			foreach (FieldInfo field in this._tableInfo.Fields)
			{
				if (field.Name != this._fieldId.Name)
				{
					updateQuery.Set(field.Name, DBParam.Param(field.Name));
				}
			}

			updateQuery.WhereFieldEquals(this._fieldId.Name, DBParam.Param(this._fieldId.Name));

			string sqlUpdate = ExecutionContext.GenerateSql(updateQuery);

			foreach (ITableRow row in this._rows)
			{
				List<DbCommandParameter> cmdParams = new List<DbCommandParameter>();

				foreach (KeyValuePair<string, object> pair in row.Values)
				{
					FieldInfo field = _fieldMap[pair.Key];

					if (field != null && !field.IsIdentity)
					{
						cmdParams.Add(new DbCommandParameter(pair.Value, field));
					}
				}

				cmdParams.Add(new DbCommandParameter(
					row.Values[this._fieldId.Name],
					this._fieldId
				));

				totalRows = this.ExecuteNonQuery(
					sqlUpdate,
					cmdParams.ToArray()
				);
			}

			this._rows.Clear();

			return totalRows;
		}

		public void AddRowToUpdate(ITableRow row)
		{
			this._rows.Add(row);
		}
	}
}
