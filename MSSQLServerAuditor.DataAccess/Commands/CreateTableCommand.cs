using System;
using System.Collections.Generic;
using MSSQLServerAuditor.DataAccess.Structure;
using NLog;
using Perceiveit.Data;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	internal class CreateTableCommand : DatabaseCommand<int>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public CreateTableCommand(
			ExecutionContext context,
			TableInfo        tableInfo
		) : base(context)
		{
			this.TableInfo = tableInfo;
		}

		public TableInfo TableInfo { get; private set; }

		public override int Execute()
		{
			try
			{
				DBCreateTableQuery createQuery =
					DBQuery.Create.Table(TableInfo.Name).IfNotExists();

				GetTableColumns().ForEach(c => createQuery.Add(c));

				IndexInfo key = TableInfo.PrimaryKey;

				if (key.IsCompound)
				{
					string[] pkColumns = key.FieldNames.ToArray();
					createQuery.Constraints(DBConstraint.PrimaryKey().Columns(pkColumns));
				}

				string query = ExecutionContext.GenerateSql(createQuery);

				return ExecuteNonQuery(query);
			}
			catch (Exception exc)
			{
				Log.Error(exc, $"Failed to create table '{TableInfo.Name}' at database '{ExecutionContext.DatabaseName}'");

				throw;
			}
		}

		private List<DBColumn> GetTableColumns()
		{
			List<DBColumn> columns = new List<DBColumn>();
			IndexInfo      key     = TableInfo.PrimaryKey;

			foreach (FieldInfo field in this.TableInfo.Fields)
			{
				DBColumnFlags flags = 0;

				if (!field.IsNotNull)
				{
					flags = DBColumnFlags.Nullable;
				}

				if (field.IsIdentity)
				{
					flags |= DBColumnFlags.AutoAssign;
				}

				if (!key.IsEmpty && !key.IsCompound)
				{
					string keyFieldName = key.FieldNames[0];

					if (field.Name == keyFieldName)
					{
						flags |= DBColumnFlags.PrimaryKey;
					}
				}

				DBColumn column = field.MaxLength == null
					? DBColumn.Column(field.Name, field.DbType, flags)
					: DBColumn.Column(field.Name, field.DbType, field.MaxLength.Value, flags);

				if (field.DefaultValue != null)
				{
					column = column.Default(DBConst.String(
						field.DefaultValue.ToString())
					);
				}

				columns.Add(column);
			}

			return columns;
		}
	}
}
