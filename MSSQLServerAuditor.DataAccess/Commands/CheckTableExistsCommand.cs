using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class CheckTableExistsCommand : DatabaseCommand<bool>
	{
		private readonly string _tableName;

		public CheckTableExistsCommand(
			ExecutionContext context,
			string           tableName
		) : base(context)
		{
			this._tableName = tableName;
		}

		public override bool Execute()
		{
			DBExistsTableQuery query     = DBQuery.Exists.Table(this._tableName);
			string             sqlExists = ExecutionContext.GenerateSql(query);

			return ExecuteDataTable(sqlExists).Rows.Count > 0;
		}
	}
}
