﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Extensions;
using NLog;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class ReadTablesCommand : CommandBase<DataTable[]>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IStorageConnectionFactory _connectionFactory;
		private readonly string                    _sqlQuery;
		private readonly DbCommandParameter[]      _cmdParameters;

		public ReadTablesCommand(
			IStorageConnectionFactory   connectionFactory,
			string                      sqlQuery,
			params DbCommandParameter[] cmdParameters)
		{
			this._connectionFactory = connectionFactory;
			this._sqlQuery          = sqlQuery;
			this._cmdParameters     = cmdParameters;
		}

		public override DataTable[] Execute()
		{
			List<DataTable> resultTables = new List<DataTable>();
			using (DbConnection connection = this._connectionFactory.CreateOpenedConnection())
			{
				using (DbCommand command = connection.CreateCommand())
				{
					command.CommandText = this._sqlQuery;
					foreach (DbCommandParameter parameter in this._cmdParameters)
					{
						command.AddParameter(parameter.Value, parameter.Field);
					}

					using (DbDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							DataTable table = new DataTable();
							table.Load(reader, LoadOption.OverwriteChanges);
							resultTables.Add(table);
						}

						reader.Close();
					}
				}
			}

			return resultTables.ToArray();
		}
	}
}
