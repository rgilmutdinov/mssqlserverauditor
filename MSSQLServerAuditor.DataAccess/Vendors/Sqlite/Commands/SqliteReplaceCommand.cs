using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Tables;
using NLog;

namespace MSSQLServerAuditor.DataAccess.Vendors.Sqlite.Commands
{
	public class SqliteReplaceCommand : CommandBase<long>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly List<ITableRow> _rows;

		public SQLiteConnection  Connection  { get; set; }
		public TableInfo         TableInfo   { get; set; }
		public SQLiteTransaction Transaction { get; set; }

		public SqliteReplaceCommand(
			SQLiteConnection  connection,
			TableInfo         tableInfo,
			SQLiteTransaction transaction = null
		)
		{
			this.Connection  = connection;
			this.TableInfo   = tableInfo;
			this.Transaction = transaction;
			this._rows       = new List<ITableRow>();
		}

		public void AddRowForReplacing(ITableRow row)
		{
			this._rows.Add(row);
		}

		public override long Execute()
		{
			long   totalRows      = 0L;
			string queryColumns    = "(";
			string queryValues     = "(";

			Dictionary<string, SQLiteParameter> parametersCache =
				new Dictionary<string, SQLiteParameter>();

			if (this.TableInfo != null)
			{
				bool ifFirst = true;

				foreach (FieldInfo field in this.TableInfo.Fields)
				{
					parametersCache.Add(
						field.Name,
						new SQLiteParameter(field.Name.AsParamName(), field.DbType)
					);

					string paramValue = field.Name.AsParamName();

					if (ifFirst)
					{
						queryColumns += field.Name.AsDbName();
						queryValues  += paramValue;

						ifFirst = false;
					}
					else
					{
						queryColumns += "," + field.Name.AsDbName();
						queryValues  += "," + paramValue;
					}
				}

				queryColumns += ")";
				queryValues  += ")";

				string tableName = this.TableInfo.Name.AsDbName();
				string sql       = $"INSERT OR REPLACE INTO {tableName} {queryColumns} VALUES {queryValues};";

				for (int i = 0; i < this._rows.Count; i++)
				{
					ITableRow row = this._rows[i];

					foreach (KeyValuePair<string, object> pair in row.Values)
					{
						parametersCache[pair.Key].Value = pair.Value;
					}

					long rows = ExecuteNonQuery(
						sql,
						parametersCache.Values);

					totalRows += rows;
				}

				this._rows.Clear();
			}

			return totalRows;
		}

		private long ExecuteNonQuery(
			string                       sql,
			IEnumerable<SQLiteParameter> parameters
		)
		{
			using (DbCommand command = new SQLiteCommand(sql, Connection, Transaction))
			{
				if (parameters != null)
				{
					foreach (SQLiteParameter parameter in parameters)
					{
						command.Parameters.Add(parameter);
					}
				}

				return command.ExecuteNonQuery();
			}
		}
	}
}
