using System;
using System.Data.SQLite;
using System.Threading;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using NLog;

namespace MSSQLServerAuditor.DataAccess.Vendors.Sqlite.Commands
{
	public class SqliteCommandExecutor : ICommandExecutor
	{
		private static readonly Logger Log         = LogManager.GetCurrentClassLogger();
		private const           int    MaxAttempts = 100;

		public TResult Execute<TResult>(CommandBase<TResult> command)
		{
			Random  random     = new Random();
			TResult result     = default(TResult);
			int     attempt    = 0;
			bool    keepTrying = true;

			while (keepTrying)
			{
				try
				{
					result     = command.Execute();
					keepTrying = false;
				}
				catch (SQLiteException exc)
				{
					if (attempt < MaxAttempts)
					{
						if (exc.ResultCode == SQLiteErrorCode.Busy)
						{
							Log.Error($"SQLite is busy: Attempt: '{attempt}', Error: '{exc}'");

							Thread.Sleep(random.Next(100, 200));
						}
						else if (exc.ResultCode == SQLiteErrorCode.Locked)
						{
							Log.Error($"SQLite is locked:Attempt: '{attempt}', Error: '{exc}'");

							Thread.Sleep(random.Next(100, 200));
						}
						else
						{
							Log.Error($"Unrecovered SQLite problem: Attempt: '{attempt}', Error: '{exc}'");

							throw;
						}
					}
					else
					{
						Log.Error($"Exceeded maximum number of attempts: Attempt: '{attempt}', Error: '{exc}'");

						throw;
					}
				}

				attempt++;
			}

			return result;
		}
	}
}
