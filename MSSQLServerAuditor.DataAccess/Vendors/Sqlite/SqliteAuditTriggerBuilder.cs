﻿using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Vendors.Sqlite
{
	public class SqliteAuditTriggerBuilder : AuditTriggerBuilder
	{
		protected override string GetDateUpdatedTriggerSql(string tableName, string keyColumnName)
		{
			string sqlUpdateTrigger = string.Format(
				"CREATE TRIGGER [{0}_date_updated] " +
				"AFTER UPDATE ON [{0}] " +
				"FOR EACH ROW " +
				"BEGIN " +
				"UPDATE [{0}] SET {1} = (datetime('now', 'localtime')) " +
				"WHERE [{2}] = old.[{2}] AND ({1} IS NULL OR {1} != (datetime('now', 'localtime'))); " +
				"END;",
				tableName,
				AuditableEntity.FieldDateUpdated,
				keyColumnName);

			return sqlUpdateTrigger;
		}

		protected override string GetDateCreatedTriggerSql(string tableName, string keyColumnName)
		{
			string sqlInsertTrigger = string.Format(
				"CREATE TRIGGER [{0}_date_created] " +
				"AFTER INSERT ON [{0}] " +
				"FOR EACH ROW " +
				"BEGIN " +
				"UPDATE [{0}] SET {1} = (datetime('now', 'localtime')) " +
				"WHERE [{2}] = NEW.[{2}]; " +
				"END;",
				tableName,
				AuditableEntity.FieldDateCreated,
				keyColumnName);

			return sqlInsertTrigger;
		}
	}
}