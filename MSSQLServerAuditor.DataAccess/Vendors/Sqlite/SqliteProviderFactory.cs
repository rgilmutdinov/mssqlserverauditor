using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Vendors.Sqlite.Commands;
using MSSQLServerAuditor.DataAccess.Vendors.Sqlite.Connections;
using Perceiveit.Data;

namespace MSSQLServerAuditor.DataAccess.Vendors.Sqlite
{
	public class SqliteProviderFactory : IDatabaseProviderFactory
	{
		private const string SqliteProviderName = "System.Data.SQLite";

		public IConnectionFactory CreateConnectionFactory()
		{
			return new SqliteConnectionFactory();
		}

		public DBDatabase CreateDatabase(string connectionString)
		{
			return DBDatabase.Create(connectionString, SqliteProviderName);
		}

		public DBDatabase GetDatabase(string connectionString)
		{
			return CreateDatabase(connectionString);
		}

		public ICommandExecutor CreateCommandExecutor()
		{
			return new SqliteCommandExecutor();
		}
	}
}
