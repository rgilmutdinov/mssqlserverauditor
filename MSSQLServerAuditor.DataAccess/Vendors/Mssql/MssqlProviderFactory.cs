using System.Data.SqlClient;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Vendors.Mssql.Commands;
using MSSQLServerAuditor.DataAccess.Vendors.Mssql.Connections;
using NLog;
using Perceiveit.Data;

namespace MSSQLServerAuditor.DataAccess.Vendors.Mssql
{
	public class MssqlProviderFactory : IDatabaseProviderFactory
	{
		private static readonly Logger Log               = LogManager.GetCurrentClassLogger();
		private const           string MssqlProviderName = "System.Data.SqlClient";

		public IConnectionFactory CreateConnectionFactory()
		{
			return new MssqlConnectionFactory();
		}

		public DBDatabase CreateDatabase(string connectionString)
		{
			SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
			string                     dbName  = builder.InitialCatalog;
			
			builder.InitialCatalog          = string.Empty;
			string creationConnectionString = builder.ToString();

			using (SqlConnection sqlConnection = new SqlConnection(creationConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
				{
					sqlCommand.CommandText = string.Format(
						"IF NOT EXISTS (SELECT 1 FROM [master].[dbo].[sysdatabases] WHERE [name] = N'{0}') CREATE DATABASE [{0}];",
						dbName
					);

					Log.Debug($"sqlCommand.CommandText:'{sqlCommand.CommandText}'");

					sqlCommand.ExecuteNonQuery();
				}
			}

			return DBDatabase.Create(connectionString, MssqlProviderName);
		}

		public DBDatabase GetDatabase(string connectionString)
		{
			return DBDatabase.Create(connectionString, MssqlProviderName);
		}

		public ICommandExecutor CreateCommandExecutor()
		{
			return new MssqlCommandExecutor();
		}
	}
}
