﻿using System;
using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Vendors.Mssql
{
	public class MssqlAuditTriggerBuilder : AuditTriggerBuilder
	{
		protected override string GetDateUpdatedTriggerSql(string tableName, string keyColumnName)
		{
			string sqlUpdateTrigger = String.Format(
				"CREATE TRIGGER [{0}_date_updated] ON [{0}]"                                                   + "\n" +
				"AFTER UPDATE"                                                                                 + "\n" +
				"AS"                                                                                           + "\n" +
				"BEGIN"                                                                                        + "\n" +
				"    UPDATE [{0}] "                                                                            + "\n" +
				"    SET {1} = GETDATE() "                                                                     + "\n" +
				"    FROM inserted"                                                                            + "\n" +
				"    WHERE [{0}].[{2}] = inserted.[{2}] AND ([{0}].[{1}] IS NULL OR [{0}].[{1}] != GETDATE())" + "\n" +
				"END",
				tableName,
				AuditableEntity.FieldDateUpdated,
				keyColumnName);

			return sqlUpdateTrigger;
		}

		protected override string GetDateCreatedTriggerSql(string tableName, string keyColumnName)
		{
			string sqlInsertTrigger = String.Format(
				"CREATE TRIGGER [{0}_date_created] ON [{0}] " + "\n" +
				"AFTER INSERT"                                + "\n" +
				"AS"                                          + "\n" +
				"BEGIN"                                       + "\n" +
				"    UPDATE [{0}]"                            + "\n" +
				"    SET {1} = GETDATE() "                    + "\n" +
				"    FROM inserted"                           + "\n" +
				"    WHERE [{0}].[{2}] = inserted.[{2}] "     + "\n" +
				"END",

				tableName,
				AuditableEntity.FieldDateCreated,
				keyColumnName);

			return sqlInsertTrigger;
		}
	}
}