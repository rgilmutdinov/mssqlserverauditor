using System.Data.Entity.Migrations.Model;
using System.Data.Entity.SqlServer;
using System.Linq;

namespace MSSQLServerAuditor.DataAccess.Vendors.Mssql.Configuration
{
	/// <summary>
	/// Sql generator that generates PKs and FKs with custom names
	/// </summary>
	public class MssqlServerMigrationGenerator : SqlServerMigrationSqlGenerator
	{
		protected override void Generate(AddForeignKeyOperation addFkOp)
		{
			addFkOp.Name = GetFkName(
				addFkOp.PrincipalTable,
				addFkOp.DependentTable,
				addFkOp.DependentColumns.ToArray()
			);

			base.Generate(addFkOp);
		}

		protected override void Generate(DropForeignKeyOperation dropFkOp)
		{
			dropFkOp.Name = GetFkName(
				dropFkOp.PrincipalTable,
				dropFkOp.DependentTable,
				dropFkOp.DependentColumns.ToArray()
			);

			base.Generate(dropFkOp);
		}

		protected override void Generate(CreateTableOperation createTableOp)
		{
			createTableOp.PrimaryKey.Name = GetPkName(
				createTableOp.Name
			);

			base.Generate(createTableOp);
		}

		protected override void Generate(AddPrimaryKeyOperation addPkOp)
		{
			addPkOp.Name = GetPkName(
				addPkOp.Table
			);

			base.Generate(addPkOp);
		}

		protected override void Generate(DropPrimaryKeyOperation dropPkOp)
		{
			dropPkOp.Name = GetPkName(
				dropPkOp.Table
			);

			base.Generate(dropPkOp);
		}

		private static string GetPkName(string table)
		{
			return string.Format(
				"PK_{0}",
				table.Replace(".", "_")
			);
		}

		private static string GetFkName(
			string          primaryKeyTable,
			string          foreignKeyTable,
			params string[] foreignTableFields
		)
		{
			string fields = string.Join("_", foreignTableFields);

			return string.Format("FK_{0}_{1}_{2}",
				primaryKeyTable.Replace(".", "_"),
				foreignKeyTable.Replace(".", "_"),
				fields
			);
		}
	}
}
