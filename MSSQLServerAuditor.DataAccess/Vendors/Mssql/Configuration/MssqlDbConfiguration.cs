using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace MSSQLServerAuditor.DataAccess.Vendors.Mssql.Configuration
{
	public class MssqlDbConfiguration : DbConfiguration
	{
		public MssqlDbConfiguration()
		{
			SetMigrationSqlGenerator(SqlProviderServices.ProviderInvariantName,
				() => new MssqlServerMigrationGenerator()
			);
		}
	}
}
