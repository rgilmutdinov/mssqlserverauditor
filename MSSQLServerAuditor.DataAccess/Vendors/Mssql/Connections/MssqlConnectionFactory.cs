using System;
using System.Collections.Generic;
using System.Data.Common;
using MSSQLServerAuditor.DataAccess.Connections;

namespace MSSQLServerAuditor.DataAccess.Vendors.Mssql.Connections
{
	public class MssqlConnectionFactory : IConnectionFactory
	{
		private const    string            MssqlProviderName = "System.Data.SqlClient";
		private readonly DbProviderFactory _factory;

		public MssqlConnectionFactory()
		{
			this._factory = DbProviderFactories.GetFactory(MssqlProviderName);
		}

		public DbConnection CreateConnection(string connectionString)
		{
			DbConnection connection = this._factory.CreateConnection();

			if (connection != null)
			{
				connection.ConnectionString = connectionString;

				return connection;
			}

			throw new ApplicationException(Errors.FailedCreateMssqlConnection);
		}

		public DbConnection CreateOpenedConnection(string connectionString)
		{
			DbConnection connection = CreateConnection(connectionString);

			connection.Open();

			return connection;
		}

		public DbConnection CreateOpenedJoinedConnection(
			string                     connectionString,
			Dictionary<string, string> databasesToAttach
		)
		{
			return CreateOpenedConnection(connectionString);
		}
	}
}
