using System.Data.Common;
using MSSQLServerAuditor.DataAccess.Connections;

namespace MSSQLServerAuditor.DataAccess.Vendors.Mssql.Connections
{
	public class MssqlStorageConnectionFactory : IStorageConnectionFactory
	{
		private readonly string                 _connectionString;
		private readonly MssqlConnectionFactory _factory;

		public MssqlStorageConnectionFactory(
			string connectionString
		)
		{
			this._connectionString = connectionString;
			this._factory          = new MssqlConnectionFactory();
		}

		public DbConnection CreateOpenedConnection()
		{
			return this._factory.CreateOpenedConnection(this._connectionString);
		}
	}
}
