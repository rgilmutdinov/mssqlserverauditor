/*  Copyright 2009 PerceiveIT Limited
 *  This file is part of the DynaSQL library.
 *
*  DynaSQL is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DynaSQL is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Query in the COPYING.txt file.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#region

using System;
using System.Data;
using System.Xml.Serialization;

#endregion

namespace Perceiveit.Data
{
	/// <summary>
	/// Contains helper methods for the Query framework
	/// </summary>
	internal static class DBHelper
	{
		#region internal static System.Data.DbType GetDBTypeForObject(object val)

		/// <summary>
		/// Helper method that Attempts to match the closet DbType for this object
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		internal static DbType GetDBTypeForObject(object val)
		{
			if (null == val || val is DBNull)
				return DbType.Object;

			Type runtime = val.GetType();
			return GetDBTypeForRuntimeType(runtime);
		}

		#endregion

		#region internal static System.Data.DbType GetDBTypeForRuntimeType(Type runtimeType)

		/// <summary>
		/// Helper method that attempts to match the closest DbType based upon the runtime type
		/// </summary>
		/// <param name="runtimeType"></param>
		/// <returns></returns>
		internal static DbType GetDBTypeForRuntimeType(Type runtimeType)
		{
			DbType type;
			TypeCode code = Type.GetTypeCode(runtimeType);
			switch (code)
			{
				case TypeCode.Boolean:
					type = DbType.Boolean;
					break;
				case TypeCode.Byte:
					type = DbType.Byte;
					break;
				case TypeCode.Char:
					type = DbType.AnsiStringFixedLength;
					break;
				case TypeCode.DBNull:
					type = DbType.Object;
					break;
				case TypeCode.DateTime:
					type = DbType.DateTime;
					break;
				case TypeCode.Decimal:
					type = DbType.Decimal;
					break;
				case TypeCode.Double:
					type = DbType.Double;
					break;
				case TypeCode.Empty:
					type = DbType.Object;
					break;
				case TypeCode.Int16:
					type = DbType.Int16;
					break;
				case TypeCode.Int32:
					type = DbType.Int32;
					break;
				case TypeCode.Int64:
					type = DbType.Int64;
					break;
				case TypeCode.SByte:
					type = DbType.SByte;
					break;
				case TypeCode.Single:
					type = DbType.Single;
					break;
				case TypeCode.String:
					type = DbType.String;
					break;
				case TypeCode.UInt16:
					type = DbType.UInt16;
					break;
				case TypeCode.UInt32:
					type = DbType.UInt32;
					break;
				case TypeCode.UInt64:
					type = DbType.UInt64;
					break;

				case TypeCode.Object:
				default:
					if (runtimeType == typeof (byte[]))
						type = DbType.Binary;
					else if (runtimeType == typeof (Guid))
						type = DbType.Guid;
					else if (runtimeType == typeof (TimeSpan))
						type = DbType.DateTimeOffset;
					else
						type = DbType.Object;

					break;
			}

			return type;
		}

		#endregion

		#region internal static Type GetRuntimeTypeForDbType(System.Data.DbType dbtype)

		/// <summary>
		/// Helper method that attempts to find the closest
		/// match runtime Type based upon the dbType
		/// </summary>
		/// <param name="dbtype"></param>
		/// <returns></returns>
		internal static Type GetRuntimeTypeForDbType(DbType dbtype)
		{
			Type runtime;
			switch (dbtype)
			{
				case DbType.AnsiString:
				case DbType.AnsiStringFixedLength:
				case DbType.String:
				case DbType.StringFixedLength:
					runtime = typeof (string);
					break;

				case DbType.Binary:
					runtime = typeof (byte[]);
					break;

				case DbType.Boolean:
					runtime = typeof (bool);
					break;

				case DbType.Byte:
					runtime = typeof (byte);
					break;

				case DbType.Date:
				case DbType.DateTime:
				case DbType.DateTime2:
				case DbType.Time:
					runtime = typeof (DateTime);
					break;

				case DbType.DateTimeOffset:
					runtime = typeof (TimeSpan);
					break;

				case DbType.Double:
					runtime = typeof (Double);
					break;

				case DbType.Guid:
					runtime = typeof (Guid);
					break;

				case DbType.Int16:
					runtime = typeof (Int16);
					break;

				case DbType.Int32:
					runtime = typeof (Int32);
					break;

				case DbType.Int64:
					runtime = typeof (Int64);
					break;

				case DbType.Object:
					runtime = typeof (Object);
					break;

				case DbType.SByte:
					runtime = typeof (SByte);
					break;

				case DbType.Single:
					runtime = typeof (Single);
					break;

				case DbType.UInt16:
					runtime = typeof (UInt16);
					break;

				case DbType.UInt32:
					runtime = typeof (UInt32);
					break;

				case DbType.UInt64:
					runtime = typeof (UInt64);
					break;

				case DbType.VarNumeric:
				case DbType.Currency:
				case DbType.Decimal:
					runtime = typeof (Decimal);
					break;

				case DbType.Xml:
					runtime = typeof (IXmlSerializable);
					break;

				default:
					throw new ArgumentOutOfRangeException("dbtype");
			}

			return runtime;
		}

		#endregion
	}
}