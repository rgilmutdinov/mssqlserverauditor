#region

using System.Xml;

#endregion

namespace Perceiveit.Data.Query
{
	public partial class DBQuery
	{
		public static class Exists
		{
			public static DBExistsTableQuery Table(string name)
			{
				return DBExistsTableQuery.Table(name);
			}
		}
	}

	public abstract class DBExistsTableQuery : DBQuery
	{
		public string TableName { get; set; }

		protected DBExistsTableQuery(string name)
		{
			TableName = name;
		}

		/// <summary>
		/// Instantiates and returns a new EXISTS query for the specified name
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static DBExistsTableQuery Table(string name)
		{
			return new DBExistsTableQueryRef(name);
		}
	}

	internal class DBExistsTableQueryRef : DBExistsTableQuery
	{
		internal DBExistsTableQueryRef(string name) : base(name)
		{
		}

		public override bool BuildStatement(DBStatementBuilder builder)
		{
			builder.BeginSelectStatement();

			builder.BeginSelectList();
			DBSelectSet.SelectAll().BuildStatement(builder);
			builder.EndSelectList();

			builder.BeginFromList();
			builder.GetTablesSchemaSet().BuildStatement(builder);
			builder.EndFromList();

			builder.BeginWhereStatement();
			builder.GetTablesFilterSet(TableName).BuildStatement(builder);
			builder.EndWhereStatement();

			builder.EndSelectStatement();

			return true;
		}

		protected override string XmlElementName
		{
			get { return XmlHelper.ExistsTable; }
		}

		protected override bool ReadAnAttribute(XmlReader reader, XmlReaderContext context)
		{
			bool b;

			if (this.IsAttributeMatch(XmlHelper.TableName, reader, context))
			{
				this.TableName = reader.Value;

				b = true;
			}
			else
			{
				b = base.ReadAnAttribute(reader, context);
			}

			return b;
		}

		protected override bool WriteAllAttributes(XmlWriter writer, XmlWriterContext context)
		{
			this.WriteOptionalAttribute(writer, XmlHelper.TableName, this.TableName, context);

			return base.WriteAllAttributes(writer, context);
		}
	}
}