﻿using System;
using System.Collections.Generic;
using System.Data;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Tables;

namespace MSSQLServerAuditor.Core.ETL
{
	public class ETLTransform
	{
		private readonly ETLTableMapping _mapping;

		public ETLTransform(ETLTableMapping mapping)
		{
			this._mapping = mapping;
		}

		public ETLData Apply(List<DataTable> srcTables, ETLQueryParameters queryParams)
		{
			TableInfo       destTable = new TableInfo(this._mapping.Schema, this._mapping.TableName);
			List<ITableRow> rows      = new List<ITableRow>();

			Dictionary<string, string> colsMap  = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
			Dictionary<string, string> paramMap = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

			foreach (ETLColumnMapping colMapping in this._mapping.Columns)
			{
				string dstCol = colMapping.Name;

				destTable.Fields.Add(new FieldInfo(dstCol, DbType.String));

				string srcCol = colMapping.QueryColumn;
				if (!string.IsNullOrWhiteSpace(srcCol))
				{
					colsMap[srcCol] = dstCol;
				}

				string srcParam = colMapping.QueryParameter;
				if (!string.IsNullOrWhiteSpace(srcParam))
				{
					paramMap[srcParam] = dstCol;
				}
			}

			Dictionary<string, string> paramValues = new Dictionary<string, string>
			{
				{ "@d_Query_id",            queryParams.QueryId?.ToString()          },
				{ "@d_ServerInstance_id",   queryParams.ServerInstanceId?.ToString() },
				{ "@d_Login_id",            queryParams.LoginId?.ToString()          },
				{ "@d_ConnectionGroup_id",  queryParams.ConnectionGroupId?.ToString()},
				{ "@d_Template_id",         queryParams.TemplateId?.ToString()       }
			};

			foreach (DataTable srcTable in srcTables)
			{
				foreach (DataRow srcRow in srcTable.Rows)
				{
					TableRow row = new TableRow(destTable);

					// resolve fields' values
					for (int i = 0; i < srcTable.Columns.Count; i++)
					{
						DataColumn srcCol = srcTable.Columns[i];

						string dstCol;
						if (colsMap.TryGetValue(srcCol.ColumnName, out dstCol))
						{
							row.SetValue(dstCol, srcRow[i]);
						}
					}

					// resolve parameters' values
					foreach (KeyValuePair<string, string> parameter in paramValues)
					{
						string dstCol;
						if (paramMap.TryGetValue(parameter.Key, out dstCol))
						{
							row.SetValue(dstCol, parameter.Value);
						}
					}

					rows.Add(row);
				}
			}
			
			return new ETLData
			{
				TableInfo = destTable,
				Rows      = rows
			};
		}
	}
}
