﻿namespace MSSQLServerAuditor.Core.ETL
{
	public class ETLQueryParameters
	{
		public long? QueryId           { get; set; }
		public long? ServerInstanceId  { get; set; }
		public long? ConnectionGroupId { get; set; }
		public long? LoginId           { get; set; }
		public long? TemplateId        { get; set; }
	}
}