﻿using System;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Vendors.Mssql;
using MSSQLServerAuditor.DataAccess.Vendors.Sqlite;

namespace MSSQLServerAuditor.Core.ETL
{
	public static class ETLDatabaseFactory
	{
		public static ETLDatabase CreateDatabase(ServerInstance instance)
		{
			ConnectionType connectionType = instance.ConnectionType;

			switch (connectionType)
			{
				case ConnectionType.MSSQL:
					IDatabaseProviderFactory mssqlFactory = new MssqlProviderFactory();
					MssqlInstance mssqlInstance = (MssqlInstance) instance;
					string mssqlConnectionString = mssqlInstance.PopulateConnectionString();
					
					return new ETLDatabase(mssqlFactory.GetDatabase(mssqlConnectionString), mssqlFactory.CreateCommandExecutor());

				case ConnectionType.SQLite:
					IDatabaseProviderFactory sqliteFactory = new SqliteProviderFactory();
					string databasePath = ((SqliteInstance) instance).DatabaseFilePath;
					string sqliteConnectionString =
						$"data source={databasePath};journal mode=Wal;pooling=True;page size=4096;cache size=10000;synchronous=Off";

					return new ETLDatabase(sqliteFactory.GetDatabase(sqliteConnectionString), sqliteFactory.CreateCommandExecutor());

				default:
					throw new ArgumentException($"Invalid ETL connection type: '{connectionType}'");
			}
		}
	}
}