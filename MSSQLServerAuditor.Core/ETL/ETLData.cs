﻿using System.Collections.Generic;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Tables;

namespace MSSQLServerAuditor.Core.ETL
{
	public class ETLData
	{
		public TableInfo       TableInfo { get; set; }
		public List<ITableRow> Rows      { get; set; }
	}
}
