﻿using System.Collections.Generic;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Extensions;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Tables;
using Perceiveit.Data;

namespace MSSQLServerAuditor.Core.ETL
{
	public class ETLDatabase
	{
		private readonly DBDatabase       _database;
		private readonly ICommandExecutor _commandExecutor;

		public ETLDatabase(DBDatabase database, ICommandExecutor commandExecutor)
		{
			Check.NotNull(database,        nameof(database));
			Check.NotNull(commandExecutor, nameof(commandExecutor));

			this._database        = database;
			this._commandExecutor = commandExecutor;
		}

		public long InsertRows(TableInfo tableInfo, List<ITableRow> rows)
		{
			return this._database.ExecuteTransactionally(context =>
			{
				InsertRowCommand insertCommand = new InsertRowCommand(context, tableInfo);

				foreach (ITableRow tableRow in rows)
				{
					insertCommand.AddRowToInsert(tableRow);
				}

				return insertCommand.Execute(this._commandExecutor);
			});
		}
	}
}
