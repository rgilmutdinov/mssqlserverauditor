﻿using System;
using System.Collections.Generic;

namespace MSSQLServerAuditor.Core.Tasks
{
	public interface IQueueTaskManager : IQueueTaskCallback
	{
		event EventHandler<EventArgs>          CancellationRequested;
		event EventHandler<EventArgs>          RunningTasksChanged;
		event EventHandler<QueueTaskEventArgs> TaskStatusChanged;
		event EventHandler<EventArgs>          WaitingTasksChanged;

		void Enqueue(QueueTask task);
		void StopAll();

		bool            HasAnyTasks  { get; }
		List<QueueTask> RunningTasks { get; }
		List<QueueTask> WaitingTasks { get; }
	}
}
