﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace MSSQLServerAuditor.Core.Tasks
{
	public abstract class QueueTask
	{
		public QueueTaskStatus Status { get; private set; } = QueueTaskStatus.Waiting;

		public CancellationTokenSource CancellationSource { get; } = new CancellationTokenSource();

		public async void ExecuteAsync(IQueueTaskCallback callback)
		{
			CancellationToken cancelToken = CancellationSource.Token;

			try
			{
				Status = QueueTaskStatus.Running;
				callback.TaskStarted(this);

				await ExecuteAsync(cancelToken);

				Status = QueueTaskStatus.Completed;
				callback.TaskCompleted(this);
			}
			catch (TaskCanceledException)
			{
				Status = QueueTaskStatus.Cancelled;
				callback.TaskCancelled(this);
			}
			catch (Exception exc)
			{
				Status = QueueTaskStatus.Failed;
				callback.TaskFailed(this, exc);
			}
		}

		public abstract long   Id    { get; }
		public abstract string Title { get; }

		public abstract bool UpdateHierarchically { get; }
		public abstract int  UpdateDeep           { get; }

		protected abstract Task ExecuteAsync(CancellationToken cancelToken);
		public abstract IEnumerable<QueueTask> GetChildTasks();
		

		public void Cancel()
		{
			this.CancellationSource.Cancel();
		}

		public bool IsCancellationRequested => CancellationSource.IsCancellationRequested;
	}
}
