﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Settings;

namespace MSSQLServerAuditor.Core.Tasks
{
	public class QueueTaskManager : IQueueTaskManager
	{
		public event EventHandler<EventArgs>          CancellationRequested;
		public event EventHandler<EventArgs>          RunningTasksChanged;
		public event EventHandler<QueueTaskEventArgs> TaskStatusChanged;
		public event EventHandler<EventArgs>          WaitingTasksChanged;

		private readonly object _syncObj         = new object();
		private volatile bool   _queueProcessing = false;

		private readonly LinkedList<QueueTask>       _tasksQueue;   // protected by lock(_syncObj)
		private readonly Dictionary<long, QueueTask> _tasksRunning; // protected by lock(_syncObj)
		private int _running = 0;

		private readonly UserSettings _settings;

		public QueueTaskManager(UserSettings settings)
		{
			Check.NotNull(settings, nameof(settings));

			this._settings     = settings;
			this._tasksQueue   = new LinkedList<QueueTask>();
			this._tasksRunning = new Dictionary<long, QueueTask>();
		}

		public void Enqueue(QueueTask task)
		{
			// Add the task to the list of tasks to be processed
			lock (this._syncObj)
			{
				this._tasksQueue.AddFirst(task);
			}

			OnWaitingTasksChanged();

			// Process queued tasks if there are enough available threads.
			if (this._running < this._settings.MaxThreads)
			{
				ProcessQueue();
			}
		}

		private void ProcessQueue()
		{
			if (this._queueProcessing)
			{
				return;
			}

			this._queueProcessing = true;

			// Process available items in the queue.
			int maxThreads = this._settings.MaxThreads;
			while (this._running < maxThreads)
			{
				QueueTask task;
				lock (this._syncObj)
				{
					// When there are no more items to be processed,
					// note that we're done processing, and get out.
					if (this._tasksQueue.Count == 0)
					{
						break;
					}

					// Get the next task from the queue
					task = this._tasksQueue.First.Value;
					this._tasksQueue.RemoveFirst();
				}

				OnWaitingTasksChanged();

				if (task != null)
				{
					bool startTask = false;

					long taskId = task.Id;
					lock (this._syncObj)
					{
						if (!this._tasksRunning.ContainsKey(taskId))
						{
							this._tasksRunning.Add(taskId, task);

							// Execute the task we pulled out of the queue
							Interlocked.Increment(ref this._running);
							startTask = true;
						}
					}

					if (startTask)
					{
						task.ExecuteAsync(this);

						OnRunningTasksChanged();
					}
				}
			}

			this._queueProcessing = false;
		}

		public void StopAll()
		{
			OnCancellationRequested();

			List<QueueTask> runningTasks;
			lock (this._syncObj)
			{
				runningTasks = this._tasksRunning.Values.ToList();

				this._tasksQueue.Clear();
			}

			foreach (QueueTask task in runningTasks)
			{
				task.Cancel();
			}
		}

		public void TaskCompleted(QueueTask task)
		{
			if (task.UpdateHierarchically && task.UpdateDeep != 0)
			{
				if (!task.IsCancellationRequested)
				{
					foreach (QueueTask childTask in task.GetChildTasks())
					{
						Enqueue(childTask);
					}
				}
			}

			RemoveTask(task.Id);
			OnTaskStatusChanged(task);

			ProcessQueue();
		}

		public void TaskStarted(QueueTask task)
		{
			OnTaskStatusChanged(task);
		}

		public void TaskCancelled(QueueTask task)
		{
			RemoveTask(task.Id);

			OnTaskStatusChanged(task);
			ProcessQueue();
		}

		public void TaskFailed(QueueTask task, Exception exc)
		{
			RemoveTask(task.Id);

			OnTaskStatusChanged(task);
			ProcessQueue();
		}

		private void RemoveTask(long taskId)
		{
			bool taskRemoved = false;
			lock (this._syncObj)
			{
				QueueTask taskToRemove;

				if (this._tasksRunning.TryGetValue(taskId, out taskToRemove))
				{
					this._tasksRunning.Remove(taskId);
					Interlocked.Decrement(ref this._running);

					taskRemoved = true;
				}
			}

			if (taskRemoved)
			{
				OnRunningTasksChanged();
			}
		}

		public bool HasAnyTasks
		{
			get
			{
				lock (this._syncObj)
				{
					return this._tasksQueue.Any() || this._running > 0;
				}
			}
		}

		public List<QueueTask> RunningTasks
		{
			get
			{
				lock (this._syncObj)
				{
					return this._tasksRunning.Values.ToList();
				}
			}
		}

		public List<QueueTask> WaitingTasks
		{
			get
			{
				lock (this._syncObj)
				{
					return this._tasksQueue.ToList();
				}
			}
		}

		private void OnWaitingTasksChanged()
		{
			WaitingTasksChanged?.Invoke(this, EventArgs.Empty);
		}

		private void OnRunningTasksChanged()
		{
			RunningTasksChanged?.Invoke(this, EventArgs.Empty);
		}

		private void OnTaskStatusChanged(QueueTask task)
		{
			TaskStatusChanged?.Invoke(this, new QueueTaskEventArgs(task));
		}

		private void OnCancellationRequested()
		{
			CancellationRequested?.Invoke(this, EventArgs.Empty);
		}
	}
}
