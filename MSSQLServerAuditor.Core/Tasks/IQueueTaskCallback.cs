using System;

namespace MSSQLServerAuditor.Core.Tasks
{
	public interface IQueueTaskCallback
	{
		void TaskCompleted(QueueTask task);
		void TaskStarted(QueueTask task);
		void TaskCancelled(QueueTask task);
		void TaskFailed(QueueTask task, Exception exc);
	}
}