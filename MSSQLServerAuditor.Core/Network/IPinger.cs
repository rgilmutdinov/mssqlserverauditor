using System.Threading;
using System.Threading.Tasks;

namespace MSSQLServerAuditor.Core.Network
{
	public interface IPinger
	{
		Task<PingResult> PingAsync(CancellationToken token);
	}
}
