using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Core.Network
{
	public class UdpPinger : IPinger
	{
		public const int DefaultTimeout = 2000;

		private readonly string _machineName;
		private readonly int    _port;
		private readonly int    _timeout;

		public UdpPinger(
			string machineName,
			int    port,
			int    timeoutMillis = 0
		)
		{
			Check.Assert(timeoutMillis >= 0, "Timeout is less than zero.");

			this._timeout = timeoutMillis == 0
				? DefaultTimeout
				: timeoutMillis;

			this._machineName = machineName;
			this._port        = port;
		}

		public UdpPinger(string machineName, int port)
			: this(machineName, port, DefaultTimeout)
		{
		}

		public async Task<PingResult> PingAsync(CancellationToken cancellationToken)
		{
			return await Task.Run(
				() => PingPort(_machineName, _port, _timeout),
				cancellationToken
			).ConfigureAwait(false);
		}

		private PingResult PingPort(string hostName, int port, int timeout)
		{
			Stopwatch stopwatch = new Stopwatch();

			using (UdpClient udpClient = new UdpClient())
			{
				udpClient.Client.ReceiveTimeout = timeout;
				stopwatch.Start();

				try
				{
					udpClient.Connect(hostName, port);
					IPEndPoint ipAddress = (IPEndPoint) udpClient.Client.RemoteEndPoint;

					Byte[] sendBytes = Encoding.ASCII.GetBytes("?");
					udpClient.Send(sendBytes, sendBytes.Length);
					IPEndPoint remoteIpEndPoint = new IPEndPoint(ipAddress.Address, port);
					udpClient.Receive(ref remoteIpEndPoint);

					stopwatch.Stop();

					string status = SocketError.Success.ToString();

					return PingResult.Succeeded(
						status,
						ipAddress.Address.ToString(),
						stopwatch.ElapsedMilliseconds
					);
				}
				catch (SocketException exc)
				{
					stopwatch.Stop();

					string errorStatus = exc.SocketErrorCode.ToString();

					return PingResult.Failed(
						errorStatus,
						exc.Message,
						-1
					);
				}
				finally
				{
					udpClient.Close();
				}
			}
		}
	}
}
