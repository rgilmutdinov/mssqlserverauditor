﻿using System;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Settings
{
	[Serializable]
	public class NotificationSettings
	{
		[XmlElement(ElementName = "RecipientEmail")]
		public string RecipientEmail { get; set; }

		[XmlElement(ElementName = "SmtpSettings")]
		public SmtpSettings SmtpSettings { get; set; }
	}
}