﻿using System.IO;
using MSSQLServerAuditor.Core.Xml;

namespace MSSQLServerAuditor.Core.Settings
{
	public class SettingsManager
	{
		public UserSettings LoadUserSettings(string settingsFile)
		{
			return XmlSerialization.Deserialize<UserSettings>(
				settingsFile
			);
		}

		public SystemSettings LoadSystemSettings(string settingsFile)
		{
			return XmlSerialization.Deserialize<SystemSettings>(
				settingsFile
			);
		}

		public void SaveUserSettings(UserSettings settings, string settingsFile)
		{
			string directory = Path.GetDirectoryName(settingsFile);
			if (!string.IsNullOrWhiteSpace(directory) && !Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}

			XmlSerialization.Serialize(
				settingsFile,
				settings
			);
		}

		public void SaveSystemSettings(SystemSettings settings, string settingsFile)
		{
			XmlSerialization.Serialize(
				settingsFile,
				settings
			);
		}
	}
}
