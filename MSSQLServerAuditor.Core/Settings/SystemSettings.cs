﻿using System.Collections.Generic;
using System.Xml.Serialization;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Settings
{
	/// <summary>
	/// System settings info
	/// </summary>
	[XmlRoot(ElementName = "SystemSettings")]
	public class SystemSettings
	{
		/// <summary>
		/// Available UI language.
		/// </summary>
		[XmlArray("UiLanguages")]
		[XmlArrayItem("Language")]
		public List<string> UiLanguages { get; set; }

		/// <summary>
		/// Available report language.
		/// </summary>
		[XmlArray("ReportLanguages")]
		[XmlArrayItem("Language")]
		public List<string> ReportLanguages { get; set; }

		/// <summary>
		/// Application window title.
		/// </summary>
		[XmlArray("WindowTitles")]
		[XmlArrayItem("Title")]
		public List<LangResource> WindowTitles { get; set; }

		/// <summary>
		/// Available Connections types.
		/// </summary>
		[XmlArray("ConnectionTypes")]
		[XmlArrayItem("ConnectionType")]
		public List<ConnectionTypeInfo> ConnectionTypes { get; set; }

		/// <summary>
		/// Template directory folder
		/// </summary>
		[XmlElement("TemplateDirectory")]
		public string TemplateDirectory { get; set; }

		/// <summary>
		/// The base directory for query scripts
		/// </summary>
		[XmlElement("ScriptsDirectory")]
		public string ScriptsDirectory { get; set; }

		/// <summary>
		/// Path for the script that will be executed on Current database after creation
		/// </summary>
		[XmlElement("PostBuildCurrentDbScript")]
		public string PostBuildCurrentDbScript { get; set; }

		/// <summary>
		/// Script to be exected after system databases creation
		/// </summary>
		[XmlElement("PostBuildScripts")]
		public List<PostBuildScript> PostBuildScripts { get; set; }

		/// <summary>
		/// Service data update timeout, seconds
		/// </summary>
		[XmlElement("ServiceDataUpdateTimeout")]
		public int ServiceDataUpdateTimeout { get; set; }

		/// <summary>
		/// Support e-mail.
		/// </summary>
		[XmlElement(ElementName = "SupportEmail")]
		public string SupportEmail { get; set; }

		[XmlElement(ElementName = "SupportUrl")]
		public string SupportUrl { get; set; }
	}
}
