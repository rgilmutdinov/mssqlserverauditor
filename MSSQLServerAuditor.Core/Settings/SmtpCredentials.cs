﻿using System;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Settings
{
	[Serializable]
	public class SmtpCredentials
	{
		[XmlAttribute(AttributeName = "username")]
		public string UserName { get; set; }

		[XmlAttribute(AttributeName = "password")]
		public string Password { get; set; }
	}
}