﻿using System;
using System.Collections.Generic;
using System.IO;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using NLog;

namespace MSSQLServerAuditor.Core.Settings
{
	public class AppSettings : IAppSettings
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly SettingsManager _settingsManager;
		private readonly string          _userSettingsFile;
		private readonly string          _userSettingsDefaultFile;
		private readonly string          _systemSettingsFile;
		private string                   _userSettingsDirectory;

		public static AppSettings CreateDefault()
		{
			return new AppSettings(
				DefaultUserSettingsFile,
				TemplateUserSettingsFile,
				DefaultSystemSettingsFile
			);
		}

		public AppSettings(
			string userSettingsFile,
			string userSettingsDefaultFile,
			string systemSettingsFile
		)
		{
			Check.NotNullOrEmpty(userSettingsFile,        nameof(userSettingsFile));
			Check.NotNullOrEmpty(userSettingsDefaultFile, nameof(userSettingsDefaultFile));
			Check.NotNullOrEmpty(systemSettingsFile,      nameof(systemSettingsFile));

			this._userSettingsFile        = userSettingsFile;
			this._userSettingsDefaultFile = userSettingsDefaultFile;
			this._systemSettingsFile      = systemSettingsFile;
			this._settingsManager         = new SettingsManager();
			this._userSettingsDirectory   = Path.GetDirectoryName(userSettingsFile);

			ReloadUserSettings();
			ReloadSystemSettings();
		}

		private static string DefaultSystemSettingsFile 
		{
			get
			{
				string startupPath = AppDomain.CurrentDomain.BaseDirectory;
				string systemSettingsFile = Path.Combine(
					startupPath,
					"MSSQLServerAuditor.SystemSettings.xml"
				);

				return systemSettingsFile;
			}
		}

		private static string TemplateUserSettingsFile
		{
			get
			{
				string startupPath = AppDomain.CurrentDomain.BaseDirectory;
				string userSettingsDefaultFile = Path.Combine(
					startupPath,
					"MSSQLServerAuditor.UserSettings.xml"
				);

				return userSettingsDefaultFile;
			}
		}

		private static string DefaultUserSettingsFile
		{
			get
			{
				string userSettingsFile = Path.Combine(
					Environment.GetFolderPath(Environment.SpecialFolder.Personal),
					CurrentAssembly.ProductName,
					string.Join(".", CurrentAssembly.ProductName, CurrentAssembly.Version, "UserSettings.xml")
				);

				return userSettingsFile;
			}
		}

		/// <summary>
		/// User settings
		/// </summary>
		public UserSettings User { get; private set; }

		public string UserSettingsDirectory => this._userSettingsDirectory;

		/// <summary>
		/// Application system settings
		/// </summary>
		public SystemSettings System { get; private set; }

		public void ReloadUserSettings()
		{
			// Load user settings from file
			// Create default user settings if the file does not exists or damaged
			if (File.Exists(this._userSettingsFile))
			{
				try
				{
					User = this._settingsManager.LoadUserSettings(this._userSettingsFile);

					this._userSettingsDirectory = Path.GetDirectoryName(this._userSettingsFile);
				}
				catch (Exception exc)
				{
					Log.Error(exc, $"Failed to load user settings from the file '{this._userSettingsFile}'");
					LoadUserDefaultSettings();
				}
			}
			else
			{
				LoadUserDefaultSettings();
			}
		}

		private void LoadUserDefaultSettings()
		{
			// Load user settings from default file
			// Create default user settings if the file does not exists or damaged
			if (File.Exists(this._userSettingsDefaultFile))
			{
				try
				{
					User = this._settingsManager.LoadUserSettings(this._userSettingsDefaultFile);

					this._userSettingsDirectory = Path.GetDirectoryName(this._userSettingsDefaultFile);
				}
				catch (Exception exc)
				{
					Log.Error(exc, $"Failed to load user default settings from the file '{this._userSettingsFile}'");
					User = CreateDefaultUserSettings(this._userSettingsDefaultFile);
				}
			}
			else
			{
				Log.Error($"User default  settings file '{this._userSettingsDefaultFile}' does not exist");
				User = CreateDefaultUserSettings(this._userSettingsDefaultFile);
			}
		}

		public void ReloadSystemSettings()
		{
			// Load system settings from file
			// Create default system settings if the file does not exists or damaged
			if (File.Exists(this._systemSettingsFile))
			{
				try
				{
					System = _settingsManager.LoadSystemSettings(this._systemSettingsFile);
				}
				catch (Exception exc)
				{
					Log.Error(exc, $"Failed to load system settings from the file '{this._systemSettingsFile}'");
					System = CreateDefaultSystemSettings(this._systemSettingsFile);
				}
			}
			else
			{
				Log.Error($"System settings file '{this._systemSettingsFile}' does not exist");
				System = CreateDefaultSystemSettings(this._systemSettingsFile);
			}
		}

		public void SaveUserSettings()
		{
			this._settingsManager.SaveUserSettings(User, this._userSettingsFile);
		}

		public void SaveSystemSettings()
		{
			this._settingsManager.SaveSystemSettings(System, this._systemSettingsFile);
		}

		private UserSettings CreateDefaultUserSettings(string settingsFile)
		{
			UserSettings settings = new UserSettings
			{
				NotificationSettings = new NotificationSettings
				{
					RecipientEmail = "info@mssqlserverauditor.com",
					SmtpSettings   = new SmtpSettings
					{
						AuthenticationRequired = false,
						Port                   = 25,
						SenderEmail            = "test@mssqlserverauditor.com",
						ServerAddress          = "mail.mssqlserverauditor.com",
						SslEnabled             = false,
						SmtpCredentials        = new SmtpCredentials
						{
							UserName = "test@mssqlserverauditor.com",
							Password = "password"
						}
					}
				}
			};

			this._settingsManager.SaveUserSettings(settings, settingsFile);
			this._userSettingsDirectory = Path.GetDirectoryName(settingsFile);

			return settings;
		}

		private SystemSettings CreateDefaultSystemSettings(string settingsFile)
		{
			SystemSettings settings = new SystemSettings
			{
				ConnectionTypes  = new List<ConnectionTypeInfo>(),
				PostBuildScripts = new List<PostBuildScript>(),
				ReportLanguages  = new List<string> {"en"},
				UiLanguages      = new List<string> {"en"},
				WindowTitles     = new List<LangResource>()
			};

			this._settingsManager.SaveSystemSettings(settings, settingsFile);
			return settings;
		}

		public IAppSettings Clone()
		{
			return this.XmlClone();
		}
	}
}
