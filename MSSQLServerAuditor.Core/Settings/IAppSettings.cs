﻿namespace MSSQLServerAuditor.Core.Settings
{
	public interface IAppSettings
	{
		SystemSettings System { get; }
		UserSettings   User   { get; }

		string UserSettingsDirectory { get; }

		void ReloadUserSettings();
		void ReloadSystemSettings();

		void SaveSystemSettings();
		void SaveUserSettings();

		IAppSettings Clone();
	}
}