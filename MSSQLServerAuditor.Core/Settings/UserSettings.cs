﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Settings
{
	/// <summary>
	/// User application settings
	/// </summary>
	[XmlRoot(ElementName = "UserSettings")]
	public class UserSettings
	{
		/// <summary>
		/// Interface language
		/// </summary>
		[XmlElement(ElementName = "UiLanguage")]
		public string UiLanguage { get; set; }

		/// <summary>
		/// Report language
		/// </summary>
		[XmlElement(ElementName = "ReportLanguage")]
		public string ReportLanguage { get; set; }

		/// <summary>
		/// Timeout
		/// </summary>
		[XmlElement(ElementName = "Timeout")]
		public int Timeout { get; set; }

		/// <summary>
		/// The maximum number of simultaneous requests.
		/// </summary>
		[XmlElement(ElementName = "MaxThreads")]
		public int MaxThreads { get; set; }

		[XmlElement(ElementName = "NotificationSettings")]
		public NotificationSettings NotificationSettings { get; set; }

		/// <summary>
		/// Show xml tab
		/// </summary>
		[XmlElement("ShowXml")]
		public bool ShowXml { get; set; }

		/// <summary>
		/// Show HTML tabs.
		/// </summary>
		[XmlElement("ShowHTML")]
		public bool ShowHtml { get; set; }
	}
}
