using System;
using SqlCodeGuardAPI;

namespace MSSQLServerAuditor.Core.CodeGuard
{
	public class CodeGuardProcessEventArgs: EventArgs
	{
		public CodeGuardProcessEventArgs(
			DatabaseObject databaseObject,
			int            total,
			int            current
		)
		{
			this.DatabaseObject = databaseObject;
			this.Total          = total;
			this.Current        = current;
		}

		public DatabaseObject DatabaseObject { get; set; }
		public int            Total          { get; set; }
		public int            Current        { get; set; }

	}
}
