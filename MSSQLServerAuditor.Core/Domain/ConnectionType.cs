﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Query type
	/// </summary>
	public enum ConnectionType
	{
		/// <summary>
		/// External MS SQL Instance
		/// </summary>
		[XmlEnum(Name= "MSSQL")]
		MSSQL,

		/// <summary>
		/// External SQLite database instance
		/// </summary>
		[XmlEnum(Name = "SQLite")]
		SQLite,

		/// <summary>
		/// External Teradata SQL Server
		/// </summary>
		[XmlEnum(Name = "TDSQL")]
		TDSQL,

		/// <summary>
		/// Windows Management Instrumentation
		/// </summary>
		[XmlEnum(Name = "WMI")]
		WMI,

		/// <summary>
		/// ActiveDirectory Connector
		/// https://msdn.microsoft.com/en-us/library/system.directoryservices.activedirectory.aspx
		/// </summary>
		[XmlEnum(Name = "ActiveDirectory")]
		ActiveDirectory,

		/// <summary>
		/// EventLog Connector
		/// https://msdn.microsoft.com/en-us/library/System.Diagnostics.EventLog.aspx
		/// </summary>
		[XmlEnum(Name = "EventLog")]
		EventLog,

		/// <summary>
		/// NetworkInformation Connector
		/// https://msdn.microsoft.com/en-us/library/system.net.networkinformation.pingreply.status.aspx
		/// </summary>
		[XmlEnum(Name = "NetworkInformation")]
		NetworkInformation,

		/// <summary>
		/// Internal connection for current, report and historic database
		/// </summary>
		[XmlEnum(Name = "Internal")]
		Internal,

		/// <summary>
		/// System connection for current database
		/// </summary>
		[XmlEnum(Name = "System")]
		System
	}
}
