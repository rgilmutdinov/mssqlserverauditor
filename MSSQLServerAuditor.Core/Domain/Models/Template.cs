﻿namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class Template : ModelBase
	{
		public string Name       { get; set; }
		public string UId        { get; set; }
		public string Directory  { get; set; }
		public bool   IsExternal { get; set; }
	}
}
