﻿namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class Login : ModelBase
	{
		public static Login CreateEmpty()
		{
			return new Login(string.Empty, string.Empty);
		}

		public Login()
		{
			
		}

		public Login(string name, string password, bool isWinAuth = false)
		{
			Name      = name;
			Password  = password;
			IsWinAuth = isWinAuth;
		}

		public string Name      { get; set; }
		public string Password  { get; set; }
		public bool   IsWinAuth { get; set; }

		public override string ToString()
		{
			return IsWinAuth ? "[Windows]" : Name;
		}
	}
}