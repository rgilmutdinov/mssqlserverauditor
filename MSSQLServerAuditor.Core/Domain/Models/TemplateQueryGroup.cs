﻿namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class TemplateQueryGroup : ModelBase
	{
		public long   TemplateNodeParentId { get; set; }
		public string GroupId              { get; set; }
		public string Name                 { get; set; }
		public string DefaultDatabaseField { get; set; }
	}
}
