﻿namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class Query : ModelBase
	{
		public long   NodeInstanceId      { get; set; }
		public long?  TemplateQueryId     { get; set; }
		public long?  ServerInstanceId    { get; set; }
		public string DefaultDatabaseName { get; set; }
	}
}
