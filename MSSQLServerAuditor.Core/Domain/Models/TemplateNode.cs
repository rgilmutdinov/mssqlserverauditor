﻿namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class TemplateNode : ModelBase
	{
		public long?  TemplateId         { get; set; }
		public long?  ParentId           { get; set; }
		public string UId                { get; set; }
		public long?  QueryGroupParentId { get; set; }
		public string Name               { get; set; }
		public string Icon               { get; set; }
		public bool?  ShowEmpty          { get; set; }
		public bool?  ShowRecordsNumber  { get; set; }
	}
}
