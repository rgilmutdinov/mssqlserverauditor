using System;
using MSSQLServerAuditor.Core.Session;

namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class ScheduleDetails : ScheduleSettings
	{
		public static ScheduleDetails Create(
			TemplateNodeInfo tnInfo,
			AuditContext     context,
			ScheduleSettings settings
		)
		{
			ScheduleDetails details = new ScheduleDetails
			{
				Id                      = settings.Id,
				UId                     = settings.UId,
				Name                    = settings.Name,
				IsEnabled               = settings.IsEnabled,
				CronExpression          = settings.CronExpression,
				TargetMachine           = settings.TargetMachine,
				ServiceOnly             = settings.ServiceOnly,
				LastRan                 = settings.LastRan,
				SendMessage             = settings.SendMessage,
				SendMessageNonEmptyOnly = settings.SendMessageNonEmptyOnly,
				MessageRecipients       = settings.MessageRecipients,
				MessageLanguage         = settings.MessageLanguage,
				UpdateHierarchically    = settings.UpdateHierarchically,
				UpdateDeep              = settings.UpdateDeep,

				NodeInstanceId          = tnInfo.NodeInstanceId,
				TemplateNodeUId         = tnInfo.UId,
				TemplateNodeName        = tnInfo.Name,

				TemplateId              = context.Template.Id,
				TemplateDirectory       = context.Template.Directory,
				TemplateName            = context.Template.Name,
				TemplateUId             = context.Template.UId,

				ConnectionGroupId       = context.ConnectionGroup.Id
			};

			return details;
		}

		public long   TemplateId        { get; set; }
		public string TemplateUId       { get; set; }
		public string TemplateName      { get; set; }
		public string TemplateDirectory { get; set; }
		public string TemplateNodeUId   { get; set; }
		public string TemplateNodeName  { get; set; }
		public long   ConnectionGroupId { get; set; }

		public DateTime  DateCreated { get; set; }
		public DateTime? DateUpdated { get; set; }
	}
}
