﻿namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class TemplateQueryParameter : ModelBase
	{
		public long?  TemplateQueryId { get; set; }
		public string Name            { get; set; }
	}
}
