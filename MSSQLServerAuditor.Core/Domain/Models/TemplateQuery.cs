﻿namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class TemplateQuery : ModelBase
	{
		public long?  TemplateNodeId { get; set; }
		public string UId            { get; set; }
		public string Name           { get; set; }
		public string Hierarchy      { get; set; }
	}
}
