﻿using System.Collections.Generic;

namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class ConnectionGroup : ModelBase
	{
		public string         Name           { get; set; }
		public ConnectionType ConnectionType { get; set; } = ConnectionType.MSSQL;
		public bool           IsDirect       { get; set; } = true;

		public List<ServerInstance> ServerInstances { get; set; } = new List<ServerInstance>();
	}
}
