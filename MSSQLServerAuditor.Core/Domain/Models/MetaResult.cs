﻿using System;
using MSSQLServerAuditor.Common;

namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class MetaResult : ModelBase
	{
		public long?    QueryId      { get; set; }
		public long?    QueryGroupId { get; set; }
		public long?    RequestId    { get; set; }
		public long     SessionId    { get; set; }
		public long?    RecordSets   { get; set; }
		public long?    Rows         { get; set; }
		public string   ErrorId      { get; set; }
		public string   ErrorCode    { get; set; }
		public string   ErrorMessage { get; set; }
		public DateTime DateCreated  { get; set; }
		public DateTime DateUpdated  { get; set; } = DefaultValues.Date.Minimum;
	}
}