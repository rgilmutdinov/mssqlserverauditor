﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	public class LangResource
	{
		/// <summary>
		/// Language
		/// </summary>
		[XmlAttribute(AttributeName = "lang")]
		public string Language { get; set; }

		/// <summary>
		/// Template name
		/// </summary>
		[XmlText]
		public string Text { get; set; }
	}
}