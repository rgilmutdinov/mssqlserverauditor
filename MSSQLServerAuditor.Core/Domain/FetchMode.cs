﻿namespace MSSQLServerAuditor.Core.Domain
{
	public enum FetchMode
	{
		/// <summary>
		/// Fetch data from a remote source
		/// </summary>
		Remote,

		/// <summary>
		/// Fetch data from the internal storage
		/// </summary>
		Internal,

		/// <summary>
		/// Fetch data from a remote source if absent in the internal storage
		/// </summary>
		Combined
	}
}
