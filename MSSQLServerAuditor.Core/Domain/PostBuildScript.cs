﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{	
	public class PostBuildScript
	{
		[XmlAttribute("Alias")]
		public string Alias { get; set; }

		[XmlText]
		public string File { get; set; }
	}
}
