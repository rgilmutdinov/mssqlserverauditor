﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	public class ScriptInfo
	{
		[XmlAttribute(AttributeName = "name")]
		public string Name { get; set; }

		[XmlAttribute(AttributeName = "signature")]
		public string Signature { get; set; }

		[XmlElement(ElementName = "sql")]
		[XmlText]
		public string Text { get; set; }
	}
}
