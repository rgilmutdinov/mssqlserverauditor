﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using MSSQLServerAuditor.Core.Domain.Exceptions;
using MSSQLServerAuditor.Core.Domain.Models;

namespace MSSQLServerAuditor.Core.Domain
{
	public class TemplateNodeInfo
	{
		/// <summary>
		/// Node name used in file hierarchy
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string Name { get; set; }

		/// <summary>
		/// Node id used in tables hierarchy
		/// </summary>
		[XmlAttribute(AttributeName = "id")]
		public string Id { get; set; }

		/// <summary>
		/// Rule "Show single Tab"
		/// </summary>
		[XmlAttribute(AttributeName = "IsHideTabs")]
		public bool HideTabs { get; set; }

		/// <summary>
		/// Group select string identifier
		/// </summary>
		[XmlAttribute(AttributeName = "group-select-id")]
		public string GroupSelectId { get; set; }

		/// <summary>
		/// Report preprocessor file
		/// </summary>
		[XmlAttribute(AttributeName = "file")]
		public string ProcessorFile { get; set; }

		/// <summary>
		/// Reference to an icon. The icon will be displayed in tree view next to the node
		/// </summary>
		[XmlAttribute(AttributeName = "icon")]
		public string DefaultIcon { get; set; }

		/// <summary>
		/// Hide empty result databases.
		/// </summary>
		[XmlAttribute(AttributeName = "hideEmptyResultDatabases")]
		public bool HideEmptyResultDatabases { get; set; }

		/// <summary>
		/// Show number of records.
		/// </summary>
		[XmlAttribute(AttributeName = "showNumberOfRecords")]
		public bool ShowNumberOfRecords { get; set; }

		/// <summary>
		/// Localization of the node
		/// </summary>
		[XmlElement(ElementName = "i18n")]
		public List<Translation> Title { get; set; }

		/// <summary>
		/// Child nodes
		/// </summary>
		[XmlElement(ElementName = "template")]
		public List<TemplateNodeInfo> Children { get; set; }

		/// <summary>
		/// Queries for node
		/// </summary>
		[XmlElement(ElementName = "sql-select")]
		public List<TemplateNodeQueryInfo> Queries { get; set; }

		[XmlElement(ElementName = "group-select")]
		public List<TemplateNodeQueryInfo> GroupQueries { get; set; }

		[XmlElement(ElementName = "connections-select")]
		public List<TemplateNodeQueryInfo> ConnectionQueries { get; set; }

		[XmlElement(ElementName = "sqlcodeguard-select")]
		public List<TemplateNodeSqlGuardQueryInfo> SqlCodeGuardQueries { get; set; }

		[XmlArray("RefreshSchedules")]
		[XmlArrayItem("RefreshSchedule")]
		public List<ScheduleSettings> RefreshSchedules { get; set; }

		[XmlIgnore]
		public bool IsInstance { get; private set; } = false;

		[XmlIgnore]
		public TemplateNodeInfo Parent { get; set; }

		[XmlIgnore]
		public TemplateNodeInfo Template { get; private set; }

		[XmlIgnore]
		public List<TemplateNodeInfo> StaticChildren
		{
			get
			{
				lock (Children)
				{
					return Children.Where(c => string.IsNullOrEmpty(c.GroupSelectId)).ToList();
				}
			}
		}

		[XmlIgnore]
		public bool HasActiveSchedules { get; set; } = false;

		[XmlIgnore]
		public long? Counter { get; set; } = null;

		[XmlIgnore]
		public long NodeInstanceId { get; set; } = 0;

		[XmlIgnore]
		public long TemplateNodeId { get; set; } = 0;

		[XmlIgnore]
		public bool IsSaved => NodeInstanceId != 0;

		[XmlIgnore]
		public TemplateNodeQueryInfo ReplicaQuery { get; set; }

		[XmlIgnore]
		public string DefaultDatabaseField { get; set; }

		[XmlIgnore]
		public NodeAttributes Attributes { get; private set; } = new NodeAttributes();

		[XmlIgnore]
		public string DefaultUId { get; set; }

		[XmlIgnore]
		public string DefaultUName { get; set; }

		[XmlIgnore]
		public string DefaultFontColor { get; set; }

		[XmlIgnore]
		public string DefaultFontStyle { get; set; }

		[XmlIgnore]
		public string UId
		{
			get { return Attributes.UId ?? DefaultUId; }
			set { Attributes.UId = value; }
		}

		[XmlIgnore]
		public string UName
		{
			get { return Attributes.UName ?? DefaultUName; }
			set { Attributes.UName = value; }
		}

		[XmlIgnore]
		public string UIcon
		{
			get { return Attributes.Icon ?? DefaultIcon; }
			set { Attributes.Icon = value; }
		}

		[XmlIgnore]
		public string FontColor
		{
			get { return Attributes.FontColor ?? DefaultFontColor; }
			set { Attributes.FontColor = value; }
		}

		[XmlIgnore]
		public string FontStyle
		{
			get { return Attributes.FontStyle ?? DefaultFontStyle; }
			set { Attributes.FontStyle = value; }
		}

		[XmlIgnore]
		public bool IsDisabled
		{
			get { return !Attributes.IsEnabled; }
			set { Attributes.IsEnabled = !value; }
		}

		public void Init()
		{
			foreach (TemplateNodeInfo child in Children)
			{
				child.Parent = this;
				
				child.Init();
			}
		}

		public void UpdateRelations()
		{
			SetParentNode(GroupQueries,      this);
			SetParentNode(Queries,           this);
			SetParentNode(ConnectionQueries, this);

			SetDafaultDbField(GroupQueries,      this);
			SetDafaultDbField(Queries,           this);
			SetDafaultDbField(ConnectionQueries, this);

			foreach (TemplateNodeInfo childNode in Children)
			{
				childNode.Parent = this;

				childNode.UpdateRelations();
			}
		}

		private static void SetDafaultDbField(
			List<TemplateNodeQueryInfo> queries,
			TemplateNodeInfo            tnInfo
		)
		{
			foreach (TemplateNodeQueryInfo queryInfo in queries.Where(qi => !string.IsNullOrWhiteSpace(qi.DatabaseForChildrenFieldName)))
			{
				TemplateNodeInfo relatedNode = tnInfo.Children
					.FirstOrDefault(c => c.GroupSelectId == queryInfo.Id);

				if (relatedNode != null)
				{
					relatedNode.DefaultDatabaseField = queryInfo.DatabaseForChildrenFieldName;
				}
			}
		}

		private static void SetParentNode(
			List<TemplateNodeQueryInfo> queries,
			TemplateNodeInfo            tnInfo
		)
		{
			foreach (TemplateNodeQueryInfo queryInfo in queries)
			{
				queryInfo.TemplateNode = tnInfo;
			}
		}

		public bool HasDynamicChildren()
		{
			if (!IsInstance)
			{
				return false;
			}

			return Template.Children.Any(childNode =>
				!string.IsNullOrWhiteSpace(childNode.GroupSelectId)
			);
		}

		public List<TemplateNodeInfo> GetGroupQueryNodes(TemplateNodeQueryInfo groupQuery)
		{
			lock (Template.Children)
			{
				return Template.Children
					.Where(t => t.GroupSelectId == groupQuery.Id)
					.ToList();
			}
		}

		public TemplateNodeQueryInfo GetParentQuery()
		{
			if (IsInstance)
			{
				return Template.GetParentQuery();
			}

			if (string.IsNullOrWhiteSpace(GroupSelectId))
			{
				return null;
			}

			if (Parent == null)
			{
				throw new InvalidTemplateException(this + " has <group-select-id> but has no parent node");
			}

			TemplateNodeQueryInfo result = Parent.GroupQueries.FirstOrDefault(q => q.Id == GroupSelectId);

			if (result == null)
			{
				throw new InvalidTemplateException(this + " has <group-select-id> = " + GroupSelectId + "  but there is no <group-select> with such Id in parent node");
			}

			return result;
		}

		public TemplateNodeInfo Replicate(
			NodeAttributes        attributes,
			TemplateNodeQueryInfo replicaQuery,
			TemplateNodeInfo      parent
		)
		{
			TemplateNodeInfo tnInfo = (TemplateNodeInfo) MemberwiseClone();

			tnInfo.Template       = this;
			tnInfo.Children       = new List<TemplateNodeInfo>();
			tnInfo.Attributes     = attributes;
			tnInfo.ReplicaQuery   = replicaQuery;
			tnInfo.Parent         = parent;
			tnInfo.NodeInstanceId = 0;
			tnInfo.IsInstance     = true;
			tnInfo.Id             = null;
			tnInfo.Name           = Name + tnInfo.Id;

			tnInfo.DefaultUName     = attributes.UName;
			tnInfo.DefaultUId       = attributes.UId;
			tnInfo.DefaultFontColor = attributes.FontColor;
			tnInfo.DefaultFontStyle = attributes.FontStyle;
			tnInfo.IsDisabled       = !attributes.IsEnabled;
			
			tnInfo.Queries             = Queries.Select(q => q.Clone()).ToList();
			tnInfo.ConnectionQueries   = ConnectionQueries.Select(q => q.Clone()).ToList();
			tnInfo.GroupQueries        = GroupQueries.Select(q => q.Clone()).ToList();
			tnInfo.SqlCodeGuardQueries = SqlCodeGuardQueries.Select(q => q.Clone()).ToList();

			tnInfo.UpdateRelations();

			LoadQueryParametersFromAttributes();
			
			return tnInfo;
		}

		public TemplateNodeInfo Instantiate(string defaultDatabaseField, TemplateNodeInfo parent)
		{
			TemplateNodeInfo tnInfo = (TemplateNodeInfo) MemberwiseClone();

			tnInfo.Template             = this;
			tnInfo.Attributes           = new NodeAttributes();
			tnInfo.Children             = new List<TemplateNodeInfo>();
			tnInfo.ReplicaQuery         = null;
			tnInfo.DefaultDatabaseField = defaultDatabaseField;
			tnInfo.IsInstance           = true;
			tnInfo.Queries              = Queries.Select(q => q.Clone()).ToList();
			tnInfo.ConnectionQueries    = ConnectionQueries.Select(q => q.Clone()).ToList();
			tnInfo.GroupQueries         = GroupQueries.Select(q => q.Clone()).ToList();
			tnInfo.SqlCodeGuardQueries  = SqlCodeGuardQueries.Select(q => q.Clone()).ToList();
			tnInfo.Parent               = parent;

			tnInfo.UpdateRelations();

			LoadQueryParametersFromAttributes();

			return tnInfo;
		}

		public string GetDefaultDatabase()
		{
			string defaultDbField = DefaultDatabaseField;

			if (defaultDbField == null && !string.IsNullOrEmpty(ReplicaQuery?.DatabaseForChildrenFieldName))
			{
				defaultDbField = ReplicaQuery.DatabaseForChildrenFieldName;
			}

			if (!string.IsNullOrWhiteSpace(defaultDbField))
			{
				string defaultDb = Attributes.Get(defaultDbField);
				if (defaultDb != null)
				{
					return defaultDb;
				}
			}

			return Parent?.GetDefaultDatabase();
		}

		public void LoadQueryParametersFromAttributes()
		{
			Queries.ForEach(q => q.ParameterValues.TakeValuesFrom(Attributes.Values));
			GroupQueries.ForEach(q => q.ParameterValues.TakeValuesFrom(Attributes.Values));
		}
	}
}
