﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Query item
	/// </summary>
	public class QueryItemInfo
	{
		/// <summary>
		/// Min supported version (* - any)
		/// </summary>
		[XmlAttribute(AttributeName = "MinSupportedVersion")]
		public string MinVersion { get; set; }

		/// <summary>
		/// Max supported version (* - any)
		/// </summary>
		[XmlAttribute(AttributeName = "MaxSupportedVersion")]
		public string MaxVersion { get; set; }

		/// <summary>
		/// Signature
		/// </summary>
		[XmlAttribute(AttributeName = "signature")]
		public string Signature { get; set; }

		/// <summary>
		/// Query scope
		/// </summary>
		[XmlAttribute(AttributeName = "scope")]
		public QueryScope Scope { get; set; }

		/// <summary>
		/// Query revision number
		/// </summary>
		[XmlAttribute("revision")]
		public string RevisionString { get; set; }

		/// <summary>
		/// Child groups
		/// </summary>
		[XmlElement(ElementName = "group-select-text")]
		public List<QueryItemInfo> ChildGroups { get; set; }

		/// <summary>
		/// Execute condition
		/// </summary>
		[XmlElement(ElementName = "executeIf-select")]
		public string ExecuteIfSqlText { get; set; }

		/// <summary>
		/// Query text
		/// </summary>
		[XmlElement(ElementName = "sql")]
		[XmlText]
		public string Text { get; set; }

		/// <summary>
		/// Query parameters
		/// </summary>
		[XmlArray(ElementName = "sql-select-parameters")]
		[XmlArrayItem(ElementName = "sql-select-parameter")]
		public List<QueryParameterInfo> Parameters { get; set; }

		[XmlIgnore]
		public QueryInfo Parent { get; set; }

		[XmlIgnore]
		public long? Revision
		{
			get
			{
				long revision;
				if (long.TryParse(RevisionString, out revision))
				{
					return revision;
				}

				return null;
			}
		}

		[XmlIgnore]
		public QueryScope? ParentQueryScope => Parent?.Scope;

		public override string ToString()
		{
			return $"MinVersion={MinVersion} MaxVersion={MaxVersion} revision={Revision}";
		}

		public QueryItemInfo Clone()
		{
			return (QueryItemInfo) MemberwiseClone();
		}
	}
}
