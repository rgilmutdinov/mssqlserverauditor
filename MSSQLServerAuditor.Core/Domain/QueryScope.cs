﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Query scope
	/// </summary>
	public enum QueryScope
	{
		/// <summary>
		/// Query for instance
		/// </summary>
		[XmlEnum(Name = "instance")]
		Instance,

		/// <summary>
		/// Query for database
		/// </summary>
		[XmlEnum(Name = "database")]
		Database,
	}
}
