﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	[Serializable]
	[XmlType(TypeName = "ConnectionType")]
	public class ConnectionTypeInfo
	{
		/// <summary>
		/// Connection type id
		/// </summary>
		[XmlAttribute("id")]
		public string Id { get; set; }

		/// <summary>
		/// Localized connection type titles
		/// </summary>
		[XmlElement(ElementName = "Title")]
		public List<LangResource> Title { get; set; }

		/// <summary>
		/// Available module types.
		/// </summary>
		[XmlArray("ModuleTypes")]
		[XmlArrayItem("ModuleType")]
		public List<ModuleTypeInfo> ModuleTypes { get; set; }

		public ConnectionType ConnectionType => ConnectionTypeHelper.Parse(Id);
	}
}
