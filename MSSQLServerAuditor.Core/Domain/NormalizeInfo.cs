﻿using System.Collections.Generic;
using System.Xml.Serialization;
using MSSQLServerAuditor.Common.Extensions;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Information for normalization of query results
	/// </summary>
	public class NormalizeInfo
	{
		private string _tableName;

		/// <summary>
		/// Table name
		/// </summary>
		[XmlAttribute(AttributeName = "table")]
		public string TableName
		{
			get { return this._tableName; }
			set { this._tableName = value.AsValidSqlName(); }
		}

		/// <summary>
		/// Table index fields, eg "id, name"
		/// </summary>
		[XmlAttribute(AttributeName = "index_fields")]
		public string IndexFields { get; set; }

		/// <summary>
		/// List of inner normalize infos
		/// </summary>
		[XmlElement(ElementName = "inner")]
		public List<NormalizeInfo> ChildDirectories { get; set; } = new List<NormalizeInfo>();

		/// <summary>
		/// Fields at this level
		/// </summary>
		[XmlElement(ElementName = "field")]
		public List<NormalizeFieldInfo> Fields { get; set; } = new List<NormalizeFieldInfo>();

		/// <summary>
		/// Record set identifier
		/// </summary>
		[XmlAttribute(AttributeName = "recordSet")]
		public long RecordSet { get; set; }

		/// <summary>
		/// Get a deep copy of data
		/// </summary>
		/// <returns></returns>
		public NormalizeInfo Clone()
		{
			return this.XmlClone();
		}
	}
}
