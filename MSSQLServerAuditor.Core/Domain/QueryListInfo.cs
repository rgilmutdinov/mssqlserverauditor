﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Query root
	/// </summary>
	public class QueryListInfo
	{
		/// <summary>
		/// Query type
		/// </summary>
		[XmlAttribute(AttributeName = "type")]
		public ConnectionType ConnectionType { get; set; }

		/// <summary>
		/// Informations.
		/// </summary>
		[XmlElement(ElementName = "sql-select")]
		public List<QueryInfoExt> Queries { get; set; }
	}
}
