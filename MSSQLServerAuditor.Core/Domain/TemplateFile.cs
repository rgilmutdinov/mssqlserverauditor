﻿using System.IO;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Core.Domain
{
	public class TemplateFile
	{
		public TemplateFile(
			TemplateInfo content,
			string       filePath,
			bool         isExternal)
		{
			Check.NotNull(content, nameof(content));
			Check.NotNullOrWhiteSpace(filePath, nameof(filePath));

			this.Content    = content;
			this.FilePath   = filePath;
			this.IsExternal = isExternal;
		}

		public TemplateInfo Content    { get; }
		public string       FilePath   { get; }
		public bool         IsExternal { get; }

		public string FileName => string.IsNullOrWhiteSpace(FilePath)
			? string.Empty
			: Path.GetFileName(FilePath);

		public string FileDirectory => string.IsNullOrWhiteSpace(FilePath)
			? string.Empty
			: Path.GetDirectoryName(FilePath);
	}
}
