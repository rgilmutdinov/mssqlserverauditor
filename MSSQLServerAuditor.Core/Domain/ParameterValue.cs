﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	public class ParameterValue
	{
		/// <summary>
		/// Parameter name
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string Name { get; set; }

		/// <summary>
		/// Is parameter null
		/// </summary>
		[XmlAttribute(AttributeName = "isnull")]
		public bool IsNull { get; set; }

		/// <summary>
		/// Default value of parameter
		/// </summary>
		[XmlAttribute(AttributeName = "value")]
		public string DefaultValue { get; set; }

		[XmlIgnore]
		public string UserValue { get; set; }

		/// <summary>
		/// Localization of the node
		/// </summary>
		[XmlElement(ElementName = "i18n")]
		public List<Translation> Title { get; set; }

		public ParameterValue Clone()
		{
			return (ParameterValue) MemberwiseClone();
		}

		public override string ToString()
		{
			return Name + "=" + (UserValue ?? DefaultValue);
		}
	}

	public static class ParameterValueExtention
	{
		public static void TakeValuesFrom(this List<ParameterValue> dst, Dictionary<string, string> src)
		{
			foreach (KeyValuePair<string, string> pair in src)
			{
				ParameterValue existing = dst.FirstOrDefault(pv => 
					string.Equals(
						pv.Name.TrimStart('@'), 
						pair.Key,
						StringComparison.OrdinalIgnoreCase
					)
				);

				if (existing != null)
				{
					existing.DefaultValue = pair.Value;
				}
			}
		}
	}
}
