﻿using System;

namespace MSSQLServerAuditor.Core.Domain
{
	public class InstanceVersion : IComparable
	{
		public static readonly InstanceVersion Empty = new InstanceVersion();

		public InstanceVersion(int major, int minor, int build)
		{
			this.Major = major;
			this.Minor = minor;
			this.Build = build;
		}

		public InstanceVersion() : this(0, 0, 0)
		{
		}

		public InstanceVersion(string version)
		{
			string s = version;

			if (string.IsNullOrEmpty(s))
			{
				this.Major = 0;
				this.Minor = 0;
				this.Build = 0;
			}
			else
			{
				s = s.Trim();
				string[] split = s.Trim().Split(' ')[0].Split('.');

				this.Major = int.Parse(split[0] ?? "0");
				this.Minor = int.Parse(split[1] ?? "0");
				this.Build = int.Parse(split[2] ?? "0");
			}
		}

		public int Major { get; }
		public int Minor { get; }
		public int Build { get; }

		public int CompareTo(object obj)
		{
			InstanceVersion version = (InstanceVersion) obj;

			if (Major != version.Major)
			{
				return Major > version.Major ? 1 : -1;
			}

			if (Minor != version.Minor)
			{
				return Minor > version.Minor ? 1 : -1;
			}

			if (Build != version.Build)
			{
				return Build > version.Build ? 1 : -1;
			}

			return 0;
		}

		public override bool Equals(object obj)
		{
			InstanceVersion version = obj as InstanceVersion;

			return version?.CompareTo(this) == 0;
		}

		public override int GetHashCode()
		{
			int hash = Major;

			hash = (hash * 397) ^ Minor.GetHashCode();
			hash = (hash * 397) ^ Build.GetHashCode();

			return hash;
		}

		public static InstanceVersion GetMinVersion(string version)
		{
			version = version.Trim();

			if (version == "*")
			{
				return new InstanceVersion(int.MinValue, 0, 0);
			}

			string[] split = version.Split(' ')[0].Split('.');

			int major = 0, minor = 0, build = 0;
			if (split.Length > 0)
			{
				string majorPart = split[0] ?? "0";
				major = majorPart == "*" ? int.MinValue : int.Parse(majorPart);
			}

			if (split.Length > 1)
			{
				string minorPart = split[1] ?? "0";
				minor = minorPart == "*" ? int.MinValue : int.Parse(minorPart);
			}

			if (split.Length > 2)
			{
				string buildPart = split[2] ?? "0";
				build = buildPart == "*" ? int.MinValue : int.Parse(buildPart);
			}

			return new InstanceVersion(major, minor, build);
		}

		public static InstanceVersion GetMaxVersion(string version)
		{
			version = version.Trim();

			if (version == "*")
			{
				return new InstanceVersion(int.MaxValue, 0, 0);
			}
			
			string[] split = version.Split(' ')[0].Split('.');

			int major = 0, minor = 0, build = 0;
			if (split.Length > 0)
			{
				string majorPart = split[0] ?? "0";
				major = majorPart == "*" ? int.MaxValue : int.Parse(majorPart);
			}

			if (split.Length > 1)
			{
				string minorPart = split[1] ?? "0";
				minor = minorPart == "*" ? int.MaxValue : int.Parse(minorPart);
			}

			if (split.Length > 2)
			{
				string buildPart = split[2] ?? "0";
				build = buildPart == "*" ? int.MaxValue : int.Parse(buildPart);
			}

			return new InstanceVersion(major, minor, build);
		}

		public override string ToString()
		{
			return $"{Major}.{Minor}.{Build}";
		}
	}
}
