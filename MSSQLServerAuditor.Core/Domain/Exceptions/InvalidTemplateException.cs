﻿using System;
using System.Runtime.Serialization;

namespace MSSQLServerAuditor.Core.Domain.Exceptions
{

	[Serializable]
	public class InvalidTemplateException : Exception
	{
		public InvalidTemplateException() { }

		public InvalidTemplateException(string message) : base(message)
		{
			
		}

		public InvalidTemplateException(string message, Exception inner) : base(message, inner)
		{
			
		}

		protected InvalidTemplateException(
			SerializationInfo info,
			StreamingContext  context) : base(info, context)
		{
			
		}
	}
}
