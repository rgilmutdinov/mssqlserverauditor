﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	[XmlRoot("etl_scripts")]
	public class ETLScriptList
	{
		[XmlElement("etl_script")]
		public List<ETLScript> ETLScripts { get; set; }
	}

	/// <summary>
	/// ETL script
	/// </summary>
	public class ETLStatement
	{
		/// <summary>
		/// Statement id
		/// </summary>
		[XmlAttribute("id")]
		public int Id { get; set; }

		/// <summary>
		/// ETL statement script file
		/// </summary>
		[XmlAttribute("file")]
		public string File { get; set; }

		/// <summary>
		/// Statement name
		/// </summary>
		[XmlAttribute("query")]
		public string Query { get; set; }

		/// <summary>
		/// ETL connection name
		/// </summary>
		[XmlAttribute("connection")]
		public string ConnectionName { get; set; }

		[XmlElement]
		public List<ETLScript> ETLScripts { get; set; } 
	}

	/// <summary>
	/// ETL Statement
	/// </summary>
	public class ETLScript
	{
		/// <summary>
		/// ETL statement script file
		/// </summary>
		[XmlAttribute("name")]
		public string Name { get; set; }

		[XmlElement("table")]
		public ETLTableMapping TableMapping { get; set; }

		[XmlIgnore]
		public ConnectionType? ConnectionType { get; set; }
	}

	/// <summary>
	/// Mapping to target table
	/// </summary>
	public class ETLTableMapping
	{
		/// <summary>
		/// Target schema
		/// </summary>
		[XmlAttribute("schema")]
		public string Schema { get; set; }

		/// <summary>
		/// Target table
		/// </summary>
		[XmlAttribute("name")]
		public string TableName { get; set; }

		/// <summary>
		/// Columns mapping
		/// </summary>
		[XmlArray("columns")]
		[XmlArrayItem("column")]
		public List<ETLColumnMapping> Columns { get; set; }
	}

	/// <summary>
	/// Field mapping
	/// </summary>
	public class ETLColumnMapping
	{
		/// <summary>
		/// Target column
		/// </summary>
		[XmlAttribute("name")]
		public string Name { get; set; }

		/// <summary>
		/// Source column
		/// </summary>
		[XmlAttribute("query_column")]
		public string QueryColumn { get; set; }

		/// <summary>
		/// Source parameter
		/// </summary>
		[XmlAttribute("parameter")]
		public string QueryParameter { get; set; }
	}
}