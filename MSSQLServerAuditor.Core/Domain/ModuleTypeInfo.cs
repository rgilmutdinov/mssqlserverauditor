﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	public class ModuleTypeInfo
	{
		[XmlAttribute("id")]
		public string Id { get; set; }

		/// <summary>
		/// Localized modules titles
		/// </summary>
		[XmlElement(ElementName = "Title")]
		public List<LangResource> Title { get; set; }
	}
}
