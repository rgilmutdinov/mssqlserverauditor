﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	public class AliasInfo
	{
		[XmlAttribute("name")]
		public string Name { get; set; }

		[XmlAttribute("alias")]
		public string Alias { get; set; }
	}
}