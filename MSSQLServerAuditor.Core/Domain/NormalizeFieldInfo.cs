using System.Data;
using System.Xml.Serialization;
using MSSQLServerAuditor.Common.Extensions;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Fields information for normalization
	/// </summary>
	public class NormalizeFieldInfo
	{
		private string _name;

		/// <summary>
		/// Field name
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string Name
		{
			get { return this._name; }
			set { this._name = value != "*" ? value.RemoveXmlSpecialChars() : value; }
		}

		/// <summary>
		/// Parameter type <see href="http://msdn.microsoft.com/en-us/library/system.data.sqldbtype.aspx"></see>
		/// </summary>
		[XmlAttribute(AttributeName = "type")]
		public SqlDbType Type { get; set; }

		/// <summary>
		/// Is field unique
		/// </summary>
		[XmlAttribute(AttributeName = "unique")]
		public bool IsUnique { get; set; }

		/// <summary>
		/// Is field is not null
		/// </summary>
		[XmlAttribute(AttributeName = "isnotnull")]
		public bool IsNotNull { get; set; }
	}
}