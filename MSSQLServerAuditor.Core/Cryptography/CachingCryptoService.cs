﻿using System.Collections.Concurrent;

namespace MSSQLServerAuditor.Core.Cryptography
{
	public class CachingCryptoService : ICryptoService
	{
		private readonly ICryptoService _cryptoService;
		private readonly ConcurrentDictionary<string, string> _encCache;
		private readonly ConcurrentDictionary<string, string> _decCache;
		
		public CachingCryptoService(string encryptionKey)
		{
			this._cryptoService = new CryptoService(encryptionKey);

			this._encCache = new ConcurrentDictionary<string, string>();
			this._decCache = new ConcurrentDictionary<string, string>();
		}

		public string Encrypt(string plainText)
		{
			string key = plainText;
			return this._encCache.GetOrAdd(
				key,
				k => this._cryptoService.Encrypt(k)
			);
		}

		public string Decrypt(string encryptedText)
		{
			string key = encryptedText;
			return this._decCache.GetOrAdd(
				key,
				k => this._cryptoService.Decrypt(k)
			);
		}
	}
}
