﻿using System;
using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Cryptography
{
	public class QueryVerificationFailedException : Exception
	{
		public QueryVerificationFailedException(QueryVerificationResult verificationResult)
		{
			VerificationResult = verificationResult;
		}

		public QueryVerificationResult VerificationResult { get; }
	}

	public class QueryVerificationResult
	{
		public QueryVerificationResult()
		{
			InvalidQueryItems = new List<QueryItemInfo>();
			InvalidStatements = new List<FillStatement>();
		}

		public List<QueryItemInfo> InvalidQueryItems { get; }
		public List<FillStatement> InvalidStatements { get; }

		public bool IsValid => !InvalidQueryItems.Any() && !InvalidStatements.Any();
	}

	public static class QueryInfoListExtentions
	{
		public static QueryVerificationResult Verify(this List<QueryInfo> queries, SignatureVerifier verifier)
		{
			QueryVerificationResult verificationResult = new QueryVerificationResult();
			foreach (QueryInfo queryInfo in queries)
			{
				List<QueryItemInfo> invalidQueries = GetInvalidQueries(
					verifier,
					queryInfo
				);

				if (invalidQueries.Any())
				{
					verificationResult.InvalidQueryItems.AddRange(invalidQueries);
				}

				List<FillStatement> invalidStatements = GetInvalidStatements(
					verifier,
					queryInfo.FillStatements
				);

				if (invalidQueries.Any())
				{
					verificationResult.InvalidStatements.AddRange(invalidStatements);
				}
			}

			return verificationResult;
		}

		private static List<QueryItemInfo> GetInvalidQueries(SignatureVerifier verifier, QueryInfo queryInfo)
		{
			List<QueryItemInfo> invalidQueries = new List<QueryItemInfo>();

			invalidQueries.AddRange(GetInvalidQueryItems(verifier, queryInfo.Items));
			invalidQueries.AddRange(GetInvalidQueryItems(verifier, queryInfo.DatabaseSelect));
			invalidQueries.AddRange(GetInvalidQueryItems(verifier, queryInfo.GroupSelect));

			return invalidQueries;
		}

		private static List<FillStatement> GetInvalidStatements(SignatureVerifier verifier, List<FillStatement> fillStatements)
		{
			List<FillStatement> invalidStatements = new List<FillStatement>();
			foreach (FillStatement statement in fillStatements)
			{
				if (!verifier.Verify(statement.Text, statement.Signature))
				{
					invalidStatements.Add(statement);
				}
			}

			return invalidStatements;
		}

		private static List<QueryItemInfo> GetInvalidQueryItems(SignatureVerifier verifier, List<QueryItemInfo> queryItems)
		{
			List<QueryItemInfo> invalidQueries = new List<QueryItemInfo>();
			foreach (QueryItemInfo queryItem in queryItems)
			{
				if (!verifier.Verify(queryItem.Text, queryItem.Signature))
				{
					invalidQueries.Add(queryItem);
				}
			}

			return invalidQueries;
		}
	}
}
