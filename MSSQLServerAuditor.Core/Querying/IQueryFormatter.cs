﻿namespace MSSQLServerAuditor.Core.Querying
{
	public interface IQueryFormatter
	{
		string Format(string query);
	}
}