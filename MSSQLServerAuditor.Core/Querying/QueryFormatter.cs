﻿using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Core.Querying
{
	public class QueryFormatter : IQueryFormatter
	{
		private readonly Dictionary<string, string> _aliases;

		public QueryFormatter(Dictionary<string, string> aliases)
		{
			Check.NotNull(aliases, nameof(aliases));

			this._aliases = aliases;
		}

		public string Format(string query)
		{
			if (string.IsNullOrEmpty(query))
			{
				return query;
			}

			return this._aliases.Aggregate(
				query,
				(current, alias) => current.Replace(alias.Key, alias.Value)
			);
		}
	}
}
