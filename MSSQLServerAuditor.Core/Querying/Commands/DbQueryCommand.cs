﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Querying.Commands
{
	public abstract class DbQueryCommand : IQueryCommand
	{
		protected readonly DbCommand DbCommand;

		protected DbQueryCommand(DbCommand dbCommand)
		{
			this.DbCommand = dbCommand;
		}

		public void Dispose()
		{
			this.DbCommand?.Dispose();
		}

		public void AssignParameters(
			List<QueryParameterInfo>  parameters,
			List<ParameterValue>      parameterValues
		)
		{
			AssignDefaultParameters();

			if (parameters != null)
			{
				foreach (QueryParameterInfo parameter in parameters)
				{
					if (this.DbCommand != null)
					{
						DbParameter p = this.DbCommand.CreateParameter();

						p.ParameterName = parameter.Name;
						p.IsNullable    = true;
						p.DbType        = parameter.Type.ToDbType();
						p.Value         = parameter.GetDefaultValue();

						this.DbCommand.Parameters.Add(p);
					}
				}
			}

			if (parameterValues != null)
			{
				foreach (ParameterValue param in parameterValues)
				{
					if (this.DbCommand != null)
					{
						DbParameter dbParam = (
							from   DbParameter p 
							in     this.DbCommand.Parameters
							where  p.ParameterName == param.Name
							select p
						).FirstOrDefault();

						if (dbParam != null)
						{
							dbParam.Value = ConvertString.ToSqlDbType(
								param.UserValue ?? param.DefaultValue,
								param.IsNull,
								dbParam.DbType.ToSqlDbType()
							);
						}
					}
				}
			}
		}

		public async Task<IDataReader> ExecuteReaderAsync(CancellationToken cancellationToken)
		{
			return await this.DbCommand.ExecuteReaderAsync(
				cancellationToken
			);
		}

		protected abstract void AssignDefaultParameters();
	}
}
