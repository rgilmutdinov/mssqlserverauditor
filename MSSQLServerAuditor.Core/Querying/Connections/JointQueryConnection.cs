﻿using System.Data.Common;
using MSSQLServerAuditor.DataAccess.Connections;
using NLog;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public abstract class JointQueryConnection : DbQueryConnection<DbConnection>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		protected JointQueryConnection(IStorageConnectionFactory factory) : base(factory.CreateOpenedConnection())
		{
		}
	}
}
