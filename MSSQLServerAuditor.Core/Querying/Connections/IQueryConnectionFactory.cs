﻿using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Session;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public interface IQueryConnectionFactory
	{
		IQueryConnection OpenNewConnection(AuditSession session, ConnectionType connectionType);
	}
}