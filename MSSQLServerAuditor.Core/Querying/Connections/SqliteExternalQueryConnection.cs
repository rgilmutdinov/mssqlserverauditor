﻿using System.Data.SQLite;
using MSSQLServerAuditor.Core.Querying.Commands;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	internal class SqliteExternalQueryConnection : DbQueryConnection<SQLiteConnection>
	{
		public SqliteExternalQueryConnection(SQLiteConnection dbConnection) : base(dbConnection)
		{
		}

		public override IQueryCommand GetCommand(CommandData commandData)
		{
			SQLiteCommand cmd = new SQLiteCommand
			{
				Connection     = base.Connection,
				CommandText    = commandData.Text,
				CommandTimeout = commandData.Timeout
			};

			return new SqliteExternalQueryCommand(cmd);
		}

		private class SqliteExternalQueryCommand : DbQueryCommand
		{
			public SqliteExternalQueryCommand(SQLiteCommand command) : base(command)
			{
			}

			protected override void AssignDefaultParameters()
			{
				// no default parameters to assign
			}
		}
	}
}
