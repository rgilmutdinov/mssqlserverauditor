﻿using System.Data;
using System.Data.Common;
using MSSQLServerAuditor.Core.Querying.Commands;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public abstract class DbQueryConnection<TConnection> : IQueryConnection where TConnection : DbConnection
	{
		private readonly TConnection _dbConnection;

		protected DbQueryConnection(TConnection dbConnection)
		{
			this._dbConnection = dbConnection;
		}

		public virtual void Dispose()
		{
			this._dbConnection?.Dispose();
		}

		public virtual void Open()
		{
			if (this._dbConnection != null && this._dbConnection.State == ConnectionState.Closed)
			{
				this._dbConnection.Open();
			}
		}

		public virtual void ChangeDatabase(string database)
		{
			if (!string.IsNullOrEmpty(database))
			{
				this._dbConnection?.ChangeDatabase(database);
			}
		}

		public virtual void Close()
		{
			this._dbConnection?.Close();
		}

		public ConnectionState? State => this._dbConnection?.State;

		protected TConnection Connection => this._dbConnection;

		public abstract IQueryCommand GetCommand(CommandData commandData);
	}
}
