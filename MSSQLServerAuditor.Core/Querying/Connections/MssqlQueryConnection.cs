﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Querying.Commands;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	internal class MsSqlQueryConnection : DbQueryConnection<SqlConnection>
	{
		public MsSqlQueryConnection(SqlConnection sqlConnection) : base(sqlConnection)
		{
		}

		public override IQueryCommand GetCommand(CommandData commandData)
		{
			SqlCommand cmd = new SqlCommand
			{
				Connection     = base.Connection,
				CommandText    = commandData.Text,
				CommandTimeout = commandData.Timeout
			};

			return new MsSqlCommand(cmd);
		}

		private class MsSqlCommand : IQueryCommand
		{
			private readonly SqlCommand _command;

			public MsSqlCommand(SqlCommand command)
			{
				this._command = command;
			}

			public void Dispose()
			{
				this._command.Dispose();
			}

			public void AssignParameters(
				List<QueryParameterInfo> parameters,
				List<ParameterValue>     parameterValues
			)
			{
				if (parameters != null)
				{
					foreach (QueryParameterInfo parameter in parameters)
					{
						SqlParameter p = this._command.CreateParameter();

						p.ParameterName = parameter.Name;
						p.IsNullable    = true;
						p.SqlDbType     = parameter.Type;
						p.Value         = parameter.GetDefaultValue();

						this._command.Parameters.Add(p);
					}
				}

				if (parameterValues != null)
				{
					foreach (ParameterValue param in parameterValues)
					{
						SqlParameter sqlParam = (
							from   SqlParameter p 
							in     this._command.Parameters
							where  p.ParameterName == param.Name
							select p
						).FirstOrDefault();

						if (sqlParam != null)
						{
							sqlParam.Value = ConvertString.ToSqlDbType(
								param.UserValue ?? param.DefaultValue,
								param.IsNull,
								sqlParam.DbType.ToSqlDbType()
							);
						}
					}
				}
			}

			public async Task<IDataReader> ExecuteReaderAsync(CancellationToken cancellationToken)
			{
				return await this._command.ExecuteReaderAsync(cancellationToken);
			}
		}
	}
}
