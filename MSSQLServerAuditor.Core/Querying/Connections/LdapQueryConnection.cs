﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Querying.Commands;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public class LdapQueryConnection : IQueryConnection
	{
		private readonly string _connectionPath;

		public LdapQueryConnection(string connectionPath)
		{
			this._connectionPath = connectionPath;
		}

		public void Dispose()
		{
		}

		public void Open()
		{
		}

		public void ChangeDatabase(string database)
		{
		}

		public void Close()
		{
		}

		public ConnectionState? State => ConnectionState.Open;

		public IQueryCommand GetCommand(CommandData commandData)
		{
			return new LdapQueryCommand(
				this._connectionPath,
				commandData.Text
			);
		}

		private class LdapQueryCommand : IQueryCommand
		{
			private const string GroupFilter     = "filter";
			private const string GroupProperties = "properties";
			private const string GroupScope      = "scope";
			private const string ScopeOneLevel   = "onelevel";
			private const string ScopeBase       = "base";

			// pattern for parsing LDAP query
			private static readonly string LdapQueryPattern =
				$"^(?<{GroupFilter}>.*?)(;(?<{GroupProperties}>[^();]*))?(;(?<{GroupScope}>[^();]*))?$";

			private static readonly Regex LdapQueryRegex = new Regex(LdapQueryPattern);

			private readonly string _connectionPath;

			private string _ldapFilterExpr;
			private string _propertiesExpr;
			private string _searchScopeExpr;

			private readonly Dictionary<string, string> _parameters;

			public LdapQueryCommand()
			{
				this._ldapFilterExpr  = string.Empty;
				this._propertiesExpr  = string.Empty;
				this._searchScopeExpr = string.Empty;
				this._parameters      = new Dictionary<string, string>();
			}

			public LdapQueryCommand(
				string       connectionPath,
				string       query
			) : this()
			{
				this._connectionPath = connectionPath;
				
				ParseQuery(query.TrimmedOrEmpty());
			}

			private void ParseQuery(string query)
			{
				Match match = LdapQueryRegex.Match(query);

				this._ldapFilterExpr = match.Groups[GroupFilter].Value;

				Group grpProperties = match.Groups[GroupProperties];

				if (grpProperties.Success)
				{
					this._propertiesExpr = grpProperties.Value;
				}

				Group grpScope = match.Groups[GroupScope];

				if (grpScope.Success)
				{
					this._searchScopeExpr = grpScope.Value;
				}
			}

			public void Dispose()
			{
			}

			public void AssignParameters(
				List<QueryParameterInfo> parameters,
				List<ParameterValue>     parameterValues
			)
			{
				foreach (ParameterValue paramValue in parameterValues)
				{
					this._parameters[paramValue.Name] = paramValue.UserValue ?? paramValue.DefaultValue;
				}
			}

			public async Task<IDataReader> ExecuteReaderAsync(CancellationToken cancellationToken)
			{
				return await Task.Run(
					() => ExecuteDirectoryReader(),
					cancellationToken
				).ConfigureAwait(false);
			}

			private IDataReader ExecuteDirectoryReader()
			{
				DirectoryEntry    entry    = new DirectoryEntry(this._connectionPath);
				DirectorySearcher searcher = new DirectorySearcher(entry)
				{
					Filter      = FillParameters(this._ldapFilterExpr),
					SearchScope = GetSearchScope()
				};

				string propertiesExpr = FillParameters(this._propertiesExpr);

				string[] propertiesToRead = propertiesExpr
					.TrimmedOrEmpty()
					.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

				bool readAllProperties = propertiesToRead.Length == 0;

				if (!readAllProperties)
				{
					foreach (string property in propertiesToRead.ToList())
					{
						searcher.PropertiesToLoad.Add(property);
					}

					return ReadDirectoryEntries(searcher);
				}

				return ReadDirectoryEntriesWithAllProperties(entry, searcher);
			}

			private SearchScope GetSearchScope()
			{
				string scope = FillParameters(this._searchScopeExpr);

				switch (scope.ToLower())
				{
					case ScopeOneLevel:
						return SearchScope.OneLevel;

					case ScopeBase:
						return SearchScope.Base;

					default:
						return SearchScope.Subtree;
				}
			}

			// retrieves LDAP filter that is filled with parameters' values
			private string FillParameters(string expression)
			{
				return this._parameters.Aggregate(
					expression, (current, parameter) => current.Replace(parameter.Key, parameter.Value));
			}

			private static IDataReader ReadDirectoryEntriesWithAllProperties(
				DirectoryEntry entry,
				DirectorySearcher searcher
			)
			{
				DataTable   dt              = new DataTable();
				ICollection entryProperties = entry.Properties.PropertyNames;

				foreach (string property in entryProperties)
				{
					string colName = GetValidColumnName(property);

					dt.Columns.Add(colName, typeof(string));
				}

				SearchResultCollection searchResults = searcher.FindAll();

				foreach (SearchResult searchResult in searchResults)
				{
					DataRow row = dt.NewRow();

					foreach (string property in entryProperties)
					{
						ResultPropertyValueCollection propValues = searchResult.Properties[property];

						string colName = GetValidColumnName(property);

						row[colName] = ConcatPropertyValues(propValues);
					}

					dt.Rows.Add(row);
				}

				return dt.CreateDataReader();
			}

			private static IDataReader ReadDirectoryEntries(DirectorySearcher searcher)
			{
				SearchResultCollection searchResults = searcher.FindAll();
				DataTable              dataTable     = new DataTable();
				string[]               propsLoaded   = searchResults.PropertiesLoaded;

				foreach (string propertyLoaded in propsLoaded)
				{
					dataTable.Columns.Add(propertyLoaded, typeof(string));
				}

				foreach (SearchResult searchResult in searchResults)
				{
					DataRow row = dataTable.NewRow();

					foreach (var propertyName in propsLoaded)
					{
						ResultPropertyValueCollection propValues = searchResult
							.Properties[propertyName];

						row[propertyName] = ConcatPropertyValues(propValues);
					}

					dataTable.Rows.Add(row);
				}

				return dataTable.CreateDataReader();
			}

			private static string ConcatPropertyValues(ResultPropertyValueCollection values)
			{
				List<string> propValues = new List<string>();

				foreach (var propValue in values)
				{
					propValues.Add(propValue.ToString());
				}

				return string.Join(", ", propValues);
			}

			private static string GetValidColumnName(string propName)
			{
				const string validSqlColumnPattern = @"[^a-zA-Z0-9_]";

				Regex columnPattern = new Regex(validSqlColumnPattern);

				return columnPattern.Replace(propName.ToLower(), "_");
			}

			public void Cancel()
			{
			}
		}
	}
}
