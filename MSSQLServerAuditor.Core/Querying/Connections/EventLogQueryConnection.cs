﻿using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Querying.Commands;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public class EventLogQueryConnection : IQueryConnection
	{
		private readonly string _machineName;

		public EventLogQueryConnection(string machineName)
		{
			this._machineName = machineName;
		}

		public void Dispose()
		{
		}

		public void Open()
		{
		}

		public void ChangeDatabase(string database)
		{
		}

		public void Close()
		{
		}

		public ConnectionState? State
		{
			get { return ConnectionState.Open; }
		}

		public IQueryCommand GetCommand(CommandData commandData)
		{
			return new EventLogCommand(this._machineName, commandData.Text);
		}

		private class EventLogCommand : IQueryCommand
		{
			private const string                               ColumnParamPrefix = "@col";
			private static readonly Dictionary<string, string> DefaultMappings;

			static EventLogCommand()
			{
				DefaultMappings = new Dictionary<string, string>
				{
					{"EventID",     "//System/EventID/text()"},
					{"TimeCreated", "//System/TimeCreated/@SystemTime"},
					{"Channel",     "//System/Channel/text()"},
					{"Computer",    "//System/Computer/text()"},
					{"Provider",    "//System/Provider/@Name"},
					{"Level",       "//System/Level/text()"}
				};
			}

			private readonly string                     _machineName;
			private readonly string                     _eventLogQuery;
			private readonly Dictionary<string, string> _mappings;
			
			public EventLogCommand(
				string machineName,
				string eventLogQuery
			)
			{
				this._machineName   = machineName;
				this._eventLogQuery = eventLogQuery;
				this._mappings      = new Dictionary<string, string>();
			}

			public void Dispose()
			{
			}

			public void AssignParameters(
				List<QueryParameterInfo> parameters,
				List<ParameterValue>     parameterValues
			)
			{
				this._mappings.Clear();

				foreach (ParameterValue parameterValue in parameterValues)
				{
					if (parameterValue.Name.StartsWith(ColumnParamPrefix))
					{
						string columnName = parameterValue.Name.Substring(ColumnParamPrefix.Length);
						string columnPath = parameterValue.UserValue ?? parameterValue.DefaultValue;

						this._mappings.Add(columnName, columnPath);
					}
				}
			}

			public async Task<IDataReader> ExecuteReaderAsync(CancellationToken cancellationToken)
			{
				return await Task.Run(
					() => ExecuteEventLogReader(),
					cancellationToken
				).ConfigureAwait(false);
			}

			private IDataReader ExecuteEventLogReader()
			{
				EventLogQuery eventLogQuery = new EventLogQuery(
					null,
					PathType.LogName,
					this._eventLogQuery.TrimmedOrEmpty()
				);

				EventLogSession session = new EventLogSession(this._machineName);
				eventLogQuery.Session   = session;

				EventLogReader eventLogReader = new EventLogReader(eventLogQuery);

				Dictionary<string, string> mappings = _mappings.Count > 0
					? new Dictionary<string, string>(this._mappings)
					: new Dictionary<string, string>(DefaultMappings);

				DataTable dt = new DataTable();

				foreach (KeyValuePair<string, string> mapping in mappings)
				{
					dt.Columns.Add(mapping.Key, typeof(string));
				}

				EventRecord eventInstance = eventLogReader.ReadEvent();

				while (eventInstance != null)
				{
					XmlDocument    eventDoc  = RemoveXmlns(eventInstance.ToXml());
					XPathNavigator navigator = eventDoc.CreateNavigator();
					DataRow        row       = dt.NewRow();

					foreach (KeyValuePair<string, string> mapping in mappings)
					{
						string         column = mapping.Key;
						string         xpath  = mapping.Value;
						XPathNavigator node   = navigator.SelectSingleNode(xpath);

						if (node != null)
						{
							row[column] = node.Value ?? string.Empty;
						}
					}

					dt.Rows.Add(row);
					eventInstance = eventLogReader.ReadEvent();
				}

				return dt.CreateDataReader();
			}

			private static XmlDocument RemoveXmlns(string xml)
			{
				XDocument doc = XDocument.Parse(xml);

				Debug.Assert(doc.Root != null, "d.Root != null");

				doc.Root.Descendants().Attributes().Where(x => x.IsNamespaceDeclaration).Remove();

				foreach (XElement elem in doc.Descendants())
				{
					elem.Name = elem.Name.LocalName;
				}

				XmlDocument xmlDocument = new XmlDocument();

				xmlDocument.Load(doc.CreateReader());

				return xmlDocument;
			}

			public void Cancel()
			{
			}
		}
	}
}
