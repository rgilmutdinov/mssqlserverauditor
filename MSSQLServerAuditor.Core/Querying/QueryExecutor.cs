﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Querying.Connections;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.DataAccess.Connections;
using NLog;

namespace MSSQLServerAuditor.Core.Querying
{
	public class QueryExecutor
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();
		
		private readonly IAppSettings           _appSettings;
		private readonly SessionExecutorFactory _executorFactory;

		public QueryExecutor(
			IAppSettings              appSettings,
			IStorageConnectionFactory connectionFactory,
			QueryFormatter            queryFormatter
		)
		{
			Check.NotNull(appSettings,       nameof(appSettings));
			Check.NotNull(connectionFactory, nameof(connectionFactory));
			Check.NotNull(queryFormatter,    nameof(queryFormatter));

			this._appSettings = appSettings;

			IQueryConnectionFactory queryConnectionFactory = new QueryConnectionFactory(
				connectionFactory
			);

			this._executorFactory = new SessionExecutorFactory(
				appSettings,
				queryConnectionFactory,
				queryFormatter
			);
		}

		private int ParallelismDegree => this._appSettings.User.MaxThreads == 0
			? int.MaxValue
			: this._appSettings.User.MaxThreads;

		public QueryResult ExecuteQueries(
			AuditContext          context,
			TemplateNodeQueryInfo tnQueryInfo,
			List<QueryInfo>       queries,
			CancellationToken     token
		)
		{
			QueryResult results = new QueryResult(tnQueryInfo);

			ParallelOptions options = new ParallelOptions
			{
				MaxDegreeOfParallelism = ParallelismDegree
			};

			List<AuditSession> sessions = context.CreateSessions();

			Parallel.ForEach(sessions, options, session =>
			{
				ConnectionType type = session.ConnectionType;
				
				QueryInfo query = queries.FirstOrDefault(q =>
					q.ConnectionType == type || q.ConnectionType.IsInternal()
				);

				if (query != null)
				{
					SessionExecutor sessionExecutor = CreateSessionExecutor(session);
					SessionResult   sessionResult   = sessionExecutor.Execute(
						tnQueryInfo,
						query,
						token
					);

					results.AddSessionResult(sessionResult);
				}
			});

			return results;
		}

		public SessionExecutor CreateSessionExecutor(AuditSession session)
		{
			return this._executorFactory.CreateSessionExecutor(session);
		}
	}
}
