﻿using MSSQLServerAuditor.Core.Querying.Connections;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Settings;

namespace MSSQLServerAuditor.Core.Querying
{
	public class SessionExecutorFactory
	{
		private readonly IAppSettings            _appSettings;
		private readonly IQueryConnectionFactory _connectionFactory;
		private readonly QueryFormatter          _queryFormatter;

		public SessionExecutorFactory(
			IAppSettings            appSettings,
			IQueryConnectionFactory connectionFactory,
			QueryFormatter          queryFormatter
		)
		{
			this._appSettings       = appSettings;
			this._connectionFactory = connectionFactory;
			this._queryFormatter    = queryFormatter;
		}

		public SessionExecutor CreateSessionExecutor(AuditSession session)
		{
			return new SessionExecutor(
				this._appSettings,
				this._connectionFactory,
				this._queryFormatter,
				session
			);
		}
	}
}