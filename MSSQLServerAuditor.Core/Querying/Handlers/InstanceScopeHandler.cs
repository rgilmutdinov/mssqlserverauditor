﻿using System.Collections.Generic;
using System.Threading;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Querying.Data;

namespace MSSQLServerAuditor.Core.Querying.Handlers
{
	public class InstanceScopeHandler : QueryScopeHandler
	{
		public InstanceScopeHandler(
			SessionExecutor   sessionExecutor,
			CancellationToken token) : base(sessionExecutor, token)
		{
		}

		public override List<GroupInfo> QueryGroups(
			ServerInstance       serverInstance,
			QueryInfo            query,
			List<ParameterValue> parameters)
		{
			return Lists.Of(GroupInfo.Empty);
		}

		public override List<ParameterValue> GetGroupParameters(GroupInfo groupRec)
		{
			return Lists.Empty<ParameterValue>();
		}
	}
}
