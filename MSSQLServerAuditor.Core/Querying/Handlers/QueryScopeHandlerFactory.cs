﻿using System;
using System.Threading;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Querying.Handlers
{
	public class QueryScopeHandlerFactory
	{
		private readonly SessionExecutor   _sessionExecutor;
		private readonly CancellationToken _token;

		public QueryScopeHandlerFactory(
			SessionExecutor      sessionExecutor,
			CancellationToken token)
		{
			this._sessionExecutor = sessionExecutor;
			this._token           = token;
		}

		public QueryScopeHandler GetHandler(QueryScope queryScope)
		{
			switch (queryScope)
			{
				case QueryScope.Instance:
					return new InstanceScopeHandler(
						this._sessionExecutor,
						this._token
					);

				case QueryScope.Database:
					return new DatabaseScopeHandler(
						this._sessionExecutor,
						this._token
					);

				default:
					throw new ArgumentException(
						$"Unknown query scope '{queryScope}'",
						nameof(queryScope)
					);
			}
		}
	}
}
