﻿using System;

namespace MSSQLServerAuditor.Core.Querying.Data
{
	public class UpdateTiming
	{
		public UpdateTiming()
		{
			
		}

		public UpdateTiming(DateTime? date, TimeSpan? duration)
		{
			Date     = date;
			Duration = duration;
		}

		public DateTime? Date     { get; set; }
		public TimeSpan? Duration { get; set; }
	}
}
