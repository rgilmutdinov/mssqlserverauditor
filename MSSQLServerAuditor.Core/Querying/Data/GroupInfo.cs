﻿using System.Collections.Generic;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Querying.Data
{
	public class GroupInfo
	{
		public static readonly GroupInfo Empty = new GroupInfo(string.Empty, string.Empty);

		public GroupInfo(string name, string id)
		{
			this.Name = name;
			this.Id   = id;

			this.GroupParameters = new Dictionary<string, string>();
			this.ChildGroups     = new List<GroupInfo>();
		}

		public string     Name  { get; }
		public string     Id    { get; }
		public QueryScope Scope { get; set; }

		public List<GroupInfo>            ChildGroups     { get; }
		public Dictionary<string, string> GroupParameters { get; set; }
	}
}
