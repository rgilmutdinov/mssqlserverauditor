﻿using System;
using System.Data.SqlClient;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Core.Querying.Data
{
	/// <summary>
	/// Error info
	/// </summary>
	public class ErrorInfo
	{
		public ErrorInfo(Exception exception)
		{
			Check.NotNull(exception, nameof(exception));

			this.DateTime = DateTime.Now;
			this.Message  = exception.Message;

			SqlException sqlException = exception as SqlException;
			if (sqlException != null)
			{
				this.Code   = sqlException.ErrorCode.ToString();
				this.Number = sqlException.Number.ToString();
			}
			else
			{
				this.Code   = "-1";
				this.Number = "-1";
			}
		}

		public ErrorInfo(string number, string code, string message, DateTime dateTime)
		{
			this.Number   = string.IsNullOrEmpty(number) ? "-1" : number;
			this.Code     = string.IsNullOrEmpty(code)   ? "-1" : code;
			this.Message  = message;
			this.DateTime = dateTime;
		}

		/// <summary>
		/// Exception datetime
		/// </summary>
		public DateTime DateTime { get; }

		/// <summary>
		/// Error message
		/// </summary>
		public string Message { get; }

		/// <summary>
		/// Error code
		/// </summary>
		public string Code { get; }

		/// <summary>
		/// Error number
		/// </summary>
		public string Number { get; }
	}
}
