﻿using System.Collections.Generic;
using System.Data;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Querying.Data
{
	/// <summary>
	/// Query result for database
	/// </summary>
	public class DatabaseResult
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="dataTables">Result datatables</param>
		/// <param name="queryItem">Query item info</param>
		/// <param name="database">Database name (null for instance scope)</param>
		/// <param name="databaseId">Database Id</param>
		public DatabaseResult(
			List<DataTable> dataTables,
			QueryItemInfo   queryItem,
			string          database    = "",
			string          databaseId  = "")
		{
			this.DataTables    = dataTables;
			this.QueryItemInfo = queryItem;
			this.Database      = database;
			this.DatabaseId    = databaseId;
		}

		/// <summary>
		/// Constructor for thrown exception
		/// </summary>
		/// <param name="errorInfo">Error info</param>
		/// <param name="queryItem">Query item info</param>
		/// <param name="database">Database name</param>
		/// <param name="databaseId">Database id (null for instance scope)</param>
		public DatabaseResult(
			ErrorInfo     errorInfo,
			QueryItemInfo queryItem,
			string        database    = "",
			string        databaseId  = ""
		)
		{
			this.ErrorInfo     = errorInfo;
			this.QueryItemInfo = queryItem;
			this.Database      = database;
			this.DatabaseId    = databaseId;
		}

		/// <summary>
		/// Database name (null for instance scope)
		/// </summary>
		public string Database { get; set; }

		/// <summary>
		/// Database Id (null for instance scope)
		/// </summary>
		public string DatabaseId { get; set; }

		/// <summary>
		/// Result datatables
		/// </summary>
		public List<DataTable> DataTables { get; }

		/// <summary>
		/// Error info if exception was thrown
		/// </summary>
		public ErrorInfo ErrorInfo { get; }

		/// <summary>
		/// Query item info
		/// </summary>
		public QueryItemInfo QueryItemInfo { get; }

		/// <summary>
		/// Parent query scope
		/// </summary>
		public QueryScope? ParentScope => QueryItemInfo?.ParentQueryScope;

		/// <summary>
		/// Database result revision
		/// </summary>
		public long? Revision => QueryItemInfo?.Revision;

		public bool IsSavable => QueryItemInfo?.Parent.SaveResults == true;

		public bool IsEmpty
		{
			get
			{
				if (this.ErrorInfo != null)
				{
					// results are not empty if they contain at least one error
					return false;
				}

				foreach (DataTable dataTable in this.DataTables)
				{
					if (dataTable != null && dataTable.Rows.Count > 0)
					{
						// data table contains at least one row
						// therefore the results are not empty
						return false;
					}
				}

				return true;
			}
		}
	}
}
