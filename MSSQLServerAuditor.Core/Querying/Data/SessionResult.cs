﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using MSSQLServerAuditor.Core.Session;

namespace MSSQLServerAuditor.Core.Querying.Data
{
	/// <summary>
	/// Query result for session
	/// </summary>
	public class SessionResult
	{
		private SessionResult()
		{
			this.DatabaseResults = new ConcurrentDictionary<string, DatabaseResult>();
			this.ErrorInfo       = null;
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="session">Session</param>
		/// <param name="timestamp">Timestamp</param>
		public SessionResult(AuditSession session, DateTime timestamp) : this()
		{
			this.Session   = session;
			this.Timestamp = timestamp;
		}

		/// <summary>
		/// Constructor with exception thrown
		/// </summary>
		/// <param name="errorInfo">Error info</param>
		/// <param name="session">Session</param>
		/// <param name="timestamp">Timestamp</param>
		public SessionResult(ErrorInfo errorInfo, AuditSession session, DateTime timestamp) : this()
		{
			this.Session   = session;
			this.ErrorInfo = errorInfo;
			this.Timestamp = timestamp;
		}

		/// <summary>
		/// Session
		/// </summary>
		public AuditSession Session { get; }

		/// <summary>
		/// Result for databases (one item for instance result)
		/// </summary>
		public ConcurrentDictionary<string, DatabaseResult> DatabaseResults { get; }

		/// <summary>
		/// Error info if exception was thrown
		/// </summary>
		public ErrorInfo ErrorInfo { get; }
		
		/// <summary>
		/// Timestamp
		/// </summary>
		public DateTime Timestamp { get; private set; }

		/// <summary>
		/// Add database result
		/// </summary>
		/// <param name="databaseResult"></param>
		public void AddDatabaseResult(DatabaseResult databaseResult)
		{
			this.DatabaseResults.TryAdd(
				databaseResult.Database,
				databaseResult
			);
		}

		public bool IsSavable
		{
			get { return DatabaseResults.Values.Any(dr => dr.IsSavable); }
		}

		public bool IsEmpty
		{
			get
			{
				if (this.ErrorInfo != null)
				{
					// results are not empty if they contain at least one error
					return false;
				}

				// check if any database result is not empty
				return this.DatabaseResults.Values.All(databaseResult => databaseResult.IsEmpty);
			}
		}

		public void RefreshTimestamp(DateTime timestamp)
		{
			if (timestamp > Timestamp)
			{
				Timestamp = timestamp;
			}
		}
	}
}
