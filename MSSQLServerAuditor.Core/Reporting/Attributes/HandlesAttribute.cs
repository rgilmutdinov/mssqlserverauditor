using System;

namespace MSSQLServerAuditor.Core.Reporting.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	public class HandlesAttribute : Attribute
	{
		public string[] PreprocessorTypes { get; private set; }

		public HandlesAttribute(params string[] preprocessorTypes)
		{
			this.PreprocessorTypes = preprocessorTypes;
		}
	}
}
