using System.Xml;
using MSSQLServerAuditor.Core.Querying.Data;

namespace MSSQLServerAuditor.Core.Reporting
{
	public interface IXmlTransformer
	{
		XmlDocument Transform(QueryResultSet results, string databaseName = "");
	}
}
