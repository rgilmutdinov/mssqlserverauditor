using System.Net;
using System.Net.Mime;

namespace MSSQLServerAuditor.Core.Reporting
{
	public class EmailContent
	{
		public string MediaType { get; set; }
		public string Message   { get; set; }
		public string Subject   { get; set; }

		public string ToHtml()
		{
			switch (MediaType)
			{
				case MediaTypeNames.Text.Html:
					return Message;

				case MediaTypeNames.Text.Xml:
					return $"<pre lang=\"xml\"><code>\n{WebUtility.HtmlEncode(Message)}\n</code></pre>";

				default:
					return WebUtility.HtmlEncode(Message);
			}
		}
	}
}
