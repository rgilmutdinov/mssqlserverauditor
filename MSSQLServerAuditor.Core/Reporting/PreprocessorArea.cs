using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Reporting.Extensions;

namespace MSSQLServerAuditor.Core.Reporting
{
	public class PreprocessorArea : IXmlReadable
	{
		public const string XmlNodePreprocessorOld = "mssqlauditorpreprocessor";
		public const string XmlNodePreprocessor    = "preprocessor";
		public const string XmlNodeConfiguration   = "configuration";

		public PreprocessorArea()
		{
			HasSplitter = true;

			PreprocessorsConfigs = new List<PreprocessorConfig>();
		}

		public string  Id          { get; set; }
		public bool    HasSplitter { get; set; }
		public float[] Columns     { get; set; }
		public float[] Rows        { get; set; }

		public string Preprocessor  { get; set; }

		public string Configuration { get; set; }

		public List<PreprocessorConfig> PreprocessorsConfigs { get; set; }

		public void XmlRead(XmlElement node)
		{
			Id           = node.ReadAttribute("id",           string.Empty);
			Preprocessor = node.ReadAttribute("preprocessor", string.Empty);

			string rows     = node.ReadAttribute("rows",     string.Empty);
			string columns  = node.ReadAttribute("columns",  string.Empty);
			string splitter = node.ReadAttribute("splitter", string.Empty);

			HasSplitter = true;

			if (splitter.EqualsIgnoreCase("no"))
			{
				HasSplitter = false;
			}

			Rows    = ParseGridDimension(rows);
			Columns = ParseGridDimension(columns);

			ParseBody(node);
		}

		private void ParseBody(XmlElement node)
		{
			List<XmlElement> nodes = node
				.ChildNodes.OfType<XmlElement>()
				.ToList();

			List<PreprocessorConfig> configs = new List<PreprocessorConfig>();

			foreach (XmlElement childNode in nodes.Where(n => n != null))
			{
				switch (childNode.Name.ToLower())
				{
					case XmlNodePreprocessor:
					case XmlNodePreprocessorOld:
						PreprocessorConfig config = new PreprocessorConfig();
						config.XmlRead(childNode);
						configs.Add(config);
						break;

					case XmlNodeConfiguration:
						Configuration = childNode.InnerXml;
						break;
				}
			}

			PreprocessorsConfigs = configs;
		}

		/// <summary>
		///     Minimal size of any grid dimension (height or width)
		/// </summary>
		private const float GridDimensionEpsilon = 0.0001F;

		/// <summary>
		///     Parses and normalizes grid dimension string.
		/// </summary>
		/// <param name="value">Grid dimension string</param>
		/// <returns>Normalized dimension array</returns>
		public static float[] ParseGridDimension(string value)
		{
			string[] values = value.Split(new[] { ';', ':' }, StringSplitOptions.None);

			float[] cells = values
				.Select(c =>
				{
					float f;
					float.TryParse(c, NumberStyles.Float, CultureInfo.InvariantCulture, out f);

					return Math.Max(f, 0.0F);
				})
				.ToArray();

			// Fill empty columns. This allows to define columns/rows i.e. "25;" or "30;;" and the system will calc the rest 1x75 or 2x35.
			float sum = cells
				.Where(c => c > GridDimensionEpsilon)
				.Sum(c => c);

			if (sum < 100.0F - GridDimensionEpsilon)
			{
				int emptyCellsCount = cells
					.Count(c => c <= GridDimensionEpsilon);

				if (emptyCellsCount > 0)
				{
					sum = 100.0F - sum;

					float inc = sum / emptyCellsCount;

					for (int i = 0; i < cells.Length; i++)
					{
						if (cells[i] > GridDimensionEpsilon)
						{
							continue;
						}

						cells[i] = --emptyCellsCount == 0 ? sum : inc;

						sum -= inc;
					}
				}
			}

			// Normalize sizes so that sum is exactly 100%
			sum = cells.Sum(c => c);

			if (sum < 100.0F - GridDimensionEpsilon || sum > 100.0F + GridDimensionEpsilon)
			{
				float factor = 100.0F / sum;

				sum = 100.0F - sum;

				int nonEmptyCount = cells.Count(c => c > GridDimensionEpsilon);

				for (int i = 0; i < cells.Length; i++)
				{
					float v = cells[i];

					if (v <= GridDimensionEpsilon)
					{
						continue;
					}

					float dec;

					if (--nonEmptyCount > 0)
					{
						cells[i] = v * factor;
						dec = cells[i] - v;
					}
					else
					{
						dec = sum;

						if ((cells[i] += sum) < 0)
						{
							cells[i] = 0;
						}
					}

					sum -= dec;
				}
			}

			return cells;
		}

		public static string FormatGridDimension(float[] values)
		{
			return string.Join(";", values.Select(v =>
				v.ToString(CultureInfo.InvariantCulture))
			);
		}

		public void Validate()
		{
			Check.NotNull(Id,      "Id");
			Check.NotNull(Columns, "Columns");
			Check.NotNull(Rows,    "Rows");

			if (HasSplitter)
			{
				int rowsCount = Rows.Length;
				int colsCount = Columns.Length;

				BitArray[] grid = new BitArray[rowsCount];

				for (int i = 0, iMax = rowsCount; i < iMax; i++)
				{
					grid[i] = new BitArray(colsCount, false);
				}

				foreach (PreprocessorConfig preprocessor in PreprocessorsConfigs)
				{
					int x = preprocessor.Column, y = preprocessor.Row, w = preprocessor.ColSpan, h = preprocessor.RowSpan;

					// Check coordinates and size to be in bounds
					if (x < 1 || x + w - 1 > colsCount || y < 1 || y + h - 1 > rowsCount)
					{
						throw new ArgumentOutOfRangeException("preprocessors", "Coordinates are incorrect for " + preprocessor.Id + "!");
					}

					for (int yMax = y + h; y < yMax; y++)
					{
						BitArray r = grid[y - 1];

						for (int xMax = x + w; x < xMax; x++)
						{
							// Two or more preprocessors overlapping!
							if (r[x - 1])
							{
								throw new ArgumentOutOfRangeException(
									"preprocessors",
									"Preporcessors overlapping at " + x + ":" + y + "! Check at least " +
									preprocessor.Id + "!"
								);
							}

							r[x - 1] = true;
						}
					}
				}
			}
		}
	}
}
