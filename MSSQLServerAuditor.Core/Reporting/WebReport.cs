using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;

namespace MSSQLServerAuditor.Core.Reporting
{
	public class WebReport : IVisualReport
	{
		private readonly WebBrowser _browser;

		private WebReport()
		{
			this._browser = new WebBrowser();
			HideScriptErrors(this._browser);
		}

		public WebReport(Uri uri) : this()
		{
			this._browser.Navigate(uri);
		}

		public WebReport(string html) : this()
		{
			this._browser.NavigateToString(html);
		}

		public FrameworkElement Control => this._browser;

		public void Release()
		{
			WebBrowser browser = Control as WebBrowser;
			if (browser != null)
			{
				try
				{
					//navigating to a blank page before disposing reduces the handle leak
					browser.Source = null;
				}
				catch
				{
					// Multiple exceptions can happen here (ArgumentException, COMException, ... so catch everything)
				}

				try
				{
					IKeyboardInputSite keyboardInputSite = ((IKeyboardInputSink) browser).KeyboardInputSite;
					if (keyboardInputSite != null)
					{
						keyboardInputSite.Unregister();

						Type type = keyboardInputSite.GetType();
						FieldInfo fieldInfo = type.GetField("_sinkElement", BindingFlags.NonPublic | BindingFlags.Instance);
						fieldInfo?.SetValue(keyboardInputSite, null);
					}
				}
				catch
				{
					// Catch everything, this is a nasty hack to dispose the WebBrowser
				}

				browser.Dispose();
			}
		}

		public void HideScriptErrors(WebBrowser browser)
		{
			// Sets the .Silent property on the underlying activeX object.
			// Which is the same as the .ScriptErrorsSuppressed property which is the Windows forms equivalent.

			FieldInfo fiComBrowser = typeof(WebBrowser)
				.GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);

			if (fiComBrowser == null)
			{
				return;
			}

			object comBrowser = fiComBrowser.GetValue(browser);
			if (comBrowser == null)
			{
				browser.Loaded += BrowserOnLoaded; //In case we are to early
				return;
			}

			comBrowser.GetType().InvokeMember(
				"Silent",
				BindingFlags.SetProperty,
				null,
				comBrowser,
				new object[] { true }
			);
		}

		private void BrowserOnLoaded(object sender, RoutedEventArgs routedEventArgs)
		{
			WebBrowser browser = sender as WebBrowser;
			if (browser != null)
			{
				HideScriptErrors(browser);
				browser.Loaded -= BrowserOnLoaded;
			}
		}
	}
}