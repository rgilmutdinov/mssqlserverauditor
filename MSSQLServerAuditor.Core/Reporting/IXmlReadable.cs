using System.Xml;

namespace MSSQLServerAuditor.Core.Reporting
{
	public interface IXmlReadable
	{
		void XmlRead(XmlElement node);
	}
}
