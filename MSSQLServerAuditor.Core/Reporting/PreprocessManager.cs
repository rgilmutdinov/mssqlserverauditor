using System;
using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Reporting.Attributes;

namespace MSSQLServerAuditor.Core.Reporting
{
	public class PreprocessManager: IPreprocessManager
	{
		public PreprocessManager()
		{
			this.AllPreprocessors = new List<IPreprocessor>();
		}

		public List<IPreprocessor> AllPreprocessors { get; }

		public IPreprocessor ResolvePreprocessor(string typeName)
		{
			foreach (IPreprocessor preprocessor in AllPreprocessors)
			{
				Type pType = preprocessor.GetType();

				string[] forTypes = pType.GetAttributeValue(
					(HandlesAttribute ha) => ha.PreprocessorTypes
				);

				typeName = typeName.ToLower();

				if (forTypes.Any(forType => typeName.EqualsIgnoreCase(forType)))
				{
					return preprocessor;
				}
			}

			return null;
		}
	}
}
