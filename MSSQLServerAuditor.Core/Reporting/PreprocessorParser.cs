using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using NLog;

namespace MSSQLServerAuditor.Core.Reporting
{
	public static class PreprocessorParser
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public const string XmlNodePreprocessorsOld = "mssqlauditorpreprocessors";
		public const string XmlNodePreprocessors    = "preprocessors";

		public static List<PreprocessorArea> ParseXml(XmlDocument xmlDoc)
		{
			List<PreprocessorArea> areas = new List<PreprocessorArea>();

			if (xmlDoc?.DocumentElement == null)
			{
				Log.Error("Xml document is null or doesn't contain root element");
				return areas;
			}

			 IEnumerable<XmlElement> nodes = xmlDoc
				.DocumentElement
				.ChildNodes.OfType<XmlElement>();

			foreach (XmlElement childNode in nodes)
			{
				switch (childNode.Name.ToLower())
				{
					case XmlNodePreprocessors:
					case XmlNodePreprocessorsOld:
						PreprocessorArea area = new PreprocessorArea();
						area.XmlRead(childNode);
						areas.Add(area);
						break;

					case PreprocessorArea.XmlNodePreprocessor:
					case PreprocessorArea.XmlNodePreprocessorOld:
						string errorMessage =
							"Invalid configuration: <mssqlauditorpreprocessor> is not embedded in <mssqlauditorpreprocessors>!";
						Log.Error(errorMessage);

						throw new ArgumentException(nameof(xmlDoc), errorMessage);
				}
			}

			return areas;
		}
	}
}
