using System.Windows;

namespace MSSQLServerAuditor.Core.Reporting
{
	public interface IVisualReport
	{
		FrameworkElement Control { get; }

		void Release();
	}
}