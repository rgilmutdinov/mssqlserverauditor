using System;
using System.ComponentModel;
using System.Xml;
using MSSQLServerAuditor.Common.Extensions;

namespace MSSQLServerAuditor.Core.Reporting.Extensions
{
	public static class XmlElementExtensions
	{
		public static string ReadAttribute(
			this XmlElement node,
			string          attrName,
			string          defaultValue
		)
		{
			XmlAttribute attribute = node.Attributes[attrName];

			if (attribute != null)
			{
				return attribute.Value;
			}

			return defaultValue;
		}

		public static T ReadAttribute<T>(
			this XmlElement node,
			string          attrName,
			T               defaultValue
		)
		{
			XmlAttribute attribute = node.Attributes[attrName];

			if (attribute == null)
			{
				return defaultValue;
			}

			TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

			try
			{
				return (T) converter.ConvertFromString(attribute.Value);
			}
			catch (Exception)
			{
				return defaultValue;
			}
		}

		public static void SetAttributeIfNotEmpty(
			this XmlElement node,
			string          attrName,
			string          attrValue
		)
		{
			if (!attrValue.IsNullOrEmpty())
			{
				node.SetAttribute(attrName, attrValue);
			}
		}
	}
}
