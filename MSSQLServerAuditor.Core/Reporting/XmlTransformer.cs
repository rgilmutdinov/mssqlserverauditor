using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Text;
using System.Xml;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.Core.Reporting.Extensions;
using MSSQLServerAuditor.Core.Session;
using NLog;

namespace MSSQLServerAuditor.Core.Reporting
{
	public class XmlTransformer : IXmlTransformer
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private const string AttributeTimestamp = "timestamp";
		private const string TagResult          = "MSSQLResult";
		private const string TagResults         = "MSSQLResults";

		private readonly string _lang;

		public XmlTransformer(string lang)
		{
			this._lang = lang;
		}

		public XmlDocument Transform(QueryResultSet resultSet, string databaseName = "")
		{
			if (resultSet == null)
			{
				return null;
			}

			try
			{
				XmlDocument xml  = CreateXmlDocument();
				XmlElement  root = CreateRoot(xml, resultSet.Timestamp);

				foreach (QueryResult queryResult in resultSet.Results)
				{
					TemplateNodeQueryInfo tnQueryInfo = queryResult.QueryInfo;
					foreach (KeyValuePair<AuditSession, SessionResult> instancePair in queryResult.SessionResults)
					{
						AuditSession  session       = instancePair.Key;
						SessionResult sessionResult = instancePair.Value;
						
						try
						{
							XmlElement resultNode = xml.CreateElement(TagResult);

							resultNode.SetAttribute("instance", session.Server.ConnectionName);
							resultNode.SetAttribute("name",     tnQueryInfo.QueryName);

							int       rowCount   = 0;
							int       recordSets = 0;
							ErrorInfo error      = null;

							if (sessionResult.ErrorInfo == null)
							{
								DatabaseResult databaseResult;

								if (sessionResult.DatabaseResults.TryGetValue(databaseName, out databaseResult))
								{
									XmlElement parametersNode = CreateParametersNode(xml, tnQueryInfo, databaseResult.QueryItemInfo);
									resultNode.AppendChild(parametersNode);

									if (databaseResult.DataTables != null)
									{
										rowCount = databaseResult.DataTables.Where(x => x != null)
											.Select(x => x.Rows)
											.Sum(x => x.Count);

										recordSets = databaseResult.DataTables.Count;

										long recordSet = 1L;

										foreach (DataTable dataTable in databaseResult.DataTables)
										{
											XmlElement xmlTable = FormatDataTable(xml, dataTable, recordSet);
											if (xmlTable != null)
											{
												resultNode.AppendChild(xmlTable);
											}

											recordSet++;
										}
									}

									error = databaseResult.ErrorInfo;

									if (databaseResult.ParentScope != null)
									{
										QueryScope scope = databaseResult.ParentScope.Value;

										switch (scope)
										{
											case QueryScope.Database:
												resultNode.SetAttributeIfNotEmpty("database",   databaseResult.Database);
												resultNode.SetAttributeIfNotEmpty("databaseId", databaseResult.DatabaseId);
												break;
										}
									}
								}
							}
							else
							{
								error = sessionResult.ErrorInfo;
							}

							resultNode.SetAttribute("RecordSets", recordSets.ToString());
							resultNode.SetAttribute("RowCount",   rowCount.ToString());

							string errorCode   = string.Empty;
							string errorNumber = "0";

							if (error != null)
							{
								XmlElement errorNode = xml.CreateElement("SqlErrorMessage");

								errorNode.InnerText = error.Message;
								resultNode.AppendChild(errorNode);

								errorCode   = error.Code;
								errorNumber = error.Number;
							}

							resultNode.SetAttribute("SqlErrorCode",   errorCode);
							resultNode.SetAttribute("SqlErrorNumber", errorNumber);
							resultNode.SetAttribute("hierarchy",      tnQueryInfo.Hierarchy);

							root.AppendChild(resultNode);
						}
						catch (Exception ex)
						{
							string error = "Failed to transform instance query results to XML file";
							Log.Error(ex, error);
							throw new XmlException(error, ex);
						}
					}
				}

				return xml;
			}
			catch (Exception ex)
			{
				string error = "Failed to transform node query results to XML file";
				Log.Error(ex, error);
				throw new XmlException(error, ex);
			}
		}

		private XmlElement CreateRoot(XmlDocument xml, DateTime timestamp)
		{
			XmlElement root = xml.CreateElement(TagResults);

			root.SetAttribute(
				AttributeTimestamp,
				timestamp.ToSortableDateString()
			);

			root.SetAttribute(
				"xml:lang",
				this._lang
			);

			xml.AppendChild(root);

			return root;
		}

		private XmlDocument CreateXmlDocument()
		{
			XmlDocument    xml            = new XmlDocument();
			XmlDeclaration xmlDeclaration = xml
				.CreateXmlDeclaration("1.0", "UTF-8", string.Empty);

			xml.AppendChild(xmlDeclaration);

			return xml;
		}

		private XmlElement CreateParametersNode(
			XmlDocument           xml,
			TemplateNodeQueryInfo tnQueryInfo,
			QueryItemInfo         queryItem)
		{
			XmlElement parametersNode = xml.CreateElement("sql-select-parameters");

			if (queryItem != null)
			{
				foreach (ParameterValue pValue in tnQueryInfo.ParameterValues)
				{
					XmlElement paramNode = xml.CreateElement("sql-select-parameter");

					QueryParameterInfo pInfo = queryItem.Parent.Parameters.FirstOrDefault(
						p => pValue.Name.EqualsIgnoreCase(p.Name)
					);

					paramNode.SetAttribute("name", pValue.Name);
					paramNode.SetAttribute("type", pInfo?.Type.ToString() ?? string.Empty);
					paramNode.SetAttribute("value", pValue.UserValue ?? pValue.DefaultValue);

					parametersNode.AppendChild(paramNode);
				}
			}

			return parametersNode;
		}

		private static XmlElement FormatDataTable(
			XmlDocument xml,
			DataTable   dataTable,
			long?       recordSetNumber
		)
		{
			if (dataTable == null)
			{
				return null;
			}

			XmlElement xmlData = xml.CreateElement("RecordSet");

			xmlData.SetAttribute("id",       recordSetNumber.ToString());
			xmlData.SetAttribute("RowCount", dataTable.Rows.Count.ToString());

			int rowNumber = 1;

			foreach (DataRow row in dataTable.Rows)
			{
				XmlElement xmlRow = xml.CreateElement("Row");
				xmlRow.SetAttribute("id", rowNumber.ToString());
				
				foreach (DataColumn col in dataTable.Columns)
				{
					if (col.ColumnName != null)
					{
						XmlElement xmlCol  = xml.CreateElement(col.ColumnName.RemoveXmlSpecialChars());
						object     value   = row[col];
						Type       colType = col.DataType;

						xmlCol.InnerText = FormatValue(value, colType);

						xmlRow.AppendChild(xmlCol);
					}
				}

				rowNumber++;

				xmlData.AppendChild(xmlRow);
			}

			return xmlData;
		}

		private static string FormatValue(
			object value,
			Type   type
		)
		{
			StringBuilder sbXml = new StringBuilder();

			if (value is DBNull)
			{
				sbXml.Append(string.Empty);
			}
			else if (type == typeof(DateTime))
			{
				string dtString = ((DateTime) value).ToSortableDateString();
				sbXml.Append(dtString);
			}
			else if (type == typeof(byte[]))
			{
				sbXml.Append("0x" + BitConverter.ToString((byte[])value));
			}
			else
			{
				sbXml.Append(Convert.ToString(value, CultureInfo.InvariantCulture.NumberFormat));
			}

			return EscapeXml(sbXml);
		}

		private static string EscapeXml(StringBuilder sb)
		{
			for (int i = 0; i < sb.Length; i++)
			{
				if (!XmlConvert.IsXmlChar(sb[i]))
				{
					string oldString = sb.ToString(i, 1);
					string newString = "\\" + XmlConvert.EncodeName(oldString).Trim('_');

					sb.Replace(oldString, newString);
				}
			}

			return SecurityElement.Escape(sb.ToString());
		}
	}
}
