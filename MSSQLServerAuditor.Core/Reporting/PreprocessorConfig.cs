using System.Xml;
using MSSQLServerAuditor.Core.Reporting.Extensions;

namespace MSSQLServerAuditor.Core.Reporting
{
	public class PreprocessorConfig : IXmlReadable
	{
		public PreprocessorConfig()
		{
			TextAlign         = TextAlign.Left;
			VerticalTextAlign = VerticalTextAlign.None;
		}

		public string Id   { get; set; }
		public int Column  { get; set; }
		public int Row     { get; set; }
		public int ColSpan { get; set; }
		public int RowSpan { get; set; }

		public string Configuration { get; set; }

		public VerticalTextAlign VerticalTextAlign { get; set; }
		public TextAlign         TextAlign         { get; set; }

		public void XmlRead(XmlElement node)
		{
			Id      = node.ReadAttribute("id",      string.Empty);
			Column  = node.ReadAttribute("column",  1);
			Row     = node.ReadAttribute("row",     1);
			ColSpan = node.ReadAttribute("colspan", 1);
			RowSpan = node.ReadAttribute("rowspan", 1);

			VerticalTextAlign = node.ReadAttribute(
				"text-vertical-align",
				VerticalTextAlign.None
			);

			TextAlign = node.ReadAttribute(
				"text-align",
				TextAlign.Left
			);

			Configuration = node.InnerXml;
		}
	}
}
