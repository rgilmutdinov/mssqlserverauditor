﻿using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using Quartz;

namespace MSSQLServerAuditor.Core.Schedule
{
	public class JobInfo
	{
		public JobInfo(TemplateNodeInfo tnInfo, ScheduleDetails details)
		{
			Check.NotNull(tnInfo,  nameof(tnInfo));
			Check.NotNull(details, nameof(details));

			TemplateNodeInfo = tnInfo;
			ScheduleDetails  = details;
		}

		public TemplateNodeInfo TemplateNodeInfo { get; set; }
		public ScheduleDetails  ScheduleDetails  { get; set; }

		public TriggerKey GetTriggerKey(string group)
		{
			string triggerName = $"trigger_{ScheduleDetails.Id}";

			return new TriggerKey(triggerName, group);
		}

		public JobKey GetJobKey(string group)
		{
			string jobName = $"job_{ScheduleDetails.Id}";

			return new JobKey(jobName, group);
		}
	}
}
