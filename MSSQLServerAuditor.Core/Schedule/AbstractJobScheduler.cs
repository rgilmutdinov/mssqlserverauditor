﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Session;
using NLog;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;

namespace MSSQLServerAuditor.Core.Schedule
{
	public abstract class AbstractJobScheduler
	{
		protected static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IScheduler _quartzScheduler;

		protected AbstractJobScheduler()
		{
			ISchedulerFactory schedFact = new StdSchedulerFactory();

			this._quartzScheduler = schedFact.GetScheduler();
		}

		protected IScheduler QuartzScheduler => this._quartzScheduler;

		public void StartJobScheduler()
		{
			if (!this._quartzScheduler.IsStarted)
			{
				this._quartzScheduler.Start();
			}
		}

		public void Pause()
		{
			this._quartzScheduler.PauseAll();
		}

		public void Resume()
		{
			this._quartzScheduler.ResumeAll();
		}

		public void Shutdown()
		{
			if (!this._quartzScheduler.IsShutdown)
			{
				this._quartzScheduler.Shutdown(false);
			}
		}

		public void UnscheduleJobs(AuditContext context)
		{
			string groupName = GetGroupName(context);

			List<JobKey> jobKeys = this._quartzScheduler
				.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(groupName))
				.ToList();

			// remove all jobs associated with the connection
			foreach (JobKey jobKey in jobKeys)
			{
				IList<ITrigger> triggers = this._quartzScheduler
					.GetTriggersOfJob(jobKey);

				IList<TriggerKey> triggerKeys = triggers
					.Select(trigger => trigger.Key)
					.ToList();

				// remove triggers associated with the job
				this._quartzScheduler.UnscheduleJobs(triggerKeys);

				// remove job
				this._quartzScheduler.DeleteJob(jobKey);
			}
		}

		protected virtual bool ShouldScheduleJob(JobInfo jobInfo)
		{
			ScheduleSettings settings = jobInfo.ScheduleDetails;
			return settings.IsEnabled == true;
		}

		public async Task ResheduleJobsAsync(AuditContext context, List<JobInfo> updateJobs)
		{
			Pause();

			try
			{
				await Task.Run(() =>
				{
					foreach (JobInfo updateJob in updateJobs)
					{
						Reschedule(context, updateJob);
					}
				});
			}
			catch (Exception exc)
			{
				Log.Error(exc, "Failed to save schedules");
			}
			finally
			{
				Resume();
			}
		}

		public bool ScheduleJob(AuditContext context, JobInfo jobInfo)
		{
			if (!ShouldScheduleJob(jobInfo))
			{
				return false;
			}

			string groupName = GetGroupName(context);

			Action updateAction = () => ExecuteJob(context, jobInfo);

			UpdateJob updateJob = UpdateJob.Create(
				jobInfo,
				groupName,
				updateAction
			);

			try
			{
				this._quartzScheduler.ScheduleJob(
					updateJob.JobDetail,
					updateJob.Trigger
				);

				return true;
			}
			catch (SchedulerException se)
			{
				string node     = jobInfo.TemplateNodeInfo.Name;
				string schedule = jobInfo.ScheduleDetails.Name;

				Log.Warn(
					$"Failed to schedule job (node: '{node}', name: '{schedule}')",
					se
				);

				return false;
			}
		}

		protected abstract void ExecuteJob(AuditContext context, JobInfo jobInfo);

		private string GetGroupName(AuditContext context)
		{
			return GetGroupName(context.ConnectionGroup);
		}

		private string GetGroupName(ConnectionGroup connectionGroup)
		{
			return GetGroupName(connectionGroup.Id);
		}

		private string GetGroupName(long? groupId)
		{
			return $"group_{groupId}";
		}

		private JobKey GetJobKey(AuditContext context, JobInfo jobInfo)
		{
			ConnectionGroup connectionGroup = context.ConnectionGroup;

			if (connectionGroup.IsNew)
			{
				return null;
			}

			return GetJobKey(
				jobInfo.ScheduleDetails.Id,
				connectionGroup.Id
			);
		}

		private JobKey GetJobKey(long settingsId, long connectionGroupId)
		{
			string jobName   = $"job_{settingsId}";
			string groupName = GetGroupName(connectionGroupId);

			return new JobKey(jobName, groupName);
		}

		public void RemoveJob(
			long settingsId,
			long connectionGroupId
		)
		{
			JobKey jobKey = GetJobKey(settingsId, connectionGroupId);

			try
			{
				RemoveJob(jobKey);
			}
			catch (SchedulerException se)
			{
				Log.Warn(
					$"Failed to unschedule job (settingsId: '{settingsId}')",
					se
				);
			}
		}

		private void RemoveJob(AuditContext context, JobInfo jobInfo)
		{
			JobKey jobKey = GetJobKey(context, jobInfo);

			try
			{
				RemoveJob(jobKey);
			}
			catch (SchedulerException se)
			{
				string node     = jobInfo.TemplateNodeInfo.Name;
				string schedule = jobInfo.ScheduleDetails.Name;

				Log.Warn(
					$"Failed to unschedule job (node: '{node}', name: '{schedule}')",
					se
				);
			}
		}

		private void RemoveJob(JobKey jobKey)
		{
			ITrigger trigger = this._quartzScheduler.GetTriggersOfJob(jobKey).FirstOrDefault();

			if (trigger != null)
			{
				this._quartzScheduler.UnscheduleJob(trigger.Key);
			}

			this._quartzScheduler.DeleteJob(jobKey);
		}

		public void Reschedule(AuditContext context, JobInfo jobInfo)
		{
			RemoveJob(context, jobInfo);
			ScheduleJob(context, jobInfo);
		}

		public DateTime GetNextFireTime(AuditContext context, JobInfo jobInfo)
		{
			DateTime nextFireTime = DefaultValues.Date.Minimum;
			JobKey   jobKey       = GetJobKey(context, jobInfo);

			if (jobKey != null)
			{
				bool isJobExisting = this._quartzScheduler.CheckExists(jobKey);

				if (isJobExisting)
				{
					ITrigger trigger = this._quartzScheduler
						.GetTriggersOfJob(jobKey)
						.FirstOrDefault();

					DateTimeOffset? nextFireTimeUtc = trigger?.GetNextFireTimeUtc();

					if (nextFireTimeUtc != null)
					{
						nextFireTime = TimeZone.CurrentTimeZone.ToLocalTime(
							nextFireTimeUtc.Value.DateTime
						);
					}
				}
			}

			return nextFireTime;
		}
	}
}
