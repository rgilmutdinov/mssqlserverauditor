﻿using System;
using Quartz;

namespace MSSQLServerAuditor.Core.Schedule
{
	public class ActionJob : IJob
	{
		public const string ParamAction = "action";

		public void Execute(IJobExecutionContext context)
		{
			Action action = context.MergedJobDataMap[ParamAction] as Action;

			action?.Invoke();
		}
	}
}
