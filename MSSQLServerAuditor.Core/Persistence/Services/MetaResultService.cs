﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Services.Base;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.Core.Persistence.Services
{
	public class MetaResultService : CachingService
	{
		private readonly NodeService _nodeService;

		private long?           _maxRequestCache = null;
		private readonly object _maxRequestLock = new object();

		public MetaResultService(
			IDbContextScopeFactory<StorageContext>  scopeFactory,
			ConcurrentDictionary<Type, EntityCache> cache,
			RepositoryCatalog                       repositories,
			Mapper                                  mapper,
			NodeService                             nodeService,
			bool                                    isReadOnly = false) 
			: base(scopeFactory, cache, repositories, mapper, isReadOnly)
		{
			this._nodeService = nodeService;
		}

		public long GetMaxRequestId()
		{
			lock (this._maxRequestLock)
			{
				if (this._maxRequestCache == null)
				{
					using (ContextScopeFactory.CreateReadOnly())
					{
						this._maxRequestCache = GetRepository<DBMetaResult>().All()
							.Where(result => result.RequestId != null)
							.Max(result => result.RequestId) ?? 0;
					}
				}
				else
				{
					this._maxRequestCache++;
				}

				return this._maxRequestCache.Value;
			}
		}

		public void SaveMetaResult(
			long          requestId,
			long          sessionId,
			SessionResult sessionResult,
			long          queryId,
			long?         rowsSaved
		)
		{
			MetaResult metaResult = CreateMetaResult(
				requestId,
				sessionId,
				sessionResult,
				queryId,
				rowsSaved
			);

			Save(Mapper.ToDb(metaResult));
		}

		public void SaveETLResult(ETLResult etlResult)
		{
			DBMetaEtlResult meta = Mapper.ToDb(etlResult);

			Save(meta);
		}

		public void SaveMeta(
			long?            groupGueryId,
			ErrorInfo        error,
			IList<DataTable> tables
		)
		{
			MetaResult metaResult = new MetaResult
			{
				QueryId      = null,
				QueryGroupId = groupGueryId,
				RequestId    = this.GetMaxRequestId() + 1,
				SessionId    = 1,
				DateCreated  = DateTime.Now
			};

			if (error == null)
			{
				metaResult.Rows = tables?.Select(x => x.Rows).Sum(x => x.Count) ?? 0L;

				metaResult.RecordSets = tables?.Count ?? 0L;
			}
			else
			{
				metaResult.Rows         = 0L;
				metaResult.RecordSets   = 0L;
				metaResult.ErrorId      = error.Number;
				metaResult.ErrorCode    = error.Code;
				metaResult.ErrorMessage = error.Message;
			}

			Save(Mapper.ToDb(metaResult));
		}

		internal void SaveMeta(
			TemplateNodeInfo templateNodeInfo,
			QueryResultSet   results,
			long             requestId,
			DateTime         timestamp
		)
		{
			long sessionId = 1L;

			foreach (QueryResult queryResult in results.Results)
			{
				TemplateNodeQueryInfo tnQueryInfo = queryResult.QueryInfo;
				
				foreach (SessionResult sessionResult in queryResult.SessionResults.Values)
				{
					DBQuery query = this._nodeService.GetQuery(
						sessionResult.Session,
						tnQueryInfo,
						timestamp,
						false
					);

					SaveMetaResult(
						requestId,
						sessionId,
						sessionResult,
						query.Id,
						null
					);
				}
			}

			// TODO: implement it
			//this.ResetRowCountCache(templateNodeInfo);
		}

		private MetaResult CreateMetaResult(
			long          requestId,
			long          sessionId,
			SessionResult sessionResult,
			long?         queryId,
			long?         rowsSaved
		)
		{
			string errorNumber    = null;
			string errorCode      = null;
			string errorMessage   = null;
			long?  recordSetCount = 0L;

			MetaResult metaResult = new MetaResult
			{
				QueryId      = queryId,
				QueryGroupId = null,
				RequestId    = requestId,
				SessionId    = sessionId,
			};

			if (sessionResult != null)
			{
				metaResult.DateCreated = sessionResult.Timestamp;

				if (sessionResult.ErrorInfo == null)
				{
					if (sessionResult.DatabaseResults.ContainsKey("")) //group.Name))
					{
						DatabaseResult dbResult = sessionResult.DatabaseResults[""]; //group.Name;

						if (rowsSaved == null)
						{
							rowsSaved = dbResult.DataTables?
								.Select(x => x.Rows)
								.Sum(x => x.Count) ?? 0L;
						}

						recordSetCount = dbResult.DataTables?.Count ?? 0L;

						if (dbResult.ErrorInfo != null)
						{
							errorNumber  = dbResult.ErrorInfo.Number;
							errorCode    = dbResult.ErrorInfo.Code;
							errorMessage = dbResult.ErrorInfo.Message;
						}
					}
				}
				else
				{
					errorNumber  = sessionResult.ErrorInfo.Number;
					errorCode    = sessionResult.ErrorInfo.Code;
					errorMessage = sessionResult.ErrorInfo.Message;
					rowsSaved = 0L;
				}
			}
			else
			{
				metaResult.DateCreated = DateTime.Now;
			}

			metaResult.Rows         = rowsSaved;
			metaResult.RecordSets   = recordSetCount;
			metaResult.ErrorId      = errorNumber;
			metaResult.ErrorCode    = errorCode;
			metaResult.ErrorMessage = errorMessage;

			return metaResult;
		}

		public MetaResult ReadLastMeta(long queryId)
		{
			DBMetaResult dbMeta = Get<DBMetaResult>(
				mr => mr.QueryId == queryId,
				mr => mr.OrderByDescending(r => r.DateCreated)
			);

			if (dbMeta != null)
			{
				return Mapper.FromDb(dbMeta);
			}

			return null;
		}

		public void SaveSubMeta(DBMetaSubResult meta)
		{
			Save(meta);
		}
	}
}
