﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Services.Base;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.Core.Persistence.Services
{
	public class TemplateService : CachingService
	{
		public TemplateService(
			IDbContextScopeFactory<StorageContext>  scopeFactory,
			ConcurrentDictionary<Type, EntityCache> cache,
			RepositoryCatalog                       repositories,
			Mapper                                  mapper,
			bool                                    isReadOnly = false)
			: base(scopeFactory, cache, repositories, mapper, isReadOnly)
		{
		}

		public long GetTemplateNodeId(
			Template         template,
			TemplateNodeInfo tnInfo)
		{
			if (tnInfo.TemplateNodeId != 0)
			{
				return tnInfo.TemplateNodeId;
			}

			return GetOrAddTemplateNode(template, tnInfo).Id;
		}

		public TemplateNode GetOrAddTemplateNode(
			Template         template,
			TemplateNodeInfo tnInfo)
		{
			Debug.Assert(!tnInfo.IsInstance);

			long? parentId = null;

			if (tnInfo.Parent != null)
			{
				TemplateNode parent = GetOrAddTemplateNode(
					template,
					tnInfo.Parent
				);

				parentId = parent.Id;
			}

			long? parentQueryId = null;
			TemplateNodeQueryInfo parentQuery = tnInfo.GetParentQuery();
			
			if (parentQuery != null)
			{
				TemplateQueryGroup queryGroup = GetOrAddTemplateQueryGroup(
					template,
					parentQuery
				);

				if (queryGroup != null)
				{
					parentQueryId = queryGroup.Id;
				}
			}

			DBTemplateNode tnSpec = new DBTemplateNode
			{
				TemplateId                 = template.Id,
				ParentId                   = parentId,
				UId                        = tnInfo.Id,
				Name                       = tnInfo.Name,
				Icon                       = tnInfo.DefaultIcon,
				ShowEmpty                  = !tnInfo.HideEmptyResultDatabases,
				ShowRecordsNumber          = tnInfo.ShowNumberOfRecords,
				TemplateQueryGroupParentId = parentQueryId
			};

			DBTemplateNode saved = GetOrAdd(tnSpec);

			tnInfo.TemplateNodeId = saved.Id;

			return Mapper.FromDb(saved);
		}

		public List<Template> GetTemplates(
			string         connectionName,
			ConnectionType connectionType
		)
		{
			string type = connectionType.ToString();
			return FilterAll<DBQuery, DBTemplate>(allQueries => (
				from q in allQueries

				where
					q.ServerInstance.ConnectionName      == connectionName &&
					q.ServerInstance.ConnectionType.Name == type

				select
					q.TemplateQuery.TemplateNode.Template
				).Distinct()
			)
			.Select(Mapper.FromDb)
			.ToList();
		}

		private TemplateQueryGroup GetOrAddTemplateQueryGroup(
			Template              template,
			TemplateNodeQueryInfo tnQueryInfo
		)
		{
			TemplateNodeInfo tnInfo = tnQueryInfo.TemplateNode;

			long templateNodeId = GetTemplateNodeId(template, tnInfo);

			return GetOrAddTemplateQueryGroup(templateNodeId, tnQueryInfo);
		}

		private TemplateQueryGroup GetOrAddTemplateQueryGroup(
			long                  templateNodeId,
			TemplateNodeQueryInfo tnQueryInfo
		)
		{
			DBTemplateQueryGroup tnqSpec = new DBTemplateQueryGroup
			{
				TemplateNodeParentId = templateNodeId,
				GroupId              = tnQueryInfo.Id,
				Name                 = tnQueryInfo.QueryName,
				DefaultDatabaseField = tnQueryInfo.DatabaseForChildrenFieldName
			};

			return Mapper.FromDb(GetOrAdd(tnqSpec));
		}

		public TemplateQuery GetOrAddTemplateQuery(
			AuditSession          session,
			TemplateNodeInfo      tnInfo,
			TemplateNodeQueryInfo tnQueryInfo
		)
		{
			long templateNodeId = GetTemplateNodeId(
				session.Template,
				tnInfo
			);

			DBTemplateQuery tnqSpec = new DBTemplateQuery
			{
				TemplateNodeId = templateNodeId,
				UId            = tnQueryInfo.Id,
				Name           = tnQueryInfo.QueryName,
				Hierarchy      = tnQueryInfo.Hierarchy
			};

			return Mapper.FromDb(GetOrAdd(tnqSpec));
		}

		public TemplateQueryGroup GetOrAddTemplateQueryGroup(
			AuditSession          session,
			TemplateNodeInfo      tnInfo,
			TemplateNodeQueryInfo tnQueryInfo
		)
		{
			long templateNodeId = GetTemplateNodeId(
				session.Template,
				tnInfo
			);

			DBTemplateQueryGroup tnqgSpec = new DBTemplateQueryGroup
			{
				TemplateNodeParentId = templateNodeId,
				GroupId              = tnQueryInfo.Id,
				Name                 = tnQueryInfo.QueryName,
				DefaultDatabaseField = tnQueryInfo.DatabaseForChildrenFieldName
			};

			return Mapper.FromDb(GetOrAdd(tnqgSpec));
		}

		public TemplateQueryParameter GetOrAddTemplateQueryParameter(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			string                pName
		)
		{
			TemplateNodeInfo tnInfo = tnQueryInfo.TemplateNode;
			if (tnInfo.IsInstance)
			{
				tnInfo = tnInfo.Template;
			}

			TemplateQuery tQuery = GetOrAddTemplateQuery(
				session,
				tnInfo,
				tnQueryInfo
			);

			DBTemplateQueryParameter paramSpec = new DBTemplateQueryParameter
			{
				TemplateQueryId = tQuery.Id,
				Name            = pName
			};

			return Mapper.FromDb(GetOrAdd(paramSpec));
		}

		public TemplateQueryGroupParameter GetOrAddTemplateQueryGroupParameter(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			string                pName
		)
		{
			TemplateNodeInfo tnInfo = tnQueryInfo.TemplateNode.IsInstance
				? tnQueryInfo.TemplateNode.Template
				: tnQueryInfo.TemplateNode;

			TemplateQueryGroup tQueryGroup = GetOrAddTemplateQueryGroup(
				session,
				tnInfo,
				tnQueryInfo
			);

			DBTemplateQueryGroupParameter paramSpec = new DBTemplateQueryGroupParameter
			{
				TemplateQueryGroupId = tQueryGroup.Id,
				Name                 = pName
			};

			return Mapper.FromDb(GetOrAdd(paramSpec));
		}

		public TemplateNode GetTemplateNode(long id)
		{
			DBTemplateNode dbTNode = Get<DBTemplateNode>(id);

			return Mapper.FromDb(dbTNode);
		}

		public TemplateQuery GetTemplateQuery(long tQueryId)
		{
			DBTemplateQuery dbTQuery = Get<DBTemplateQuery>(tQueryId);

			return Mapper.FromDb(dbTQuery);
		}

		public void SaveTemplate(Template template)
		{
			DBTemplate dbTemplate = GetOrAdd(Mapper.ToDb(template));

			template.Id = dbTemplate.Id;
		}
	}
}
