﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Services.Base;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.Core.Persistence.Services
{
	public class NodeService : CachingService
	{
		private readonly TemplateService _templateService;
		private readonly SettingsService _settingsService;

		public NodeService(
			IDbContextScopeFactory<StorageContext>  scopeFactory,
			ConcurrentDictionary<Type, EntityCache> cache,
			RepositoryCatalog                       repositories,
			Mapper                                  mapper,
			TemplateService                         templateService,
			SettingsService                         settingsService,
			bool                                    isReadOnly = false)
			: base(scopeFactory, cache, repositories, mapper, isReadOnly)
		{
			this._templateService = templateService;
			this._settingsService = settingsService;
		}

		public void SaveNode(AuditContext context, TemplateNodeInfo tnInfo)
		{
			SaveNode(
				context.Template,
				context.ConnectionGroup,
				tnInfo
			);
		}

		public void SaveNode(
			Template         template,
			ConnectionGroup  group,
			TemplateNodeInfo tnInfo
		)
		{
			DBNodeInstance node = GetNodeInstance(template, group, tnInfo);

			if (!tnInfo.IsSaved)
			{
				node = InsertOrUpdate(node);
			}

			tnInfo.NodeInstanceId = node.Id;
			tnInfo.Counter        = node.Counter;

			List<DBNodeAttribute> attributes = GetNodeAttributes(tnInfo);

			foreach (DBNodeAttribute attr in attributes)
			{
				attr.NodeInstanceId = tnInfo.NodeInstanceId;

				Save(attr);
			}
		}

		public void SaveNodeWithParams(AuditContext context, TemplateNodeInfo tnInfo, string uiLang)
		{
			this._settingsService.SaveSettings(
				tnInfo,
				uiLang
			);

			SaveNode(context, tnInfo);

			SaveQueryParameters(context, tnInfo);
		}

		public void SaveNodes(AuditContext context, List<TemplateNodeInfo> nodes)
		{
			foreach (TemplateNodeInfo nodeInfo in nodes)
			{
				SaveNode(
					context.Template,
					context.ConnectionGroup,
					nodeInfo
				);
			}
		}

		public void SaveQueryParameters(AuditContext context, TemplateNodeInfo tnInfo)
		{
			UpdateQueryParameters(context, tnInfo);
			UpdateQueryGroupParameters(context, tnInfo);
		}

		public void LoadQueryParameters(AuditContext context, TemplateNodeInfo tnInfo)
		{
			ReadQueryParameters(context, tnInfo);
			ReadQueryGroupParameters(context, tnInfo);
		}

		public void LoadChildren(TemplateNodeInfo tnInfo)
		{
			List<DBNodeInstance> childNodes = GetAllOrdered<DBNodeInstance>(
					instance  => instance.IsEnabled == true && instance.ParentId == tnInfo.NodeInstanceId,
					instances => instances.OrderBy(i => i.SequenceNumber),
					instance  => instance.TemplateNode.TemplateQueryGroupParent)
				.ToList();

			tnInfo.Children.Clear();
			foreach (DBNodeInstance childNode in childNodes)
			{
				string templateUserId = childNode.TemplateNode.UId;
				TemplateNodeInfo template = tnInfo.Template
					.Children
					.FirstOrDefault(n => n.Id == templateUserId.ToString());

				if (template == null)
				{
					DisableNode(childNode.Id);
					continue;
				}

				DBTemplateQueryGroup qgParent = childNode
					.TemplateNode
					.TemplateQueryGroupParent;

				string databaseField = string.Empty;

				if (qgParent != null)
				{
					databaseField = qgParent.DefaultDatabaseField;
				}

				TemplateNodeInfo childInfo = template.Instantiate(databaseField, tnInfo);
				
				childInfo.NodeInstanceId = childNode.Id;
				
				childInfo.Counter          = childNode.Counter;
				childInfo.DefaultUName     = childNode.Name;
				childInfo.DefaultUId       = childNode.UId;
				childInfo.DefaultFontColor = childNode.FontColor;
				childInfo.DefaultFontStyle = childNode.FontStyle;
				childInfo.IsDisabled       = !childNode.IsEnabled ?? false;

				childInfo.LoadQueryParametersFromAttributes();

				tnInfo.Children.Add(childInfo);
			}
		}

		public void LoadNodeSettings(
			AuditContext     context,
			TemplateNodeInfo tnInfo,
			string           lang)
		{
			LoadAttributesValues(tnInfo);

			this._settingsService.LoadUserSettings(tnInfo, lang);

			LoadQueryParameters(context, tnInfo);
		}

		public bool? AreChildrenNotProcessed(long nodeId)
		{
			DBNodeInstance node = Get<DBNodeInstance>(nodeId);
			return node?.ChildrenNotProcessed;
		}

		public void SaveNodeCounter(TemplateNodeInfo tnInfo)
		{
			if (!tnInfo.IsSaved)
			{
				return;
			}

			SaveNodeCounter(tnInfo.NodeInstanceId, tnInfo.Counter);
		}

		public void SaveNodeCounter(long nodeId, long? counter)
		{
			DBNodeInstance dbNode = new DBNodeInstance
			{
				Id      = nodeId,
				Counter = counter
			};

			UpdateProperties(dbNode, ni => ni.Counter);
		}

		public long? GetNodeCounter(TemplateNodeInfo tnInfo)
		{
			if (!tnInfo.IsSaved)
			{
				return null;
			}

			DBNodeInstance dbNode = Get<DBNodeInstance>(tnInfo.NodeInstanceId);

			return dbNode?.Counter;
		}

		public void SetChildrenNotProcessed(
			AuditContext     context,
			TemplateNodeInfo tnInfo,
			bool             value)
		{
			if (!tnInfo.IsSaved)
			{
				SaveNode(
					context.Template,
					context.ConnectionGroup,
					tnInfo
				);
			}

			SetChildrenNotProcessed(tnInfo.NodeInstanceId, value);
		}

		public void SetChildrenNotProcessed(long nodeId, bool value)
		{
			DBNodeInstance nodeInstance = new DBNodeInstance
			{
				Id                   = nodeId,
				ChildrenNotProcessed = value
			};

			UpdateProperties(nodeInstance, ni => ni.ChildrenNotProcessed);
		}

		private void LoadAttributesValues(TemplateNodeInfo tnInfo)
		{
			List<DBNodeAttribute> attrs = GetAll<DBNodeAttribute>(
				attr => attr.NodeInstanceId == tnInfo.NodeInstanceId
			);

			foreach (DBNodeAttribute attr in attrs)
			{
				tnInfo.Attributes.Values[attr.Name] = attr.Value;
			}

			tnInfo.LoadQueryParametersFromAttributes();
		}

		private List<DBNodeAttribute> GetNodeAttributes(TemplateNodeInfo nodeInfo)
		{
			List<DBNodeAttribute> attributes = new List<DBNodeAttribute>();

			foreach (KeyValuePair<string, string> pair in nodeInfo.Attributes.Values)
			{
				DBNodeAttribute attribute = new DBNodeAttribute
				{
					Name  = pair.Key,
					Value = pair.Value
				};

				attributes.Add(attribute);
			}

			return attributes;
		}

		private void DisableNode(long nodeId)
		{
			DBNodeInstance nodeInstance = new DBNodeInstance
			{
				Id        = nodeId,
				IsEnabled = false
			};

			UpdateProperties(nodeInstance, ni => ni.IsEnabled);
		}

		public void DisableMissingChildren(long nodeId, List<TemplateNodeInfo> currentChildren)
		{
			List<DBNodeInstance> childNodes = GetAll<DBNodeInstance>(ni =>
				ni.ParentId  == nodeId &&
				ni.IsEnabled == true,

				ni => ni.TemplateNode
			);

			foreach (DBNodeInstance dbChild in childNodes)
			{
				TemplateNodeInfo foundChild = currentChildren.FirstOrDefault(
					child => child.Id.Equals(dbChild.TemplateNode.UId, StringComparison.OrdinalIgnoreCase)
				);

				if (foundChild == null)
				{
					dbChild.IsEnabled = false;

					UpdateProperties(dbChild, ni => ni.IsEnabled);
				}
			}
		}

		public void DisableChildren(long nodeId)
		{
			List<DBNodeInstance> childNodes = GetAll<DBNodeInstance>(ni =>
				ni.ParentId  == nodeId &&
				ni.IsEnabled == true
			);

			foreach (DBNodeInstance nodeInstance in childNodes)
			{
				nodeInstance.IsEnabled = false;
			}

			UpdateProperties(childNodes, ni => ni.IsEnabled);
		}

		private DBNodeInstance GetNodeInstance(
			Template         template,
			ConnectionGroup  group,
			TemplateNodeInfo tnInfo)
		{
			long? parentId = null;

			if (tnInfo.Parent != null)
			{
				if (!tnInfo.Parent.IsSaved)
				{
					throw new InvalidOperationException(tnInfo + ": it's parent is not saved (has no id)");
				}

				parentId = tnInfo.Parent.NodeInstanceId;
			}

			TemplateNode templateNode = this._templateService
				.GetOrAddTemplateNode(template, tnInfo.Template);

			DBNodeInstance node = new DBNodeInstance
			{
				ConnectionGroupId = group.Id,
				TemplateNodeId    = templateNode.Id,
				ParentId          = parentId,
				Name              = tnInfo.DefaultUName,
				SequenceNumber    = tnInfo.Parent?.Children.IndexOf(tnInfo) ?? 0
			};

			DBNodeInstance savedNode = Get(node);

			if (savedNode != null)
			{
				node = savedNode;
			}
			else
			{
				node.UId       = tnInfo.DefaultUId;
				node.Icon      = tnInfo.DefaultIcon;
				node.FontColor = tnInfo.DefaultFontColor;
				node.FontStyle = tnInfo.DefaultFontStyle;
			}

			node.IsEnabled = !tnInfo.IsDisabled;

			return node;
		}

		public NodeInstance GetNode(long nodeId)
		{
			DBNodeInstance dbNode = Get<DBNodeInstance>(nodeId);

			return Mapper.FromDb(dbNode);
		}

		public UpdateTiming GetUpdateTiming(TemplateNodeInfo tnInfo)
		{
			Debug.Assert(tnInfo.IsInstance);

			if (tnInfo.IsSaved)
			{
				DBNodeInstance dbNode = Get<DBNodeInstance>(
					tnInfo.NodeInstanceId
				);

				if (dbNode != null)
				{
					TimeSpan? duration = null;
					long?     millis   = dbNode.LastUpdateDuration;

					if (millis != null)
					{
						duration = TimeSpan.FromMilliseconds(millis.Value);
					}

					return new UpdateTiming(dbNode.LastUpdate, duration);
				}
			}

			return new UpdateTiming();
		}

		public void SaveUpdateTimings(
			TemplateNodeInfo tnInfo,
			DateTime?        updateDate,
			long?            updateDuration
		)
		{
			Debug.Assert(tnInfo.IsInstance);

			if (tnInfo.IsSaved)
			{
				DBNodeInstance dbNode = Get<DBNodeInstance>(
					tnInfo.NodeInstanceId
				);

				if (dbNode != null)
				{
					dbNode.LastUpdate         = updateDate;
					dbNode.LastUpdateDuration = updateDuration;

					UpdateProperties(
						dbNode, 

						n => n.LastUpdate,
						n => n.LastUpdateDuration
					);
				}
			}
		}

		public DBQuery GetQuery(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			DateTime              dateCreated,
			bool                  onlyFind
		)
		{
			TemplateNodeInfo tnInfo = tnQueryInfo.TemplateNode;

			TemplateQuery tnQuery = this._templateService.GetOrAddTemplateQuery(
				session,
				tnInfo.Template,
				tnQueryInfo
			);

			DBQuery qSpec = new DBQuery
			{
				DefaultDatabaseName = tnInfo.GetDefaultDatabase(),
				DateCreated         = dateCreated,
				NodeInstanceId      = tnInfo.NodeInstanceId,
				TemplateQueryId     = tnQuery.Id,
				ServerInstanceId    = session.Server.Id
			};

			if (onlyFind)
			{
				return Get(qSpec);
			}

			return GetOrAdd(qSpec);
		}

		public DBQueryGroup GetQueryGroup(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			DateTime              dateCreated,
			bool                  onlyFind
		)
		{
			TemplateNodeInfo tnInfo = tnQueryInfo.TemplateNode.IsInstance
				? tnQueryInfo.TemplateNode.Template
				: tnQueryInfo.TemplateNode;

			TemplateQueryGroup tnQueryGroup = this._templateService.GetOrAddTemplateQueryGroup(
				session,
				tnInfo,
				tnQueryInfo
			);

			DBQueryGroup qSpec = new DBQueryGroup
			{
				DefaultDatabaseName  = tnInfo.GetDefaultDatabase(),
				DateCreated          = dateCreated,
				NodeInstanceId       = tnQueryInfo.TemplateNode.NodeInstanceId,
				TemplateQueryGroupId = tnQueryGroup.Id,
				ServerInstanceId     = session.Server.Id
			};

			if (onlyFind)
			{
				return Get(qSpec);
			}

			return GetOrAdd(qSpec);
		}

		private void UpdateQueryGroupParameters(AuditContext context, TemplateNodeInfo tnInfo)
		{
			foreach (TemplateNodeQueryInfo tnQueryInfo in tnInfo.GroupQueries)
			{
				foreach (AuditSession session in context.CreateSessions())
				{
					DBQueryGroup queryGroup = GetQueryGroup(
						session,
						tnQueryInfo,
						DateTime.Now,
						false
					);

					if (queryGroup != null)
					{
						UpdateQueryGroupParameters(
							session,
							queryGroup.Id,
							tnQueryInfo,
							tnQueryInfo.ParameterValues
						);
					}
				}
			}
		}

		private void UpdateQueryParameters(AuditContext context, TemplateNodeInfo tnInfo)
		{
			foreach (TemplateNodeQueryInfo tnQueryInfo in tnInfo.Queries)
			{
				foreach (AuditSession session in context.CreateSessions())
				{
					DBQuery query = GetQuery(
						session,
						tnQueryInfo,
						DateTime.Now,
						false
					);

					if (query != null)
					{
						UpdateQueryParameters(
							session,
							query.Id,
							tnQueryInfo,
							tnQueryInfo.ParameterValues
						);
					}
				}
			}
		}

		private void ReadQueryGroupParameters(AuditContext context, TemplateNodeInfo tnInfo)
		{
			foreach (TemplateNodeQueryInfo tnQueryInfo in tnInfo.GroupQueries)
			{	
				foreach (AuditSession session in context.CreateSessions())
				{
					DBQueryGroup queryGroup = GetQueryGroup(
						session,
						tnQueryInfo,
						DateTime.Now,
						false
					);

					if (queryGroup != null)
					{
						foreach (ParameterValue paramInfo in tnQueryInfo.ParameterValues)
						{
							ReadQueryGroupParameters(
								session,
								queryGroup.Id,
								tnQueryInfo,
								paramInfo
							);
						}
					}
				}
			}
		}

		private void ReadQueryParameters(AuditContext context, TemplateNodeInfo tnInfo)
		{
			foreach (TemplateNodeQueryInfo tnQueryInfo in tnInfo.Queries)
			{
				foreach (AuditSession session in context.CreateSessions())
				{
					DBQuery query = GetQuery(
						session,
						tnQueryInfo,
						DateTime.Now,
						false
					);

					if (query != null)
					{
						foreach (ParameterValue paramInfo in tnQueryInfo.ParameterValues)
						{
							ReadQueryParameters(
								session,
								query.Id,
								tnQueryInfo,
								paramInfo
							);
						}
					}
				}
			}
		}

		public void ReadQueryParameters(
			AuditSession          session,
			long                  queryId,
			TemplateNodeQueryInfo queryInfo,
			ParameterValue        pValue
		)
		{
			TemplateQueryParameter tParam = this._templateService
				.GetOrAddTemplateQueryParameter(
					session,
					queryInfo,
					pValue.Name
				);

			if (tParam != null)
			{
				DBQueryParameter qParam = Get<DBQueryParameter>(p => 
					p.QueryId                  == queryId &&
					p.TemplateQueryParameterId == tParam.Id
				);

				if (qParam != null)
				{
					pValue.UserValue = qParam.Value;
				}
			}
		}

		public void ReadQueryGroupParameters(
			AuditSession          session,
			long                  queryGroupId,
			TemplateNodeQueryInfo queryInfo,
			ParameterValue        pValue
		)
		{
			TemplateQueryGroupParameter tParam =
				this._templateService.GetOrAddTemplateQueryGroupParameter(
					session,
					queryInfo,
					pValue.Name
				);

			if (tParam != null)
			{
				DBQueryGroupParameter qParam = Get<DBQueryGroupParameter>(p => 
					p.QueryGroupId                  == queryGroupId &&
					p.TemplateQueryGroupParameterId == tParam.Id
				);

				if (qParam != null)
				{
					pValue.UserValue = qParam.Value;
				}
			}
		}

		public void UpdateQueryGroupParameters(
			AuditSession          session,
			long                  queryGroupId,
			TemplateNodeQueryInfo queryInfo,
			List<ParameterValue>  parameterValues
		)
		{
			List<DBQueryGroupParameter> existingParams = GetAll<DBQueryGroupParameter>(
				parameter => parameter.QueryGroupId == queryGroupId);

			Dictionary<long, DBQueryGroupParameter> paramMap = existingParams
				.Where(parameter => parameter.TemplateQueryGroupParameterId.HasValue)
				.ToDictionary(parameter => parameter.TemplateQueryGroupParameterId.Value);

			foreach (ParameterValue pValue in parameterValues)
			{
				TemplateQueryGroupParameter tParam = this._templateService
					.GetOrAddTemplateQueryGroupParameter(
						session,
						queryInfo,
						pValue.Name
					);

				DBQueryGroupParameter param = new DBQueryGroupParameter
				{
					IsEnabled    = true,
					QueryGroupId = queryGroupId
				};

				if (tParam != null)
				{
					DBQueryGroupParameter savedParam;

					if (paramMap.TryGetValue(tParam.Id, out savedParam))
					{
						param = savedParam;
						paramMap.Remove(tParam.Id);
					}
					else
					{
						param.TemplateQueryGroupParameterId = tParam.Id;
					}
				}

				param.Value = pValue.UserValue;

				Save(param);
			}

			foreach (KeyValuePair<long, DBQueryGroupParameter> absent in paramMap)
			{
				DBQueryGroupParameter param = absent.Value;

				if (param.IsEnabled == null || param.IsEnabled.Value)
				{
					param.IsEnabled = false;

					Save(param);
				}
			}
		}

		public void UpdateQueryParameters(
			AuditSession          session,
			long                  queryId,
			TemplateNodeQueryInfo queryInfo,
			List<ParameterValue>  parameterValues
		)
		{
			List<DBQueryParameter> existingParams = GetAll<DBQueryParameter>(
				parameter => parameter.QueryId == queryId);

			Dictionary<long, DBQueryParameter> paramMap = existingParams
				.Where(parameter => parameter.TemplateQueryParameterId.HasValue)
				.ToDictionary(parameter => parameter.TemplateQueryParameterId.Value);

			foreach (ParameterValue pValue in parameterValues)
			{
				TemplateQueryParameter tParam = this._templateService
					.GetOrAddTemplateQueryParameter(
						session,
						queryInfo,
						pValue.Name
					);

				DBQueryParameter param = new DBQueryParameter
				{
					IsEnabled = true,
					QueryId   = queryId
				};

				if (tParam != null)
				{
					DBQueryParameter savedParam;

					if (paramMap.TryGetValue(tParam.Id, out savedParam))
					{
						param = savedParam;
						paramMap.Remove(tParam.Id);
					}
					else
					{
						param.TemplateQueryParameterId = tParam.Id;
					}
				}

				param.Value = pValue.UserValue;

				Save(param);
			}

			foreach (KeyValuePair<long, DBQueryParameter> absent in paramMap)
			{
				DBQueryParameter param = absent.Value;

				if (param.IsEnabled)
				{
					param.IsEnabled = false;

					Save(param);
				}
			}
		}
	}
}
