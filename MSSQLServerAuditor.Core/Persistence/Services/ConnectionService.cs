﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Services.Base;
using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.Core.Persistence.Services
{
	public class ConnectionService : CachingService
	{
		public ConnectionService(
			IDbContextScopeFactory<StorageContext>  scopeFactory,
			ConcurrentDictionary<Type, EntityCache> cache,
			RepositoryCatalog                       repositories,
			Mapper                                  mapper,
			bool                                    isReadOnly = false) 
			: base(scopeFactory, cache, repositories, mapper, isReadOnly)
		{
		}

		/// <summary>
		/// Get connection group from the database by id and user
		/// </summary>
		/// <param name="groupId">The id of connection group</param>
		/// <param name="winUser">Windows user name</param>
		/// <returns>Connection group with eagerly loaded related connections</returns>
		public ConnectionGroup GetConnectionGroup(long groupId, string winUser)
		{
			ConnectionGroup group = GetConnectionGroup(groupId);
			if (string.IsNullOrWhiteSpace(winUser))
			{
				List<ServerInstance> filteredInstances = new List<ServerInstance>();

				// remove connections not available for winUser
				foreach (ServerInstance instance in group.ServerInstances)
				{
					if (instance.Login.IsWinAuth && !string.Equals(winUser, instance.Login.Name))
					{
						continue;
					}

					filteredInstances.Add(instance);
				}

				group.ServerInstances = filteredInstances;
			}

			return group;
		}

		/// <summary>
		/// Get connection group from the database by id
		/// </summary>
		/// <param name="groupId">The id of connection group</param>
		/// <returns>Connection group with eagerly loaded related connections</returns>
		public ConnectionGroup GetConnectionGroup(long groupId)
		{
			DBConnectionGroup dbGroup = Get<DBConnectionGroup>(
				groupId,

				g => g.ConnectionType,
				g => g.GroupServers,
				g => g.GroupServers.Select(gs => gs.ServerInstance.Login)
			);

			if (dbGroup == null)
			{
				return null;
			}

			return Mapper.FromDb(dbGroup);
		}

		/// <summary>
		/// Query connection group from the database
		/// </summary>
		/// <param name="groupName">The name of connection group</param>
		/// <param name="connectionType">Connection group type</param>
		/// <param name="isDirectConnection">Specifies whether the connection is direct</param>
		/// <returns>Connection group with eagerly loaded related connections</returns>
		public ConnectionGroup GetConnectionGroup(
			string         groupName,
			ConnectionType connectionType,
			bool           isDirectConnection
		)
		{
			DBConnectionGroup dbGroup = Get<DBConnectionGroup>(g => 
				g.Name                == groupName &&
				g.ConnectionType.Name == connectionType.ToString() &&
				g.IsDirectConnection  == isDirectConnection,
				
				g => g.ConnectionType,
				g => g.GroupServers,
				g => g.GroupServers.Select(gs => gs.ServerInstance.Login)
			);

			if (dbGroup == null)
			{
				return null;
			}

			return Mapper.FromDb(dbGroup);
		}

		/// <summary>
		/// Updates connection group servers
		/// </summary>
		/// <param name="groupId">Connection group Id</param>
		/// <param name="instances">Server instances to update</param>
		public void UpdateGroupServerInstances(long groupId, List<ServerInstance> instances)
		{
			DBConnectionGroup savedDbGroup = Get<DBConnectionGroup>(
				groupId,

				// eager load of related entities
				cg => cg.ConnectionType,
				cg => cg.GroupServers.Select(gs => gs.ServerInstance.Login)
			);

			if (savedDbGroup == null)
			{
				return;
			}

			List<DBGroupServer> storedGroupServers = savedDbGroup.GroupServers
				.Where(gs => gs.ServerInstance.IsDeleted == false && gs.ServerInstance.ConnectionTypeId == savedDbGroup.ConnectionTypeId)
				.ToList();

			foreach (DBGroupServer storedGroupServer in storedGroupServers)
			{
				DBServerInstance storedInstance = storedGroupServer.ServerInstance;

				bool needRemove = instances.All(instance => !storedInstance.ConnectionName.EqualsIgnoreCase(instance.ConnectionName));

				if (needRemove)
				{
					Delete(storedGroupServer);
				}
			}

			foreach (ServerInstance instance in instances)
			{
				bool needAdd = true;
				foreach (DBGroupServer storedGroupServer in storedGroupServers)
				{
					DBServerInstance storedInstance = storedGroupServer.ServerInstance;
					if (instance.ConnectionName.EqualsIgnoreCase(storedInstance.ConnectionName))
					{
						needAdd = false;
						break;
					}
				}

				if (needAdd)
				{
					DBServerInstance server = GetOrAddServerInstance(instance);

					GetOrAdd(new DBGroupServer
					{
						ServerInstanceId  = server.Id,
						ConnectionGroupId = groupId
					});
				}
			}
		}

		/// <summary>
		/// Get connection groups for a specific connection type
		/// </summary>
		/// <param name="connectionType">Connection type</param>
		/// <returns>Connection groups with eagerly loaded related connections</returns>
		public List<ConnectionGroup> GetConnectionGroups(ConnectionType connectionType)
		{
			List<DBConnectionGroup> dbGroups = GetAll<DBConnectionGroup>(
				cg => cg.IsDirectConnection == true && cg.ConnectionType.Name == connectionType.ToString(),

				// eager load related entities
				cg => cg.ConnectionType,
				cg => cg.GroupServers.Select(gs => gs.ServerInstance.Login)
			);

			return dbGroups.Select(Mapper.FromDb).ToList();
		}

		/// <summary>
		/// Get server instances for a specific connection type
		/// </summary>
		/// <param name="connectionType">Connection type</param>
		/// <returns>Server instances with eagerly loaded related entities</returns>
		public List<ServerInstance> GetServerInstances(ConnectionType connectionType)
		{
			List<DBServerInstance> dbInstances = GetAll<DBServerInstance>(si =>
				si.IsDynamic           == false && 
				si.IsDeleted           == false &&
				si.ConnectionType.Name == connectionType.ToString(),

				// eager load related entities
				si => si.ConnectionType,
				si => si.Login,
				si => si.GroupServers.Select(gs => gs.ConnectionGroup)
			);

			return dbInstances.Select(Mapper.FromDb).ToList();
		}

		/// <summary>
		/// Get server instances with a specific alias name
		/// </summary>
		/// <param name="connectionAlias">Alias</param>
		/// <returns>Server instances with eagerly loaded related entities</returns>
		public List<ServerInstance> GetServerInstancesByAlias(string connectionAlias)
		{
			List<DBServerInstance> dbInstances = GetAll<DBServerInstance>(si =>
					si.IsDynamic       == false &&
					si.IsDeleted       == false &&
					si.ConnectionAlias == connectionAlias,

				// eager load related entities
				si => si.ConnectionType,
				si => si.Login
			);

			return dbInstances.Select(Mapper.FromDb).ToList();
		}

		/// <summary>
		/// Get logins for a specific connection and authentication types
		/// </summary>
		/// <param name="connectionType">Connection type</param>
		/// <param name="isWinAuth">Authentication type</param>
		/// <returns>Logins for a given connection and authentication types</returns>
		public List<Login> GetLogins(ConnectionType connectionType, bool isWinAuth)
		{
			string cnnType = connectionType.ToString();
			return FilterAll<DBQuery, DBLogin>(allQueries => (
				from
					q in allQueries
				where
					q.ServerInstance.ConnectionType.Name == cnnType &&
					q.ServerInstance.Login.IsWinAuth     == isWinAuth
				select
					q.ServerInstance.Login
				).Distinct()
			)
			.Select(Mapper.FromDb)
			.ToList();
		}

		/// <summary>
		/// Save server instance to database
		/// </summary>
		/// <param name="instance">Server instance to save</param>
		public void SaveServerInstance(ServerInstance instance)
		{
			DBLogin          dbLogin = GetOrAddLogin(instance.Login);
			DBConnectionType dbType  = GetOrAddConnectionType(instance.ConnectionType);

			DBServerInstance dbInstance = Mapper.ToDb(instance);
			dbInstance.LoginId          = dbLogin.Id;
			dbInstance.ConnectionTypeId = dbType.Id;

			DBServerInstance savedInstance = InsertOrUpdate(dbInstance);

			// update identifiers
			instance.Id       = savedInstance.Id;
			instance.Login.Id = dbLogin.Id;
		}

		/// <summary>
		/// Saves connection group and updates the list of related server instances
		/// </summary>
		/// <param name="group">Connection group to save</param>
		public void SaveConnectionGroup(ConnectionGroup group)
		{
			DBConnectionType  dbType  = GetOrAddConnectionType(group.ConnectionType);
			DBConnectionGroup dbGroup = Mapper.ToDb(group);

			dbGroup.ConnectionTypeId = dbType.Id;

			DBConnectionGroup savedDbGroup = GetOrAdd(dbGroup);

			group.Id = savedDbGroup.Id;

			UpdateGroupServerInstances(group.Id, group.ServerInstances);
		}

		/// <summary>
		/// Disable server instance
		/// </summary>
		/// <param name="instance">Server instance to disable</param>
		public void DisableServerInstance(ServerInstance instance)
		{
			if (instance != null)
			{
				if (instance.IsNew)
				{
					SaveServerInstance(instance);
				}

				DBServerInstance dbInstance = Mapper.ToDb(instance);
				dbInstance.IsDeleted = true;

				UpdateProperties(dbInstance, s => s.IsDeleted);

				// save succeeded. Update the flag
				instance.IsDeleted = true;
			}
		}

		public void UpdateGroupConnections(ConnectionGroup group)
		{
			DBConnectionGroup dbSavedGroup = Get<DBConnectionGroup>(
				cg => cg.Name == group.Name && cg.ConnectionType.Name == group.ConnectionType.ToString(),

				g => g.ConnectionType,
				g => g.GroupServers,
				g => g.GroupServers.Select(gs => gs.ServerInstance.Login)
			);

			if (dbSavedGroup == null)
			{
				return;
			}

			List<ServerInstance> newInstances = group.ServerInstances;

			List<DBServerInstance> dbInstances = dbSavedGroup.GroupServers
				.Select(gs => gs.ServerInstance)
				.Where(si => !si.IsDeleted)
				.ToList();

			List<ServerInstance> instancesToAdd = new List<ServerInstance>(newInstances);

			foreach (DBServerInstance dbInstance in dbInstances)
			{
				bool exist = newInstances.Any(instance =>
					string.Equals(instance.ConnectionName, dbInstance.ConnectionName, StringComparison.OrdinalIgnoreCase)
				);

				if (!exist)
				{
					DBGroupServer gsSpec = new DBGroupServer
					{
						ConnectionGroupId = dbSavedGroup.Id,
						ServerInstanceId  = dbInstance.Id
					};

					DBGroupServer groupServer = Get(gsSpec);

					if (groupServer != null)
					{
						Delete(groupServer);
					}
				}
				else
				{
					instancesToAdd.RemoveAll(instance =>
						string.Equals(instance.ConnectionName, dbInstance.ConnectionName, StringComparison.OrdinalIgnoreCase)
					);
				}
			}

			foreach (ServerInstance newInstance in instancesToAdd)
			{
				DBServerInstance server = GetOrAddServerInstance(newInstance);

				GetOrAdd(new DBGroupServer
				{
					ServerInstanceId  = server.Id,
					ConnectionGroupId = dbSavedGroup.Id
				});
			}
		}

		public void DeleteDynamicConnections(long queryParentId)
		{
			Delete<DBDynamicConnection>(dc =>
				dc.QueryParentId == queryParentId
			);
		}

		public void UpdateDynamicConnections(
			long                      parentQueryId,
			List<DBDynamicConnection> dynamicConnections
		)
		{
			foreach (DBDynamicConnection dynamicConnection in dynamicConnections)
			{
				dynamicConnection.QueryParentId = parentQueryId;
				UpdateDynamicConnection(dynamicConnection);
			}
		}

		public void UpdateDynamicConnection(DBDynamicConnection dConnection)
		{
			Save(dConnection);
		}

		public LastConnection GetLastConnection(
			string connectionType,
			string machineName
		)
		{
			DBLastConnection dbLastConnection = Get<DBLastConnection>(lc =>
				lc.ConnectionType.Name == connectionType &&
				lc.MachineName         == machineName,

				lc => lc.Template,
				lc => lc.ConnectionType,
				lc => lc.ConnectionGroup
			);

			return Mapper.FromDb(dbLastConnection);
		}

		public List<LastConnection> GetLastConnections(string machineName)
		{
			List<DBLastConnection> dbLastConnections = GetAllOrdered<DBLastConnection>(lc =>
				lc.MachineName == machineName,

				lcs => lcs.OrderByDescending(lc => lc.DateUpdated),

				lc => lc.Template,
				lc => lc.ConnectionType,
				lc => lc.ConnectionGroup
			);

			return dbLastConnections
				.Select(Mapper.FromDb)
				.ToList();
		}

		public void UpdateLastConnection(
			long   groupId,
			long   templateId,
			string connectionType,
			string moduleType,
			string machineName
		)
		{
			DBConnectionType typeSpec = new DBConnectionType
			{
				Name = connectionType
			};

			long connectionTypeId = GetOrAdd(typeSpec).Id;

			DBLastConnection lastConnection = new DBLastConnection
			{
				ConnectionGroupId = groupId,
				TemplateId        = templateId,
				MachineName       = machineName,
				ConnectionTypeId  = connectionTypeId,
				ModuleType        = moduleType
			};

			InsertOrUpdate(lastConnection);
		}

		public List<DynamicConnectionInfo> GetDynamicConnections(long queryParentId)
		{
			List<DBDynamicConnection> dbDynamicConnections = GetAll<DBDynamicConnection>(
				dc => dc.QueryParentId == queryParentId,
				dc => dc.ServerInstance,
				dc => dc.ServerInstance.ConnectionType,
				dc => dc.ServerInstance.Login
			);

			return dbDynamicConnections
				.Select(Mapper.FromDb)
				.ToList();
		}

		public void DeleteConnectionFromGroups(long serverInstanceId)
		{
			Delete<DBGroupServer>(
				gs => gs.ServerInstance.Id == serverInstanceId
			);
		}

		private DBServerInstance GetOrAddServerInstance(ServerInstance instance)
		{
			DBLogin          dbLogin = GetOrAddLogin(instance.Login);
			DBConnectionType dbType  = GetOrAddConnectionType(instance.ConnectionType);

			DBServerInstance dbInstance = Mapper.ToDb(instance);
			dbInstance.LoginId          = dbLogin.Id;
			dbInstance.ConnectionTypeId = dbType.Id;
			
			return GetOrAdd(dbInstance);
		}

		private DBConnectionType GetOrAddConnectionType(ConnectionType connectionType)
		{
			DBConnectionType dbConnectionType = Mapper.ToDb(connectionType);
			return GetOrAdd(dbConnectionType);
		}

		private DBLogin GetOrAddLogin(Login login)
		{
			DBLogin dbLogin = Mapper.ToDb(login);
			return GetOrAdd(dbLogin);
		}
	}
}
