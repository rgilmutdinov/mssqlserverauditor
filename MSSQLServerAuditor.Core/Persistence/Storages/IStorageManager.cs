﻿using System.Collections.Generic;
using MSSQLServerAuditor.Core.Querying;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Connections;

namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	/// <summary>
	/// Storage manager interface
	/// </summary>
	public interface IStorageManager
	{
		/// <summary>
		/// Collection (list) of historical databases
		/// </summary>
		List<HistoryStorage> HistoryStorages { get; }

		/// <summary>
		/// Main (current) storage
		/// </summary>
		MainStorage MainStorage { get; }

		/// <summary>
		/// Reports storage
		/// </summary>
		IReportStorage ReportStorage { get; }

		/// <summary>
		/// DB file system for temporary data
		/// </summary>
		FsStorage FsStorage { get; }

		/// <summary>
		/// Factory to create connections for system databases
		/// </summary>
		IStorageConnectionFactory StorageConnectionFactory { get; }

		StorageConnectionInfo ConnectionInfo { get; }

		ICommandExecutor CommandExecutor { get; }

		QueryFormatter QueryFormatter { get; }
	}
}
