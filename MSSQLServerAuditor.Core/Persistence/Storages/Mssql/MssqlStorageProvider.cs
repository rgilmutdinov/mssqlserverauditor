﻿using System.Collections.Generic;
using System.Data.Entity;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Cryptography;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.Core.Persistence.Providers;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Vendors.Mssql;
using MSSQLServerAuditor.DataAccess.Vendors.Mssql.Configuration;
using MSSQLServerAuditor.DataAccess.Vendors.Mssql.Connections;
using NLog;
using Perceiveit.Data;

namespace MSSQLServerAuditor.Core.Persistence.Storages.Mssql
{
	public class MssqlStorageProvider : StorageProviderBase
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public MssqlStorageProvider(StorageConnectionInfo connectionInfo, XmlLoader xmlLoader) 
			: base(connectionInfo, new MssqlProviderFactory(), xmlLoader)
		{
		}

		public override MainStorage CreateCurrentStorage()
		{
			string pwd = $"@{CurrentAssembly.ProductName}@"; // use project name for password base
			ICryptoService cryptoService = new CachingCryptoService(pwd);

			Mapper mapper = new Mapper(cryptoService);

			string storageName      = GetDbName(this.ConnectionInfo.CurrentDatabaseInfo);
			string connectionString = GetDbConnectionString(storageName);

			List<string> currentStorageScripts = XmlLoader.GetScripts(
				ConnectionInfo.CurrentDatabaseInfo.ScriptPath,
				ConnectionInfo.StorageType
			);

			StorageContextFactory mssqlContextFactory = new StorageContextFactory(
				this.ConnectionFactory,
				new MssqlStorageInitializerFactory(currentStorageScripts),
				connectionString
			);

			DbConfiguration.SetConfiguration(new MssqlDbConfiguration());

			return new MainStorage(
				mssqlContextFactory,
				mapper,
				storageName
			);
		}

		public override IReportStorage CreateReportStorage()
		{
			string           dbName           = GetDbName(this.ConnectionInfo.ReportDatabaseInfo);
			string           connectionString = GetDbConnectionString(dbName);
			DBDatabase       reportStorage    = this.ProviderFactory.CreateDatabase(connectionString);
			ICommandExecutor cmdExecutor      = this.ProviderFactory.CreateCommandExecutor();

			return new CommonReportStorage(reportStorage, cmdExecutor, dbName);
		}

		public override List<HistoryStorage> CreateExtraStorages()
		{
			List<HistoryStorage> historyStorages = new List<HistoryStorage>();
			List<DatabaseInfo>   dbInfoList      = ConnectionInfo.Databases;

			if (dbInfoList.IsNullOrEmpty())
			{
				Log.Error("No history storages to be created.");

				return historyStorages;
			}

			foreach (DatabaseInfo dbInfo in dbInfoList)
			{
				if (string.IsNullOrEmpty(dbInfo.Name) || string.IsNullOrEmpty(dbInfo.Alias))
				{
					Log.Error("History storage filename or alias is not defined.");

					continue;
				}

				string           dbName           = GetDbName(dbInfo);
				string           connectionString = GetDbConnectionString(dbName);
				DBDatabase       historyBase      = this.ProviderFactory.CreateDatabase(connectionString);
				ICommandExecutor cmdExecutor      = this.ProviderFactory.CreateCommandExecutor();

				HistoryStorage histStorage = new HistoryStorage(
					historyBase,
					cmdExecutor,
					dbName,
					dbInfo.Alias
				);

				if (this.ConnectionInfo.UpdateHistory)
				{
					RunScripts(histStorage, dbInfo.ScriptPath);
				}

				historyStorages.Add(histStorage);
			}

			return historyStorages;
		}

		public override FsStorage CreateFsStorage()
		{
			string           dbName           = GetDbName(this.ConnectionInfo.FsDatabaseInfo);
			string           connectionString = GetDbConnectionString(dbName);
			DBDatabase       fsBase           = this.ProviderFactory.CreateDatabase(connectionString);
			ICommandExecutor cmdExecutor      = this.ProviderFactory.CreateCommandExecutor();

			return new FsStorage(fsBase, cmdExecutor, dbName);
		}

		public override IStorageConnectionFactory CreateStorageConnectionFactory()
		{
			string dbName = GetDbName(ConnectionInfo.CurrentDatabaseInfo);

			return new MssqlStorageConnectionFactory(GetDbConnectionString(dbName));
		}

		private string GetDbConnectionString(string dbName)
		{
			return string.Format(
				this.ConnectionInfo.ConnectionStringPattern,
				dbName
			);
		}

		private string GetDbName(DatabaseInfo database)
		{
			return database.Name.Replace("${version}$", $"{CurrentAssembly.Version}");
		}
	}
}
