﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Vendors.Sqlite;
using MSSQLServerAuditor.DataAccess.Vendors.Sqlite.Connections;
using NLog;
using Perceiveit.Data;

namespace MSSQLServerAuditor.Core.Persistence.Storages.Sqlite.Storages
{
	public class SqliteReportStorage : CommonReportStorage
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public static SqliteReportStorage CreateFileStorage(string connectionString, string dbName)
		{
			SqliteProviderFactory factory         = new SqliteProviderFactory();
			DBDatabase            database        = factory.CreateDatabase(connectionString);
			ICommandExecutor      commandExecutor = factory.CreateCommandExecutor();

			return new SqliteReportStorage(
				factory,
				database,
				commandExecutor,
				dbName
			);
		}

		private volatile SqliteMemoryReportStorage _memoryStorage;
		private readonly object                    _memoryStorageLock = new object();

		protected SqliteReportStorage(
			SqliteProviderFactory factory,
			DBDatabase            dbDatabase,
			ICommandExecutor      commandExecutor,
			string                dbName
		) : base(
			dbDatabase,
			commandExecutor,
			dbName
		)
		{
			Dictionary<string, string> dbToAttach = new Dictionary<string, string>
			{
				{"file:memdb1?mode=memory&cache=shared", "temp_report"}
			};

			this.StorageConnectionFactory = new SqliteStorageConnectionFactory(
				DbDatabase.ConnectionString,
				dbToAttach
			);

			this.ProviderFactory = factory;
		}

		public SqliteProviderFactory        ProviderFactory        { get; set; }
		public SqliteStorageConnectionFactory StorageConnectionFactory { get; set; }

		protected override long? SaveQueryData(
			long          queryId,
			NormalizeInfo dbStructure,
			DataTable     queryData,
			long          sessionId,
			bool          recreateSchema
		)
		{
			long  rowsInserted        = 0L;
			long  rowsUpdated         = 0L;
			long  rowsDeleted         = 0L;
			long  rowsUpdatedRowOrder = 0L;
			long  rowsTotal           = 0L;
			long? rowsInMemory        = 0L;

			this.PrepareTables(dbStructure, recreateSchema);

			DataTable currentData = ReadResult(dbStructure, queryId, true);

			this.MemoryStorage.ClearData(queryId, dbStructure, sessionId);

			rowsInMemory = this.MemoryStorage.SaveMemoryQueryData(
				queryId,
				dbStructure,
				queryData,
				sessionId,
				currentData,
				recreateSchema
			);
			
			string rowHashF         = FieldRowHash.Name.AsDbName();
			string rowHashOldF      = (FieldRowHash.Name + "Old").AsDbName();
			string queryIdF         = FieldQueryId.Name.AsDbName();
			string sessionIdF       = FieldSessionId.Name.AsDbName();
			string masterFieldsList = GetFieldsString(dbStructure, string.Empty);
			string memoryFieldsList = this.MemoryStorage.GetFieldsString(dbStructure, "tR");

			memoryFieldsList = memoryFieldsList.Replace(",tR." + rowHashOldF, string.Empty);

			string insertMemoryFieldsList = memoryFieldsList;

			// restore key fields
			memoryFieldsList = memoryFieldsList.Replace(",tR." + rowHashF,   ",dest." + rowHashF);
			memoryFieldsList = memoryFieldsList.Replace(",tR." + queryIdF,   ",dest." + queryIdF);
			memoryFieldsList = memoryFieldsList.Replace(",tR." + sessionIdF, ",dest." + sessionIdF);

			string deleteQuery = string.Format(
				  " DELETE FROM {0}"
				+ " WHERE"
				+ "    {1} NOT IN ("
				+ "       SELECT"
				+ "          tR.{2}"
				+ "       FROM"
				+ "          [temp_report].{0} tR"
				+ "       WHERE"
				+ "          tR.{3} = {4}"
				+ "          AND tR.{5} = {6}"
				+ "    )"
				+ "    AND {3} = {4}"
				+ "    AND {5} = {6}"
				+ ";",
				dbStructure.TableName.AsDbName(),
				rowHashF,
				rowHashOldF,
				queryIdF,
				queryId,
				sessionIdF,
				sessionId
			);

			string updateQuery = string.Format(
				  " REPLACE INTO {0}"
				+ " ("
				+ "    {1}"
				+ " )"
				+ " SELECT"
				+ "    {2}"
				+ " FROM"
				+ "    [temp_report].{0} tR"
				+ "    INNER JOIN {0} dest ON"
				+ "       tR.{3} = dest.{4}"
				+ "       AND tR.{4} != dest.{4}"
				+ " WHERE"
				+ "    tR.{5} = {6}"
				+ "    AND tR.{7} = {8}"
				+ " ;",
				dbStructure.TableName.AsDbName(),
				masterFieldsList,
				memoryFieldsList,
				rowHashOldF,
				rowHashF,
				queryIdF,
				queryId,
				sessionIdF,
				sessionId
			);

			string insertQuery = string.Format(
				  " INSERT INTO {0}"
				+ " ("
				+ "    {1}"
				+ " )"
				+ " SELECT"
				+ "    {2}"
				+ " FROM"
				+ "    [temp_report].{0} tR"
				+ " WHERE"
				+ "    tR.{3} = -1"
				+ "    AND tR.{4} = {5}"
				+ "    AND tR.{6} = {7}"
				+ "    AND tR.{8} NOT IN ("
				+ "       SELECT"
				+ "          {8}"
				+ "       FROM"
				+ "          {0}"
				+ "       WHERE"
				+ "          {4} = {5}"
				+ "          AND {6} = {7}"
				+ "    )"
				+ ";",
				dbStructure.TableName.AsDbName(),
				masterFieldsList,
				insertMemoryFieldsList,
				rowHashOldF,
				queryIdF,
				queryId,
				sessionIdF,
				sessionId,
				rowHashF
			);

			string updateRowOrderQuery = string.Format(
				  " UPDATE {0} SET"
				+ "    {1} = ("
				+ "       SELECT"
				+ "          tR.{1}"
				+ "       FROM"
				+ "          [temp_report].{0} tR"
				+ "       WHERE"
				+ "          tR.{2} = {3}"
				+ "          AND tR.{4} = {5}"
				+ "          AND tR.{6} = {0}.{6}"
				+ "          AND tR.{1} != {0}.{1}"
				+ "    )"
				+ " WHERE"
				+ "    EXISTS ("
				+ "       SELECT"
				+ "          1"
				+ "       FROM"
				+ "          [temp_report].{0} tR"
				+ "       WHERE"
				+ "          tR.{2} = {3}"
				+ "          AND tR.{4} = {5}"
				+ "          AND tR.{6} = {0}.{6}"
				+ "          AND tR.{1} != {0}.{1}"
				+ "    )"
				+ ";",
				dbStructure.TableName.AsDbName(),
				FieldRowOrder.Name.AsDbName(),
				queryIdF,
				queryId,
				sessionIdF,
				sessionId,
				rowHashF
			);

			string countRowsQuery = string.Format(
				  " SELECT"
				+ "    COUNT(*)"
				+ " FROM"
				+ "    {0}"
				+ " WHERE"
				+ "    {1} = {2}"
				+ "    AND {3} = {4};",
				dbStructure.TableName.AsDbName(),
				queryIdF,
				queryId,
				sessionIdF,
				sessionId
			);

			// operations
			using (DbConnection connection = StorageConnectionFactory.CreateOpenedConnection())
			{
				rowsDeleted         = ExecuteNonQuery(connection, deleteQuery);
				rowsUpdated         = ExecuteNonQuery(connection, updateQuery);
				rowsInserted        = ExecuteNonQuery(connection, insertQuery);
				rowsUpdatedRowOrder = ExecuteNonQuery(connection, updateRowOrderQuery);
				rowsTotal           = ExecuteScalar<long>(connection, countRowsQuery);
			}

			Log.Debug(
				$"rowsInserted:{rowsInserted};" +
				$"rowsUpdated:{rowsUpdated};" +
				$"rowsDeleted:{rowsDeleted};" +
				$"rowsUpdatedRowOrder:{rowsUpdatedRowOrder};" +
				$"totalRows:{rowsTotal}"
			);

			if (rowsTotal != rowsInMemory)
			{
				Log.Error($"Rows added to in-memory table: '{rowsInMemory}'. Rows formed by merge: '{rowsTotal}'.");
			}

			return rowsTotal;
		}

		private SqliteMemoryReportStorage MemoryStorage
		{
			get
			{
				if (this._memoryStorage == null)
				{
					lock (this._memoryStorageLock)
					{
						if (this._memoryStorage == null)
						{
							this._memoryStorage = new SqliteMemoryReportStorage(this, "temp_report");
						}
					}
				}

				return this._memoryStorage;
			}
		}

		private T ExecuteScalar<T>(DbConnection connection, string sql)
		{
			SqlScalarCommand<T> command = new SqlScalarCommand<T>(
				connection,
				sql
			);

			lock (this._tablesLock)
			{
				return command.Execute(CommandExecutor);
			}
		}

		public class SqlScalarCommand<T> : CommandBase<T>
		{
			private readonly DbConnection _connection;
			private readonly string _sql;

			public SqlScalarCommand(
				DbConnection connection,
				string       sql
			)
			{
				this._connection = connection;
				this._sql        = sql;
			}

			public override T Execute()
			{
				using (DbCommand command = this.GetCommand(this._sql))
				{
					object result = command.ExecuteScalar();

					return result == DBNull.Value ? default(T) : (T)result;
				}
			}

			private DbCommand GetCommand(string sql)
			{
				DbCommand dbCommand = this._connection.CreateCommand();
				dbCommand.CommandText = sql;

				return dbCommand;
			}
		}

		public class SqlCustomCommand : CommandBase<long>
		{
			private readonly DbConnection _connection;
			private readonly string       _sql;

			public SqlCustomCommand(
				DbConnection connection,
				string       sql
			)
			{
				this._connection = connection;
				this._sql        = sql;
			}

			public override long Execute()
			{
				using (DbCommand command = this.GetCommand(this._sql))
				{
					return command.ExecuteNonQuery();
				}
			}

			private DbCommand GetCommand(string sql)
			{
				DbCommand dbCommand = this._connection.CreateCommand();
				dbCommand.CommandText = sql;

				return dbCommand;
			}
		}

		private long ExecuteNonQuery(
			DbConnection connection,
			string sql
		)
		{
			SqlCustomCommand command = new SqlCustomCommand(
				connection,
				sql
			);

			lock (this._tablesLock)
			{
				return command.Execute(CommandExecutor);
			}
		}

		protected override void RemoveServiceColumns(DataTable dataTable)
		{
			List<string> columnsToRemove = new List<string>
			{
				FieldQueryId.Name,
				FieldSessionId.Name,
				TableInfo.FieldDateCreated,
				TableInfo.FieldDateUpdated,
				FieldRowOrder.Name,
				FieldRowHash.Name,
				FieldRowHash.Name + "Old"
			};

			RemovePrimaryKey(dataTable);

			foreach (string columnName in columnsToRemove)
			{
				if (dataTable.Columns.Contains(columnName))
				{
					dataTable.Columns.Remove(columnName);
				}
			}
		}
	}
}
