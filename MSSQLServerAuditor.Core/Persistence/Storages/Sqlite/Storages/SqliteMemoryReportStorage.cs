﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Tables;
using MSSQLServerAuditor.DataAccess.Vendors.Sqlite.Commands;
using NLog;
using Perceiveit.Data;

namespace MSSQLServerAuditor.Core.Persistence.Storages.Sqlite.Storages
{
	public class SqliteMemoryReportStorage : CommonReportStorage
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private static readonly FieldInfo FieldRowHashOld = new FieldInfo(
			FieldRowHash.Name + "Old",
			DbType.String,
			FieldInfoFlag.None,
			200
		);

		private const string MemoryConnectionString =
			"FullUri=file:memdb1?mode=memory&cache=shared; " +
			"PRAGMA journal_mode=off; "                      +
			"PRAGMA temp_store = MEMORY; "                   +
			"PRAGMA synchronous=off; "                       +
			"PRAGMA count_changes=off; "                     +
			"PRAGMA encoding = \"UTF-8\";";

		private readonly SQLiteConnection _memoryConnection;

		public SqliteMemoryReportStorage(
			SqliteReportStorage reportStorage,
			string              alias
		) : base(
			CreateMemoryDatabase(),
			reportStorage.CommandExecutor,
			alias
		)
		{
			this._memoryConnection = new SQLiteConnection(MemoryConnectionString);
			this._memoryConnection.Open();
		}

		private static DBDatabase CreateMemoryDatabase()
		{
			DBDatabase memoryDatabase = DBDatabase.Create(
				MemoryConnectionString,
				"System.Data.SQLite"
			);

			return memoryDatabase;
		}

		public long? SaveMemoryQueryData(
			long          queryId,
			NormalizeInfo dbStructure,
			DataTable     queryData,
			long          sessionId,
			DataTable     currentTable,
			bool          recreateSchema
		)
		{
			Dictionary<string, int> currentDict   = new Dictionary<string, int>();
			HashSet<string>         duplicateRows = new HashSet<string>();

			if (currentTable?.Rows != null)
			{
				for (int i = 0; i < currentTable.Rows.Count; i++)
				{
					DataRow row  = currentTable.Rows[i];
					string  hash = row[FieldRowHash.Name].ToString();

					if (!currentDict.ContainsKey(hash))
					{
						currentDict.Add(hash, i);
					}
					else
					{
						duplicateRows.Add(hash);
					}
				}
			}

			PrepareTables(dbStructure, recreateSchema);

			ReportTable reportTable = GetReportTable(dbStructure);

			if (reportTable == null)
			{
				return null;
			}

			Dictionary<string, int> insertDict   = new Dictionary<string, int>();
			Dictionary<string, int> updateDict   = new Dictionary<string, int>();
			Dictionary<int, int>    rowOrderDict = new Dictionary<int, int>();
			HashSet<string>         updatedRows  = new HashSet<string>();

			int currentRowOrder = 0;
			int queryRowOrder   = 0;
			foreach (DataRow queryRow in queryData.Rows)
			{
				string hash       = GetHashFromDataRow(queryRow);
				bool   hashExists = currentDict.ContainsKey(hash);

				if (hashExists && !duplicateRows.Contains(hash))
				{
					updateDict.Add(hash, currentRowOrder);
					updatedRows.Add(hash);
					currentDict.Remove(hash);
					rowOrderDict.Add(currentRowOrder, queryRowOrder);

					currentRowOrder++;
				}
				else
				{
					bool needInsert = !insertDict.ContainsKey(hash) && !updatedRows.Contains(hash);

					if (needInsert)
					{
						insertDict.Add(hash, currentRowOrder);
						rowOrderDict.Add(currentRowOrder, queryRowOrder);

						currentRowOrder++;
					}
				}

				queryRowOrder++;
			}

			List<ITableRow> rows             = new List<ITableRow>();
			List<ITableRow> rowsToUpdateList = new List<ITableRow>();
			List<string>    rowsToDelete     = new List<string>();

			foreach (string hash in currentDict.Keys)
			{
				if (!updatedRows.Contains(hash) && !duplicateRows.Contains(hash))
				{
					rowsToDelete.Add(hash);
				}
			}

			bool needInsertCommand = false;
			long rowsInserted      = 0L;
			long rowsUpdated       = 0L;

			lock (this._tablesLock)
			{
				foreach (KeyValuePair<string, int> keyPair in insertDict)
				{
					string    hash     = keyPair.Key;
					int       rowOrder = keyPair.Value;
					DataRow   row      = queryData.Rows[rowOrderDict[rowOrder]];
					ITableRow tableRow;

					ProcessRowForWrite(row, dbStructure, out tableRow, false);

					if (rowsToDelete.Count > 0L)
					{
						string hashOld = rowsToDelete[0];

						ModifyTableRow(tableRow, queryId, sessionId, rowOrder, hash);

						tableRow.Values[FieldRowHashOld.Name] = hashOld;

						rowsToUpdateList.Add(tableRow);

						rowsToDelete.RemoveAt(0);
					}
					else
					{
						needInsertCommand = true;

						ModifyTableRow(tableRow, queryId, sessionId, rowOrder, hash);

						tableRow.Values[FieldRowHashOld.Name] = -1;

						rows.Add(tableRow);
					}
				}

				foreach (KeyValuePair<string, int> keyPair in updateDict)
				{
					string    hash     = keyPair.Key;
					int       rowOrder = keyPair.Value;
					DataRow   row      = queryData.Rows[rowOrderDict[rowOrder]];
					ITableRow tableRow;

					ProcessRowForWrite(row, dbStructure, out tableRow, false);

					ModifyTableRow(tableRow, queryId, sessionId, rowOrder, hash);

					tableRow.Values[FieldRowHashOld.Name] = hash;

					rowsToUpdateList.Add(tableRow);
				}

				rowsUpdated += ReplaceRowsTrans(reportTable, rowsToUpdateList);

				if (needInsertCommand)
				{
					rowsInserted += ReplaceRowsTrans(reportTable, rows);
				}
			}

			Log.Debug($"rowsInserted: {rowsInserted}; rowsUpdated: {rowsUpdated}");

			return rowsInserted + rowsUpdated;
		}

		private ReportTable GetReportTable(NormalizeInfo dbStructure)
		{
			ReportTable reportTable;

			lock (this._tablesLock)
			{
				this._tables.TryGetValue(dbStructure, out reportTable);
			}

			return reportTable;
		}

		protected override long? InsertOrUpdateRow(
			ReportTable reportTable,
			ITableRow   tableRow
		)
		{
			return reportTable.InsertOrUpdateRow(
				this._memoryConnection,
				tableRow
			);
		}

		protected override void PrepareTables(NormalizeInfo normalizeInfo, bool recreateSchema, bool mainTable = true)
		{
			if (normalizeInfo != null)
			{
				TableInfo tableInfo = GetTableInfo(normalizeInfo);

				lock (this._tablesLock)
				{
					if (!this._tables.ContainsKey(normalizeInfo))
					{
						if (mainTable)
						{
							ModifyTableInfo(tableInfo);

							tableInfo.AddField(FieldRowHashOld);
							tableInfo.AddIndex(FieldRowHashOld.Name + "_index", false, new[] { FieldRowHashOld.Name });
						}

						if (tableInfo.Fields.Any())
						{
							ReportTable table = new ReportTable(this, tableInfo);

							if (recreateSchema)
							{
								table.DropAndCreate();
							}
							else
							{
								table.CreateIfNotExists();
							}

							this._tables.Add(normalizeInfo, table);
						}

						foreach (NormalizeInfo childDirectory in normalizeInfo.ChildDirectories)
						{
							this.PrepareTables(childDirectory, recreateSchema, false);
						}
					}
				}
			}
		}

		private long ReplaceRowsTrans(ReportTable table, List<ITableRow> rows)
		{
			long rowsAffected;

			using (SQLiteTransaction transaction = this._memoryConnection.BeginTransaction())
			{
				SqliteReplaceCommand replaceCommand = new SqliteReplaceCommand(
					this._memoryConnection,
					table.TableInfo,
					transaction
				);

				foreach (ITableRow tableRow in rows)
				{
					replaceCommand.AddRowForReplacing(tableRow);
				}

				rowsAffected = replaceCommand.Execute(CommandExecutor);

				transaction.Commit();
			}

			return rowsAffected;
		}

		private string GetHashFromDataRow(DataRow row)
		{
			StringBuilder result = new StringBuilder();

			if (row != null)
			{
				foreach (var colValue in row.ItemArray)
				{
					result.Append(colValue).Append("_");
				}
			}

			return GetHashString(result.ToString());
		}
	}
}
