using System.Configuration;

namespace MSSQLServerAuditor.Core.Persistence.Config
{
	// ReSharper disable once ClassNeverInstantiated.Global
	[ConfigurationCollection(typeof(DatabaseElement), AddItemName = "database", CollectionType = ConfigurationElementCollectionType.BasicMap)]
	public class DatabaseCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement()
		{
			return new DatabaseElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((DatabaseElement) element).Alias;
		}

		public DatabaseElement this[int index]
		{
			get { return (DatabaseElement) BaseGet(index); }
			set
			{
				if (BaseGet(index) != null)
				{
					BaseRemoveAt(index);
				}

				BaseAdd(index, value);
			}
		}

		public void Add(DatabaseElement databaseElement)
		{
			BaseAdd(databaseElement);
		}

		public void Clear()
		{
			BaseClear();
		}

		public void Remove(DatabaseElement databaseElement)
		{
			BaseRemove(databaseElement.Alias);
		}

		public void RemoveAt(int index)
		{
			BaseRemoveAt(index);
		}

		public void Remove(string alias)
		{
			BaseRemove(alias);
		}
	}
}