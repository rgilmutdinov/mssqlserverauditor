﻿using System.Configuration;

namespace MSSQLServerAuditor.Core.Persistence.Config
{
	// ReSharper disable once ClassNeverInstantiated.Global
	public class DatabaseElement : ConfigurationElement
	{
		[ConfigurationProperty("alias", IsKey = true, IsRequired = true)]
		public string Alias
		{
			get { return (string) this["alias"]; }
			set { this["alias"] = value; }
		}

		[ConfigurationProperty("name", IsRequired = true)]
		public string Name
		{
			get { return (string) this["name"]; }
			set { this["name"] = value; }
		}
	}
}