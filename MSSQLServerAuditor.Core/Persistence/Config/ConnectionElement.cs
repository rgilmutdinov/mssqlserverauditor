using System.Configuration;
using MSSQLServerAuditor.Core.Persistence.Storages;

namespace MSSQLServerAuditor.Core.Persistence.Config
{
	// ReSharper disable once ClassNeverInstantiated.Global
	public class ConnectionElement : ConfigurationElement
	{
		[ConfigurationProperty("type", DefaultValue = StorageType.SQLite, IsRequired = true)]
		public StorageType Type
		{
			get { return (StorageType) this["type"]; }
			set { this["type"] = value; }
		}

		[ConfigurationProperty("connectionString", IsRequired = true)]
		public string ConnectionString
		{
			get { return (string) this["connectionString"]; }
			set { this["connectionString"] = value; }
		}

		[ConfigurationProperty("databases")]
		public DatabaseCollection Databases
		{
			get { return base["databases"] as DatabaseCollection; }
		}
	}
}