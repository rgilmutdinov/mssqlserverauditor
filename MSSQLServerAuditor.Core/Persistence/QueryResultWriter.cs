﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Services;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using MSSQLServerAuditor.DataAccess.Structure;
using NLog;

namespace MSSQLServerAuditor.Core.Persistence
{
	public class QueryResultWriter
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IStorageManager _storage;
		private readonly IQueryLoader    _queryLoader;
		private readonly QueryFormatter  _queryFormatter;

		public QueryResultWriter(
			IStorageManager storage,
			IQueryLoader    queryLoader)
		{
			this._storage     = storage;
			this._queryLoader = queryLoader;

			this._queryFormatter = storage.QueryFormatter;
		}

		public long? SaveResults(QueryResultSet resultSet)
		{
			try
			{
				// check whether there are any results to be saved
				bool saveResults = resultSet.IsSavable;

				if (!saveResults)
				{
					return null;
				}

				return SaveResultsImpl(resultSet);
			}
			catch (Exception exc)
			{
				Log.Error(exc, "Failed to save query results");
				throw;
			}
		}

		private long? SaveResultsImpl(QueryResultSet resultSet)
		{
			long? totalRowsSaved = null;

			const long sessionId = 1L;

			long requestId = this._storage.MainStorage.MetaResults.GetMaxRequestId() + 1L;

			foreach (QueryResult queryResult in resultSet.Results)
			{
				TemplateNodeQueryInfo tnQueryInfo = queryResult.QueryInfo;
				
				foreach (KeyValuePair<AuditSession, SessionResult> instancePair in queryResult.SessionResults)
				{
					AuditSession  session       = instancePair.Key;
					SessionResult sessionResult = instancePair.Value;

					if (!sessionResult.IsSavable)
					{
						continue;
					}

					DBQuery query = this._storage.MainStorage.Nodes.GetQuery(
						session,
						tnQueryInfo,
						sessionResult.Timestamp,
						false
					);

					if (sessionResult.ErrorInfo == null)
					{
						IEnumerable<DatabaseResult> dbResults = sessionResult
							.DatabaseResults.Values
							.Where(d => d?.DataTables != null);

						if (totalRowsSaved == null)
						{
							totalRowsSaved = 0;
						}

						long rowsSaved = 0;
						foreach (DatabaseResult dbResult in dbResults)
						{
							if (dbResult.IsSavable)
							{
								rowsSaved += SaveDatabaseResult(
									session,
									tnQueryInfo,
									dbResult,
									query.Id
								);
							}
						}

						totalRowsSaved += rowsSaved;
					}

					this._storage.MainStorage.MetaResults.SaveMetaResult(
						requestId,
						sessionId,
						sessionResult,
						query.Id,
						totalRowsSaved
					);
				}
			}

			return totalRowsSaved;
		}

		private long SaveDatabaseResult(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			DatabaseResult        dbResult,
			long                  queryId
		)
		{
			long totalRows = 0L;
			long recordSet = 1L;

			foreach (DataTable table in dbResult.DataTables)
			{
				long? savedRows = SaveTableResults(
					session.Template,
					tnQueryInfo,
					recordSet,
					dbResult.Revision,
					table,
					queryId
				);

				if (savedRows.HasValue)
				{
					totalRows += savedRows.Value;
				}

				recordSet++;
			}

			UpdateHistory(
				session,
				tnQueryInfo,
				queryId
			);

			return totalRows;
		}

		private long? SaveTableResults(
			Template              template,
			TemplateNodeQueryInfo tnQueryInfo,
			long                  recordSet,
			long?                 revision,
			DataTable             dataTable,
			long                  queryId
		)
		{
			QueryService queryService = this._storage.MainStorage.Queries;

			long templateNodeId = this._storage.MainStorage.Templates.GetTemplateNodeId(
				template,
				tnQueryInfo.TemplateNode.Template
			);

			string tableName = this._storage.ReportStorage.GetTableName(
				templateNodeId,
				template.UId,
				tnQueryInfo,
				recordSet
			);

			DBQueryResultTable qrtSpec = new DBQueryResultTable
			{
				Name     = tableName,
				Revision = revision
			};

			DBQueryResultTable resultTable = queryService
				.GetOrAddQueryResultTable(qrtSpec);

			DBQueryResults qrSpec = new DBQueryResults
			{
				QueryId       = queryId,
				RecordSet     = recordSet,
				ResultTableId = resultTable.Id
			};

			DBQueryResults result = queryService
				.GetOrAddQueryResult(qrSpec);

			// check if revision of the query has changed
			bool recreateSchema = result != null && revision != resultTable.Revision;

			NormalizeInfo dbStructure = this._storage.ReportStorage.GetDbStucture(
				tableName,
				recordSet,
				recreateSchema,
				dataTable
			);
			
			long? savedRows = this._storage.ReportStorage.SaveResults(
				queryId,
				dbStructure,
				dataTable,
				recreateSchema
			);

			if (recreateSchema)
			{
				resultTable.Revision = revision;

				queryService.SaveQueryResultTable(resultTable);
				queryService.SaveQueryResult(qrSpec);
			}

			return savedRows;
		}

		private void UpdateHistory(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			long                  queryId
		)
		{
			if (this._storage.HistoryStorages != null && this._storage.ConnectionInfo.UpdateHistory)
			{
				ServerInstance instance = session.Server;

				List<QueryInfo> queries = this._queryLoader.LoadQueries(tnQueryInfo);

				QueryInfo query = queries.FirstOrDefault(x =>
					x.ConnectionType == instance.ConnectionType || x.ConnectionType.IsInternal());

				try
				{
					SaveHistoryData(
						session,
						tnQueryInfo,
						query,
						queryId
					);
				}
				catch (Exception ex)
				{
					Log.Error(ex, $"Error executing update history query:\n{query}\non the instance:\n{instance.ConnectionName}");

					throw;
				}
			}
		}

		private void SaveHistoryData(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			QueryInfo             query,
			long                  queryId
		)
		{
			QueryInfo queryInfo = new QueryInfo { ConnectionType = ConnectionType.Internal };
			Regex     regex     = new Regex(@"\[\$\{(?<QueryName>[\w]+)\}\$_\$\{(?<RecordSetNumber>[\w]+)\}\$\]");

			if (query.FillStatements == null || !query.SaveResults)
			{
				return;
			}

			List<FillStatement> fillStatements = query.FillStatements
				.OrderBy(statement => statement.Id)
				.ToList();

			long templateNodeId = this._storage.MainStorage.Templates.GetTemplateNodeId(
				session.Template,
				tnQueryInfo.TemplateNode.Template
			);

			foreach (FillStatement statement in fillStatements)
			{
				QueryItemInfo queryItem = new QueryItemInfo
				{
					Parent = queryInfo,
					Text   = statement.Text
				};

				MatchCollection results = regex.Matches(queryItem.Text);

				foreach (Match match in results)
				{
					string queryName       = match.Groups["QueryName"].Value;
					long   recordSetNumber = long.Parse(match.Groups["RecordSetNumber"].Value);

					if (string.Equals(queryName, query.Name, StringComparison.OrdinalIgnoreCase))
					{
						string tableName = this._storage.ReportStorage.GetTableName(
							templateNodeId,
							session.Template.UId,
							tnQueryInfo,
							recordSetNumber
						);

						tableName = $"[{tableName}]";

						queryItem.Text = queryItem.Text.Replace(
							match.Value,
							tableName
						);
					}
				}

				ExecuteNonQuery(
					session,
					queryItem,
					queryId,
					statement.Id
				);
			}
		}

		private int ExecuteNonQuery(
			AuditSession  session,
			QueryItemInfo queryInfo,
			long          queryId,
			long          statementId
		)
		{
			bool shouldExecute = true;

			if (queryInfo == null)
			{
				Log.Warn("There is no sql statement to execute (QueryItemInfo == null).");

				return 0;
			}

			string sqlCondition = queryInfo.ExecuteIfSqlText
				.TrimmedOrEmpty();

			if (!sqlCondition.IsNullOrEmpty())
			{
				DataTable[] dataTables = ExecuteReadTables(
					session,
					sqlCondition,
					queryId
				);

				if (dataTables.Length > 0 && dataTables[0].Rows.Count > 0)
				{
					shouldExecute = (int) dataTables[0].Rows[0][0] == 1;
				}
			}

			if (shouldExecute)
			{
				DBMetaSubResult meta = new DBMetaSubResult
				{
					QueryId    = queryId,
					SubQueryId = statementId
				};

				try
				{
					int rows = ExecuteNonQuery(
						session,
						this._queryFormatter.Format(queryInfo.Text),
						queryId
					);

					meta.Rows = rows;

					// log query results to database table
					SaveSubMeta(meta);

					return rows;
				}
				catch (Exception qExc)
				{
					Log.Error(
						qExc,
						$"Error when executing subquery (queryId: {queryId}, statementId: {statementId})"
					);

					ErrorInfo errorInfo = new ErrorInfo(qExc);
					meta.ErrorCode      = errorInfo.Code;
					meta.ErrorId        = errorInfo.Number;
					meta.ErrorMessage   = errorInfo.Message;

					// log query error to database table
					SaveSubMeta(meta);
				}
			}

			return 0;
		}

		private void SaveSubMeta(DBMetaSubResult meta)
		{
			try
			{
				this._storage.MainStorage.MetaResults.SaveSubMeta(meta);
			}
			catch (Exception ex)
			{
				Log.Error(
					ex,
					$"Failed to save meta result of subquery (queryId: {meta.QueryId}, statementId: {meta.SubQueryId})"
				);

				throw;
			}
		}

		private int ExecuteNonQuery(
			AuditSession session,
			string       sql,
			long         queryId
		)
		{
			List<DbCommandParameter> parameters = GetDefaultCommandParameters(session);

			DbCommandParameter idParam = new DbCommandParameter(
				queryId,
				new FieldInfo(
					DBQueryParameter.FieldQueryId,
					DbType.Int64
				)
			);

			parameters.Add(idParam);

			ExecuteNonQueryCommand command = new ExecuteNonQueryCommand(
				this._storage.StorageConnectionFactory,
				sql,
				parameters.ToArray()
			);

			return command.Execute(this._storage.CommandExecutor);
		}

		private DataTable[] ExecuteReadTables(
			AuditSession session,
			string       sql,
			long         queryId
		)
		{
			List<DbCommandParameter> parameters = GetDefaultCommandParameters(session);

			DbCommandParameter idParam = new DbCommandParameter(
				queryId,
				new FieldInfo(
					DBQueryParameter.FieldQueryId,
					DbType.Int64
				)
			);

			parameters.Add(idParam);

			ReadTablesCommand command = new ReadTablesCommand(
				this._storage.StorageConnectionFactory,
				sql,
				parameters.ToArray()
			);

			return command.Execute(this._storage.CommandExecutor);
		}

		private List<DbCommandParameter> GetDefaultCommandParameters(AuditSession session)
		{
			List<DbCommandParameter> result = new List<DbCommandParameter>();

			long groupId    = session.Group.Id;
			long templateId = session.Template.Id;
			long instanceId = session.Server.Id;
			long loginId    = session.Server.Login.Id;

			result.Add(new DbCommandParameter(groupId,
				new FieldInfo(DBConnectionGroup.FieldId, DbType.Int64, FieldInfoFlag.Identity)));

			result.Add(new DbCommandParameter(instanceId,
				new FieldInfo(DBServerInstance.FieldId, DbType.Int64, FieldInfoFlag.Identity)));

			result.Add(new DbCommandParameter(loginId,
				new FieldInfo(DBLogin.FieldId, DbType.Int64, FieldInfoFlag.Identity)));

			result.Add(new DbCommandParameter(templateId,
				new FieldInfo(DBTemplate.FieldId, DbType.Int64, FieldInfoFlag.Identity)));

			return result;
		}
	}
}
