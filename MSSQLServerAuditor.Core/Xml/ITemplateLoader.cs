﻿using System.Collections.Generic;
using MSSQLServerAuditor.Core.Cryptography.Xml;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Xml
{
	public interface ITemplateLoader
	{
		XmlEncryptor Encryptor { get; set; }

		List<TemplateFile> LoadTemplates();
		TemplateInfo LoadTemplate(string xmlFile);
	}
}
