﻿using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain.Models;

namespace MSSQLServerAuditor.Core.Session
{
	public class AuditContext
	{
		public AuditContext(
			ConnectionGroup connectionGroup,
			Template        template)
		{
			Check.NotNull(connectionGroup, nameof(connectionGroup));
			Check.NotNull(template,        nameof(template));

			this.ConnectionGroup = connectionGroup;
			this.Template        = template;
		}

		public ConnectionGroup ConnectionGroup { get; }
		public Template        Template        { get; }

		public List<AuditSession> CreateSessions()
		{
			List<AuditSession> sessions = new List<AuditSession>();
			foreach (ServerInstance server in ConnectionGroup.ServerInstances)
			{
				AuditSession session = new AuditSession(
					ConnectionGroup,
					server,
					Template
				);

				sessions.Add(session);
			}

			return sessions;
		}

		public string Login
		{
			get
			{
				List<ServerInstance> servers = ConnectionGroup.ServerInstances;
				if (servers.Any())
				{
					Login login = servers[0].Login;

					// check all logins are equal
					if (servers.Skip(1).All(s => string.Equals(login.Name, s.Login.Name)))
					{
						return login.Name;
					}
				}

				return "...";
			}
		}

		public int? ServersCount => ConnectionGroup?.ServerInstances.Count;
	}
}
