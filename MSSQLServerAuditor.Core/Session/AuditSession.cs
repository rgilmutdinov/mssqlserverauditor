﻿using System.Collections.Generic;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;

namespace MSSQLServerAuditor.Core.Session
{
	public class AuditSession
	{
		public AuditSession(
			ConnectionGroup group,
			ServerInstance  server,
			Template        template)
		{
			Check.NotNull(server,   nameof(server));
			Check.NotNull(group,    nameof(group));
			Check.NotNull(template, nameof(template));

			this.Group    = group;
			this.Server   = server;
			this.Template = template;
		}

		public ConnectionGroup Group    { get; }
		public ServerInstance  Server   { get; }
		public Template        Template { get; }

		public ConnectionType ConnectionType => Server.ConnectionType;

		public override string ToString()
		{
			List<string> parts = new List<string>();

			if (Server?.ConnectionName != null)
			{
				parts.Add($"server: {Server?.ConnectionName}");
			}

			if (Group?.Name != null)
			{
				parts.Add($"group: {Group?.Name}");
			}

			if (Server?.Login?.Name != null)
			{
				parts.Add($"login: {Server?.Login?.Name}");
			}

			return $"{parts.Join(",")}";
		}
	}
}
