﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.ViewModels
{
	public class ScheduleSettingsViewModel : ValidatingScreen
	{
		private readonly IWindowManager _windowManager;
		private readonly IAppSettings   _settings;

		private ScheduleViewModel _selectedSchedule;

		public ScheduleSettingsViewModel(
			List<ScheduleSettings> schedules,
			IWindowManager         windowManager,
			IAppSettings           settings)
		{
			Check.NotNull(schedules,     nameof(schedules));
			Check.NotNull(settings,      nameof(settings));
			Check.NotNull(windowManager, nameof(windowManager));

			this._windowManager  = windowManager;
			this._settings       = settings;

			Schedules = new ObservableCollection<ScheduleViewModel>();

			InitializeSchedules(schedules);
			Schedules.CollectionChanged += (sender, args) =>
			{
				NotifyOfPropertyChange(() => HasSchedules);
			};
		}

		public ObservableCollection<ScheduleViewModel> Schedules { get; private set; }

		public ScheduleViewModel SelectedSchedule
		{
			get { return this._selectedSchedule; }
			set
			{
				if (this._selectedSchedule != value)
				{
					this._selectedSchedule = value;
					NotifyOfPropertyChange(() => SelectedSchedule);
				}
			}
		}

		public bool HasSchedules => Schedules.Any();
		
		public void AddSchedule()
		{
			ScheduleIdViewModel scheduleIdDialog = new ScheduleIdViewModel(Schedules.ToList());

			bool ok = this._windowManager.ShowDialog(scheduleIdDialog) ?? false;

			if (ok)
			{
				ScheduleViewModel schedule = CreateNewSchedule(scheduleIdDialog.Id);

				Schedules.Add(schedule);

				this.SelectedSchedule = schedule;
			}
		}

		public async void Accept()
		{
			bool isValid = true;
			foreach (ScheduleViewModel schedule in Schedules)
			{
				schedule.EnableValidation();

				ValidationResult result = await schedule.Validate();
				if (!result.IsValid)
				{
					isValid = false;
				}
			}

			if (isValid)
			{
				TryClose(true);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.AutoUpdateNodeSettings;
		}

		private void InitializeSchedules(List<ScheduleSettings> scheduleSettings)
		{
			foreach (ScheduleSettings settings in scheduleSettings)
			{
				Schedules.Add(CreateSchedule(settings));
			}

			if (Schedules.Any())
			{
				this.SelectedSchedule = Schedules.First();
			}
		}

		private ScheduleViewModel CreateSchedule(ScheduleSettings settings)
		{
			ScheduleViewModel schedule = new ScheduleViewModel(settings.UId, this._settings)
			{
				CronExpression                = settings.CronExpression,
				ScheduleName                  = settings.Name,
				IsScheduleEnabled             = settings.IsEnabled.HasValue && settings.IsEnabled.Value,
				IsEmailEnabled                = settings.SendMessage.HasValue && settings.SendMessage.Value,
				SendNonEmptyNotificationsOnly = settings.SendMessageNonEmptyOnly,
				IsServiceOnly                 = settings.ServiceOnly,
				NotificationLanguage          = settings.MessageLanguage,
				TargetMachine                 = settings.TargetMachine,
				Recipients                    = settings.MessageRecipients,
				UpdateHierarchically          = settings.UpdateHierarchically,
				UpdateDeep                    = settings.UpdateDeep
			};

			return schedule;
		}

		private ScheduleViewModel CreateNewSchedule(string scheduleId)
		{
			ScheduleViewModel schedule = new ScheduleViewModel(
				scheduleId,
				this._settings
			);

			return schedule;
		}
	}
}
