﻿using System;
using System.Management;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying.Connections;
using MSSQLServerAuditor.Infrastructure;
using MSSQLServerAuditor.Presentation.Dialogs;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;
using NLog;
using LogManager = NLog.LogManager;

namespace MSSQLServerAuditor.ViewModels
{
	public class WmiConnectionViewModel : ValidatingScreen, IConnectionDialog<WmiInstance>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();
		
		private readonly IStorageManager _storageManager;
		private readonly IDialogService  _dialogService;

		private readonly IDisposable _validationSuspender;
		private          bool        _validationSuspenderDisposed;
		private          bool        _isKeyModificationEnabled = true;

		private string              _connectionName;
		private string              _computerName;
		private string              _authority;
		private string              _username;
		private string              _password;
		private AuthenticationLevel _auth;
		private ImpersonationLevel  _impersonation;

		public WmiConnectionViewModel(
			IStorageManager storageManager,
			IWindowManager  windowManager
		)
		{
			this._storageManager = storageManager;
			this._dialogService  = new DialogService(windowManager);

			ConfigureValidationRules();

			// suppress validation till the first attempt to accept
			this._validationSuspender = Validator.SuppressValidation();
			this._validationSuspenderDisposed = false;

			AuthenticationLevels = new BindableCollection<AuthenticationLevel>
			{ 
				AuthenticationLevel.None,
				AuthenticationLevel.Call,
				AuthenticationLevel.Connect,
				AuthenticationLevel.Packet,
				AuthenticationLevel.PacketIntegrity,
				AuthenticationLevel.PacketPrivacy
			};

			ImpersonationLevels = new BindableCollection<ImpersonationLevel>
			{
				ImpersonationLevel.Identify,
				ImpersonationLevel.Impersonate,
				ImpersonationLevel.Delegate
			};

			Authentication = AuthenticationLevel.Packet;
			Impersonation  = ImpersonationLevel.Impersonate;
		}

		public IObservableCollection<AuthenticationLevel> AuthenticationLevels { get; }
		public IObservableCollection<ImpersonationLevel>  ImpersonationLevels  { get; }

		public string ConnectionName
		{
			get { return this._connectionName; }
			set
			{
				if (this._connectionName != value)
				{
					this._connectionName = value;

					NotifyOfPropertyChange(() => ConnectionName);
					Validator.Validate(nameof(ConnectionName));
				}
			}
		}

		public string ComputerName
		{
			get { return this._computerName; }
			set
			{
				if (this._computerName != value)
				{
					this._computerName = value;

					NotifyOfPropertyChange(() => ComputerName);
					Validator.Validate(nameof(ComputerName));
				}
			}
		}

		public string Authority
		{
			get { return this._authority; }
			set
			{
				if (this._authority != value)
				{
					this._authority = value;

					NotifyOfPropertyChange(() => Authority);
					Validator.Validate(nameof(Authority));
				}
			}
		}

		public string Username
		{
			get { return this._username; }
			set
			{
				if (this._username != value)
				{
					this._username = value;

					NotifyOfPropertyChange(() => Username);
					Validator.Validate(nameof(Username));
				}
			}
		}

		public AuthenticationLevel Authentication
		{
			get { return this._auth; }
			set
			{
				this._auth = value;

				NotifyOfPropertyChange(() => Authentication);
				Validator.Validate(nameof(Authentication));
			}
		}

		public ImpersonationLevel Impersonation
		{
			get { return this._impersonation; }
			set
			{
				if (this._impersonation != value)
				{
					this._impersonation = value;

					NotifyOfPropertyChange(() => Impersonation);
					Validator.Validate(nameof(Impersonation));
				}
			}
		}

		public string Password
		{
			get { return this._password; }
			set
			{
				if (this._password != value)
				{
					this._password = value;

					NotifyOfPropertyChange(() => Password);
					Validator.Validate(nameof(Password));
				}
			}
		}

		public bool IsKeyModificationEnabled
		{
			get { return this._isKeyModificationEnabled; }
			set
			{
				if (this._isKeyModificationEnabled != value)
				{
					this._isKeyModificationEnabled = value;
					NotifyOfPropertyChange(() => IsKeyModificationEnabled);
				}
			}
		}

		public WmiInstance Instance
		{
			get
			{
				Login login = Login.CreateEmpty();

				if (!string.IsNullOrWhiteSpace(Username))
				{
					login.Name = Username;
				}

				if (!string.IsNullOrWhiteSpace(Password))
				{
					login.Password = Password;
				}

				WmiInstance instance = new WmiInstance
				{
					ConnectionName   = ConnectionName,
					ConnectionType   = ConnectionType.WMI,
					Authentication   = Authentication.ToString(),
					Authority        = Authority,
					Impersonation    = Impersonation.ToString(),
					Host             = ComputerName,
					Login            = login,
					IsActive         = true
				};

				return instance;
			}
			set
			{
				Check.NotNull(value, nameof(value));

				WmiConnectionInfo info = value.ToWmiConnectionOptions();

				ConnectionName = value.ConnectionName;
				ComputerName   = info.ComputerName;
				Authority      = info.Authority;
				Impersonation  = info.Impersonation ?? ImpersonationLevel.Impersonate;
				Authentication = info.Authentication ?? AuthenticationLevel.Packet;

				Username       = value.Login?.Name;
				Password       = value.Login?.Password;
			}
		}

		ServerInstance IConnectionDialog.Instance
		{
			get { return Instance; }
			set { Instance = (WmiInstance) value; }
		}

		public void Accept()
		{
			EnableValidation();

			ValidationResult validationResult = Validator.ValidateAll();
			if (validationResult.IsValid)
			{
				TryClose(true);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.WmiConnection;
		}

		private void EnableValidation()
		{
			if (!this._validationSuspenderDisposed)
			{
				// dispose validation suspender to allow on-fly validation
				this._validationSuspender.Dispose();
				this._validationSuspenderDisposed = true;
			}
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRequiredRule(() => ConnectionName, Strings.ConnectionNameIsRequired);
			Validator.AddRequiredRule(() => ComputerName, Strings.ComputerNameIsRequired);
		}
	}
}
