﻿using System;
using System.Collections.Generic;
using System.Text;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Tasks;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.ViewModels
{
	public class TaskMonitorViewModel : PropertyChangedBase
	{
		private readonly IQueueTaskManager _taskManager;
		private bool _notifyProgress;
		private bool _isCancelling;

		public TaskMonitorViewModel(IQueueTaskManager taskManager)
		{
			Check.NotNull(taskManager, nameof(taskManager));

			this._taskManager = taskManager;

			this._taskManager.RunningTasksChanged   += TaskManagerOnRunningTasksChanged;
			this._taskManager.WaitingTasksChanged   += TaskManagerOnWaitingTasksChanged;
			this._taskManager.TaskStatusChanged     += TaskManagerOnTaskStatusChanged;
			this._taskManager.CancellationRequested += TaskManagerOnCancellationRequested;

			this._notifyProgress = false;
			this.IsCancelling    = false;
		}

		public string RunningTasks { get; private set; }
		public string WaitingTasks { get; private set; }

		public string RunningTasksSummary { get; private set; }

		public bool IsCancelling
		{
			get { return this._isCancelling; }
			set
			{
				if (this._isCancelling != value)
				{
					this._isCancelling = value;

					NotifyOfPropertyChange(() => IsCancelling);
				}
			}
		}

		public bool NotifyProgress
		{
			get { return this._notifyProgress; }
			set
			{
				if (this._notifyProgress != value)
				{
					this._notifyProgress = value;

					UpdateWaitingTasks();
					UpdateRunningTasks();

					NotifyOfPropertyChange(() => NotifyProgress);
				}
			}
		}

		private void TaskManagerOnWaitingTasksChanged(object sender, EventArgs eventArgs)
		{
			if (!NotifyProgress)
			{
				return;
			}

			UpdateWaitingTasks();
		}

		private void TaskManagerOnRunningTasksChanged(object sender, EventArgs eventArgs)
		{
			if (!NotifyProgress)
			{
				return;
			}

			UpdateRunningTasks();
		}

		private void TaskManagerOnTaskStatusChanged(object sender, QueueTaskEventArgs e)
		{
			UpdateIsCancelling();
		}

		private void UpdateIsCancelling()
		{
			if (!this._taskManager.HasAnyTasks)
			{
				IsCancelling = false;
			}
		}

		private void TaskManagerOnCancellationRequested(object sender, EventArgs eventArgs)
		{
			IsCancelling = true;
		}

		private void UpdateRunningTasks()
		{
			List<QueueTask> runningTasks = this._taskManager.RunningTasks;
			StringBuilder sb = new StringBuilder();
			foreach (QueueTask runningTask in runningTasks)
			{
				sb.AppendLine(runningTask.Title);
			}

			RunningTasks = sb.ToString();

			int runningTasksCount = runningTasks.Count;
			int waitingTasksCount = this._taskManager.WaitingTasks.Count;
			int totalTasksCount   = runningTasksCount + waitingTasksCount;

			RunningTasksSummary = string.Format(
				Strings.RunningTasksPattern,
				runningTasksCount,
				totalTasksCount
			);

			NotifyOfPropertyChange(() => RunningTasksSummary);
			NotifyOfPropertyChange(() => RunningTasks);
		}

		private void UpdateWaitingTasks()
		{
			List<QueueTask> waitingTasks = this._taskManager.WaitingTasks;
			StringBuilder sb = new StringBuilder();
			foreach (QueueTask waitingTask in waitingTasks)
			{
				sb.AppendLine(waitingTask.Title);
			}

			WaitingTasks = sb.ToString();

			NotifyOfPropertyChange(() => WaitingTasks);
		}
	}
}
