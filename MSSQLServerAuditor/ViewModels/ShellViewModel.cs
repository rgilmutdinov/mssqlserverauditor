﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Controls.TreeView;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Tasks;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Messages;
using MSSQLServerAuditor.Presentation.Localization;
using MSSQLServerAuditor.Presentation.Menu.System;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tree;
using MSSQLServerAuditor.Web;

namespace MSSQLServerAuditor.ViewModels
{
	public class ShellViewModel : Screen, IShell, IHandle<SettingsUpdateMessage>
	{
		private readonly IStorageManager   _storageManager;
		private readonly IEventAggregator  _eventAggregator;
		private readonly QueryExecutor     _queryExecutor;
		private readonly WebServer         _webServer;
		private readonly IQueueTaskManager _taskManager;

		private bool _isTreeLoading = false;

		public ShellViewModel(
			IWindowManager   windowManager,
			IStorageManager  storageManager,
			IAppSettings     appSettings,
			IEventAggregator eventAggregator,
			XmlLoader        xmlLoader,
			QueryExecutor    queryExecutor,
			WebServer        webServer
		)
		{
			Check.NotNull(windowManager,   nameof(windowManager));
			Check.NotNull(storageManager,  nameof(storageManager));
			Check.NotNull(appSettings,     nameof(appSettings));
			Check.NotNull(eventAggregator, nameof(eventAggregator));
			Check.NotNull(xmlLoader,       nameof(xmlLoader));
			Check.NotNull(queryExecutor,   nameof(queryExecutor));
			Check.NotNull(webServer,       nameof(webServer));

			AppSettings = appSettings;
			WindowManager = windowManager;

			this._storageManager  = storageManager;
			this._eventAggregator = eventAggregator;
			this._webServer       = webServer;
			this._queryExecutor   = queryExecutor;

			XmlLoader = xmlLoader;

			this._taskManager = new QueueTaskManager(appSettings.User);
			
			StatusBar = new StatusBarViewModel(this._taskManager);
			Workspace = new WorkspaceViewModel(this._taskManager, StatusBar);

			AuditForestViewModel auditForest = new AuditForestViewModel(this);

			auditForest.CurrentNodeChanged += (sender, args) =>
			{
				UpdateDisplayName();

				NotifyOfPropertyChange(() => CanCloseCurrentConnection);
				NotifyOfPropertyChange(() => CanCloseAllConnections);
			};

			auditForest.TreeChanged += (sender, args) =>
			{
				NotifyOfPropertyChange(() => CanCloseCurrentConnection);
				NotifyOfPropertyChange(() => CanCloseAllConnections);
			};

			AuditForest = auditForest;

			SystemMenuItems = new ObservableCollectionEx<SystemMenuItem>();

			UpdateSystemMenu();
		}

		public void UpdateSystemMenu()
		{
			List<SystemMenuItem> systemMenuItems = Lists.Of(SystemMenuItem.Separator);

			if (!Properties.Settings.Default.ShowMainMenu)
			{
				systemMenuItems.Add
				(
					new SystemMenuItem
					{
						IsSeparator = false,
						Id          = 0x101,
						Text        = Strings.DisplayMainMenu,
						Action      = DisplayMainMenu
					}
				);
			}
			else
			{
				systemMenuItems.Add
				(
					new SystemMenuItem
					{
						IsSeparator = false,
						Id          = 0x102,
						Text        = Strings.HideMainMenu,
						Action      = HideMainMenu
					}
				);
			}

			SystemMenuItems.ReplaceRange(systemMenuItems);
		}

		private void HideMainMenu()
		{
			ShowMainMenu(false);
		}

		private void DisplayMainMenu()
		{
			ShowMainMenu(true);
		}

		public void ShowMainMenu(bool show)
		{
			Properties.Settings.Default.ShowMainMenu = show;
			UpdateSystemMenu();
		}

		public ObservableCollectionEx<SystemMenuItem> SystemMenuItems { get; }

		public IWorkspace     Workspace     { get; }
		public IStatusBar     StatusBar     { get; }
		public IAuditForest   AuditForest   { get; }

		public IAppSettings   AppSettings   { get; set; }
		public IWindowManager WindowManager { get; set; }

		public XmlLoader      XmlLoader     { get; }

		public bool IsTreeLoading
		{
			get { return this._isTreeLoading; }
			set
			{
				if (this._isTreeLoading != value)
				{
					this._isTreeLoading = value;
					NotifyOfPropertyChange(() => IsTreeLoading);
				}
			}
		}

		public void UpdateCurrentNode()
		{
			AuditForest.UpdateCurrentNode();
		}

		public async void Connect()
		{
			ConnectionGroupViewModel groupDialog = new ConnectionGroupViewModel(
				this._storageManager,
				WindowManager,
				AppSettings,
				XmlLoader
			);

			bool ok = WindowManager.ShowDialog(groupDialog) ?? false;
			if (ok)
			{
				AuditContext auditContext = groupDialog.SelectedAuditContext;

				this._storageManager.MainStorage.Connections
					.UpdateGroupConnections(auditContext.ConnectionGroup);

				AuditTree tree = await OpenConnectionAsync(
					auditContext,
					groupDialog.SelectedModuleType
				);

				await AuditForest.AddTreeAsync(tree.Root);
			}
		}

		public void OpenSettingsDialog()
		{
			SettingsViewModel settings = new SettingsViewModel(
				WindowManager,
				AppSettings,
				this._eventAggregator
			);

			WindowManager.ShowDialog(settings);
		}

		public void OpenAboutDialog()
		{
			AboutViewModel aboutDialog = new AboutViewModel(
				this._storageManager,
				AppSettings
			);

			WindowManager.ShowDialog(aboutDialog);
		}

		public void OpenComponentsDialog()
		{
			ComponentsViewModel componentsDialog = new ComponentsViewModel();
			WindowManager.ShowDialog(componentsDialog);
		}

		public void CloseCurrentConnection()
		{
			AdvTreeNode currentNode = AuditForest.CurrentNode;
			while (currentNode.Parent != null)
			{
				currentNode = currentNode.Parent;
				TemplateTreeRoot root = currentNode as TemplateTreeRoot;
				if (root != null)
				{
					AuditForest.CloseTreeAsync(root);
				}
			}
		}

		public void CloseAllConnections()
		{
			AuditForest.ClearForestAsync();
		}

		public void Exit()
		{
			Application.Current.Shutdown();
		}

		public bool CanCloseCurrentConnection => AuditForest.CurrentNode != null;
		public bool CanCloseAllConnections    => AuditForest.Origin.Children.Any();

		public async void Handle(SettingsUpdateMessage message)
		{
			UserSettings oldSettings = message.OldSettings;
			UserSettings newSettings = message.NewSettings;

			if (!oldSettings.UiLanguage.Equals(newSettings.UiLanguage))
			{
				CultureInfo newCulture = new CultureInfo(newSettings.UiLanguage);
				AppLocalizer.ChangeCulture(newCulture);
				Strings.Culture = newCulture;

				AuditForest.RefreshTrees();
				UpdateSystemMenu();

				// update title
				UpdateDisplayName();
			}

			bool refreshContent = false;

			if (oldSettings.ShowXml != newSettings.ShowXml)
			{
				ReportHistory.LastTabIndex += newSettings.ShowXml ? 1 : -1;
				refreshContent = true;
			}

			if (oldSettings.ShowHtml != newSettings.ShowHtml)
			{
				refreshContent = true;
			}

			if (!oldSettings.ReportLanguage.Equals(newSettings.ReportLanguage))
			{
				refreshContent = true;
			}

			if (refreshContent)
			{
				await Workspace.RefreshContentAsync();
			}
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = DefaultDisplayName;
		}

		protected override void OnActivate()
		{
			this._eventAggregator.Subscribe(this);
			base.OnActivate();
		}

		private void UpdateDisplayName()
		{
			AuditTreeNode auditNode = AuditForest.CurrentNode as AuditTreeNode;

			string title = auditNode?.AuditTree.Title;
			if (!string.IsNullOrWhiteSpace(title))
			{
				DisplayName = title;
				return;
			}

			DisplayName = DefaultDisplayName;
		}

		private string DefaultDisplayName =>
			string.Format($"{CurrentAssembly.Title} - {CurrentAssembly.Version}");

		protected override void OnDeactivate(bool close)
		{
			this._eventAggregator.Unsubscribe(this);

			AuditForest?.JobScheduler.Shutdown();

			base.OnDeactivate(close);
		}

		public async Task<AuditTree> OpenConnectionAsync(AuditContext context)
		{
			IsTreeLoading = true;

			try
			{
				TemplateInfo templateInfo = await DeserializeTemplateAsync(context);

				return new AuditTree(
					AppSettings,
					context,
					this._queryExecutor,
					XmlLoader,
					this._storageManager,
					this._taskManager,
					this.AuditForest,
					this._webServer.HostUri,
					templateInfo
				);
			}
			finally
			{
				IsTreeLoading = false;
			}
		}

		private async Task<AuditTree> OpenConnectionAsync(AuditContext context, ModuleTypeInfo moduleInfo)
		{
			IsTreeLoading = true;

			try
			{
				await SaveConnectionDataAsync(context, moduleInfo);

				TemplateInfo templateInfo = await DeserializeTemplateAsync(context);
				
				return new AuditTree(
					AppSettings,
					context,
					this._queryExecutor,
					XmlLoader,
					this._storageManager,
					this._taskManager,
					this.AuditForest,
					this._webServer.HostUri,
					templateInfo
				);
			}
			finally
			{
				IsTreeLoading = false;
			}
		}

		private async Task<TemplateInfo> DeserializeTemplateAsync(AuditContext context)
		{
			return await Task.Run(() => DeserializeTemplate(context));
		}

		private TemplateInfo DeserializeTemplate(AuditContext context)
		{
			string templatePath = Path.Combine(
				context.Template.Directory,
				context.Template.Name
			);

			return XmlLoader.LoadTemplate(templatePath);
		}

		private async Task SaveConnectionDataAsync(AuditContext context, ModuleTypeInfo moduleInfo)
		{
			await Task.Run(() =>
			{
				this._storageManager.MainStorage.Connections
					.SaveConnectionGroup(context.ConnectionGroup);

				this._storageManager.MainStorage.Templates
					.SaveTemplate(context.Template);

				SaveLastConnection(context, moduleInfo);
			});
		}

		private void SaveLastConnection(AuditContext context, ModuleTypeInfo moduleInfo)
		{
			string machineName = Environment.MachineName;

			this._storageManager.MainStorage.Connections.UpdateLastConnection(
				context.ConnectionGroup.Id,
				context.Template.Id,
				context.ConnectionGroup.ConnectionType.ToString(),
				moduleInfo.Id,
				machineName
			);
		}
	}
}
