﻿using System;
using System.Threading.Tasks;
using MSSQLServerAuditor.Controls.TreeView;
using MSSQLServerAuditor.Schedule;
using MSSQLServerAuditor.Tree;

namespace MSSQLServerAuditor.ViewModels
{
	public interface IAuditForest
	{
		event EventHandler CurrentNodeChanged;
		event EventHandler TreeChanged;

		AdvTreeNode CurrentNode { get; set; }
		AdvTreeNode Origin      { get; set; }

		Task AddTreeAsync(TemplateTreeRoot root);
		Task InsertTreeAsync(TemplateTreeRoot root, int index);
		Task<bool> RemoveTreeAsync(TemplateTreeRoot root);
		Task RebuildTreeAsync(TemplateTreeRoot root);
		Task CloseTreeAsync(TemplateTreeRoot root);
		Task ClearForestAsync();

		void RefreshTrees();
		void UpdateCurrentNode();

		GuiJobScheduler JobScheduler { get; }
	}
}