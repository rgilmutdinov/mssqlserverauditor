﻿using System;
using System.Linq;
using System.Windows.Media;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Infrastructure;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tree;
using NLog;

namespace MSSQLServerAuditor.ViewModels
{
	public class NodeSettingsViewModel : ValidatingScreen
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IAppSettings     _appSettings;
		private readonly TemplateTreeNode _treeNode;

		private Color?    _selectedColor;
		private NamedIcon _selectedIcon;
		private string    _nodeName;
		private bool      _isDeactivated;

		public NodeSettingsViewModel(IAppSettings appSettings, TemplateTreeNode treeNode)
		{
			Check.NotNull(appSettings, nameof(appSettings));
			Check.NotNull(treeNode,    nameof(treeNode));

			this._appSettings = appSettings;
			this._treeNode    = treeNode;

			ConfigureValidationRules();

			TemplateNodeInfo tnInfo = treeNode.TemplateNodeInfo;

			AllIcons      = new ObservableCollectionEx<NamedIcon>(Icons.All);
			SelectedIcon  = GetNodeIcon(tnInfo);
			SelectedColor = GetNodeColor(tnInfo.FontColor);
			NodeName      = treeNode.Text?.ToString();

			IsDeactivated = tnInfo.IsDisabled;
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.NodeSettings;
		}

		public void Accept()
		{
			ValidationResult result = Validator.ValidateAll();
			if (result.IsValid)
			{
				TryClose(true);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}

		public void SetDefaultName()
		{
			TemplateNodeInfo tnInfo = this._treeNode.TemplateNodeInfo;

			if (!string.IsNullOrWhiteSpace(tnInfo.GroupSelectId))
			{
				NodeName = tnInfo.GetDefaultDatabase();
				return;
			}

			Translation title = tnInfo.Title.FirstOrDefault(
				l => l.Language == _appSettings.User.UiLanguage
			);

			if (title != null)
			{
				NodeName = title.Text;
			}
		}

		public ObservableCollectionEx<NamedIcon> AllIcons { get; set; }

		public string NodeInstanceId
		{
			get
			{
				if (this._treeNode.TemplateNodeInfo.IsSaved)
				{
					return this._treeNode.TemplateNodeInfo.NodeInstanceId.ToString();
				}

				return string.Empty;
			}
		}

		public string NodeInstanceParentId
		{
			get
			{
				TemplateTreeNode parentNode = this._treeNode?.Parent as TemplateTreeNode;
				if (parentNode != null)
				{
					TemplateNodeInfo parentTemplate = parentNode.TemplateNodeInfo;
					if (parentTemplate.IsSaved)
					{
						return parentTemplate.NodeInstanceId.ToString();
					}
				}

				return string.Empty;
			}
		}

		public string ConnectionGroupId => this._treeNode.AuditTree.Context.ConnectionGroup.Id.ToString();

		public string TemplateNodeId
		{
			get
			{
				TemplateNodeInfo template = this._treeNode.TemplateNodeInfo.Template;
				if (template != null && template.TemplateNodeId != 0)
				{
					return template.TemplateNodeId.ToString();
				}

				return string.Empty;
			}
		}

		public string NodeName
		{
			get { return this._nodeName; }
			set
			{
				if (this._nodeName != value)
				{
					this._nodeName = value;

					NotifyOfPropertyChange(() => NodeName);
					Validator.Validate(nameof(NodeName));
				}
			}
		}

		public Color? SelectedColor
		{
			get { return this._selectedColor; }
			set
			{
				if (this._selectedColor != value)
				{
					this._selectedColor = value;
					NotifyOfPropertyChange(() => SelectedColor);
				}
			}
		}

		public NamedIcon SelectedIcon
		{
			get { return this._selectedIcon; }
			set
			{
				if (this._selectedIcon != value)
				{
					this._selectedIcon = value;
					NotifyOfPropertyChange(() => SelectedIcon);
				}
			}
		}

		public bool IsDeactivated
		{
			get { return this._isDeactivated; }
			set
			{
				if (this._isDeactivated != value)
				{
					this._isDeactivated = value;
					NotifyOfPropertyChange(() => IsDeactivated);
				}
			}
		}

		public bool IsDeactivationEnabled
		{
			get
			{
				TemplateTreeNode parentNode = this._treeNode?.Parent as TemplateTreeNode;
				if (parentNode == null)
				{
					return true;
				}

				return parentNode.IsActivated;
			}
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRequiredRule(() => NodeName, Strings.NodeNameIsRequired);
		}

		private NamedIcon GetNodeIcon(TemplateNodeInfo tnInfo)
		{
			NamedIcon nodeIcon = AllIcons.FirstOrDefault(i =>
				i.Name == this._treeNode.AuditTree.GetNamedIcon(tnInfo)?.Name
			);

			return nodeIcon ?? AllIcons.FirstOrDefault();
		}

		private Color? GetNodeColor(string colorHex)
		{
			if (colorHex == null)
			{
				return Colors.Black;
			}

			try
			{
				return ColorUtils.GetColor(colorHex);
			}
			catch (Exception exc)
			{
				Log.Error(exc, "Error parsing template node font color: " + colorHex);

				return Colors.Black;
			}
		}
	}
}
