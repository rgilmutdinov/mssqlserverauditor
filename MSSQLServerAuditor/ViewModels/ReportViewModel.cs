﻿using System.Collections.ObjectModel;
using System.Linq;

namespace MSSQLServerAuditor.ViewModels
{
	public static class ReportHistory
	{
		public static int  LastTabIndex { get; set; } = -1;
		public static long LastReportId { get; set; } = -1;
	}

	public class ReportViewModel : ContentViewModel
	{
		private string _xmlResult;
		private int    _selectedTabIndex;

		public ReportViewModel(long id)
		{
			Id   = id;
			Tabs = new ObservableCollection<TabViewModel>();
		}

		public long Id { get; private set; }

		public string XmlResult
		{
			get { return _xmlResult; }
			set
			{
				if (value != this._xmlResult)
				{
					this._xmlResult = value;
					NotifyOfPropertyChange();
				}
			}
		}

		public ObservableCollection<TabViewModel> Tabs { get; set; }

		public TabViewModel SelectedTab => Tabs.ElementAtOrDefault(SelectedTabIndex);

		public int SelectedTabIndex
		{
			get { return this._selectedTabIndex; }
			set
			{
				ReportHistory.LastTabIndex = value;

				if (this._selectedTabIndex != value)
				{
					this._selectedTabIndex = value;

					NotifyOfPropertyChange(() => SelectedTabIndex);
				}
			}
		}

		public override void Release()
		{
			foreach (TabViewModel tab in Tabs)
			{
				tab?.Release();
			}
		}
	}
}
