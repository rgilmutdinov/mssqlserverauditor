﻿using System;
using Caliburn.Micro;

namespace MSSQLServerAuditor.ViewModels
{
	public class ContentViewModel : PropertyChangedBase
	{
		public DateTime? UpdateDate     { get; set; } = null;
		public TimeSpan? UpdateDuration { get; set; } = null;
		public DateTime? NextUpdateDate { get; set; } = null;

		public virtual void Release()
		{
			
		}
	}
}
