﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Services;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Infrastructure;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;
using NLog;

namespace MSSQLServerAuditor.ViewModels
{
	public class InternalConnectionViewModel : ValidatingScreen, IConnectionDialog<InternalServerInstance>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IStorageManager _storageManager;
		private readonly IAppSettings    _appSettings;

		private readonly List<TemplateFile> _templateFiles;

		private ConnectionTypeInfo _selectedConnectionType;
		private ConnectionGroup    _selectedGroup;
		private ServerInstance     _selectedServerInstance;
		private TemplateViewModel  _selectedTemplate;
		private string             _connectionName;

		private readonly IDisposable _validationSuspender;
		private          bool        _validationSuspenderDisposed;

		public InternalConnectionViewModel(
			IStorageManager    storageManager,
			IAppSettings       appSettings,
			List<TemplateFile> templateFiles)
		{
			Check.NotNull(storageManager, nameof(storageManager));
			Check.NotNull(appSettings,    nameof(appSettings));
			Check.NotNull(templateFiles,  nameof(templateFiles));

			this._storageManager = storageManager;
			this._appSettings    = appSettings;
			this._templateFiles  = templateFiles;

			ConfigureValidationRules();

			// suppress validation till the first attempt to accept
			this._validationSuspender         = Validator.SuppressValidation();
			this._validationSuspenderDisposed = false;

			InitializeCollections();

			SelectedConnectionType = ConnectionTypes.FirstOrDefault();
		}

		private void InitializeCollections()
		{
			ConnectionTypes = new ObservableCollectionEx<ConnectionTypeInfo>(
				this._appSettings.System.ConnectionTypes
			);

			Groups          = new ObservableCollectionEx<ConnectionGroup>();
			ServerInstances = new ObservableCollectionEx<ServerInstance>();
			Templates       = new ObservableCollectionEx<TemplateViewModel>();
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.InternalConnection;
		}

		public ObservableCollectionEx<ConnectionTypeInfo> ConnectionTypes { get; set; }
		public ObservableCollectionEx<ConnectionGroup>    Groups          { get; set; }
		public ObservableCollectionEx<ServerInstance>     ServerInstances { get; set; }
		public ObservableCollectionEx<TemplateViewModel>  Templates       { get; set; }

		public string ConnectionName
		{
			get { return this._connectionName; }
			set
			{
				if (this._connectionName != value)
				{
					this._connectionName = value;

					NotifyOfPropertyChange(() => ConnectionName);
					Validator.Validate(nameof(ConnectionName));
				}
			}
		}

		public ConnectionTypeInfo SelectedConnectionType
		{
			get { return this._selectedConnectionType; }
			set
			{
				if (this._selectedConnectionType != value)
				{
					this._selectedConnectionType = value;

					NotifyTaskCompletion.Create(RefreshConnectionGroups());

					NotifyOfPropertyChange(() => SelectedConnectionType);
					Validator.Validate(nameof(SelectedConnectionType));
				}
			}
		}

		public ConnectionGroup SelectedGroup
		{
			get { return this._selectedGroup; }
			set
			{
				if (this._selectedGroup != value)
				{
					this._selectedGroup = value;

					RefreshServerInstances();

					NotifyOfPropertyChange(() => SelectedGroup);
					Validator.Validate(nameof(SelectedGroup));
				}
			}
		}

		public ServerInstance SelectedServerInstance
		{
			get { return _selectedServerInstance; }
			set
			{
				if (this._selectedServerInstance != value)
				{
					this._selectedServerInstance = value;

					RefreshTemplates();

					NotifyOfPropertyChange(() => SelectedServerInstance);
					NotifyOfPropertyChange(() => SelectedServerInstanceId);
					Validator.Validate(nameof(SelectedServerInstance));
				}
			}
		}

		public TemplateViewModel SelectedTemplate
		{
			get { return this._selectedTemplate; }
			set
			{
				if (this._selectedTemplate != value)
				{
					this._selectedTemplate = value;

					NotifyOfPropertyChange(() => SelectedTemplate);
					NotifyOfPropertyChange(() => SelectedTemplateId);
					Validator.Validate(nameof(SelectedTemplate));
				}
			}
		}

		public long? SelectedTemplateId       => SelectedTemplate?.Template?.Id;
		public long? SelectedServerInstanceId => SelectedServerInstance?.Id;

		public void Accept()
		{
			if (!this._validationSuspenderDisposed)
			{
				// dispose validation suspender to allow on-fly validation
				this._validationSuspender.Dispose();
				this._validationSuspenderDisposed = true;
			}

			ValidationResult validationResult = Validator.ValidateAll();
			if (validationResult.IsValid)
			{
				TryClose(true);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}

		public InternalServerInstance Instance
		{
			get
			{
				return new InternalServerInstance
				{
					ConnectionName          = ConnectionName,
					ConnectionType          = ConnectionType.Internal,
					IsDeleted               = false,
					IsDynamic               = false,
					IsActive                = true,
					Login                   = Login.CreateEmpty(),
					ConnectionGroupSourceId = SelectedGroup.Id,
					ServerInstanceSourceId  = SelectedServerInstance.Id,
					TemplateSourceId        = SelectedTemplate.Template.Id
				};
			}
			set
			{
				Check.NotNull(value, nameof(value));

				ConnectionName = value.ConnectionName;

				ApplyConnectionParameters(value);
			}
		}

		ServerInstance IConnectionDialog.Instance
		{
			get { return Instance; }
			set { Instance = (InternalServerInstance) value; }
		}

		public bool IsKeyModificationEnabled { get; set; }

		private async void ApplyConnectionParameters(InternalServerInstance instance)
		{
			SelectedConnectionType = ConnectionTypes.First(
				t => t.ConnectionType == instance.ConnectionType
			);

			await RefreshConnectionGroups();

			SelectedGroup = Groups.First(
				g => g.Id == instance.ConnectionGroupSourceId
			);

			SelectedServerInstance = ServerInstances.First(
				si => si.Id == instance.ServerInstanceSourceId
			);

			SelectedTemplate = Templates.First(
				t => t.Template.Id == instance.TemplateSourceId
			);
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRequiredRule(() => ConnectionName,         Strings.ConnectionNameIsRequired);
			Validator.AddRequiredRule(() => SelectedConnectionType, Strings.ConnectionTypeIsRequired);
			Validator.AddRequiredRule(() => SelectedGroup,          Strings.ConnectionGroupIsRequired);
			Validator.AddRequiredRule(() => SelectedServerInstance, Strings.ServerInstanceIsRequired);
			Validator.AddRequiredRule(() => SelectedTemplate,       Strings.TemplateIsRequired);
		}

		private async Task RefreshConnectionGroups()
		{
			if (SelectedConnectionType == null)
			{
				Groups.Clear();
				return;
			}

			ConnectionService service = this._storageManager
				.MainStorage.Connections;

			List<ConnectionGroup> groups = await Task.Run(() => service.GetConnectionGroups(
				SelectedConnectionType.ConnectionType
			));

			Groups.ReplaceRange(groups);

			SelectedGroup = Groups.FirstOrDefault();
		}

		private void RefreshServerInstances()
		{
			if (SelectedGroup == null)
			{
				ServerInstances.Clear();
				return;
			}

			ServerInstances.ReplaceRange(SelectedGroup.ServerInstances);

			SelectedServerInstance = ServerInstances.FirstOrDefault();
		}

		private void RefreshTemplates()
		{
			if (SelectedServerInstance == null)
			{
				Templates.Clear();
				return;
			}

			List<Template> templates = this._storageManager.MainStorage.Templates
					.GetTemplates(SelectedServerInstance.ConnectionName, SelectedServerInstance.ConnectionType)
					.Where(t => !Path.IsPathRooted(t.Directory))
					.ToList();

			List<TemplateViewModel> templateVms = new List<TemplateViewModel>();
			foreach (Template template in templates)
			{
				TemplateFile templateFile = this._templateFiles
					.FirstOrDefault(t => t.Content.Id == template.UId);

				TemplateViewModel vm = new TemplateViewModel(
					template,
					templateFile?.Content
				);

				templateVms.Add(vm);
			}

			Templates.ReplaceRange(templateVms);

			SelectedTemplate = Templates.FirstOrDefault();
		}
	}
}
