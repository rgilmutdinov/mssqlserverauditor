﻿using MSSQLServerAuditor.Presentation.Localization;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.ViewModels
{
	public enum AuthenticationMode
	{
		[Localized("AuthModeSqlServer", typeof(Strings))]
		SqlServer,

		[Localized("AuthModeWindows", typeof(Strings))]
		Windows
	}
}
