﻿using Caliburn.Micro;
using MSSQLServerAuditor.Core.Reporting;

namespace MSSQLServerAuditor.ViewModels
{
	public class TabViewModel : PropertyChangedBase
	{
		private string        _header;
		private IVisualReport _visualReport;

		public TabViewModel()
		{
			
		}

		public string Header
		{
			get { return this._header; }
			set
			{
				if (this._header != value)
				{
					this._header = value;
					NotifyOfPropertyChange(() => Header);
				}
			}
		}

		public IVisualReport VisualReport
		{
			get { return this._visualReport; }
			set
			{
				if (!Equals(this._visualReport, value))
				{
					this._visualReport = value;
					NotifyOfPropertyChange(() => VisualReport);
				}
			}
		}

		public void Release()
		{
			VisualReport?.Release();
		}
	}
}
