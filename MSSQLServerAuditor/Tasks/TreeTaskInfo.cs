﻿using System;
using System.Threading;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Tree;

namespace MSSQLServerAuditor.Tasks
{
	public class TreeTaskInfo
	{
		public TreeTaskInfo(AuditTreeNode treeNode)
		{
			Check.NotNull(treeNode, nameof(treeNode));

			TreeNode = treeNode;
		}

		public AuditTreeNode TreeNode { get; private set; }

		public bool          Update               { get; set; } = true;
		public bool          UpdateHierarchically { get; set; } = false;
		public int           UpdateDeep           { get; set; } = -1; // infinite deep

		public FetchMode     FetchMode            { get; set; } = FetchMode.Combined;
		public TreeTaskInfo  ParentTask           { get; set; }
		public bool          Expand               { get; set; } = false;

		public Action<CancellationToken> PostUpdateAction { get; set; }

		public long Id => TreeNode.GetHashCode();
	}
}
