﻿using System.Windows.Media.Imaging;

namespace MSSQLServerAuditor.Images
{
	public class NamedIcon
	{
		public NamedIcon(BitmapImage image, string name)
		{
			this.Image = image;
			this.Name  = name;
		}

		public BitmapImage Image { get; }
		public string      Name  { get; }
	}
}