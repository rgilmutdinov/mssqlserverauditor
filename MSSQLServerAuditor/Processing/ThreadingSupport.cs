﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Tree;

namespace MSSQLServerAuditor.Processing
{
	/// <summary>
	/// Adds threading support to nodes
	/// </summary>
	public sealed class ThreadingSupport
	{
		private readonly AuditTree        _auditTree;
		private Task<List<AuditTreeNode>> _fetchChildrenTask;
		
		public ThreadingSupport(AuditTree auditTree)
		{
			this._auditTree = auditTree;
		}

		public bool IsRunning => this._fetchChildrenTask != null && !this._fetchChildrenTask.IsCompleted;

		/// <summary>
		/// Starts loading the children of the specified node.
		/// </summary>
		public async Task LoadChildrenAsync(
			AuditTreeNode     node,
			FetchMode         mode,
			CancellationToken token,
			Func<FetchMode, CancellationToken, IEnumerable<AuditTreeNode>> fetchChildren)
		{
			if (IsRunning)
			{
				// load children task is already be running
				return;
			}

			node.Children.Clear();
			
			AuditTreeNode loadingNode = new LoadingTreeNode(this._auditTree);
			try
			{
				this._fetchChildrenTask = Task.Run(() =>
					{
						IEnumerable<AuditTreeNode> fetchedChildren = fetchChildren(mode, token);

						List<AuditTreeNode> result = new List<AuditTreeNode>();
						foreach (AuditTreeNode child in fetchedChildren)
						{
							token.ThrowIfCancellationRequested();
							result.Add(child);
						}
						return result;
					},
					token
				);

				// Give the task a bit time to complete before we return to WPF - this keeps "Loading..."
				// from showing up for very short waits.
				Task delayLoadingShowUp = Task.Delay(200, token);
				Task firstFinished      = await Task.WhenAny(this._fetchChildrenTask, delayLoadingShowUp);

				if (firstFinished != this._fetchChildrenTask)
				{
					node.Children.Add(loadingNode);
				}

				List<AuditTreeNode> childNodes = await this._fetchChildrenTask;
				foreach (AuditTreeNode childNode in childNodes)
				{
					node.Children.Add(childNode);
				}

				await TaskConstants.Completed;
			}
			catch (Exception exc)
			{
				AuditTreeNode errorNode = new ErrorTreeNode(this._auditTree, exc.Message);

				node.Children.Add(errorNode);
			}
			finally
			{
				if (node.Children.Contains(loadingNode))
				{
					node.Children.Remove(loadingNode); // remove 'Loading...'
				}
			}
		}
	}
}
