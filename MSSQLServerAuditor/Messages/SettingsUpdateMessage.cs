﻿using MSSQLServerAuditor.Core.Settings;

namespace MSSQLServerAuditor.Messages
{
	public class SettingsUpdateMessage
	{
		public SettingsUpdateMessage(UserSettings oldSettings, UserSettings newSettings)
		{
			this.OldSettings = oldSettings;
			this.NewSettings = newSettings;
		}

		public UserSettings OldSettings { get; }
		public UserSettings NewSettings { get; }
	}
}
