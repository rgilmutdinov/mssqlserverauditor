﻿using System;
using System.Windows.Media;
using MSSQLServerAuditor.Common.Extensions;

namespace MSSQLServerAuditor.Infrastructure
{
	public static class ColorUtils
	{
		public static Color GetColor(string colorHex)
		{
			if (!colorHex.IsNullOrEmpty())
			{
				object colorObj = ColorConverter.ConvertFromString(colorHex);
				if (colorObj is Color)
				{
					return (Color) colorObj;
				}
			}

			throw new ArgumentException("Failed to parse color: " + colorHex);
		}

		public static Brush GetBrush(string colorHex)
		{
			return new SolidColorBrush(GetColor(colorHex));
		}
	}
}
