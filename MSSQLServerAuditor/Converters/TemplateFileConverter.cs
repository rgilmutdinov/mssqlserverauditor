﻿using System;
using System.Globalization;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Presentation.Converters;

namespace MSSQLServerAuditor.Converters
{
	public class TemplateFileConverter : ConverterBase
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string lang = culture.TwoLetterISOLanguageName;
			TemplateFile templateFile = value as TemplateFile;
			if (templateFile != null)
			{
				TemplateInfo templateInfo = templateFile.Content;
				foreach (Translation title in templateInfo.TemplateTitle)
				{
					if (lang.EqualsIgnoreCase(title.Language))
					{
						return title.Text.TrimmedOrEmpty();
					}
				}

				return templateInfo.Id.TrimmedOrEmpty();
			}

			return string.Empty;
		}
	}
}
