﻿using System;
using System.Globalization;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Presentation.Converters;

namespace MSSQLServerAuditor.Converters
{
	public class ModuleTypeInfoConverter : ConverterBase
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string lang = culture.TwoLetterISOLanguageName;
			ModuleTypeInfo module = value as ModuleTypeInfo;
			if (module != null)
			{
				foreach (LangResource title in module.Title)
				{
					if (lang.EqualsIgnoreCase(title.Language))
					{
						return title.Text;
					}
				}

				return module.Id;
			}

			return string.Empty;
		}
	}
}
