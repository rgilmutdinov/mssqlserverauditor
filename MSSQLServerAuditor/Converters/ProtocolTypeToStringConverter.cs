﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using MSSQLServerAuditor.Core.Querying.Connections;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.Converters
{
	public class ProtocolTypeToStringConverter : MarkupExtension, IValueConverter
	{
		public static readonly ProtocolTypeToStringConverter Instance = new ProtocolTypeToStringConverter();

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return Instance;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			ProtocolType? protocol = value as ProtocolType?;
			if (protocol != null)
			{
				switch (protocol.Value)
				{
					case ProtocolType.Icmp: return Strings.ICMP;
					case ProtocolType.Tcp:  return Strings.TCP;
					case ProtocolType.Udp:  return Strings.UDP;

					default: throw new ArgumentOutOfRangeException(nameof(protocol));
				}
			}

			return string.Empty;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return DependencyProperty.UnsetValue;
		}
	}
}
