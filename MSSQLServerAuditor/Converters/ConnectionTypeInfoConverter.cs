﻿using System;
using System.Globalization;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Presentation.Converters;

namespace MSSQLServerAuditor.Converters
{
	public class ConnectionTypeInfoConverter : ConverterBase
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string lang = culture.TwoLetterISOLanguageName;
			ConnectionTypeInfo connectionType = value as ConnectionTypeInfo;
			if (connectionType != null)
			{
				foreach (LangResource title in connectionType.Title)
				{
					if (lang.EqualsIgnoreCase(title.Language))
					{
						return title.Text;
					}
				}

				return connectionType.Id;
			}

			return string.Empty;
		}
	}
}
