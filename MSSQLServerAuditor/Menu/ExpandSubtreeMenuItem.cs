﻿using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tasks;
using MSSQLServerAuditor.Tree;

namespace MSSQLServerAuditor.Menu
{
	public class ExpandSubtreeMenuItem : ContextMenuItem
	{
		public override bool IsEnabled(MenuItemContext context)
		{
			AuditTreeNode auditNode = context.AuditNode;
			return auditNode.CanExpand;
		}

		public override BitmapImage GetIcon(MenuItemContext context)
		{
			return StaticImages.Expand;
		}

		public override string GetHeader(MenuItemContext context)
		{
			return Strings.ExpandHierarchically;
		}

		public override async Task ExecuteAsync(MenuItemContext context)
		{
			AuditTreeNode node = context.AuditNode;
			AuditTree     tree = node.AuditTree;

			TreeTaskInfo taskInfo = new TreeTaskInfo(node)
			{
				FetchMode            = FetchMode.Combined,
				UpdateHierarchically = true,
				Update               = false,
				Expand               = true
			};

			tree.RequestUpdate(taskInfo);

			await TaskConstants.Completed;
		}
	}
}
