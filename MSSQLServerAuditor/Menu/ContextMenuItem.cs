﻿using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MSSQLServerAuditor.Menu
{
	public abstract class ContextMenuItem : IContextMenuItem
	{
		public virtual bool IsVisible(MenuItemContext context) => true;
		public virtual bool IsEnabled(MenuItemContext context) => true;

		public abstract Task ExecuteAsync(MenuItemContext context);

		public abstract BitmapImage GetIcon(MenuItemContext context);
		public abstract string      GetHeader(MenuItemContext context);
	}
}
