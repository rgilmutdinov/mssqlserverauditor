﻿using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tree;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Menu
{
	public class RebuildTreeMenuItem : ContextMenuItem
	{
		public override async Task ExecuteAsync(MenuItemContext context)
		{
			TemplateTreeRoot rootNode = context.AuditNode as TemplateTreeRoot;

			if (rootNode != null)
			{
				AuditTree    tree   = rootNode.AuditTree;
				IAuditForest forest = tree.AuditForest;

				if (forest != null)
				{
					await forest.RebuildTreeAsync(rootNode);
				}
			}

			await TaskConstants.Completed;
		}

		public override bool IsEnabled(MenuItemContext context)
		{
			return context.AuditNode is TemplateTreeRoot;
		}

		public override BitmapImage GetIcon(MenuItemContext context)
		{
			return StaticImages.RebuildTree;
		}

		public override string GetHeader(MenuItemContext context)
		{
			return Strings.RebuildTree;
		}
	}
}
