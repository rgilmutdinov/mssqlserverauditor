﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tree;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Menu
{
	public class OpenConnectionPropertiesMenuItem : ContextMenuItem
	{
		public override BitmapImage GetIcon(MenuItemContext context)
		{
			return StaticImages.Properties;
		}

		public override string GetHeader(MenuItemContext context)
		{
			return Strings.Properties + "...";
		}

		public override async Task ExecuteAsync(MenuItemContext context)
		{
			AuditTree auditTree = context.AuditNode.AuditTree;
			if (auditTree != null)
			{
				AuditContext auditContext = auditTree.Context;
				ConnectionGroupViewModel dialog = new ConnectionGroupViewModel(
					auditTree.StorageManager,
					context.WindowManager,
					context.AppSettings,
					auditTree.XmlLoader
				)
				{
					IsEditMode = true
				};

				await dialog.SetAuditContextAsync(auditContext);

				bool ok = context.WindowManager.ShowDialog(dialog) ?? false;

				if (ok)
				{
					List<ServerInstance> newInstances = dialog
						.SelectedAuditContext.ConnectionGroup.ServerInstances;

					// update the list of connections for current group
					auditContext.ConnectionGroup.ServerInstances = newInstances;

					// save changes to database
					auditTree.StorageManager.MainStorage.Connections.UpdateGroupConnections(
						auditContext.ConnectionGroup
					);
				}
			}

			await TaskConstants.Completed;
		}
	}
}
