﻿using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MSSQLServerAuditor.Menu
{
	public interface IContextMenuItem
	{
		bool IsVisible(MenuItemContext context);
		bool IsEnabled(MenuItemContext context);

		Task ExecuteAsync(MenuItemContext context);

		BitmapImage GetIcon(MenuItemContext context);
		string      GetHeader(MenuItemContext context);
	}
}
