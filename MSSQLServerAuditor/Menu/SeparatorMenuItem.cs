﻿using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Common.Async;

namespace MSSQLServerAuditor.Menu
{
	public class SeparatorMenuItem : ContextMenuItem
	{
		public static readonly SeparatorMenuItem Instance = new SeparatorMenuItem();

		private SeparatorMenuItem()
		{
			
		}

		public override async Task ExecuteAsync(MenuItemContext context)
		{
			await TaskConstants.Completed;
		}

		public override BitmapImage GetIcon(MenuItemContext context)
		{
			return null;
		}

		public override string GetHeader(MenuItemContext context)
		{
			return string.Empty;
		}
	}
}
