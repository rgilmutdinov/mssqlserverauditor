﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Schedule;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tree;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Menu
{
	public class OpenScheduleSettingsMenuItem : ContextMenuItem
	{
		public override BitmapImage GetIcon(MenuItemContext context)
		{
			return StaticImages.Schedule;
		}

		public override string GetHeader(MenuItemContext context)
		{
			return Strings.ScheduleSettings;
		}

		public override async Task ExecuteAsync(MenuItemContext context)
		{
			TemplateTreeNode treeNode = context.AuditNode as TemplateTreeNode;

			if (treeNode == null)
			{
				throw new ArgumentException(
					"Node settings menu is not implemented for tree node type: " + context.AuditNode.GetType()
				);
			}

			TemplateNodeInfo tnInfo = treeNode.TemplateNodeInfo;
			AuditTree        tree   = treeNode.AuditTree;

			List<ScheduleSettings> schedules = tree.LoadSchedules(tnInfo);

			ScheduleSettingsViewModel scheduleDialog = new ScheduleSettingsViewModel(
				schedules,
				context.WindowManager,
				context.AppSettings
			);

			bool ok = context.WindowManager.ShowDialog(scheduleDialog) ?? false;

			if (ok)
			{
				ObservableCollection<ScheduleViewModel> updatedSchedules = scheduleDialog.Schedules;
				List<ScheduleSettings> updatedSettings = new List<ScheduleSettings>();
				foreach (ScheduleViewModel scheduleVm in updatedSchedules)
				{
					ScheduleSettings settings = new ScheduleSettings
					{
						NodeInstanceId          = tnInfo.NodeInstanceId,
						UId                     = scheduleVm.Id,
						Name                    = scheduleVm.ScheduleName,
						CronExpression          = scheduleVm.CronExpression,
						IsEnabled               = scheduleVm.IsScheduleEnabled,
						SendMessage             = scheduleVm.IsEmailEnabled,
						SendMessageNonEmptyOnly = scheduleVm.SendNonEmptyNotificationsOnly,
						ServiceOnly             = scheduleVm.IsServiceOnly,
						MessageLanguage         = scheduleVm.NotificationLanguage,
						TargetMachine           = scheduleVm.TargetMachine,
						MessageRecipients       = scheduleVm.Recipients,
						UpdateHierarchically    = scheduleVm.UpdateHierarchically,
						UpdateDeep              = scheduleVm.UpdateDeep
					};

					updatedSettings.Add(settings);
				}

				await tree.SaveSchedulesAsync(tnInfo, updatedSettings);

				tnInfo.HasActiveSchedules = await tree.HasActiveSchedulesAsync(tnInfo);

				treeNode.Refresh();

				List<JobInfo> updatedJobs = tree.StorageManager.MainStorage
					.Settings.GetJobs(tnInfo);

				await tree.AuditForest.JobScheduler
					.ResheduleJobsAsync(tree.Context, updatedJobs);

				DateTime? nextUpdate = tree.GetNextUpdateTime(updatedJobs);
				context.Workspace.SetNextUpdateDate(nextUpdate);
			}

			await TaskConstants.Completed;
		}
	}
}
