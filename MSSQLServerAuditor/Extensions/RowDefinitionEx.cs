﻿using System.Windows;
using System.Windows.Controls;

namespace MSSQLServerAuditor.Extensions
{
	public class RowDefinitionEx : RowDefinition
	{
		public static readonly DependencyProperty VisibleProperty;

		public bool Visible
		{
			get { return (bool) GetValue(VisibleProperty); }
			set { SetValue(VisibleProperty, value); }
		}

		static RowDefinitionEx()
		{
			VisibleProperty = DependencyProperty.Register(
				nameof(Visible),
				typeof(bool),
				typeof(RowDefinitionEx),
				new PropertyMetadata(true, OnVisibleChanged)
			);

			HeightProperty.OverrideMetadata(
				typeof(RowDefinitionEx),
				new FrameworkPropertyMetadata(new GridLength(1, GridUnitType.Star), null, CoerceHeight)
			);

			MinHeightProperty.OverrideMetadata(
				typeof(RowDefinitionEx),
				new FrameworkPropertyMetadata((double)0, null, CoerceMinHeight)
			);
		}

		public static void SetVisible(DependencyObject obj, bool nVisible)
		{
			obj.SetValue(VisibleProperty, nVisible);
		}

		public static bool GetVisible(DependencyObject obj)
		{
			return (bool) obj.GetValue(VisibleProperty);
		}

		private static void OnVisibleChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			obj.CoerceValue(HeightProperty);
			obj.CoerceValue(MinHeightProperty);
		}

		private static object CoerceHeight(DependencyObject obj, object nValue)
		{
			return ((RowDefinitionEx) obj).Visible ? nValue : new GridLength(0);
		}

		private static object CoerceMinHeight(DependencyObject obj, object nValue)
		{
			return ((RowDefinitionEx) obj).Visible ? nValue : (double)0;
		}
	}
}
