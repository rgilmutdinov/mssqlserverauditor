﻿using System.Collections.Generic;
using System.Diagnostics;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Schedule;
using MSSQLServerAuditor.Tree;
using NLog;

namespace MSSQLServerAuditor.Schedule
{
	public class ScheduleTree
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly TemplateTreeRoot _templateRoot;

		private TemplateNodeInfo _staticRoot;
		private readonly object  _lock;

		public ScheduleTree(TemplateTreeRoot root)
		{
			this._templateRoot = root;
			this._lock         = new object();
		}

		public TemplateNodeInfo StaticRoot
		{
			get
			{
				if (this._staticRoot == null)
				{
					lock (this._lock)
					{
						if (this._staticRoot == null)
						{
							this._staticRoot = LoadStaticTree();
						}
					}
				}

				return this._staticRoot;
			}
		}

		public AuditTree AuditTree => this._templateRoot.AuditTree;

		private TemplateNodeInfo LoadStaticTree()
		{
			return InstatiateStaticTree(
				this._templateRoot.TemplateInfo.CreateRoot(),
				null
			);
		}

		public TemplateNodeInfo InstatiateStaticTree(TemplateNodeInfo tnInfo, TemplateNodeInfo parent)
		{
			TemplateNodeInfo nodeInstance = tnInfo.IsInstance ? tnInfo : tnInfo.Instantiate(null, parent);

			AuditTree.UpdateChildren(nodeInstance);

			foreach (TemplateNodeInfo child in nodeInstance.Children)
			{
				InstatiateStaticTree(child, nodeInstance);
			}

			return nodeInstance;
		}

		public List<JobInfo> GetScheduleJobs()
		{
			List<JobInfo> jobs = new List<JobInfo>();

			GetScheduleJobs(StaticRoot, jobs);

			return jobs;
		}

		private void GetScheduleJobs(TemplateNodeInfo tnInstance, List<JobInfo> allJobs)
		{
			Debug.Assert(tnInstance.IsInstance);

			List<JobInfo> jobs = AuditTree.StorageManager
				.MainStorage.Settings.GetJobs(tnInstance);

			foreach (JobInfo jobInfo in jobs)
			{
				if (jobInfo != null)
				{
					allJobs.Add(jobInfo);
				}
			}

			foreach (TemplateNodeInfo child in tnInstance.Children)
			{
				GetScheduleJobs(child, allJobs);
			}
		}
	}
}
