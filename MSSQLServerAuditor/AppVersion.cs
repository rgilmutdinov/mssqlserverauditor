﻿namespace MSSQLServerAuditor
{
	/// <summary>
	/// The app edition.
	/// </summary>
	public enum AppEdition
	{
		/// <summary>
		/// The debug.
		/// </summary>
		Debug,

		/// <summary>
		/// The release.
		/// </summary>
		Release
	}

	public static class AppVersion
	{
		/// <summary>
		/// Gets the current app version.
		/// </summary>
		public static AppEdition Edition
		{
			get
			{
#if DEBUG
				return AppEdition.Debug;
#else
				return AppEdition.Release;
#endif
			}
		}
	}
}
