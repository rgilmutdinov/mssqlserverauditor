﻿using System.Windows;

namespace MSSQLServerAuditor.Views
{
	/// <summary>
	/// Interaction logic for InternalConnectionView.xaml
	/// </summary>
	public partial class InternalConnectionView : Window
	{
		public InternalConnectionView()
		{
			InitializeComponent();
		}
	}
}
