﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Menu;
using MSSQLServerAuditor.Processing;
using MSSQLServerAuditor.Tasks;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Tree
{
	public sealed class TemplateTreeRoot : AuditTreeNode
	{
		private readonly TemplateNodeInfo _rootInfo;
		private readonly TemplateInfo     _templateInfo;
		private readonly TreeNodeFactory  _factory;
		private readonly ThreadingSupport _threading;

		public TemplateTreeRoot(
			TemplateInfo    templateInfo,
			TreeNodeFactory factory,
			AuditTree       auditTree)
			: base(auditTree)
		{
			Check.NotNull(templateInfo, nameof(templateInfo));
			Check.NotNull(factory,      nameof(factory));

			this._templateInfo = templateInfo;
			this._rootInfo     = templateInfo.CreateRoot().Instantiate(null, null);
			this._factory      = factory;

			this.LazyLoading = true;
			
			this._threading = new ThreadingSupport(AuditTree);
		}

		public TemplateInfo TemplateInfo => this._templateInfo;

		public override object Text
		{
			get
			{
				string title = AuditTree.FormatTitle(this._templateInfo.TreeTitle);

				if (!string.IsNullOrWhiteSpace(title))
				{
					return title;
				}

				return StringUtils.FirstNonEmpty(
					AuditTree.GetUiText(this._templateInfo.TemplateTitle),
					this._templateInfo.Id
				);
			}
		}

		public override List<ContextMenuItem> CreateMenuItems()
		{
			List<ContextMenuItem> menuItems = new List<ContextMenuItem>
			{
				new UpdateSubtreeMenuItem(),
				new ExpandSubtreeMenuItem(),
				new CollapseSubtreeMenuItem(),

				SeparatorMenuItem.Instance,

				new RebuildTreeMenuItem(),
				new OpenConnectionPropertiesMenuItem(),

				SeparatorMenuItem.Instance,

				new CloseConnectionMenuItem()
			};

			return menuItems;
		}

		public override bool CanExpand   => !this._rootInfo.Template.Children.IsNullOrEmpty() && !IsExpanded;
		public override bool CanCollapse => IsExpanded;

		protected override async Task LoadChildrenAsync(FetchMode fetchMode, CancellationToken token)
		{
			if (this._rootInfo.Template.Children.Any())
			{
				await this._threading.LoadChildrenAsync(this, fetchMode, token, FetchChildren);
			}
		}

		protected override async Task LoadChildrenOnExpandAsync(CancellationToken token)
		{
			TreeTaskInfo taskInfo = new TreeTaskInfo(this)
			{
				FetchMode            = FetchMode.Combined,
				UpdateHierarchically = false,
				Update               = false,
				Expand               = true
			};

			if (!token.IsCancellationRequested)
			{
				AuditTree.RequestUpdate(taskInfo);
			}

			await TaskConstants.Completed;
		}

		private IEnumerable<AuditTreeNode> FetchChildren(FetchMode fetchMode, CancellationToken cancelToken)
		{
			return this._factory.FetchChildren(
				this._rootInfo,
				fetchMode,
				cancelToken
			);
		}

		public override async Task<ContentViewModel> CreateContentAsync(CancellationToken cancelToken)
		{
			return await Task.FromResult(new TextContentViewModel {Text = Text.ToString()});
		}
	}
}
