﻿using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Tree
{
	public class LoadingTreeNode : AuditTreeNode
	{
		public LoadingTreeNode(AuditTree auditTree) : base(auditTree)
		{

		}

		public override object Icon => StaticImages.Waiting;

		public override object Text => Strings.Loading;

		public override async Task<ContentViewModel> CreateContentAsync(CancellationToken cancelToken)
		{
			return await Task.FromResult(new TextContentViewModel { Text = Strings.LoadingWait });
		}
	}
}
