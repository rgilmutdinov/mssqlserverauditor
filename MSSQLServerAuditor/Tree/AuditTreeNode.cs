﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Controls.TreeView;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Menu;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Tree
{
	public abstract class AuditTreeNode : AdvTreeNode
	{
		public AuditTreeNode(AuditTree auditTree)
		{
			Check.NotNull(auditTree, nameof(auditTree));

			AuditTree = auditTree;
		}

		public AuditTree AuditTree { get; }

		public virtual List<ContextMenuItem> CreateMenuItems()
		{
			return Lists.Empty<ContextMenuItem>();
		}

		public abstract Task<ContentViewModel> CreateContentAsync(CancellationToken cancelToken);

		public virtual async Task ExecuteUpdateAsync(CancellationToken token)
		{
			await TaskConstants.Completed;
		}

		public virtual bool CanExpand   => false;
		public virtual bool CanCollapse => false;

		public async Task EnsureLazyChildren(FetchMode fetchMode, CancellationToken token, bool reload = false)
		{
			if (LazyLoading || reload)
			{
				LazyLoading = false;

				if (!token.IsCancellationRequested)
				{
					await LoadChildrenAsync(fetchMode, token);
				}
			}
		}

		protected virtual async Task LoadChildrenAsync(FetchMode fetchMode, CancellationToken token)
		{
			await Common.Async.Tasks.FromException(
				new NotSupportedException(GetType().Name + " does not support async lazy loading")
			);
		}
	}
}
