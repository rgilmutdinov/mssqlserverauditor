﻿using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Tree
{
	public class ErrorTreeNode : AuditTreeNode
	{
		private readonly string _errorText;

		public override object Text => _errorText;

		public ErrorTreeNode(AuditTree auditTree, string errorText) : base(auditTree)
		{
			this._errorText = errorText;
		}

		public override async Task<ContentViewModel> CreateContentAsync(CancellationToken cancelToken)
		{
			return await Task.FromResult(new TextContentViewModel { Text = this._errorText, IsError = true });
		}
	}
}
