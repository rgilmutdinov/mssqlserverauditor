﻿using System.Linq;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Session;

namespace MSSQLServerAuditor.Utils
{
	public class TitleFormatter
	{
		private readonly AuditContext _context;
		private readonly TemplateInfo _template;

		public TitleFormatter(AuditContext context, TemplateInfo template)
		{
			this._context  = context;
			this._template = template;
		}

		public  string Format(string titlePattern, string lang)
		{
			string title = titlePattern;

			Translation templateName = this._template.TemplateTitle
				.FirstOrDefault(t => t.Language == lang);

			title = title.Replace("$ApplicationProductName$",    $"{CurrentAssembly.Title}");
			title = title.Replace("$ApplicationProductVersion$", $"{CurrentAssembly.Version}");
			title = title.Replace("$ConnectionName$",            $"{this._context.ConnectionGroup.Name}");
			title = title.Replace("$ModuleName$",                $"{templateName?.Text.TrimmedOrEmpty()}");

			return title;
		}
	}
}
