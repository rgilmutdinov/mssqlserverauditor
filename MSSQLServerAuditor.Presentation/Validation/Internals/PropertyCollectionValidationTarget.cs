﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Presentation.Validation.Internals
{
	internal class PropertyCollectionValidationTarget : IValidationTarget
	{
		[SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
		public PropertyCollectionValidationTarget(IEnumerable<string> properties)
		{
			Check.NotNull(properties != null, nameof(properties));
			Check.Assert(properties != null && properties.Any(), "properties.Any()");

			Properties = properties;
		}

		private IEnumerable<string> Properties { get; set; }

		#region IValidationTarget Members

		public IEnumerable<object> UnwrapTargets()
		{
			return Properties.ToArray();
		}

		public bool IsMatch(object target)
		{
			return Properties.Any(p => Equals(p, target));
		}

		#endregion
	}
}