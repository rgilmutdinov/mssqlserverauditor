﻿using System.Threading.Tasks;

namespace MSSQLServerAuditor.Presentation.Validation
{
	/// <summary>
	/// Represents an object that can be validated.
	/// </summary>
	public interface IValidatable
	{
		/// <summary>
		/// Validates the object asyncrhonously.
		/// </summary>
		/// <returns>Task that represents the validation operation.</returns>
		Task<ValidationResult> Validate();
	}
}