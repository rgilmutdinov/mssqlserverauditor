﻿using System;

namespace MSSQLServerAuditor.Presentation.Validation
{
	/// <summary>
	/// Represents a validation rule.
	/// </summary>
	public interface IValidationRule
	{
		/// <summary>
		/// Allows changing the rule settings. 
		/// </summary>
		/// <param name="setSettingsDelegate">A function that accepts an instance of <see cref="ValidationRuleSettings"/> that contains settings for this rule.</param>
		/// <returns>The same fule instance (allows for "fluent" interface with chained calls).</returns>
		IValidationRule WithSettings(Action<ValidationRuleSettings> setSettingsDelegate);
	}
}