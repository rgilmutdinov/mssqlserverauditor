﻿namespace MSSQLServerAuditor.Presentation.Validation
{
	/// <summary>
	/// Represents an asynchronous validation rule.
	/// </summary>
	public interface IAsyncValidationRule : IValidationRule
	{
	}
}