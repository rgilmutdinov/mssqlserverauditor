﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Caliburn.Micro;

namespace MSSQLServerAuditor.Presentation
{
	public class PresentationBootstrapper : BootstrapperBase
	{
		protected override IEnumerable<Assembly> SelectAssemblies()
		{
			List<Assembly> assemblies = base.SelectAssemblies().ToList();

			Assembly thisAssembly = typeof(PresentationBootstrapper).GetTypeInfo().Assembly;
			if (assemblies.All(a => a != thisAssembly))
			{
				assemblies.Add(thisAssembly);
			}

			return assemblies;
		}
	}
}
