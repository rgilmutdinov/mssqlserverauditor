using System;

namespace MSSQLServerAuditor.Presentation.Menu.System
{
	public class SystemMenuItem
	{
		public static readonly SystemMenuItem Separator = new SystemMenuItem
		{
			IsSeparator = true
		};

		public uint   Id          { get; set; }
		public string Text        { get; set; }
		public bool   IsSeparator { get; set; }
		public Action Action      { get; set; }
	}
}