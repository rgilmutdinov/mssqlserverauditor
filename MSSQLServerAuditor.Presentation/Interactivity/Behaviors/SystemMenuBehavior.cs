﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Interactivity;
using MSSQLServerAuditor.Presentation.Menu.System;

namespace MSSQLServerAuditor.Presentation.Interactivity.Behaviors
{
	public class SystemMenuBehavior : Behavior<Window>
	{
		public static readonly DependencyProperty MenuItemsProperty = DependencyProperty.Register(
			nameof(MenuItems),
			typeof(ObservableCollection<SystemMenuItem>),
			typeof(SystemMenuBehavior),
			new FrameworkPropertyMetadata(null, MenuItemsChanged)
		);

		private static void MenuItemsChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			SystemMenuBehavior me = sender as SystemMenuBehavior;
			if (me == null)
			{
				return;
			}

			if (e.OldValue != e.NewValue)
			{
				me.CreateSystemMenu();
			}

			INotifyCollectionChanged oldCollection = e.OldValue as INotifyCollectionChanged;
			INotifyCollectionChanged newCollection = e.NewValue as INotifyCollectionChanged;

			if (oldCollection != null)
			{
				oldCollection.CollectionChanged -= me.OnMenuItemsCollectionChanged;
			}

			if (newCollection != null)
			{
				newCollection.CollectionChanged += me.OnMenuItemsCollectionChanged;
			}
		}

		private void OnMenuItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			CreateSystemMenu();
		}

		public ObservableCollection<SystemMenuItem> MenuItems
		{
			get { return (ObservableCollection<SystemMenuItem>) GetValue(MenuItemsProperty); }
			set { SetValue(MenuItemsProperty, value); }
		}

		private void CreateSystemMenu()
		{
			SystemMenu systemMenu = new SystemMenu(AssociatedObject);
			
			if (MenuItems != null)
			{
				foreach (SystemMenuItem menuItem in MenuItems)
				{
					systemMenu.AppendItem(menuItem);
				}
			}

			systemMenu.Apply();
		}
	}
}
