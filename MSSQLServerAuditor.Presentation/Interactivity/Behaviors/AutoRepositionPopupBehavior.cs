﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;

namespace MSSQLServerAuditor.Presentation.Interactivity.Behaviors
{
	/// <summary>
	/// A behavior that forces the associated popup to update its position when the <see cref="Popup.PlacementTarget"/>
	/// location or size has changed.
	/// </summary>
	public class AutoRepositionPopupBehavior : Behavior<Popup>
	{
		private bool _eventsAttached = false;

		protected override void OnAttached()
		{
			base.OnAttached();

			// use Opened, as {Binding} based PlacementTargets may not yet be initialized
			this.AssociatedObject.Opened += AssociatedObject_Opened;
		}

		void AssociatedObject_Opened(object sender, EventArgs e)
		{
			if (!this._eventsAttached)
			{
				Window window = Window.GetWindow(this.AssociatedObject);

				if (window != null)
				{
					window.LocationChanged += window_LocationChanged;
					window.SizeChanged     += window_SizeChanged;

					this._eventsAttached = true;
				}
			}
		}

		private void window_LocationChanged(object sender, EventArgs e) { this.ResetHorizontalOffset(); }
		private void window_SizeChanged(object sender, SizeChangedEventArgs e) { this.ResetHorizontalOffset(); }

		private void ResetHorizontalOffset()
		{
			this.AssociatedObject.HorizontalOffset += 1;
			this.AssociatedObject.HorizontalOffset -= 1;
		}
	}
}
