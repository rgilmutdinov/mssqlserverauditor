﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace MSSQLServerAuditor.Presentation.Interactivity.Behaviors
{
	public class DataGridSelectedItemsBehavior : Behavior<DataGrid>
	{
		private bool _selectionChangedInProgress; // Flag to avoid infinite loop if same viewmodel is shared by multiple controls

		public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.Register(
			nameof(SelectedItems),
			typeof(IList),
			typeof(DataGridSelectedItemsBehavior),
			new PropertyMetadata(new ObservableCollection<object>(), PropertyChangedCallback));
		
		public DataGridSelectedItemsBehavior()
		{
			SelectedItems = new ObservableCollection<object>();
		}

		public IList SelectedItems
		{
			get { return (IList) GetValue(SelectedItemsProperty); }
			set { SetValue(SelectedItemsProperty, value); }
		}

		protected override void OnAttached()
		{
			base.OnAttached();
			AssociatedObject.SelectionChanged += OnSelectionChanged;
		}

		protected override void OnDetaching()
		{
			base.OnDetaching();
			AssociatedObject.SelectionChanged -= OnSelectionChanged;
		}

		private static void PropertyChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs args)
		{
			NotifyCollectionChangedEventHandler handler = (s, e) => SelectedItemsChanged(sender, e);
			INotifyCollectionChanged oldValues = args.OldValue as INotifyCollectionChanged;
			if (oldValues != null)
			{
				oldValues.CollectionChanged -= handler;
			}

			INotifyCollectionChanged newValues = args.NewValue as INotifyCollectionChanged;
			if (newValues != null)
			{
				newValues.CollectionChanged += handler;
			}
		}

		private static void SelectedItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			DataGridSelectedItemsBehavior behavior = sender as DataGridSelectedItemsBehavior;
			if (behavior != null)
			{
				DataGrid dataGrid = behavior.AssociatedObject;

				IList listItems = dataGrid.SelectedItems;
				if (e.OldItems != null)
				{
					foreach (object item in e.OldItems)
					{
						if (listItems.Contains(item))
						{
							listItems.Remove(item);
						}
					}
				}

				if (e.NewItems != null)
				{
					foreach (object item in e.NewItems)
					{
						if (!listItems.Contains(item))
						{
							listItems.Add(item);
						}
					}
				}
			}
		}

		private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this._selectionChangedInProgress)
			{
				return;
			}

			if (SelectedItems == null)
			{
				SelectedItems = new ObservableCollection<object>();
			}

			this._selectionChangedInProgress = true;
			foreach (object item in e.RemovedItems)
			{
				if (SelectedItems.Contains(item))
				{
					SelectedItems.Remove(item);
				}
			}

			foreach (object item in e.AddedItems)
			{
				if (!SelectedItems.Contains(item))
				{
					SelectedItems.Add(item);
				}
			}

			this._selectionChangedInProgress = false;
		}
	}
}
