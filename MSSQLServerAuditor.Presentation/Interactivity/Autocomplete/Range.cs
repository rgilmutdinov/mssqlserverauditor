﻿using System.Windows.Controls;

namespace MSSQLServerAuditor.Presentation.Interactivity.Autocomplete
{
	public class Range
	{
		public TextBox TextBox { get; private set; }
		public int     Start   { get; set; }
		public int     End     { get; set; }

		public Range(TextBox textBox)
		{
			this.TextBox = textBox;
		}

		public string Text
		{
			get
			{
				string text = TextBox.Text;

				if (string.IsNullOrEmpty(text))
				{
					return "";
				}

				if (Start >= text.Length)
				{
					return "";
				}

				if (End > text.Length)
				{
					return "";
				}

				return TextBox.Text.Substring(Start, End - Start);
			}

			set
			{
				TextBox.SelectionStart  = Start;
				TextBox.SelectionLength = End - Start;
				TextBox.SelectedText    = value;
			}
		}
	}
}
