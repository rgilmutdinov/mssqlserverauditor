﻿using System;

namespace MSSQLServerAuditor.Presentation.Interactivity.Autocomplete
{
	/// <summary>
	/// Item of autocomplete menu
	/// </summary>
	public class AutocompleteItem
	{
		/// <summary>
		/// Text for inserting into textbox
		/// </summary>
		public string Text { get; set; }

		public AutocompleteItem(string text)
		{
			Text = text;
		}

		/// <summary>
		/// Returns text for inserting into Textbox
		/// </summary>
		public virtual string GetTextForReplace()
		{
			return Text;
		}

		/// <summary>
		/// Compares fragment text with this item
		/// </summary>
		public virtual CompareResult Compare(Range fragment)
		{
			if (Text.StartsWith(fragment.Text, StringComparison.InvariantCultureIgnoreCase) &&
				Text != fragment.Text)
			{
				return CompareResult.VisibleAndSelected;
			}

			return CompareResult.Hidden;
		}

		/// <summary>
		/// Returns text for display into popup menu
		/// </summary>
		public override string ToString()
		{
			return Text;
		}

		/// <summary>
		/// This method is called after item was inserted into text
		/// </summary>
		public virtual void OnSelected(SelectedItemEventArgs e)
		{
		}
	}

	public enum CompareResult
	{
		/// <summary>
		/// Item do not appears
		/// </summary>
		Hidden,
		/// <summary>
		/// Item appears
		/// </summary>
		Visible,
		/// <summary>
		/// Item appears and will selected
		/// </summary>
		VisibleAndSelected
	}
}
