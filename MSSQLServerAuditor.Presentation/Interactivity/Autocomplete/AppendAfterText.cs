﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Presentation.Interactivity.Autocomplete
{
	public class AppendAfterText : AutocompleteItem
	{
		private readonly Regex _endsWithRegex;

		public AppendAfterText(string textToAppend, string endsWithPattern)
			: base(textToAppend)
		{
			Check.NotNull(endsWithPattern, nameof(endsWithPattern));

			if (!endsWithPattern.EndsWith("$"))
			{
				endsWithPattern += "$";
			}

			this._endsWithRegex = new Regex(endsWithPattern);
		}

		public override CompareResult Compare(Range fragment)
		{
			string fragmentText = fragment.Text.Trim();

			TextBox sourceBox = fragment.TextBox;

			string lastPart = sourceBox.Text.Substring(0, fragment.Start);
			
			Match match = this._endsWithRegex.Match(lastPart);
			if (match.Success)
			{
				if (Text.StartsWith(fragmentText, StringComparison.InvariantCultureIgnoreCase))
				{
					return CompareResult.VisibleAndSelected;
				}

				if (Text.ToLower().Contains(fragmentText.ToLower()))
				{
					return CompareResult.Visible;
				}
			}

			return CompareResult.Hidden;
		}
	}
}
