﻿using System;
using System.Windows.Controls;

namespace MSSQLServerAuditor.Presentation.Interactivity.Autocomplete
{
	public class SelectingItemEventArgs : EventArgs
	{
		public AutocompleteItem Item { get; set; }

		public bool Cancel        { get; set; }
		public int  SelectedIndex { get; set; }
		public bool Handled       { get; set; }
	}

	public class SelectedItemEventArgs : EventArgs
	{
		public AutocompleteItem Item    { get; set; }
		public TextBox          TextBox { get; set; }
	}

	public class HoveredItemEventArgs : EventArgs
	{
		public AutocompleteItem Item { get; internal set; }
	}
}
