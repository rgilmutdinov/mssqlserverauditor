﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MSSQLServerAuditor.Presentation.Converters
{
	[ValueConversion(typeof(bool?), typeof(bool))]
	public class InverseBooleanConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (targetType == typeof(bool?))
			{
				bool? b = (bool?) value;
				return b.HasValue && !b.Value;
			}

			if (targetType == typeof(bool))
			{
				return value != null && !(bool) value;
			}

			throw new InvalidOperationException("The target must be a boolean");
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Convert(value, targetType, parameter, culture);
		}
	}
}
