﻿using System;
using System.Globalization;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Presentation.Converters
{
	public class InstanceVersionConverter : ConverterBase
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			InstanceVersion version = value as InstanceVersion;
			if (version != null)
			{
				return version.ToString();
			}

			return string.Empty;
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value != null)
			{
				try
				{
					return new InstanceVersion(value.ToString());
				}
				catch (Exception)
				{
					return new InstanceVersion();
				}
			}

			return new InstanceVersion();
		}
	}
}
