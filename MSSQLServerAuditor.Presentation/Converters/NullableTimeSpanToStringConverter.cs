﻿using System;
using System.Globalization;

namespace MSSQLServerAuditor.Presentation.Converters
{
	public class NullableTimeSpanToStringConverter : ConverterBase
	{
		private static string DefaultString = "00:00:00";
		private static string TimeFormat    = @"{0:00}:{1:00}:{2:00}";

		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			TimeSpan? timeObj = value as TimeSpan?;
			if (timeObj != null)
			{
				TimeSpan time = timeObj.Value;

				return string.Format(
					TimeFormat,
					Math.Floor(time.TotalHours),
					time.Minutes,
					time.Seconds
				);
			}

			return DefaultString;
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
			{
				return null;
			}

			string stime = value.ToString();
			if (stime != DefaultString)
			{
				string[] parts = stime.Split(':');
				try
				{
					TimeSpan time = new TimeSpan(
						int.Parse(parts[0]),
						int.Parse(parts[1]),
						int.Parse(parts[2])
					);

					return time;
				}
				catch (Exception)
				{
					return null;
				}
			}

			return null;
		}
	}
}
