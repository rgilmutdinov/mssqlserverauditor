﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MSSQLServerAuditor.Presentation.Converters
{
	public class ConverterBase : IValueConverter
	{
		public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return DependencyProperty.UnsetValue;
		}

		public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return DependencyProperty.UnsetValue;
		}
	}
}
