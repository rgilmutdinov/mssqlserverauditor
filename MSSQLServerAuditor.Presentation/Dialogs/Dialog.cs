﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MSSQLServerAuditor.Presentation.Localization;

namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public class Dialog
	{
		private Answer _answer;

		public Dialog(string subject, string message, params Answer[] possibleAnswers)
			: this(DialogType.None, subject, message, possibleAnswers)
		{
		}

		public Dialog(DialogType dialogType, string message, params Answer[] possibleAnswers)
			: this(dialogType, EnumLocalizationHelper.GetUiText(dialogType, CultureInfo.CurrentUICulture), message, possibleAnswers)
		{
		}

		public Dialog(DialogType dialogType, string subject, string message, params Answer[] possibleAnswers)
		{
			if (!possibleAnswers.Any())
			{
				throw new ArgumentException("No possible answers are given", nameof(possibleAnswers));
			}

			DialogType      = dialogType;
			Subject         = subject;
			Message         = message;
			PossibleAnswers = possibleAnswers.ToList();
		}

		public DialogType DialogType { get; set; }
		public string     Message    { get; set; }
		public string     Subject    { get; set; }

		public List<Answer> PossibleAnswers { get; set; }

		public bool IsAnswerGiven { get; private set; }

		public Answer Answer
		{
			get { return this._answer; }
			set
			{
				this._answer = value;
				IsAnswerGiven = true;
			}
		}
	}
}