﻿using System.Media;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public class DialogService : IDialogService
	{
		private readonly IWindowManager _windowManager;

		public DialogService(IWindowManager windowManager)
		{
			Check.NotNull(windowManager, nameof(windowManager));

			this._windowManager = windowManager;
		}

		public Answer ShowDialog(Dialog dialog)
		{
			DialogViewModel dialogViewModel = new DialogViewModel(dialog);

			PlayMessageBeep(dialog.DialogType);
			this._windowManager.ShowDialog(dialogViewModel);

			return dialog.Answer;
		}

		private static void PlayMessageBeep(DialogType dialogType)
		{
			switch (dialogType)
			{
				case DialogType.Error:
					SystemSounds.Hand.Play();
					break;

				case DialogType.Warning:
					SystemSounds.Exclamation.Play();
					break;

				case DialogType.Question:
					SystemSounds.Question.Play();
					break;

				case DialogType.Information:
					SystemSounds.Asterisk.Play();
					break;

				default:
					SystemSounds.Beep.Play();
					break;
			}
		}
	}
}