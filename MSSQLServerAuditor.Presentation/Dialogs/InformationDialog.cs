﻿namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public class InformationDialog : Dialog
	{
		public InformationDialog(string message)
			: base(DialogType.Information, message, Answer.Ok)
		{
		}

		public InformationDialog(string message, params Answer[] possibleResponens)
			: base(DialogType.Information, message, possibleResponens)
		{
		}

		public InformationDialog(string subject, string message, params Answer[] possibleResponens)
			: base(DialogType.Information, subject, message, possibleResponens)
		{
		}
	}
}