﻿namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public class BindableResponse
	{
		public Answer Answer  { get; set; }
		public bool IsDefault { get; set; }
		public bool IsCancel  { get; set; }
	}
}