﻿namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public class QuestionDialog : Dialog
	{
		public QuestionDialog(string message)
			: base(DialogType.Question, message, Answer.Yes, Answer.No)
		{
		}

		public QuestionDialog(string message, params Answer[] possibleResponens)
			: base(DialogType.Question, message, possibleResponens)
		{
		}

		public QuestionDialog(string subject, string message, params Answer[] possibleResponens)
			: base(DialogType.Question, subject, message, possibleResponens)
		{
		}
	}
}