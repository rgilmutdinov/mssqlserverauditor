﻿namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public interface IDialogService
	{
		Answer ShowDialog(Dialog dialog);
	}
}