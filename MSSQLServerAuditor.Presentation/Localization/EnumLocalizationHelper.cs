﻿using System;
using System.Globalization;
using System.Reflection;

namespace MSSQLServerAuditor.Presentation.Localization
{
	public static class EnumLocalizationHelper
	{
		public static string GetUiText(Enum e, CultureInfo culture)
		{
			FieldInfo fi = e.GetType().GetField(e.ToString());

			LocalizedAttribute[] attributes = (LocalizedAttribute[])fi.GetCustomAttributes(
				typeof(LocalizedAttribute), false);

			if (attributes.Length > 0)
			{
				return attributes[0].GetLocalizedText(culture);
			}

			return e.ToString();
		}
	}
}
