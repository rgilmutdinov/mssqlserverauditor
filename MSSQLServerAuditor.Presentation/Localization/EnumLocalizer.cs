﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MSSQLServerAuditor.Presentation.Localization
{
	/// <summary>
	/// Convention based localization of Enums
	/// </summary>
	public class EnumLocalizer : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			Enum e = value as Enum;
			if (e != null)
			{
				return EnumLocalizationHelper.GetUiText(e, culture);
			}

			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}