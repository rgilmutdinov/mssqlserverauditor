﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Builder.Utils;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Cryptography;
using MSSQLServerAuditor.Core.Cryptography.Xml;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Xml;

namespace MSSQLServerAuditor.Builder.Wix
{
	public class WixConfigurator
	{
		private readonly BuildConfig _config;
		private readonly AppSettings _appSettings;
		
		private readonly List<string> _tempDirectories;

		private readonly bool _protect;

		private readonly IFileProtector _queryFileProtector;
		private readonly IFileProtector _scriptFileProtector;
		private readonly IFileProtector _queryListFileProtector;
		private readonly IFileProtector _fileEncryptor;

		public WixConfigurator(BuildConfig config, AppSettings appSettings)
		{
			this._config      = config;
			this._appSettings = appSettings;

			if (config.ProtectData)
			{
				this._protect   = true;

				XmlEncryptor encryptor = GetEncryptor(config.EncryptionKey);
				Signer       signer    = GetSigner(config.SignPrivateKey);

				this._fileEncryptor          = new FileEncryptor(encryptor);
				this._queryFileProtector     = new QueryProtector(signer, encryptor);
				this._queryListFileProtector = new QueryListProtector(signer, encryptor);
				this._scriptFileProtector    = new ScriptProtector(signer, encryptor);
			}
			else
			{
				this._protect = false;
			}

			this._tempDirectories = new List<string>();
		}

		private static XmlEncryptor GetEncryptor(string encryptionKey)
		{
			RSA rsa = new RSACryptoServiceProvider();
			rsa.FromXmlString(encryptionKey);

			return new XmlEncryptor(rsa);
		}

		private static Signer GetSigner(string signatureKey)
		{
			return new Signer(signatureKey);
		}

		public void Configure(WixSetup wixSetup)
		{
			this._tempDirectories.Clear();

			// set setup icon
			string iconPath = Path.Combine(this._config.WixProjectDirectory, "appIcon.ico");
			wixSetup.SetIconPath(iconPath);

			// set license agreement file
			wixSetup.SetLicenseDirectory(this._config.WixProjectDirectory);

			// add images
			wixSetup.AddImages(Path.Combine(this._config.DataDirectory, "Images"));

			// add java scripts
			wixSetup.AddJs(Path.Combine(this._config.DataDirectory, "js")); 

			// prepare template files and dependent scripts
			// create setup components
			Dictionary<string, string> filesMap = PrepareTemplateFiles();

			wixSetup.CreateFileComponents(
				this._config,
				filesMap,
				this._appSettings.System.TemplateDirectory
			);

			// get and append post build scripts
			Dictionary<string, string> scriptsMap = PreparePostBuildScripts();

			wixSetup.CreateFileComponents(
				this._config,
				scriptsMap,
				"PostBuild"
			);
		}

		public void Cleanup()
		{
			if (this._tempDirectories.Any())
			{
				foreach (string tempDirectory in this._tempDirectories)
				{
					if (Directory.Exists(tempDirectory))
					{
						Directory.Delete(tempDirectory, true);
					}
				}
			}

			this._tempDirectories.Clear();
		}

		private Dictionary<string, string> PreparePostBuildScripts()
		{
			Dictionary<string, string> scriptsMap =
				new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

			string postBuildDirectory = Path.GetFullPath(Path.Combine(this._config.DataDirectory, "PostBuild"));
			
			foreach (string xmlFile in Directory.EnumerateFiles(postBuildDirectory, "*.xml", SearchOption.AllDirectories))
			{
				string fullPath = Path.GetFullPath(xmlFile);

				string subPath = Paths.GetRelativePath(Path.GetFullPath(postBuildDirectory), fullPath);
				string dstPath = Path.Combine("PostBuild", subPath);

				if (!scriptsMap.ContainsKey(dstPath))
				{
					PrepareFile(fullPath, dstPath, this._fileEncryptor);
					scriptsMap.Add(dstPath, fullPath);
				}
				
			}

			return scriptsMap;
		}

		private Dictionary<string, string> PrepareTemplateFiles()
		{
			Dictionary<string, string> filesMap = 
				new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

			IList<string> templateFiles = this._config.TemplateFiles;
			if (templateFiles.Any())
			{
				string modulesDirectory = this._appSettings.System.TemplateDirectory;

				foreach (string templateFile in templateFiles)
				{
					if (File.Exists(templateFile))
					{
						string fullPath = Path.GetFullPath(templateFile);
						string fileName = Path.GetFileName(templateFile);

						if (fileName != null)
						{
							string dstPath = Path.Combine(modulesDirectory, fileName);

							if (!filesMap.ContainsKey(dstPath))
							{
								filesMap.Add(dstPath, fullPath);
								PrepareFile(fullPath, dstPath, this._fileEncryptor);
							}
						}

						PrepareTemplateDependencies(
							filesMap,
							templateFile
						);
					}
				}
			}

			return filesMap;
		}

		private void PrepareFile(string srcFullPath, string dstPath, IFileProtector protector)
		{
			string   dstFullPath  = Path.GetFullPath(Path.Combine(this._config.WixTempDirectory, dstPath));
			string[] dstPathParts = Paths.Split(dstPath);
			string   dstDirectory = Path.GetDirectoryName(dstFullPath);

			if (dstDirectory != null)
			{
				if (!Directory.Exists(dstDirectory))
				{
					Directory.CreateDirectory(dstDirectory);
				}

				if (this._protect)
				{
					protector.MakeProtectedCopy(srcFullPath, dstFullPath);
				}
				else
				{
					File.Copy(srcFullPath, dstFullPath, true);
				}
			}

			if (dstPathParts.Any())
			{
				// track temporary directories
				string tempRelativeDir = Path.Combine(this._config.WixTempDirectory, dstPathParts[0].ToLower());

				if (!this._tempDirectories.Contains(tempRelativeDir))
				{
					this._tempDirectories.Add(tempRelativeDir);
				}
			}
		}

		private void PrepareTemplateDependencies(
			Dictionary<string, string> filesMap,
			string                     templateFile
		)
		{
			string modulesDirectory = this._appSettings.System.TemplateDirectory;

			string templateDirectory = Path.GetDirectoryName(templateFile);

			if (templateDirectory == null)
			{
				throw new ApplicationException(
					$"Can't get directory of the file '{templateFile}'"
				);
			}

			string procDirectory = Path.Combine(templateDirectory, "Templates");

			TemplateInfo template = XmlSerialization.Deserialize<TemplateInfo>(templateFile);

			List<TemplateNodeInfo> allNodes = GetAllTemplateNodes(template)
				.ToList();

			foreach (TemplateNodeInfo tnInfo in allNodes)
			{
				string procFile = tnInfo.ProcessorFile;

				if (!string.IsNullOrWhiteSpace(procFile))
				{
					string fullPath = Path.GetFullPath(Path.Combine(procDirectory, procFile));
					string dstPath  = Path.Combine(modulesDirectory, "Templates", procFile);

					if (!filesMap.ContainsKey(dstPath))
					{
						filesMap.Add(dstPath, fullPath);
						PrepareFile(fullPath, dstPath, this._fileEncryptor);
					}
				}

				IEnumerable<TemplateNodeQueryInfo> allQueries = tnInfo.Queries
					.Union(tnInfo.GroupQueries)
					.Union(tnInfo.ConnectionQueries)
					.Union(tnInfo.SqlCodeGuardQueries);

				foreach (TemplateNodeQueryInfo tnQuery in allQueries)
				{
					string queryFile = tnQuery.QueryFileName;
					if (!string.IsNullOrWhiteSpace(queryFile))
					{
						string fullPath = Path.GetFullPath(Path.Combine(templateDirectory, queryFile));
						string dstPath  = Path.Combine(modulesDirectory, queryFile);

						if (!filesMap.ContainsKey(dstPath))
						{
							filesMap.Add(dstPath, fullPath);
							PrepareFile(fullPath, dstPath, this._queryFileProtector);
						}

						QueryRoot queryRoot = XmlSerialization.Deserialize<QueryRoot>(fullPath);
						foreach (QueryListInfo query in queryRoot.Queries)
						{
							foreach (QueryInfoExt queryInfo in query.Queries)
							{
								List<FillStatement> fillStatements = queryInfo.FillStatements;
								if (fillStatements != null && fillStatements.Any())
								{
									foreach (FillStatement fillStatement in fillStatements)
									{
										string fillStatementFile = fillStatement.File;

										// check if statement is defined in external file
										if (!string.IsNullOrWhiteSpace(fillStatementFile))
										{
											// process external script file for all supported storage types
											PrepareScriptsForAllStorageTypes(
												filesMap,
												fillStatementFile,
												templateDirectory,
												this._scriptFileProtector
											);
										}
									}
								}

								List<ETLStatement> etlStatements = queryInfo.ETLStatements;
								if (etlStatements != null && etlStatements.Any())
								{
									foreach (ETLStatement etlStatement in etlStatements)
									{
										string etlStatementFile = etlStatement.File;

										// check if statement is defined in external file
										if (!string.IsNullOrWhiteSpace(etlStatementFile))
										{
											// process external ETL script file for all supported ETL connections
											PrepareScriptsForSupportedEtlConnections(
												filesMap,
												etlStatementFile,
												templateDirectory,
												this._fileEncryptor
											);
										}
									}
								}

								if (queryInfo.IsExternal)
								{
									string queryInfoFile = queryInfo.File;
									if (!string.IsNullOrWhiteSpace(queryInfoFile))
									{
										// process external script file for all supported storage types
										PrepareScriptsForAllStorageTypes(
											filesMap,
											queryInfoFile,
											templateDirectory,
											this._queryListFileProtector
										);
									}
								}
							}
						}
					}
				}
			}
		}

		private void PrepareScriptsForSupportedEtlConnections(
			Dictionary<string, string> filesMap,
			string                     scriptFile,
			string                     templateDirectory,
			IFileProtector             fileProtector
		)
		{
			IEnumerable<string> types = new [] { ConnectionType.MSSQL, ConnectionType.SQLite }
				.Select(st => st.ToString());

			PrepareScripts(
				filesMap,
				scriptFile,
				templateDirectory,
				fileProtector,
				types
			);
		}

		private void PrepareScriptsForAllStorageTypes(
			Dictionary<string, string> filesMap,
			string                     scriptFile,
			string                     templateDirectory,
			IFileProtector             fileProtector
		)
		{
			IEnumerable<string> types = EnumUtil
				.GetValues<StorageType>()
				.Select(st => st.ToString());

			PrepareScripts(
				filesMap,
				scriptFile,
				templateDirectory,
				fileProtector,
				types
			);
		}

		private void PrepareScripts(
			Dictionary<string, string> filesMap,
			string                     scriptFile,
			string                     templateDirectory,
			IFileProtector             fileProtector,
			IEnumerable<string>        types
		)
		{
			string scriptPathTemplate = GetScriptPathTemplate(scriptFile);

			foreach (string type in types)
			{
				string scriptPath = string.Format(scriptPathTemplate, type);

				string fullScriptPath = Path.GetFullPath(
					Path.Combine(templateDirectory, scriptPath)
				);

				string scriptDstPath = Path.Combine(
					string.Format(this._appSettings.System.ScriptsDirectory, type),
					scriptFile
				);

				if (!filesMap.ContainsKey(scriptDstPath))
				{
					PrepareFile(fullScriptPath, scriptDstPath, fileProtector);
					filesMap.Add(scriptDstPath, fullScriptPath);
				}
			}
		}

		private string GetScriptPathTemplate(string scriptFile)
		{
			string modulesDirectory = this._appSettings.System.TemplateDirectory;

			string scriptPathTemplate = Path.Combine(
				this._appSettings.System.ScriptsDirectory,
				scriptFile
			);

			if (!Path.IsPathRooted(modulesDirectory) && scriptPathTemplate.StartsWith(modulesDirectory))
			{
				scriptPathTemplate = Paths.TrimFirstSeparator(
					scriptPathTemplate.Remove(0, modulesDirectory.Length)
				);
			}

			return scriptPathTemplate;
		}

		private static IEnumerable<TemplateNodeInfo> GetAllTemplateNodes(TemplateInfo template)
		{
			List<TemplateNodeInfo> result = new List<TemplateNodeInfo>();

			foreach (TemplateNodeInfo node in template.Nodes)
			{
				result.AddRange(GetChildrenPlainList(node));
			}

			return result;
		}

		private static IEnumerable<TemplateNodeInfo> GetChildrenPlainList(TemplateNodeInfo node)
		{
			List<TemplateNodeInfo> result = new List<TemplateNodeInfo> { node };

			foreach (TemplateNodeInfo child in node.Children)
			{
				result.AddRange(GetChildrenPlainList(child));
			}

			return result;
		}
	}
}
