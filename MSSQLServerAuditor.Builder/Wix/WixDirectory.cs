﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Builder.Wix
{
	[XmlRoot("Directory", Namespace = WixNamespace)]
	public class WixDirectory : WixItem
	{
		public WixDirectory()
		{

		}

		public WixDirectory(string id, string name) : base(id)
		{
			this.Name = name;
		}

		[XmlAttribute("Name", Namespace = WixNamespace)]
		public string Name { get; set; }
	}
}
