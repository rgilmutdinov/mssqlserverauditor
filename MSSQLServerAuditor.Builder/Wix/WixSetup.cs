﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using MSSQLServerAuditor.Builder.Infrastructure;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Common.Utils;

namespace MSSQLServerAuditor.Builder.Wix
{
	public class WixSetup
	{
		public class DummyProgress : ILogProgress
		{
			public void Report(LogEntry value)
			{
			}
		}

		private readonly XDocument           _xmlFiles;
		private readonly XDocument           _xmlProduct;
		private readonly XNamespace          _wixNamespace = @"http://schemas.microsoft.com/wix/2006/wi";
		private readonly XmlNamespaceManager _xmlNamespace;

		public WixSetup(XDocument xmlFiles, XDocument xmlProduct)
		{
			this._xmlFiles   = xmlFiles;
			this._xmlProduct = xmlProduct;

			this._xmlNamespace = new XmlNamespaceManager(new NameTable());
			this._xmlNamespace.AddNamespace("n", this._wixNamespace.NamespaceName);
		}

		public static WixSetup Load(string wxsFilesPath, string wxsProductPath)
		{
			XDocument xmlProduct = XDocument.Load(wxsProductPath);
			XDocument xmlFiles   = XDocument.Load(wxsFilesPath);
			
			return new WixSetup(xmlFiles, xmlProduct);
		}

		public ILogProgress LogProgress { get; set; } = new DummyProgress();

		public void Save(string wsxFilesPath, string wxsProductPath)
		{
			this._xmlFiles.Save(wsxFilesPath);
			this._xmlProduct.Save(wxsProductPath);
		}

		public void SetIconPath(string iconPath)
		{
			iconPath = Path.GetFullPath(iconPath);

			if (!File.Exists(iconPath))
			{
				throw new FileNotFoundException($"Icon file '{iconPath}' does not exist.");
			}

			XElement iconNode = this._xmlProduct.XPathSelectElement(
				"//n:Icon[@Id='appIcon.ico']",
				this._xmlNamespace
			);

			XAttribute sourceAttr = iconNode?.Attribute("SourceFile");
			if (sourceAttr != null)
			{
				sourceAttr.Value = iconPath;
			}
		}

		public void SetLicenseDirectory(string directory)
		{
			XElement licNode = this._xmlProduct.XPathSelectElement(
				"//n:WixVariable[@Id='WixUILicenseRtf']",
				this._xmlNamespace
			);

			XAttribute valueAttr = licNode?.Attribute("Value");
			string licPath = valueAttr?.Value;

			if (licPath != null)
			{
				licPath = Path.Combine(directory, licPath);
				licPath = Path.GetFullPath(licPath);

				if (!File.Exists(licPath))
				{
					throw new FileNotFoundException($"License agreement file '{licPath}' does not exist.");
				}

				valueAttr.Value = licPath;
			}
		}

		public void AddImages(string imagesDirectory)
		{
			AddStaticComponents(imagesDirectory, "MSSQLServerAuditor.Images");
		}

		public void AddJs(string jsDirectory)
		{
			AddStaticComponents(jsDirectory, "MSSQLServerAuditor.js");
		}

		private void AddStaticComponents(string sourceDirectory, string wixDirectoryId)
		{
			XElement dirComponent = this._xmlFiles.XPathSelectElement(
				$"//n:Directory[@Id='{wixDirectoryId}']",
				this._xmlNamespace
			);

			List<string> componentIds = new List<string>();
			if (dirComponent != null)
			{
				WixDirectory wixJs = WixItem.FromXmlElement<WixDirectory>(dirComponent);
				AddDirectoryComponents(wixJs, sourceDirectory);

				dirComponent.ReplaceNodes(wixJs.ToXmlElement().Nodes());

				componentIds.AddRange(GetChildrenComponentsIds(wixJs));
			}

			AddComponentReferences(componentIds);
		}

		public void CreateFileComponents(
			BuildConfig                config,
			Dictionary<string, string> filesMap,
			string                     baseDirectory
		)
		{
			string[] templateDirPaths = Paths.Split(baseDirectory);

			XElement xmlRoot = this._xmlFiles.XPathSelectElement(
				"//n:Directory[@Id='MSSQLServerAuditorFolder']",
				this._xmlNamespace
			);

			if (xmlRoot == null)
			{
				throw new Exception("Setup template does not contain required 'MSSQLServerAuditorFolder' directory");
			}

			XElement xmlDirectory = xmlRoot;
			foreach (string dirPart in templateDirPaths)
			{
				xmlDirectory = xmlDirectory.XPathSelectElement(
					$"//n:Directory[@Name='{dirPart}']",
					this._xmlNamespace
				);

				if (xmlDirectory == null)
				{
					xmlDirectory = new XElement(this._wixNamespace + "Directory");

					xmlDirectory.SetAttributeValue("Name", dirPart);
					xmlDirectory.SetAttributeValue("Id",   WixIds.New());
				}
			}

			WixDirectory wixDirectory = WixItem.FromXmlElement<WixDirectory>(xmlDirectory);
			WixDirectory wixRoot      = WixFactory.NewDirectory("\\");

			wixRoot.Children.Add(wixDirectory);

			foreach (KeyValuePair<string, string> fileMapping in filesMap)
			{
				string dstPath     = fileMapping.Key;
				
				string dstFullPath = Path.GetFullPath(Path.Combine(config.WixTempDirectory, dstPath));
				string[] parts     = Paths.Split(dstPath);

				PopulateComponents(wixRoot, dstFullPath, parts.ToList());
			}

			xmlDirectory.ReplaceNodes(wixDirectory.ToXmlElement().Nodes());

			AddComponentReferences(GetChildrenComponentsIds(wixDirectory));
		}

		private void AddDirectoryComponents(
			WixDirectory wixDir,
			string       directory
		)
		{
			List<string> files = Directory.EnumerateFiles(directory)
				.Where(file => !string.IsNullOrWhiteSpace(file))
				.Select(file => Path.Combine(directory, Path.GetFileName(file)))
				.ToList();

			string dirId = wixDir.Id;

			if (files.Any())
			{
				WixComponent wixComponent = WixFactory.NewComponent(dirId);

				foreach (string filePath in files)
				{
					string file = Path.GetFileName(filePath);

					WixFile wixFile = WixFactory.NewFile(
						file,
						Path.GetFullPath(filePath)
					);

					wixComponent.Children.Add(wixFile);
				}

				wixDir.Children.Add(wixComponent);
			}

			IEnumerable<string> directories = Directory.EnumerateDirectories(directory)
				.Where(dir => !string.IsNullOrWhiteSpace(dir))
				.Select(dir => Path.Combine(directory, Path.GetFileName(dir)));

			foreach (string dir in directories)
			{
				string name = dir.Remove(0, directory.Length + 1);

				WixDirectory wixChildDir = WixFactory.NewDirectory(name);

				AddDirectoryComponents(wixChildDir, dir);

				wixDir.Children.Add(wixChildDir);
			}
		}

		private List<string> GetChildrenComponentsIds(WixItem wix)
		{
			List<string> componentsIds = wix.Children
				.OfType<WixComponent>()
				.Select(component => component.Id)
				.ToList();

			foreach (WixItem child in wix.Children)
			{
				componentsIds.AddRange(GetChildrenComponentsIds(child));
			}

			return componentsIds;
		}

		private void AddComponentReferences(List<string> components)
		{
			XElement feature = this._xmlProduct.XPathSelectElement(
				"//n:Feature[@Id='Complete']",
				this._xmlNamespace
			);

			if (feature != null)
			{
				foreach (string componentId in components)
				{
					XElement componentRef = new XElement(
						this._wixNamespace + "ComponentRef"
					);

					componentRef.SetAttributeValue("Id", componentId);

					feature.Add(componentRef);
				}
			}
		}

		private void PopulateComponents(
			WixDirectory wixBaseDir,
			string       fullPath,
			List<string> restPathParts)
		{
			string       firstPart = restPathParts.First();
			List<string> leftParts = restPathParts.Skip(1).ToList();

			if (leftParts.Any())
			{
				WixDirectory childDir = wixBaseDir.Children.OfType<WixDirectory>()
					.SingleOrDefault(x => x.Name == firstPart);

				if (childDir == null)
				{
					childDir = WixFactory.NewDirectory(firstPart);

					wixBaseDir.Children.Add(childDir);
				}

				PopulateComponents(childDir, fullPath, restPathParts.Skip(1).ToList());
			}
			else
			{
				WixComponent childComponent = wixBaseDir.Children.OfType<WixComponent>()
					.FirstOrDefault();

				if (childComponent == null)
				{
					childComponent = WixFactory.NewComponent();

					wixBaseDir.Children.Add(childComponent);
				}

				string filename = firstPart;

				WixFile wixFile = WixFactory.NewFile(filename, fullPath);

				childComponent.Children.Add(wixFile);
			}
		}
	}
}