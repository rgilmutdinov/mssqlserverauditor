﻿using System;
using System.Text.RegularExpressions;

namespace MSSQLServerAuditor.Builder.Wix
{
	public static class WixIds
	{
		public static string New()
		{
			return "Id" + Sanitize(Guid.NewGuid().ToString());
		}

		private static string Sanitize(string id)
		{
			const string regexPattern = @"[^a-zA-Z0-9_\.]";
			return Regex.Replace(id, regexPattern, "_");
		}
	}
}
