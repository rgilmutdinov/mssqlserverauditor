﻿using System;

namespace MSSQLServerAuditor.Builder.Wix
{
	public static class WixFactory
	{
		public static WixDirectory NewDirectory(string name)
		{
			return new WixDirectory(WixIds.New(), name);
		}

		public static WixFile NewFile(string name, string source)
		{
			return new WixFile(WixIds.New(), name, source);
		}

		public static WixComponent NewComponent()
		{
			return NewComponent(WixIds.New());
		}

		public static WixComponent NewComponent(string customId)
		{
			return new WixComponent(customId, Guid.NewGuid().ToString());
		}
	}
}
