﻿using MahApps.Metro.Controls;

namespace MSSQLServerAuditor.Builder.Views
{
	/// <summary>
	/// Interaction logic for ShellView.xaml
	/// </summary>
	public partial class ShellView : MetroWindow
	{
		public ShellView()
		{
			InitializeComponent();
		}
	}
}
