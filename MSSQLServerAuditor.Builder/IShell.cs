﻿using System.Threading.Tasks;
using Caliburn.Micro;
using MSSQLServerAuditor.Builder.Infrastructure;

namespace MSSQLServerAuditor.Builder
{
	public interface IShell
	{
		bool  CanMoveNextPage     { get; }
		bool  CanMovePreviousPage { get; }
		IPage CurrentPage         { get; set; }

		Task Build();

		IObservableCollection<IPage> Pages { get; }

		void MoveNextPage();
		void MovePreviousPage();

		void Refresh();
		void EnableConfigEdit();
	}
}
