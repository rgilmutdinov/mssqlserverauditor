﻿using System;
using System.IO;
using Microsoft.Win32;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Builder.Resources;
using MSSQLServerAuditor.Core.Xml;
using NLog;

namespace MSSQLServerAuditor.Builder.ViewModels
{
	public class ConfigViewModel : PageViewModel
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public ConfigViewModel(IShell shell, BuildConfig buildConfig)
			: base(shell, buildConfig)
		{

		}

		public override string Title => Strings.Configuration;

		public string Location
		{
			get { return BuildConfig.Location; }
			set
			{
				if (BuildConfig.Location != value)
				{
					BuildConfig.Location = value;
					NotifyOfPropertyChange(() => Location);
				}
			}
		}

		public void CreateNewConfig()
		{
			SaveFileDialog saveDialog = new SaveFileDialog
			{
				FileName   = "buildConfig.xml",
				Filter     = $"{Strings.ConfigurationFile} (*.xml)|*.xml",
				DefaultExt = ".xml"
			};

			if (saveDialog.ShowDialog() == true)
			{
				Location = saveDialog.FileName;

				try
				{
					BuildConfig.CopyFrom(BuildConfig.CreateDefault());
					XmlSerialization.Serialize(Location, BuildConfig);

					UpdateShell();
				}
				catch (Exception exc)
				{
					string message = string.Format(Strings.FailedCreateBuildConfig, Location);
					Log.Error(exc, message);

					throw new FileLoadException(
						message,
						exc
					);
				}
			}
		}

		public void OpenConfig()
		{
			OpenFileDialog openDialog = new OpenFileDialog
			{
				Filter      = $"{Strings.ConfigurationFile} (*.xml)|*.xml",
				DefaultExt  = ".xml",
				Multiselect = false
			};

			if (openDialog.ShowDialog() == true)
			{
				string location = openDialog.FileName;
				try
				{
					BuildConfig cfg = XmlSerialization.Deserialize<BuildConfig>(location);

					BuildConfig.CopyFrom(cfg);

					Location = location;

					UpdateShell();
				}
				catch (Exception exc)
				{
					string message = string.Format(Strings.FailedOpenBuildConfig, location);
					Log.Error(exc, message);

					throw new FileLoadException(
						message,
						exc
					);
				}
			}
		}

		private void UpdateShell()
		{
			Shell.EnableConfigEdit();
			Shell.Refresh();
		}
	}
}
