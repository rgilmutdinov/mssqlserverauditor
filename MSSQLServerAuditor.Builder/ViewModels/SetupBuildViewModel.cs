﻿using System.IO;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Builder.Resources;
using MSSQLServerAuditor.Presentation.Validation;

namespace MSSQLServerAuditor.Builder.ViewModels
{
	public class SetupBuildViewModel : PageViewModel
	{
		public SetupBuildViewModel(IShell shell, BuildConfig buildConfig)
			: base(shell, buildConfig)
		{
			ConfigureValidationRules();
		}

		public string BaseDirectory => Directory.GetCurrentDirectory();

		public string WixDirectory
		{
			get { return BuildConfig.WixDirectory; }
			set
			{
				if (BuildConfig.WixDirectory != value)
				{
					BuildConfig.WixDirectory = value;

					NotifyOfPropertyChange(() => WixDirectory);
					Validator.Validate(nameof(WixDirectory));
				}
			}
		}

		public string WixProjectDirectory
		{
			get { return BuildConfig.WixProjectDirectory; }
			set
			{
				if (BuildConfig.WixProjectDirectory != value)
				{
					BuildConfig.WixProjectDirectory = value;

					NotifyOfPropertyChange(() => WixProjectDirectory);
					Validator.Validate(nameof(WixProjectDirectory));
				}
			}
		}

		public string WixTempDirectory
		{
			get { return BuildConfig.WixTempDirectory; }
			set
			{
				if (BuildConfig.WixTempDirectory != value)
				{
					BuildConfig.WixTempDirectory = value;

					NotifyOfPropertyChange(() => WixTempDirectory);
					Validator.Validate(nameof(WixTempDirectory));
				}
			}
		}

		public override string Title => Strings.SetupBuild;

		private void ConfigureValidationRules()
		{
			Validator.AddRule(
				nameof(WixProjectDirectory),
				() =>
				{
					if (string.IsNullOrWhiteSpace(WixProjectDirectory))
					{
						return RuleResult.Invalid(Strings.WixSetupFileIsRequired);
					}

					if (!Directory.Exists(WixProjectDirectory))
					{
						return RuleResult.Invalid(Strings.WixSetupFileDoesNotExist);
					}

					return RuleResult.Valid();
				}
			);

			Validator.AddRule(
				nameof(WixDirectory),
				() =>
				{
					if (string.IsNullOrWhiteSpace(WixDirectory))
					{
						return RuleResult.Invalid(Strings.WixDirectoryIsRequired);
					}

					if (!Directory.Exists(WixDirectory))
					{
						return RuleResult.Invalid(Strings.WixDirectoryDoesNotExist);
					}

					return RuleResult.Valid();
				}
			);

			Validator.AddRule(
				nameof(WixTempDirectory),
				() =>
				{
					if (string.IsNullOrWhiteSpace(WixTempDirectory))
					{
						return RuleResult.Invalid(Strings.WixTempDirectoryIsRequired);
					}

					return RuleResult.Valid();
				}
			);
		}
	}
}
