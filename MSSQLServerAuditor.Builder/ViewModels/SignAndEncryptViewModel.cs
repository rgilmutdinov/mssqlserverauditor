﻿using System.Security.Cryptography;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Builder.Resources;
using MSSQLServerAuditor.Presentation.Validation;

namespace MSSQLServerAuditor.Builder.ViewModels
{
	public class SignAndEncryptViewModel : PageViewModel
	{
		public SignAndEncryptViewModel(IShell shell, BuildConfig buildConfig)
			: base(shell, buildConfig)
		{
			ConfigureValidationRules();
		}

		public override string Title => Strings.SignAndEncrypt;

		public bool ProtectData
		{
			get { return BuildConfig.ProtectData; }
			set
			{
				if (BuildConfig.ProtectData != value)
				{
					BuildConfig.ProtectData = value;
					NotifyOfPropertyChange(() => ProtectData);

					Validator.Validate(nameof(EncryptionKey));
					Validator.Validate(nameof(SignPublicKey));
					Validator.Validate(nameof(SignPrivateKey));
				}
			}
		}

		public string EncryptionKey
		{
			get { return BuildConfig.EncryptionKey; }
			set
			{
				if (BuildConfig.EncryptionKey != value)
				{
					BuildConfig.EncryptionKey = value;
					NotifyOfPropertyChange(() => EncryptionKey);

					Validator.Validate(nameof(EncryptionKey));
				}
			}
		}

		public string SignPublicKey
		{
			get { return BuildConfig.SignPublicKey; }
			set
			{
				if (BuildConfig.SignPublicKey != value)
				{
					BuildConfig.SignPublicKey = value;
					NotifyOfPropertyChange(() => SignPublicKey);

					Validator.Validate(nameof(SignPublicKey));
				}
			}
		}

		public string SignPrivateKey
		{
			get { return BuildConfig.SignPrivateKey; }
			set
			{
				if (BuildConfig.SignPrivateKey != value)
				{
					BuildConfig.SignPrivateKey = value;
					NotifyOfPropertyChange(() => SignPrivateKey);

					Validator.Validate(nameof(SignPrivateKey));
				}
			}
		}

		public void GenerateKeys()
		{
			RSACryptoServiceProvider encryptRsa = new RSACryptoServiceProvider();

			EncryptionKey = encryptRsa.ToXmlString(true);

			RSACryptoServiceProvider signRsa = new RSACryptoServiceProvider();

			SignPrivateKey = signRsa.ToXmlString(true);
			SignPublicKey  = signRsa.ToXmlString(false);
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRule(nameof(EncryptionKey),  ValidateEncryptionKey);
			Validator.AddRule(nameof(SignPrivateKey), ValidateSignPrivateKey);
			Validator.AddRule(nameof(SignPublicKey),  ValidateSignPublicKey);
		}

		private RuleResult ValidateEncryptionKey()
		{
			if (ProtectData)
			{
				if (string.IsNullOrWhiteSpace(EncryptionKey))
				{
					return RuleResult.Invalid(Strings.PrivateKeyIsRequired);
				}
			}

			return RuleResult.Valid();
		}

		private RuleResult ValidateSignPrivateKey()
		{
			if (ProtectData)
			{
				if (string.IsNullOrWhiteSpace(SignPrivateKey))
				{
					return RuleResult.Invalid(Strings.PrivateKeyIsRequired);
				}
			}

			return RuleResult.Valid();
		}

		private RuleResult ValidateSignPublicKey()
		{
			if (ProtectData)
			{
				if (string.IsNullOrWhiteSpace(SignPublicKey))
				{
					return RuleResult.Invalid(Strings.PublicKeyIsRequired);
				}
			}

			return RuleResult.Valid();
		}
	}
}
