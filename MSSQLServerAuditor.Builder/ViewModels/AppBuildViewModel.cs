﻿using System.IO;
using Caliburn.Micro;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Builder.Resources;
using MSSQLServerAuditor.Presentation.Validation;

namespace MSSQLServerAuditor.Builder.ViewModels
{
	public class AppBuildViewModel : PageViewModel
	{
		public AppBuildViewModel(IShell shell, BuildConfig buildConfig)
			: base(shell, buildConfig)
		{
			TargetPlatforms = new BindableCollection<string>(BuildConfig.TargetPlatforms);
			Configurations  = new BindableCollection<string>(BuildConfig.BuildConfigurations);

			Configuration  = BuildConfig.BuildConfiguration;
			TargetPlatform = BuildConfig.TargetPlatform;

			ConfigureValidationRules();
		}

		public BindableCollection<string> TargetPlatforms { get; }
		public BindableCollection<string> Configurations  { get; }

		public string BaseDirectory => Directory.GetCurrentDirectory();

		public string BuildScriptFile
		{
			get { return BuildConfig.BuildScriptFile; }
			set
			{
				if (BuildConfig.BuildScriptFile != value)
				{
					BuildConfig.BuildScriptFile = value;

					NotifyOfPropertyChange(() => BuildScriptFile);
					Validator.Validate(nameof(BuildScriptFile));
				}
			}
		}

		public string TargetPlatform
		{
			get { return BuildConfig.TargetPlatform; }
			set
			{
				if (BuildConfig.TargetPlatform != value)
				{
					BuildConfig.TargetPlatform = value;
					NotifyOfPropertyChange(() => TargetPlatform);
				}
			}
		}

		public string Configuration
		{
			get { return BuildConfig.BuildConfiguration; }
			set
			{
				if (BuildConfig.BuildConfiguration != value)
				{
					BuildConfig.BuildConfiguration = value;
					NotifyOfPropertyChange(() => Configuration);
				}
			}
		}


		public bool Compile
		{
			get { return BuildConfig.Compile; }
			set
			{
				if (BuildConfig.Compile != value)
				{
					BuildConfig.Compile = value;
					NotifyOfPropertyChange(() => Compile);
				}
			}
		}

		public bool RunTests
		{
			get { return BuildConfig.RunTests; }
			set
			{
				if (BuildConfig.RunTests != value)
				{
					BuildConfig.RunTests = value;
					NotifyOfPropertyChange(() => RunTests);
				}
			}
		}

		public override string Title => Strings.ApplicationBuild;

		private void ConfigureValidationRules()
		{
			Validator.AddRule(
				nameof(BuildScriptFile),
				() =>
				{
					if (string.IsNullOrWhiteSpace(BuildScriptFile))
					{
						return RuleResult.Invalid(Strings.BuildScriptFileIsRequired);
					}

					if (!File.Exists(BuildScriptFile))
					{
						return RuleResult.Invalid(Strings.BuildScriptFileDoesNotExist);
					}

					return RuleResult.Valid();
				}
			);
		}
	}
}
