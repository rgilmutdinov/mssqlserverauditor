﻿using MSSQLServerAuditor.Builder.Infrastructure;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Presentation.Validation;

namespace MSSQLServerAuditor.Builder.ViewModels
{
	public abstract class PageViewModel : ValidatingViewModel, IPage
	{
		private bool        _isEnabled = true;
		private BuildConfig _buildConfig;

		public PageViewModel(IShell shell, BuildConfig config)
		{
			this.Shell       = shell;
			this.BuildConfig = config;

			Validator.ResultChanged += ValidatorOnResultChanged;
		}

		public BuildConfig BuildConfig
		{
			get { return this._buildConfig; }
			set
			{
				if (this._buildConfig != value)
				{
					this._buildConfig = value;
					NotifyOfPropertyChange(() => BuildConfig);
				}
			}
		}

		public bool IsEnabled
		{
			get { return this._isEnabled; }
			set
			{
				this._isEnabled = value;
				NotifyOfPropertyChange(() => IsEnabled);
			}
		}

		public IShell Shell { get; }

		public abstract string Title { get; }

		private void ValidatorOnResultChanged(object sender, ValidationResultChangedEventArgs args)
		{
			NotifyOfPropertyChange(() => HasErrors);
		}
	}
}
