﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Caliburn.Micro;
using MSSQLServerAuditor.Builder.Infrastructure;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Builder.Resources;

namespace MSSQLServerAuditor.Builder.ViewModels
{
	public class BuildProgressViewModel : PageViewModel, IBuildProgress
	{
		private bool _isBuilding = false;

		private LogEntry _currentLogEntry;

		public BuildProgressViewModel(IShell shell, BuildConfig buildConfig)
			: base(shell, buildConfig)
		{
			LogEntries = new BindableCollection<LogEntry>();
		}

		public IObservableCollection<LogEntry> LogEntries { get; set; }

		public LogEntry CurrentLogEntry
		{
			get { return this._currentLogEntry; }
			set
			{
				if (this._currentLogEntry != value)
				{
					this._currentLogEntry = value;
					NotifyOfPropertyChange(() => CurrentLogEntry);
				}
			}
		}

		public bool IsBuilding
		{
			get { return _isBuilding; }
			set
			{
				if (this._isBuilding != value)
				{
					this._isBuilding = value;
					NotifyOfPropertyChange(() => IsBuilding);
				}
			}
		}

		public override string Title => Strings.BuildProgress;

		public void Report(LogEntry value)
		{
			LogEntries.Add(value);
			CurrentLogEntry = value;
		}

		public void Clear()
		{
			LogEntries.Clear();
			CurrentLogEntry = null;
		}

		public void CopyEntriesToClipboard(IList entriesCollection)
		{
			StringBuilder sb = new StringBuilder();

			List<LogEntry> selectedEntries = entriesCollection
				.OfType<LogEntry>()
				.ToList();

			if (selectedEntries.Any())
			{
				foreach (LogEntry entry in LogEntries)
				{
					if (selectedEntries.Contains(entry))
					{
						sb.Append(entry.Message);
						sb.AppendLine();
					}
				}

				Clipboard.SetText(sb.ToString());
			}
		}
	}
}
