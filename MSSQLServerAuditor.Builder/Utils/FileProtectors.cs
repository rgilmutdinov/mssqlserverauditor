﻿using System.Xml;
using MSSQLServerAuditor.Builder.Extensions;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Cryptography;
using MSSQLServerAuditor.Core.Cryptography.Xml;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Xml;

namespace MSSQLServerAuditor.Builder.Utils
{
	public interface IFileProtector
	{
		void MakeProtectedCopy(string srcFile, string dstFile);
	}

	public class FileEncryptor : IFileProtector
	{
		private readonly XmlEncryptor _encryptor;

		public FileEncryptor(XmlEncryptor encryptor)
		{
			Check.NotNull(encryptor, nameof(encryptor));

			this._encryptor = encryptor;
		}

		public void MakeProtectedCopy(string srcFile, string dstFile)
		{
			this._encryptor.Encrypt(srcFile, dstFile, "rsaKey");
		}
	}

	public abstract class XmlFileProtector<T> : IFileProtector
	{
		private readonly XmlEncryptor _encryptor;
		private readonly Signer       _signer;

		public XmlFileProtector(Signer signer, XmlEncryptor encryptor)
		{
			Check.NotNull(signer,    nameof(signer));
			Check.NotNull(encryptor, nameof(encryptor));

			this._signer    = signer;
			this._encryptor = encryptor;
		}

		public void MakeProtectedCopy(string srcFile, string dstFile)
		{
			T xmlObject = XmlSerialization.Deserialize<T>(srcFile);

			Sign(this._signer, xmlObject);

			XmlDocument xmlScript = XmlSerialization.SerializeToXmlDocument(xmlObject);
			this._encryptor.EncryptToFile(xmlScript, dstFile, "rsaKey");
		}

		protected abstract void Sign(Signer signer, T xmlObject);
	}

	public class ScriptProtector : XmlFileProtector<ScriptListInfo>
	{
		public ScriptProtector(Signer signer, XmlEncryptor encryptor)
			: base(signer, encryptor)
		{
			
		}

		protected override void Sign(Signer signer, ScriptListInfo xmlObject)
		{
			xmlObject.Sign(signer);
		}
	}


	public class QueryProtector : XmlFileProtector<QueryRoot>
	{
		public QueryProtector(Signer signer, XmlEncryptor encryptor)
			: base(signer, encryptor)
		{

		}

		protected override void Sign(Signer signer, QueryRoot xmlObject)
		{
			xmlObject.Sign(signer);
		}
	}

	public class QueryListProtector : XmlFileProtector<QueryListExt>
	{
		public QueryListProtector(Signer signer, XmlEncryptor encryptor)
			: base(signer, encryptor)
		{

		}

		protected override void Sign(Signer signer, QueryListExt xmlObject)
		{
			xmlObject.Sign(signer);
		}
	}
}
