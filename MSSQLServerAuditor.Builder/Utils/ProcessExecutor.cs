﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Builder.Infrastructure;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Builder.Utils
{
	public class ProcessExecutor
	{
		private readonly ILogProgress _progress;

		public ProcessExecutor(ILogProgress progress, string workingDirectory)
		{
			Check.NotNull(progress, nameof(progress));

			this._progress = progress;

			WorkingDirectory = workingDirectory;
		}

		public string WorkingDirectory { get; set; }

		public void RunProcess(string fileName, string args, CancellationToken cancelToken)
		{
			Process process = new Process
			{
				StartInfo =
				{
					FileName               = fileName,
					WorkingDirectory       = WorkingDirectory,
					RedirectStandardOutput = true,
					RedirectStandardError  = true,
					Arguments              = args,
					CreateNoWindow         = true,
					UseShellExecute        = false,
					WindowStyle            = ProcessWindowStyle.Hidden
				}
			};

			using (RegisterProcessCancellation(cancelToken, process))
			{
				process.OutputDataReceived += ProcessOnOutputDataReceived;
				process.ErrorDataReceived += ProcessOnErrorDataReceived;

				process.Start();
				process.BeginOutputReadLine();
				process.BeginErrorReadLine();

				process.WaitForExit();
			}

			cancelToken.ThrowIfCancellationRequested();
		}

		private static CancellationTokenRegistration RegisterProcessCancellation(CancellationToken cancelToken, Process process)
		{
			return cancelToken.Register(() =>
			{
				if (process != null && !process.HasExited)
				{
					process.Kill();
				}
			});
		}

		public async Task RunProcessAsync(string fileName, string args, CancellationToken cancelToken)
		{
			await Task.Run(
				() => RunProcess(fileName, args, cancelToken),
				cancelToken
			);
		}

		private void ProcessOnErrorDataReceived(object sender, DataReceivedEventArgs args)
		{
			if (args.Data != null)
			{
				this._progress.ReportError(args.Data);
			}
		}

		private void ProcessOnOutputDataReceived(object sender, DataReceivedEventArgs args)
		{
			if (args.Data != null)
			{
				this._progress.ReportData(args.Data);
			}
		}
	}
}
