﻿namespace MSSQLServerAuditor.Builder.Model
{
	public enum LogEntryType
	{
		Info,
		Data,
		Warning,
		Error,
	}

	public class LogEntry
	{
		public static readonly LogEntry Empty = new LogEntry(string.Empty, LogEntryType.Info);

		public static LogEntry Error(string message)
		{
			return new LogEntry(message, LogEntryType.Error);
		}

		public static LogEntry Info(string message)
		{
			return new LogEntry(message, LogEntryType.Info);
		}

		public static LogEntry Data(string message)
		{
			return new LogEntry(message, LogEntryType.Data);
		}

		public static LogEntry Warning(string message)
		{
			return new LogEntry(message, LogEntryType.Warning);
		}

		public LogEntry(string message, LogEntryType type)
		{
			this.Message = message;
			this.Type    = type;
		}

		public string       Message { get; }
		public LogEntryType Type    { get; }
	}
}
