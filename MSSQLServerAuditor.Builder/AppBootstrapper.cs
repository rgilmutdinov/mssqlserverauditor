﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Threading;
using Caliburn.Micro;
using MSSQLServerAuditor.Builder.Localization;
using MSSQLServerAuditor.Builder.Properties;
using MSSQLServerAuditor.Builder.Resources;
using MSSQLServerAuditor.Builder.ViewModels;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Presentation.Services;
using NLog;

namespace MSSQLServerAuditor.Builder
{
	public class AppBootstrapper : BootstrapperBase
	{
		private static readonly Logger Log = NLog.LogManager.GetCurrentClassLogger();

		private SimpleContainer _container;

		public AppBootstrapper()
		{
			Initialize();
		}

		protected override void Configure()
		{
			this._container = new SimpleContainer();

			this._container.Singleton<IFileDialogService, FileDialogService>();
			this._container.Singleton<IWindowManager,     WindowManager>();
			this._container.Singleton<IEventAggregator,   EventAggregator>();

			this._container.PerRequest<IShell, ShellViewModel>();
		}

		protected override object GetInstance(Type serviceType, string key)
		{
			return this._container.GetInstance(serviceType, key);
		}

		protected override IEnumerable<object> GetAllInstances(Type serviceType)
		{
			return this._container.GetAllInstances(serviceType);
		}

		protected override void BuildUp(object instance)
		{
			this._container.BuildUp(instance);
		}

		protected override void OnStartup(object sender, StartupEventArgs e)
		{
			LanguageItem currentLanguage = LanguageProvider.CurrentLanguage;

			LocalizationManager.Localize(currentLanguage.Name);

			// apply current UI language as default language for views
			// see: http://www.amaravadee.com/how-to-set-and-change-the-culture-in-wpf/
			FrameworkElement.LanguageProperty.OverrideMetadata(
				typeof(FrameworkElement),
				new FrameworkPropertyMetadata(
					XmlLanguage.Empty,
					null,
					CoerceCurrentLang
				)
			);

			DisplayRootViewFor<IShell>();
		}

		protected override void OnExit(object sender, EventArgs e)
		{
			try
			{
				Settings.Default.Save();
			}
			catch (Exception exc)
			{
				Log.Error(exc, "Failed to save settings");
			}
		}

		private object CoerceCurrentLang(DependencyObject dependencyObject, object baseValue)
		{
			XmlLanguage lang = baseValue as XmlLanguage;
			CultureInfo culture = CultureInfo.CurrentUICulture;

			return lang != null && lang.IetfLanguageTag.EqualsIgnoreCase(culture.Name)
				? lang
				: XmlLanguage.GetLanguage(culture.Name);
		}

		protected override void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			Exception exception = e.Exception;
			if (exception is TargetInvocationException)
			{
				exception = exception.InnerException;
			}

			HandleException(exception, false);
			e.Handled = true;
		}

		private void HandleException(Exception exc, bool isTerminating)
		{
			if (exc != null)
			{
				Log.Error(exc);

				if (!isTerminating)
				{
					MessageBox.Show(
						exc.Message,
						Strings.ApplicationError,
						MessageBoxButton.OK,
						MessageBoxImage.Error
					);
				}
			}
		}
	}
}
