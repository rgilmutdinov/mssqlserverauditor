﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CSharp;
using MSSQLServerAuditor.Builder.Infrastructure;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Builder.Utils;
using MSSQLServerAuditor.Common;

namespace MSSQLServerAuditor.Builder.Tasks
{
	public class CompileAppTask : BuilderTask
	{
		public CompileAppTask(BuildConfig config) : base(config)
		{
		}

		public override async Task ExecuteAsync(CancellationToken cancelToken, ILogProgress progress)
		{
			Dictionary<string, string> parameters = new Dictionary<string, string>
			{
				{ "-platform",       BuildConfig.TargetPlatform },
				{ "-configuration",  BuildConfig.BuildConfiguration },
				{ "-compileBuilder", "$false" },
				{ "-runTests",       "$" + BuildConfig.RunTests },
				{ "-verbosity",      "Minimal" }
			};

			progress.ReportInfo("Compiling application...");

			await new PowerShellExecutor(progress).ExecuteAsync(
				BuildConfig.BuildScriptFile,
				parameters,
				cancelToken
			);

			progress.ReportInfo("Application has been compiled.");

			await CreateEnvironmentAssemblyAsync(progress, cancelToken);
		}

		private async Task CreateEnvironmentAssemblyAsync(ILogProgress progress, CancellationToken cancelToken)
		{
			await Task.Run(() => CreateEnvironmentAssembly(progress, cancelToken), cancelToken);
		}

		private void CreateEnvironmentAssembly(ILogProgress progress, CancellationToken cancelToken)
		{
			string outputAssemblyPath = "..\\..\\out\\bin\\MSSQLServerAuditor.Metadata.dll";

			progress.EmptyLine();
			progress.ReportInfo($"Compiling satellite assembly {Path.GetFullPath(outputAssemblyPath)}...");

			CompilerParameters paremeters = new CompilerParameters
			{
				GenerateExecutable = false,
				OutputAssembly     = outputAssemblyPath,
				CompilerOptions    = $"/platform:{BuildConfig.TargetPlatform}"
			};

			CSharpCodeProvider provider = new CSharpCodeProvider();

			paremeters.ReferencedAssemblies.Add("System.dll");
			paremeters.ReferencedAssemblies.Add("System.Core.dll");
			paremeters.ReferencedAssemblies.Add(typeof(BuildDateAttribute).Assembly.Location);

			string sources = Properties.Resources.MetadataAssembly
				.Replace("${PublicKey}$",  BuildConfig.SignPublicKey ?? string.Empty)
				.Replace("${PrivateKey}$", BuildConfig.EncryptionKey ?? string.Empty);

			cancelToken.ThrowIfCancellationRequested();

			CompilerResults results = provider.CompileAssemblyFromSource(
				paremeters,
				sources
			);

			progress.EmptyLine();
			if (results.Errors.HasErrors)
			{
				progress.ReportError("Errors compiling satellite assembly.");
				foreach (CompilerError error in results.Errors)
				{
					progress.ReportError(
						$"({error.Line}, {error.Column}) {error.ErrorNumber}, {error.ErrorText}"
					);
				}
			}
			else
			{
				progress.ReportInfo("Satellite assembly has been compiled.");
			}
		}
	}
}
