﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Builder.Infrastructure;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Builder.Utils;
using MSSQLServerAuditor.Builder.Wix;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Core.Settings;

namespace MSSQLServerAuditor.Builder.Tasks
{
	public class BuildSetupTask : BuilderTask
	{
		private const string WixFiles   = "Files.wxs";
		private const string WixProduct = "Product.wxs";
		private const string WixCommon  = "Common.wxi";

		private const string WixobjFiles   = "Files.wixobj";
		private const string WixobjProduct = "Product.wixobj";

		private readonly string _sourceProjDir;
		private readonly string _tempProjDir;

		public BuildSetupTask(BuildConfig config) : base(config)
		{
			string sourceProjDir = BuildConfig.WixProjectDirectory;
			string tempProjDir   = BuildConfig.WixTempDirectory;

			if (string.IsNullOrWhiteSpace(sourceProjDir))
			{
				throw new ArgumentException("Work directory should not be empty");
			}

			if (string.IsNullOrWhiteSpace(tempProjDir))
			{
				throw new ArgumentException("Temp directory should not be empty");
			}

			if (!Directory.Exists(sourceProjDir))
			{
				throw new ArgumentException($"Wix project directory '{sourceProjDir}' does not exists");
			}

			this._sourceProjDir = Path.GetFullPath(sourceProjDir);
			this._tempProjDir   = Path.GetFullPath(tempProjDir);
		}

		public override async Task ExecuteAsync(CancellationToken cancelToken, ILogProgress progress)
		{
			await Task.Run(async () =>
			{
				progress.EmptyLine();
				progress.ReportInfo("Building setup...");

				try
				{
					progress.ReportInfo("Creating temporary files...");
					if (!Directory.Exists(this._tempProjDir))
					{
						Directory.CreateDirectory(this._tempProjDir);
					}

					string wixCommonCopy = GetTempPath(WixCommon);
					string wixFileCopy   = GetTempPath(WixFiles);
					string wixProdCopy   = GetTempPath(WixProduct);

					// copy wix template files to temp directory
					File.Copy(GetSourcePath(WixCommon), wixCommonCopy, true);
					File.Copy(GetSourcePath(WixFiles), wixFileCopy, true);
					File.Copy(GetSourcePath(WixProduct), wixProdCopy, true);

					// load wix setup from temp files
					progress.ReportInfo("Loading setup project...");

					WixSetup wixSetup = WixSetup.Load(
						wixFileCopy,
						wixProdCopy
					);

					wixSetup.LogProgress = progress;

					cancelToken.ThrowIfCancellationRequested();

					progress.ReportInfo("Configuring setup...");

					AppSettings appSettings = new AppSettings(
						BuildConfig.UserSettingsFile,
						BuildConfig.UserSettingsFile,
						BuildConfig.SystemSettingsFile
					);

					WixConfigurator configurator = new WixConfigurator(BuildConfig, appSettings);

					try
					{
						// configure setup: add scripts, images, templates and etc.
						configurator.Configure(wixSetup);

						cancelToken.ThrowIfCancellationRequested();

						// save changes
						wixSetup.Save(wixFileCopy, wixProdCopy);

						// link and compile setup
						progress.ReportInfo("Linking and compiling setup...");
						await CompileSetup(cancelToken, progress);

						progress.ReportInfo("Setup has been built.");
					}
					finally
					{
						configurator.Cleanup();
					}
				}
				finally
				{
					progress.ReportInfo("Cleaning up temporary files...");

					File.Delete(GetTempPath(WixCommon));
					File.Delete(GetTempPath(WixFiles));
					File.Delete(GetTempPath(WixProduct));

					File.Delete(GetTempPath(WixobjFiles));
					File.Delete(GetTempPath(WixobjProduct));
				}
			}, cancelToken);
		}

		private string GetSourcePath(string filename)
		{
			return Path.Combine(this._sourceProjDir, filename);
		}

		private string GetTempPath(string filename)
		{
			return Path.Combine(this._tempProjDir, filename);
		}

		private async Task CompileSetup(CancellationToken cancelToken, ILogProgress progress)
		{
			ProcessExecutor executor = new ProcessExecutor(progress, this._tempProjDir);

			cancelToken.ThrowIfCancellationRequested();

			progress.ReportInfo("Running candle...");
			await RunCandle(executor, progress, cancelToken);

			cancelToken.ThrowIfCancellationRequested();

			progress.ReportInfo("Running light...");
			await RunLight(executor, progress, cancelToken);
		}

		private async Task RunCandle(
			ProcessExecutor   executor,
			ILogProgress      progress,
			CancellationToken cancelToken
		)
		{
			string wixDir   = BuildConfig.WixDirectory;
			string platform = BuildConfig.TargetPlatform;
			
			string candlePath = Path.Combine(wixDir, "candle");
			string wixUiPath  = Path.Combine(wixDir, "WixUIExtension.dll");

			string currentDir  = AppDomain.CurrentDomain.BaseDirectory;
			string binDir      = Path.GetFullPath(Path.Combine(currentDir, "..\\..\\out\\bin"));
			string outSetupDir = Path.GetFullPath(Path.Combine(currentDir, "..\\..\\out\\Setup"));

			string wxsFilesPath   = Path.Combine(this._tempProjDir, "Files.wxs");
			string wxsProductPath = Path.Combine(this._tempProjDir, "Product.wxs");

			string command =
				$"-dBinSourceDir=\"{binDir}\" -dProjectDir=\"{this._tempProjDir}\" -dPlatform={platform} -dTargetExt=.msi -dOutDir=\"{outSetupDir}\" -arch {platform} -ext \"{wixUiPath}\" \"{wxsFilesPath}\" \"{wxsProductPath}\"";

			progress.ReportInfo("Execute command:");
			progress.ReportInfo(command);

			await executor.RunProcessAsync(
				candlePath,
				command,
				cancelToken
			);
		}

		private async Task RunLight(
			ProcessExecutor   executor,
			ILogProgress      progress,
			CancellationToken cancelToken
		)
		{
			string wixDir = BuildConfig.WixDirectory;

			string lightPath = Path.Combine(wixDir, "light");
			string wixUiPath = Path.Combine(wixDir, "WixUIExtension.dll");

			string currentDir = AppDomain.CurrentDomain.BaseDirectory;

			string version    = CurrentAssembly.Version;
			string setupName  = string.Format($"MSSQLServerAuditor {version} ({BuildConfig.TargetPlatform}).msi");
			string outSetup   = Path.GetFullPath(Path.Combine(currentDir, $"..\\..\\out\\Setup\\{setupName}"));

			string wixobjFilesPath   = Path.Combine(this._tempProjDir, "Files.wixobj");
			string wixobjProductPath = Path.Combine(this._tempProjDir, "Product.wixobj");

			string command =
				$"-sice:ICE60 -ext \"{wixUiPath}\" -spdb -out \"{outSetup}\" \"{wixobjFilesPath}\" \"{wixobjProductPath}\"";

			progress.ReportInfo("Execute command:");
			progress.ReportInfo(command);

			await executor.RunProcessAsync(
				lightPath,
				command,
				cancelToken
			);
		}
	}
}
