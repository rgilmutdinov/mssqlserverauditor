﻿using MSSQLServerAuditor.Core.Cryptography;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Builder.Extensions
{
	public static class DomainExtensions
	{
		public static void Sign(this QueryRoot queryRoot, Signer signer)
		{
			foreach (QueryListInfo query in queryRoot.Queries)
			{
				foreach (QueryInfoExt queryInfoExt in query.Queries)
				{
					SignQueryInfo(queryInfoExt, signer);
				}
			}
		}

		public static void Sign(this ScriptListInfo scriptRoot, Signer signer)
		{
			foreach (ScriptInfo script in scriptRoot.Scripts)
			{
				script.Signature = signer.Sign(script.Text);
			}
		}

		public static void Sign(this QueryListExt queryList, Signer signer)
		{
			foreach (QueryInfo queryInfo in queryList.Queries)
			{
				SignQueryInfo(queryInfo, signer);
			}
		}

		private static void SignQueryInfo(QueryInfo queryInfo, Signer signer)
		{
			foreach (QueryItemInfo queryItemInfo in queryInfo.Items)
			{
				queryItemInfo.Signature = signer.Sign(queryItemInfo.Text);
			}

			foreach (QueryItemInfo queryItemInfo in queryInfo.GroupSelect)
			{
				queryItemInfo.Signature = signer.Sign(queryItemInfo.Text);
			}

			foreach (QueryItemInfo queryItemInfo in queryInfo.DatabaseSelect)
			{
				queryItemInfo.Signature = signer.Sign(queryItemInfo.Text);
			}

			foreach (FillStatement statement in queryInfo.FillStatements)
			{
				// sign fill statements
				// check if statement is defined internally
				if (string.IsNullOrWhiteSpace(statement.File))
				{
					// sign it
					statement.Signature = signer.Sign(statement.Text);
				}
			}
		}
	}
}
