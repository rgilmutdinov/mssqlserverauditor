﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MSSQLServerAuditor.Service")]
[assembly: AssemblyDescription("Reporting and Audit tool for MS SQL Servers (2000/2005/2008/2008R2/2012/2014)")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components. If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("60631743-E76C-414A-BCCC-CE8DC3C6829F")]
