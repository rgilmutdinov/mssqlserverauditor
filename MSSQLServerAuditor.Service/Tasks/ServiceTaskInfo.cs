﻿using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Session;

namespace MSSQLServerAuditor.Service.Tasks
{
	public class ServiceTaskInfo
	{
		public ServiceTaskInfo(AuditContext auditContext, TemplateNodeInfo tnInfo)
		{
			Check.NotNull(auditContext, nameof(auditContext));
			Check.NotNull(tnInfo,       nameof(tnInfo));

			AuditContext     = auditContext;
			TemplateNodeInfo = tnInfo;
		}

		public AuditContext     AuditContext     { get; private set; }
		public TemplateNodeInfo TemplateNodeInfo { get; private set; }

		public bool UpdateHierarchically { get; set; } = false;
		public int  UpdateDeep           { get; set; } = -1; // infinite deep

		public bool?  SendMessage             { get; set; }
		public bool   SendMessageNonEmptyOnly { get; set; }
		public string MessageRecipients       { get; set; }
		public string MessageLanguage         { get; set; }

		public ServiceTaskInfo ParentTask { get; set; }

		public long Id => TemplateNodeInfo.NodeInstanceId;
	}
}
