﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Tasks;

namespace MSSQLServerAuditor.Service.Tasks
{
	public class ServiceTask : QueueTask
	{
		private readonly ServiceTaskInfo     _taskInfo;
		private readonly ServiceTaskExecutor _executor;

		public ServiceTask(ServiceTaskInfo taskInfo, ServiceTaskExecutor executor)
		{
			Check.NotNull(taskInfo, nameof(taskInfo));
			Check.NotNull(executor, nameof(executor));

			this._taskInfo = taskInfo;
			this._executor = executor;
		}

		public override long   Id    => this._taskInfo.Id;
		public override string Title => Id.ToString();

		public override bool UpdateHierarchically => this._taskInfo.UpdateHierarchically;
		public override int  UpdateDeep           => this._taskInfo.UpdateDeep;

		protected override async Task ExecuteAsync(CancellationToken cancelToken)
		{
			await this._executor.ExecuteUpdateAsync(
				this._taskInfo,
				cancelToken
			);
		}

		public override IEnumerable<QueueTask> GetChildTasks()
		{
			TemplateNodeInfo tnInfo = this._taskInfo.TemplateNodeInfo;

			foreach (TemplateNodeInfo tnChild in tnInfo.Children)
			{
				ServiceTaskInfo childTaskInfo = new ServiceTaskInfo(this._taskInfo.AuditContext, tnChild)
				{
					ParentTask = this._taskInfo
				};

				if (this._taskInfo.UpdateDeep < 0) // infinite update deep
				{
					childTaskInfo.UpdateHierarchically = true;
					childTaskInfo.UpdateDeep           = -1;
				}
				else
				{
					childTaskInfo.UpdateHierarchically = true;
					childTaskInfo.UpdateDeep           = this._taskInfo.UpdateDeep - 1;
				}

				ServiceTask childTask = new ServiceTask(childTaskInfo, this._executor);

				yield return childTask;
			}
		}
	}
}
