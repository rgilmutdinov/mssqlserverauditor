﻿using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Schedule;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Tasks;
using MSSQLServerAuditor.Service.Tasks;

namespace MSSQLServerAuditor.Service
{
	public class ServiceJobScheduler : AbstractJobScheduler
	{
		private readonly QueueTaskManager    _taskManager;
		private readonly ServiceTaskExecutor _executor;

		public ServiceJobScheduler(QueueTaskManager taskManager, ServiceTaskExecutor executor)
		{
			Check.NotNull(taskManager, nameof(taskManager));
			Check.NotNull(executor,    nameof(executor));

			this._taskManager = taskManager;
			this._executor    = executor;
		}

		protected override void ExecuteJob(
			AuditContext context,
			JobInfo      jobInfo
		)
		{
			ServiceTaskInfo taskInfo = new ServiceTaskInfo(context, jobInfo.TemplateNodeInfo);

			ScheduleDetails schedule = jobInfo.ScheduleDetails;

			if (schedule != null)
			{
				taskInfo.UpdateHierarchically    = schedule.UpdateHierarchically;
				taskInfo.UpdateDeep              = schedule.UpdateDeep;
				taskInfo.SendMessage             = schedule.SendMessage;
				taskInfo.SendMessageNonEmptyOnly = schedule.SendMessageNonEmptyOnly;
				taskInfo.MessageRecipients       = schedule.MessageRecipients;
				taskInfo.MessageLanguage         = schedule.MessageLanguage;
			}
			
			ServiceTask task = new ServiceTask(taskInfo, this._executor);

			this._taskManager.Enqueue(task);
		}
	}
}
