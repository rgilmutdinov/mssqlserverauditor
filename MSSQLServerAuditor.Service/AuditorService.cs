﻿using System;
using System.IO;
using System.ServiceProcess;
using System.Timers;
using MSSQLServerAuditor.Core.Persistence.Providers;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Xml;
using NLog;

namespace MSSQLServerAuditor.Service
{
	public partial class AuditorService : ServiceBase
	{
		private const int DefaultUpdateSchedulesTimeoutSec = 60;

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private ScheduleManager _scheduleManager;

		private Timer _timerUpdateSchedules;
		private Timer _timerInitialization;
		private bool  _initializing  = false;
		private bool  _initialized   = false;
		private bool  _updateRunning = false;

		public AuditorService()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Start service
		/// </summary>
		public void Run()
		{
			Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

			this._timerInitialization = new Timer
			{
				Interval = 500
			};

			this._timerInitialization.Elapsed += OnInintialize;
			this._timerInitialization.Enabled = true;

			Log.Debug("Initializing storage...");
		}

		/// <summary>
		/// Shutdowns service
		/// </summary>
		public void Shutdown()
		{
			if (this._timerUpdateSchedules != null && this._timerUpdateSchedules.Enabled)
			{
				this._timerUpdateSchedules.Enabled = false;
			}

			if (this._timerInitialization != null && this._timerInitialization.Enabled)
			{
				this._timerInitialization.Enabled = false;
			}

			if (this._initialized)
			{
				this._scheduleManager?.Shutdown();
			}
		}

		/// <summary>
		/// On service start
		/// </summary>
		/// <param name="args"></param>
		protected override void OnStart(string[] args)
		{
			Run();
		}

		/// <summary>
		/// On service stop
		/// </summary>
		protected override void OnStop()
		{
			Shutdown();
		}

		private void OnInintialize(object sender, ElapsedEventArgs e)
		{
			if (!this._initialized && !this._initializing)
			{
				this._initializing = true;

				IAppSettings appSettings = AppSettings.CreateDefault();

				StorageConnectionInfo connectionInfo = StorageInfoProvider.GetConnectionInfo(appSettings);

				XmlLoader xmlLoader = XmlLoader.Create(
					appSettings,
					connectionInfo.StorageType
				);

				IStorageManager storageManager = StorageProviders.CreateProvider(connectionInfo, xmlLoader).CreateStorages();

				try
				{
					this._scheduleManager = new ScheduleManager(
						storageManager,
						appSettings,
						xmlLoader
					);

					this._scheduleManager.StartScheduler();

					this._initialized = true;
					this._timerInitialization.Enabled = false;

					EnableUpdateTimers(appSettings);

					Log.Debug("Storage initialized, timers enabled");
				}
				catch (Exception exc)
				{
					Log.Error(exc, "Initialization failed");
				}
				finally
				{
					this._initializing = false;
				}
			}
		}

		/// <summary>
		/// Set update timers
		/// </summary>
		private void EnableUpdateTimers(IAppSettings settings)
		{
			int updateTimeoutSec = GetUpdateSchedulesTimeout(settings);

			this._timerUpdateSchedules = new Timer
			{
				Interval = TimeSpan.FromSeconds(updateTimeoutSec).TotalMilliseconds
			};

			this._timerUpdateSchedules.Elapsed += UpdateSchedulesCallback;
			this._timerUpdateSchedules.Enabled = true;
		}

		/// <summary>
		/// Update schedules timer elapsed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void UpdateSchedulesCallback(object sender, ElapsedEventArgs e)
		{
			if (!this._updateRunning && this._scheduleManager != null)
			{
				this._updateRunning = true;

				try
				{
					this._scheduleManager.UpdateSchedules();
				}
				finally
				{
					this._updateRunning = false;
				}
			}
		}

		private static int GetUpdateSchedulesTimeout(IAppSettings settings)
		{
			try
			{
				return settings.System.ServiceDataUpdateTimeout;
			}
			catch (Exception exc)
			{
				Log.Warn(
					exc,
					$"Failed to get update service timeout from the config, applying default value: {DefaultUpdateSchedulesTimeoutSec}"
				);

				return DefaultUpdateSchedulesTimeoutSec;
			}
		}
	}
}
