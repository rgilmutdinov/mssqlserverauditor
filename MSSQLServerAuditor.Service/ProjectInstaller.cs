using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

namespace MSSQLServerAuditor.Service
{
	[RunInstaller(true)]
	public partial class ProjectInstaller : Installer
	{
		public ProjectInstaller()
		{
			InitializeComponent();
		}

		public override void Install(IDictionary stateSaver)
		{
			SetServiceName();
			base.Install(stateSaver);
		}

		public override void Uninstall(IDictionary savedState)
		{
			SetServiceName();
			base.Uninstall(savedState);
		}

		private void SetServiceName()
		{
			string serviceName = Context.Parameters["servicename"];
			string displayName = Context.Parameters["servicedisplayname"];

			if (!string.IsNullOrEmpty(serviceName))
			{
				AuditorServiceInstaller.ServiceName = serviceName;
			}

			if (!string.IsNullOrEmpty(displayName))
			{
				AuditorServiceInstaller.DisplayName = displayName;
			}
		}
	}
}
