﻿using CommandLine;

namespace MSSQLServerAuditor.Service
{
	public class Options
	{
		[Option(
			'i',
			"install",
			DefaultValue = false,
			HelpText = "Installs the service."
		)]
		public bool Install { get; set; }

		[Option(
			'u',
			"uninstall",
			DefaultValue = false,
			HelpText = "Uninstalls the service."
		)]
		public bool Uninstall { get; set; }

		[Option(
			'c',
			"console",
			DefaultValue = false,
			HelpText = "Starts service in console mode."
		)]
		public bool Console { get; set; }

		[Option(
			'h',
			"help",
			DefaultValue = false,
			HelpText = "Displays help infomation and available options."
		)]
		public bool Help { get; set; }

		[Option(
			'n',
			"servicename",
			HelpText = "Set service name."
		)]
		public string ServiceName { get; set; }

		[Option(
			'd',
			"servicedisplayname",
			HelpText = "Set service display name."
		)]
		public string DisplayName { get; set; }
	}
}
