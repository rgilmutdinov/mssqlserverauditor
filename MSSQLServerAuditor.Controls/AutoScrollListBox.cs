﻿using System.Windows.Controls;

namespace MSSQLServerAuditor.Controls
{
	public class AutoScrollListBox : ListBox
	{
		public AutoScrollListBox()
		{
			SelectionChanged += delegate { ScrollIntoView(SelectedItem); };
		}
	}
}
