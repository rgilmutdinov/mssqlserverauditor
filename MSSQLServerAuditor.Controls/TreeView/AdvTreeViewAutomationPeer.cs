﻿using System.Windows.Automation.Peers;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public class AdvTreeViewAutomationPeer : FrameworkElementAutomationPeer
	{
		internal AdvTreeViewAutomationPeer(AdvTreeView owner ): base(owner)
		{
		}

		protected override AutomationControlType GetAutomationControlTypeCore()
		{
			return AutomationControlType.Tree;
		}
	}
}
