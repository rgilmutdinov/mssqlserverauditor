﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;

namespace MSSQLServerAuditor.Controls.TreeView
{
	/// <summary>
	/// Collection that validates that inserted nodes do not have another _parent.
	/// </summary>
	public sealed class AdvTreeNodeCollection : IList<AdvTreeNode>, INotifyCollectionChanged
	{
		private readonly AdvTreeNode _parent;
		private List<AdvTreeNode> _list = new List<AdvTreeNode>();
		private bool _isRaisingEvent;
		
		public AdvTreeNodeCollection(AdvTreeNode parent)
		{
			this._parent = parent;
		}
		
		public event NotifyCollectionChangedEventHandler CollectionChanged;

		private void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
		{
			Debug.Assert(!_isRaisingEvent);
			this._isRaisingEvent = true;
			try
			{
				this._parent.OnChildrenChanged(e);
				CollectionChanged?.Invoke(this, e);
			}
			finally
			{
				this._isRaisingEvent = false;
			}
		}
		
		private void ThrowOnReentrancy()
		{
			if (this._isRaisingEvent)
			{
				throw new InvalidOperationException();
			}
		}
		
		private void ThrowIfValueIsNullOrHasParent(AdvTreeNode node)
		{
			if (node == null)
			{
				throw new ArgumentNullException(nameof(node));
			}

			if (node.Parent != null)
			{
				throw new ArgumentException("The node already has a _parent", nameof(node));
			}
		}
		
		public AdvTreeNode this[int index] {
			get
			{
				return this._list[index];
			}
			set
			{
				ThrowOnReentrancy();
				AdvTreeNode oldItem = this._list[index];

				if (oldItem == value)
				{
					return;
				}

				ThrowIfValueIsNullOrHasParent(value);
				this._list[index] = value;
				OnCollectionChanged(new NotifyCollectionChangedEventArgs(
					NotifyCollectionChangedAction.Replace, 
					value, 
					oldItem, 
					index)
				);
			}
		}
		
		public int Count => _list.Count;

		bool ICollection<AdvTreeNode>.IsReadOnly => false;

		public int IndexOf(AdvTreeNode node)
		{
			if (node == null || node.Parent != this._parent)
			{
				return -1;
			}

			return this._list.IndexOf(node);
		}

		public void Insert(int index, AdvTreeNode node)
		{
			ThrowOnReentrancy();
			ThrowIfValueIsNullOrHasParent(node);
			this._list.Insert(index, node);

			OnCollectionChanged(new NotifyCollectionChangedEventArgs(
				NotifyCollectionChangedAction.Add,
				node, 
				index)
			);
		}
		
		public void InsertRange(int index, IEnumerable<AdvTreeNode> nodes)
		{
			if (nodes == null)
			{
				throw new ArgumentNullException(nameof(nodes));
			}

			ThrowOnReentrancy();
			List<AdvTreeNode> newNodes = nodes.ToList();
			if (newNodes.Count == 0)
			{
				return;
			}

			foreach (AdvTreeNode node in newNodes)
			{
				ThrowIfValueIsNullOrHasParent(node);
			}

			this._list.InsertRange(index, newNodes);
			OnCollectionChanged(new NotifyCollectionChangedEventArgs(
				NotifyCollectionChangedAction.Add,
				newNodes,
				index)
			);
		}
		
		public void RemoveAt(int index)
		{
			ThrowOnReentrancy();
			AdvTreeNode oldItem = this._list[index];
			this._list.RemoveAt(index);
			OnCollectionChanged(new NotifyCollectionChangedEventArgs(
				NotifyCollectionChangedAction.Remove,
				oldItem, 
				index)
			);
		}
		
		public void RemoveRange(int index, int count)
		{
			ThrowOnReentrancy();
			if (count == 0)
			{
				return;
			}

			List<AdvTreeNode> oldItems = this._list.GetRange(index, count);
			_list.RemoveRange(index, count);
			OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, oldItems, index));
		}
		
		public void Add(AdvTreeNode node)
		{
			ThrowOnReentrancy();
			ThrowIfValueIsNullOrHasParent(node);
			this._list.Add(node);
			OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, node, _list.Count - 1));
		}
		
		public void AddRange(IEnumerable<AdvTreeNode> nodes)
		{
			InsertRange(this.Count, nodes);
		}
		
		public void Clear()
		{
			ThrowOnReentrancy();
			List<AdvTreeNode> oldList = this._list;
			this._list = new List<AdvTreeNode>();
			OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, oldList, 0));
		}
		
		public bool Contains(AdvTreeNode node)
		{
			return IndexOf(node) >= 0;
		}
		
		public void CopyTo(AdvTreeNode[] array, int arrayIndex)
		{
			this._list.CopyTo(array, arrayIndex);
		}
		
		public bool Remove(AdvTreeNode item)
		{
			int pos = IndexOf(item);
			if (pos >= 0)
			{
				RemoveAt(pos);
				return true;
			}

			return false;
		}
		
		public IEnumerator<AdvTreeNode> GetEnumerator()
		{
			return this._list.GetEnumerator();
		}
		
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return this._list.GetEnumerator();
		}
		
		public void RemoveAll(Predicate<AdvTreeNode> match)
		{
			if (match == null)
			{
				throw new ArgumentNullException(nameof(match));
			}

			ThrowOnReentrancy();
			int firstToRemove = 0;
			for (int i = 0; i < this._list.Count; i++)
			{
				bool removeNode;
				this._isRaisingEvent = true;
				try
				{
					removeNode = match(this._list[i]);
				}
				finally
				{
					this._isRaisingEvent = false;
				}

				if (!removeNode)
				{
					if (firstToRemove < i)
					{
						RemoveRange(firstToRemove, i - firstToRemove);
						i = firstToRemove - 1;
					}
					else
					{
						firstToRemove = i + 1;
					}

					Debug.Assert(firstToRemove == i + 1);
				}
			}

			if (firstToRemove < this._list.Count)
			{
				RemoveRange(firstToRemove, this._list.Count - firstToRemove);
			}
		}
	}
}
