﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public class GeneralAdorner : Adorner
	{
		public GeneralAdorner(UIElement target)
			: base(target)
		{
		}

		FrameworkElement _child;

		public FrameworkElement Child
		{
			get
			{
				return this._child;
			}
			set
			{
				if (this._child != value)
				{
					RemoveVisualChild(this._child);
					RemoveLogicalChild(this._child);

					this._child = value;

					AddLogicalChild(value);
					AddVisualChild(value);

					InvalidateMeasure();
				}
			}
		}

		protected override int VisualChildrenCount => this._child == null ? 0 : 1;

		protected override Visual GetVisualChild(int index)
		{
			return this._child;
		}

		protected override Size MeasureOverride(Size constraint)
		{
			if (this._child != null)
			{
				this._child.Measure(constraint);
				return this._child.DesiredSize;
			}
			return new Size();
		}

		protected override Size ArrangeOverride(Size finalSize)
		{
			if (this._child != null)
			{
				this._child.Arrange(new Rect(finalSize));
				return finalSize;
			}
			return new Size();
		}
	}
}
