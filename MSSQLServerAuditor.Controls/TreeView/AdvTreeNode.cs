﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Async;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public partial class AdvTreeNode : PropertyChangedBase
	{
		private AdvTreeNodeCollection _modelChildren;
		private AdvTreeNode           _modelParent;
		private bool                  _isVisible = true;
		private bool                  _isHidden;
		private bool                  _isExpanded;
		private bool                  _isSelected;
		private bool                  _lazyLoading;
		private bool                  _isEditing;
		private bool?                 _isChecked;
		private bool                  _canExpandRecursively = true;

		private void UpdateIsVisible(bool parentIsVisible, bool updateFlattener)
		{
			bool newIsVisible = parentIsVisible && !this._isHidden;
			if (this._isVisible != newIsVisible)
			{
				this._isVisible = newIsVisible;
				
				// invalidate the augmented data
				AdvTreeNode node = this;
				while (node != null && node._totalListLength >= 0)
				{
					node._totalListLength = -1;
					node = node._listParent;
				}

				// Remember the removed nodes:
				List<AdvTreeNode> removedNodes = null;
				if (updateFlattener && !newIsVisible)
				{
					removedNodes = VisibleDescendantsAndSelf().ToList();
				}

				// also update the model children:
				UpdateChildIsVisible(false);
				
				// Validate our invariants:
				if (updateFlattener)
				{
					CheckRootInvariants();
				}

				// Tell the flattener about the removed nodes:
				if (removedNodes != null)
				{
					TreeFlattener flattener = GetListRoot().TreeFlattener;
					if (flattener != null)
					{
						flattener.NodesRemoved(GetVisibleIndexForNode(this), removedNodes);
						foreach (AdvTreeNode removedNode in removedNodes)
						{
							removedNode.OnIsVisibleChanged();
						}
					}
				}
				// Tell the flattener about the new nodes:
				if (updateFlattener && newIsVisible)
				{
					TreeFlattener flattener = GetListRoot().TreeFlattener;
					if (flattener != null)
					{
						flattener.NodesInserted(GetVisibleIndexForNode(this), VisibleDescendantsAndSelf());
						foreach (AdvTreeNode n in VisibleDescendantsAndSelf())
						{
							n.OnIsVisibleChanged();
						}
					}
				}
			}
		}
		
		protected virtual void OnIsVisibleChanged() {}
		
		private void UpdateChildIsVisible(bool updateFlattener)
		{
			if (this._modelChildren != null && _modelChildren.Count > 0)
			{
				bool showChildren = this._isVisible && this._isExpanded;
				foreach (AdvTreeNode child in this._modelChildren)
				{
					child.UpdateIsVisible(showChildren, updateFlattener);
				}
			}
		}
		
		#region Main
		
		public AdvTreeNode()
		{
		}
		
		public AdvTreeNodeCollection Children => this._modelChildren ?? (this._modelChildren = new AdvTreeNodeCollection(this));

		public AdvTreeNode Parent => this._modelParent;

		public virtual object AuxText => null;

		public virtual object Text => null;

		public virtual Brush Foreground => SystemColors.WindowTextBrush;

		public virtual Brush AuxForeground => SystemColors.WindowTextBrush;

		public virtual object Icon => null;

		public virtual object OverlayIcon => null;

		public virtual object ToolTip => null;

		public int Level => Parent?.Level + 1 ?? 0;

		public bool IsRoot => Parent == null;
		
		public bool IsHidden
		{
			get { return this._isHidden; }
			set
			{
				if (this._isHidden != value)
				{
					this._isHidden = value;
					if (this._modelParent != null)
					{
						UpdateIsVisible(this._modelParent._isVisible && this._modelParent._isExpanded, true);
					}

					NotifyOfPropertyChange(() => IsHidden);
					Parent?.NotifyOfPropertyChange(() => ShowExpander);
				}
			}
		}
		
		/// <summary>
		/// Return true when this node is not hidden and when all parent nodes are expanded and not hidden.
		/// </summary>
		public bool IsVisible => this._isVisible;
		
		public bool IsSelected
		{
			get { return this._isSelected; }
			set
			{
				if (this._isSelected != value)
				{
					this._isSelected = value;
					NotifyOfPropertyChange(() => IsSelected);
				}
			}
		}
		
		#endregion
		
		#region OnChildrenChanged
		protected internal virtual void OnChildrenChanged(NotifyCollectionChangedEventArgs e)
		{
			if (e.OldItems != null)
			{
				foreach (AdvTreeNode node in e.OldItems)
				{
					Debug.Assert(node._modelParent == this);
					node._modelParent = null;

					Debug.WriteLine("Removing {0} from {1}", node, this);

					AdvTreeNode removeEnd = node;
					while (removeEnd._modelChildren != null && removeEnd._modelChildren.Count > 0)
					{
						removeEnd = removeEnd._modelChildren.Last();
					}

					List<AdvTreeNode> removedNodes = null;
					int visibleIndexOfRemoval = 0;
					if (node._isVisible)
					{
						visibleIndexOfRemoval = GetVisibleIndexForNode(node);
						removedNodes = node.VisibleDescendantsAndSelf().ToList();
					}
					
					RemoveNodes(node, removeEnd);
					
					if (removedNodes != null)
					{
						TreeFlattener flattener = GetListRoot().TreeFlattener;
						flattener?.NodesRemoved(visibleIndexOfRemoval, removedNodes);
					}
				}
			}
			if (e.NewItems != null)
			{
				AdvTreeNode insertionPos = e.NewStartingIndex == 0 ? null : _modelChildren[e.NewStartingIndex - 1];

				foreach (AdvTreeNode node in e.NewItems)
				{
					Debug.Assert(node._modelParent == null);
					node._modelParent = this;
					node.UpdateIsVisible(_isVisible && _isExpanded, false);
					//Debug.WriteLine("Inserting {0} after {1}", node, insertionPos);
					
					while (insertionPos?._modelChildren != null && insertionPos._modelChildren.Count > 0)
					{
						insertionPos = insertionPos._modelChildren.Last();
					}

					InsertNodeAfter(insertionPos ?? this, node);
					
					insertionPos = node;
					if (node._isVisible)
					{
						TreeFlattener flattener = GetListRoot().TreeFlattener;
						flattener?.NodesInserted(
							GetVisibleIndexForNode(node), 
							node.VisibleDescendantsAndSelf()
						);
					}
				}
			}

			NotifyOfPropertyChange(() => ShowExpander);
			RaiseIsLastChangedIfNeeded(e);
		}
		#endregion
		
		#region Expanding / LazyLoading
		
		public virtual object ExpandedIcon => Icon;

		public virtual bool ShowExpander
		{
			get { return LazyLoading || Children.Any(c => !c._isHidden); }
		}

		public bool IsExpanded
		{
			get { return this._isExpanded; }
			set
			{
				if (value)
				{
					NotifyTaskCompletion.Create(Expand(CancellationToken.None));
				}
				else
				{
					NotifyTaskCompletion.Create(Collapse(CancellationToken.None));
				}
			}
		}

		public async Task Expand()
		{
			await Toggle(CancellationToken.None, true);
		}

		public async Task Collapse()
		{
			await Toggle(CancellationToken.None, false);
		}

		public async Task Expand(CancellationToken token)
		{
			await Toggle(token, true);
		}

		public async Task Collapse(CancellationToken token)
		{
			await Toggle(token, false);
		}

		internal async Task Toggle(bool expand)
		{
			await Toggle(CancellationToken.None, expand);
		}

		internal async Task Toggle(CancellationToken token, bool expand)
		{
			if (this._isExpanded != expand)
			{
				this._isExpanded = expand;
				if (this._isExpanded)
				{
					await LoadChildrenOnExpandAsync(token);
					OnExpanding();
				}
				else
				{
					OnCollapsing();
				}

				UpdateChildIsVisible(true);
				NotifyOfPropertyChange(() => IsExpanded);
			}
		}

		protected virtual void OnExpanding() {}
		protected virtual void OnCollapsing() {}
		
		public bool LazyLoading
		{
			get { return this._lazyLoading; }
			set
			{
				this._lazyLoading = value;
				if (this._lazyLoading)
				{
					NotifyTaskCompletion.Create(Collapse);
					if (this._canExpandRecursively)
					{
						this._canExpandRecursively = false;
						NotifyOfPropertyChange(() => CanExpandRecursively);
					}
				}

				NotifyOfPropertyChange(() => LazyLoading);
				NotifyOfPropertyChange(() => ShowExpander);
			}
		}
		
		/// <summary>
		/// Gets whether this node can be expanded recursively.
		/// If not overridden, this property returns false if the node is using lazy-loading, and true otherwise.
		/// </summary>
		public virtual bool CanExpandRecursively => this._canExpandRecursively;

		public virtual bool ShowIcon => Icon != null;

		protected virtual async Task LoadChildrenAsync(CancellationToken token)
		{
			await Tasks.FromException(new NotSupportedException(GetType().Name + " does not support async lazy loading"));
		}

		protected virtual async Task LoadChildrenOnExpandAsync(CancellationToken token)
		{
			await EnsureLazyChildren(token);
		}

		private async Task EnsureLazyChildren(CancellationToken token, bool reload = false)
		{
			if (LazyLoading || reload)
			{
				LazyLoading = false;

				if (!token.IsCancellationRequested)
				{
					await LoadChildrenAsync(token);
				}
			}
		}

		#endregion

		#region Ancestors / Descendants

		public IEnumerable<AdvTreeNode> Descendants()
		{
			return TreeTraversal.PreOrder(this.Children, n => n.Children);
		}
		
		public IEnumerable<AdvTreeNode> DescendantsAndSelf()
		{
			return TreeTraversal.PreOrder(this, n => n.Children);
		}
		
		internal IEnumerable<AdvTreeNode> VisibleDescendants()
		{
			return TreeTraversal.PreOrder(this.Children.Where(c => c._isVisible), n => n.Children.Where(c => c._isVisible));
		}
		
		internal IEnumerable<AdvTreeNode> VisibleDescendantsAndSelf()
		{
			return TreeTraversal.PreOrder(this, n => n.Children.Where(c => c._isVisible));
		}
		
		public IEnumerable<AdvTreeNode> Ancestors()
		{
			for (AdvTreeNode node = this.Parent; node != null; node = node.Parent)
			{
				yield return node;
			}
		}
		
		public IEnumerable<AdvTreeNode> AncestorsAndSelf()
		{
			for (AdvTreeNode node = this; node != null; node = node.Parent)
			{
				yield return node;
			}
		}
		
		#endregion
		
		#region Editing
		
		public virtual bool IsEditable => false;
		
		public bool IsEditing
		{
			get { return this._isEditing; }
			set
			{
				if (this._isEditing != value)
				{
					this._isEditing = value;

					NotifyOfPropertyChange(() => IsEditing);
				}
			}
		}
		
		public virtual string LoadEditText()
		{
			return null;
		}
		
		public virtual bool SaveEditText(string value)
		{
			return true;
		}
		
		#endregion
		
		#region Checkboxes
		
		public virtual bool IsCheckable => false;
		
		public bool? IsChecked
		{
			get { return this._isChecked; }
			set
			{
				SetIsChecked(value, true);
			}
		}
		
		private void SetIsChecked(bool? value, bool update)
		{
			if (this._isChecked != value)
			{
				this._isChecked = value;
				
				if (update)
				{
					if (IsChecked != null)
					{
						foreach (AdvTreeNode child in Descendants())
						{
							if (child.IsCheckable)
							{
								child.SetIsChecked(IsChecked, false);
							}
						}
					}
					
					foreach (AdvTreeNode parent in Ancestors())
					{
						if (parent.IsCheckable)
						{
							if (!parent.TryValueForIsChecked(true))
							{
								if (!parent.TryValueForIsChecked(false))
								{
									parent.SetIsChecked(null, false);
								}
							}
						}
					}
				}
				
				NotifyOfPropertyChange(() => IsChecked);
			}
		}
		
		private bool TryValueForIsChecked(bool? value)
		{
			if (Children.Where(n => n.IsCheckable).All(n => n.IsChecked == value))
			{
				SetIsChecked(value, false);
				return true;
			}

			return false;
		}
		
		#endregion

		#region IsLast (for TreeView lines)
		
		public bool IsLast => 
			Parent == null || Parent.Children[Parent.Children.Count - 1] == this;

		private void RaiseIsLastChangedIfNeeded(NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					if (e.NewStartingIndex == Children.Count - 1)
					{
						if (Children.Count > 1)
						{
							Children[Children.Count - 2].NotifyOfPropertyChange(() => IsLast);
						}
						Children[Children.Count - 1].NotifyOfPropertyChange(() => IsLast);
					}
					break;
				case NotifyCollectionChangedAction.Remove:
					if (e.OldStartingIndex == Children.Count)
					{
						if (Children.Count > 0)
						{
							Children[Children.Count - 1].NotifyOfPropertyChange(() => IsLast);
						}
					}
					break;
			}
		}

		#endregion

		/// <summary>
		/// Gets called when the item is double-clicked.
		/// </summary>
		public virtual void ActivateItem(RoutedEventArgs e)
		{
		}

		public virtual void ShowContextMenu(ContextMenuEventArgs e)
		{
		}

		public override string ToString()
		{
			// used for keyboard navigation
			object text = this.Text;
			return text?.ToString() ?? string.Empty;
		}
	}
}
