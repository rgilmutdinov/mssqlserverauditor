﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public class AdvTreeNodeView : Control
	{
		static AdvTreeNodeView()
		{
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(AdvTreeNodeView),
				new FrameworkPropertyMetadata(typeof(AdvTreeNodeView))
			);
		}

		public static readonly DependencyProperty TextBackgroundProperty =
			DependencyProperty.Register(nameof(TextBackground), typeof(Brush), typeof(AdvTreeNodeView));

		public Brush TextBackground
		{
			get { return (Brush) GetValue(TextBackgroundProperty); }
			set { SetValue(TextBackgroundProperty, value); }
		}

		public static readonly DependencyProperty AuxForegroundProperty =
			DependencyProperty.Register(nameof(AuxForeground), typeof(Brush), typeof(AdvTreeNodeView));

		public Brush AuxForeground
		{
			get { return (Brush)GetValue(AuxForegroundProperty); }
			set { SetValue(AuxForegroundProperty, value); }
		}

		public AdvTreeNode Node => DataContext as AdvTreeNode;

		public AdvTreeViewItem ParentItem { get; private set; }
		
		public static readonly DependencyProperty CellEditorProperty = DependencyProperty.Register(
			nameof(CellEditor),
			typeof(Control),
			typeof(AdvTreeNodeView),
			new FrameworkPropertyMetadata()
		);
		
		public Control CellEditor {
			get { return (Control)GetValue(CellEditorProperty); }
			set { SetValue(CellEditorProperty, value); }
		}

		public AdvTreeView ParentTreeView => ParentItem.ParentTreeView;

		internal LinesRenderer LinesRenderer { get; private set; }

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			LinesRenderer = Template.FindName("linesRenderer", this) as LinesRenderer;
			UpdateTemplate();
		}

		protected override void OnVisualParentChanged(DependencyObject oldParent)
		{
			base.OnVisualParentChanged(oldParent);
			ParentItem = this.FindAncestor<AdvTreeViewItem>();
			ParentItem.NodeView = this;
		}

		protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
		{
			base.OnPropertyChanged(e);
			if (e.Property == DataContextProperty)
			{
				UpdateDataContext(e.OldValue as AdvTreeNode, e.NewValue as AdvTreeNode);
			}
		}

		void UpdateDataContext(AdvTreeNode oldNode, AdvTreeNode newNode)
		{
			if (newNode != null)
			{
				newNode.PropertyChanged += Node_PropertyChanged;
				if (Template != null)
				{
					UpdateTemplate();
				}
			}

			if (oldNode != null)
			{
				oldNode.PropertyChanged -= Node_PropertyChanged;
			}
		}

		void Node_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(AdvTreeNode.IsEditing))
			{
				OnIsEditingChanged();
			}
			else if (e.PropertyName == nameof(AdvTreeNode.IsLast))
			{
				if (ParentTreeView.ShowLines)
				{
					foreach (var child in Node.VisibleDescendantsAndSelf())
					{
						AdvTreeViewItem container = ParentTreeView.ItemContainerGenerator.ContainerFromItem(child) as AdvTreeViewItem;
						container?.NodeView.LinesRenderer.InvalidateVisual();
					}
				}
			}
			else if (e.PropertyName == nameof(AdvTreeNode.IsExpanded))
			{
				if (Node.IsExpanded)
				{
					ParentTreeView.HandleExpanding(Node);
				}
			}
		}

		void OnIsEditingChanged()
		{
			Border textEditorContainer = Template.FindName("textEditorContainer", this) as Border;
			if (textEditorContainer == null)
			{
				return;
			}

			if (Node.IsEditing)
			{
				textEditorContainer.Child = CellEditor ?? new EditTextBox { Item = ParentItem };
			}
			else
			{
				textEditorContainer.Child = null;
			}
		}

		void UpdateTemplate()
		{
			FrameworkElement spacer = Template.FindName("spacer", this) as FrameworkElement;
			if (spacer == null)
			{
				return;
			}

			spacer.Width = CalculateIndent();

			ToggleButton expander = Template.FindName("expander", this) as ToggleButton;

			if (expander == null)
			{
				return;
			}

			if (ParentTreeView.Root == Node && !ParentTreeView.ShowRootExpander)
			{
				expander.Visibility = Visibility.Collapsed;
			}
			else
			{
				expander.ClearValue(VisibilityProperty);
			}
		}

		internal double CalculateIndent()
		{
			int result = 19 * Node.Level;
			if (ParentTreeView.ShowRoot)
			{
				if (!ParentTreeView.ShowRootExpander)
				{
					if (ParentTreeView.Root != Node)
					{
						result -= 15;
					}
				}
			}
			else
			{
				result -= 19;
			}

			if (result < 0)
			{
				throw new InvalidOperationException();
			}

			return result;
		}
	}
}
