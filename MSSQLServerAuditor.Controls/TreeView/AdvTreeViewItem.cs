﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public class AdvTreeViewItem : ListViewItem
	{
		private bool _wasSelected;
		private bool _wasDoubleClick;

		static AdvTreeViewItem()
		{
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(AdvTreeViewItem),
				new FrameworkPropertyMetadata(typeof(AdvTreeViewItem))
			);
		}

		public AdvTreeNode Node => DataContext as AdvTreeNode;

		public AdvTreeNodeView NodeView       { get; internal set; }
		public AdvTreeView     ParentTreeView { get; internal set; }

		protected override void OnKeyDown(KeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.F2:
					if (Node.IsEditable && ParentTreeView != null && ParentTreeView.SelectedItems.Count == 1 && ParentTreeView.SelectedItems[0] == Node)
					{
						Node.IsEditing = true;
						e.Handled = true;
					}
					break;
				case Key.Escape:
					if (Node.IsEditing)
					{
						Node.IsEditing = false;
						e.Handled = true;
					}
					break;
			}
		}

		protected override System.Windows.Automation.Peers.AutomationPeer OnCreateAutomationPeer()
		{
			return new AdvTreeViewItemAutomationPeer(this);
		}

		#region Mouse

		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			this._wasSelected = IsSelected;
			if (!IsSelected)
			{
				base.OnMouseLeftButtonDown(e);
			}

			if (Mouse.LeftButton == MouseButtonState.Pressed)
			{
				CaptureMouse();

				if (e.ClickCount == 2)
				{
					this._wasDoubleClick = true;
				}
			}
		}

		protected override async void OnMouseLeftButtonUp(MouseButtonEventArgs e)
		{
			if (this._wasDoubleClick)
			{
				this._wasDoubleClick = false;
				Node.ActivateItem(e);
				if (!e.Handled)
				{
					if (!Node.IsRoot || ParentTreeView.ShowRootExpander)
					{
						await Node.Toggle(!Node.IsExpanded);
					}
				}
			}
			
			ReleaseMouseCapture();
			if (this._wasSelected)
			{
				base.OnMouseLeftButtonDown(e);
			}
		}

		protected override void OnContextMenuOpening(ContextMenuEventArgs e)
		{
			Node?.ShowContextMenu(e);
		}

		#endregion
	}
}
