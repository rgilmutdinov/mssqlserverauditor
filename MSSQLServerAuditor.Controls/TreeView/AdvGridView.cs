﻿using System.Windows;
using System.Windows.Controls;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public class AdvGridView : GridView
	{
		static AdvGridView()
		{
			ItemContainerStyleKey = new ComponentResourceKey(
				typeof(AdvTreeView),
				"GridViewItemContainerStyleKey"
			);
		}

		public static ResourceKey ItemContainerStyleKey { get; }

		protected override object ItemContainerDefaultStyleKey => ItemContainerStyleKey;
	}
}
