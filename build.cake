#tool nuget:?package=NUnit.ConsoleRunner&version=3.4.0
//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target        = Argument("target", "Default");
var configuration = Argument("configuration", "Release");
var platform      = Argument("platform", "x86");
var builder       = Argument<bool>("compileBuilder", true);
var runTests      = Argument<bool>("runTests", true);
var verbosity     = Argument<Verbosity>("verbosity", Verbosity.Verbose);

Information("target: "         + target);
Information("configuration: "  + configuration);
Information("platform: "       + platform);
Information("compileBuilder: " + builder);
Information("runTests: "       + runTests);
Information("verbosity: "      + verbosity);

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

// Define files & directories.
var solution    = File("./MSSQLServerAuditor.sln");
var appProj     = File("./MSSQLServerAuditor/MSSQLServerAuditor.csproj");
var serviceProj = File("./MSSQLServerAuditor.Service/MSSQLServerAuditor.Service.csproj");
var testsProj   = File("./MSSQLServerAuditor.Tests/MSSQLServerAuditor.Tests.csproj");

var binDir   = Directory("./out/bin");
var dataDir  = Directory("./data");

var isx64 = platform == "x64";
var warnDisable = verbosity < Verbosity.Normal;
//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("CleanAll")
	.Does(() =>
{
	CleanDirectory(binDir);
});

Task("Restore-NuGet-Packages")
	.IsDependentOn("CleanAll")
	.Does(() =>
{
	NuGetRestore(solution);
});

Task("Build")
	.IsDependentOn("Restore-NuGet-Packages")
	.Does(() =>
{
	var buildSettings = new MSBuildSettings()
		.SetConfiguration(configuration)
		.SetVerbosity(verbosity)
		.WithProperty("Platform", isx64 ? "x64" : "x86");
		
		
	if (warnDisable) {
		buildSettings.WithProperty("WarningLevel", "0");
	}

	if (builder)
	{
		// compile complete solution
		MSBuild(solution, buildSettings);
	}
	else
	{
		// compile desktop application and service only
		MSBuild(appProj, buildSettings);
		MSBuild(serviceProj, buildSettings);
		MSBuild(testsProj, buildSettings);
	}
});

Task("Run-Unit-Tests")
	.WithCriteria(() => runTests)
	.IsDependentOn("Build")
	.Does(() =>
{
	NUnit3(
		binDir.ToString() + "/*.Tests.dll",
		new NUnit3Settings { NoResults = true }
	);
});

Task("Copy-Data")
	.Does(() =>
{
	var imagesDir    = Directory("Images");
	var modulesDir   = Directory("Modules");
	var postBuildDir = Directory("PostBuild");
	var jsDir        = Directory("js");

	//Setup
	EnsureDirectoryExists(binDir + imagesDir);
	EnsureDirectoryExists(binDir + modulesDir);
	EnsureDirectoryExists(binDir + postBuildDir);
	EnsureDirectoryExists(binDir + jsDir);

	CopyDirectory(dataDir + imagesDir,    binDir + imagesDir);
	CopyDirectory(dataDir + modulesDir,   binDir + modulesDir);
	CopyDirectory(dataDir + postBuildDir, binDir + postBuildDir);
	CopyDirectory(dataDir + jsDir,        binDir + jsDir);
});

Task("Copy-Configs")
	.Does(() =>
{
	var sysConfig = File("MSSQLServerAuditor.SystemSettings.xml");
	var usrConfig = File("MSSQLServerAuditor.UserSettings.xml");

	CopyFile(dataDir + sysConfig, binDir + sysConfig);
	CopyFile(dataDir + usrConfig, binDir + usrConfig);
});

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Complete")
	.IsDependentOn("Run-Unit-Tests")
	.IsDependentOn("Copy-Data")
	.IsDependentOn("Copy-Configs");

Task("Default")
	.IsDependentOn("Complete");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
