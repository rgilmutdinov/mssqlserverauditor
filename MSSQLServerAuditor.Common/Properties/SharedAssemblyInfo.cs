﻿using System.Reflection;
using MSSQLServerAuditor.Common;

[assembly: AssemblyConfiguration("Beta")]
[assembly: AssemblyCompany("MSSQLServerAuditor")]
[assembly: AssemblyProduct("MSSQLServerAuditor")]
[assembly: AssemblyCopyright("Copyright © City24 Pty. Ltd., 2013-2017")]
[assembly: AssemblyTrademark("City24 Pty. Ltd.")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("0.7.0.1")]
[assembly: AssemblyFileVersion("0.7.0.1")]
// Build date. Format: "yyyy/MM/dd";
[assembly: BuildDate("2017/01/01")]
