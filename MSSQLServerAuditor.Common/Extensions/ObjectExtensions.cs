namespace MSSQLServerAuditor.Common.Extensions
{
	public static class ObjectExtensions
	{
		public static string ToSafeString(this object obj)
		{
			return ToStringOrDefault(obj, string.Empty);
		}

		public static string ToStringOrDefault(this object obj, string defaultString)
		{
			return obj?.ToString() ?? defaultString;
		}

		/// <summary>
		/// Returns object as specified type.
		/// </summary>
		public static T As<T>(this object obj)
		{
			if (obj is T)
			{
				return (T) obj;
			}

			return default(T);
		}
	}
}
