﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Common.Extensions
{
	public static class CloneExtensions
	{
		public static T DeepClone<T>(this T obj)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(stream, obj);
				stream.Position = 0;
				return (T)formatter.Deserialize(stream);
			}
		}

		public static T XmlClone<T>(this T xmlObj)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				XmlSerializer xs = new XmlSerializer(typeof(T));
				xs.Serialize(stream, xmlObj);
				stream.Position = 0;

				return (T) xs.Deserialize(stream);
			}
		}
	}
}
