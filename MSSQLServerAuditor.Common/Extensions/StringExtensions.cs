using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace MSSQLServerAuditor.Common.Extensions
{
	public static class StringExtensions
	{
		public static string AsParamName(this string name)
		{
			return "@" + name;
		}

		public static string AsDbName(this string name)
		{
			return name == "*" ? name : $"[{name}]";
		}

		public static string AsFk(this string name)
		{
			return $"{name}_id";
		}

		public static string AsValidSqlName(this string name)
		{
			Regex regexSpecialChars = new Regex(@"[\~\!\@\#\$\%\^\&\*\(\)\{\}\?\/\=\-\.]");

			return regexSpecialChars.Replace(name.Replace(" ", string.Empty), string.Empty);
		}

		public static string Join(this IEnumerable<string> strings, string separator)
		{
			return string.Join(separator, strings.ToArray());
		}

		public static bool EqualsIgnoreCase(this string s1, string s2)
		{
			return s1.Equals(s2, StringComparison.OrdinalIgnoreCase);
		}

		public static string RemoveWhitespaces(this string st)
		{
			return st.Trim(' ', '\n', '\t', '\r');
		}

		public static string TrimmedOrEmpty(this string st)
		{
			return TrimmedOrDefault(st, string.Empty);
		}

		public static string TrimmedOrDefault(this string st, string def)
		{
			return string.IsNullOrEmpty(st)
				? def
				: st.RemoveWhitespaces();
		}

		public static string GetValidFileName(this string st)
		{
			return Path.GetInvalidFileNameChars()
				.Aggregate(st, (current, c) => current.Replace(c, '_'))
				.Replace(' ', '_');
		}

		private static readonly Regex RegexXmlSpecialChars = new Regex(@"[\~\!\@\#\$\%\^\&\*\(\)\{\}\?\/\=]", RegexOptions.Compiled);

		/// <summary>
		/// Delete spec chars for XML node name.
		/// </summary>
		/// <param name="str">Innser string.</param>
		/// <returns></returns>
		public static string RemoveXmlSpecialChars(this string str)
		{
			return RegexXmlSpecialChars.Replace(str.Replace(" ", string.Empty), string.Empty);
		}

		public static string FormatXml(this XmlDocument doc)
		{
			StringBuilder     sb       = new StringBuilder();
			XmlWriterSettings settings = new XmlWriterSettings
			{
				Indent          = true,
				IndentChars     = "  ",
				NewLineChars    = "\r\n",
				NewLineHandling = NewLineHandling.Replace
			};

			using (XmlWriter writer = XmlWriter.Create(sb, settings))
			{
				doc.Save(writer);
			}

			return sb.ToString();
		}
	}
}
