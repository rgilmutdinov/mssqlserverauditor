using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace MSSQLServerAuditor.Common.Extensions
{
	public static class EnumExtensions
	{
		public static string GetDescription(this Enum enumValue)
		{
			FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

			DescriptionAttribute[] attributes =
				(DescriptionAttribute[]) fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

			if (attributes.Length > 0)
			{
				return attributes[0].Description;
			}

			return enumValue.ToString();
		}
	}

	public static class EnumUtil
	{
		public static IEnumerable<T> GetValues<T>()
		{
			return Enum.GetValues(typeof(T)).Cast<T>();
		}
	}
}
