using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace MSSQLServerAuditor.Common.Extensions
{
	public static class CollectionExtensions
	{
		/// <summary>
		/// Determines whether the collection is null or contains no elements.
		/// </summary>
		/// <typeparam name="T">The IEnumerable type.</typeparam>
		/// <param name="enumerable">The enumerable, which may be null or empty.</param>
		/// <returns>
		///     <c>true</c> if the IEnumerable is null or empty; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
		{
			if (enumerable == null)
			{
				return true;
			}

			ICollection<T> collection = enumerable as ICollection<T>;

			if (collection != null)
			{
				return collection.Count < 1;
			}

			return !enumerable.Any();
		}

		/// <summary>
		/// Determines whether the dictionary is null or contains no elements.
		/// </summary>
		/// <typeparam name="TK">The Key type.</typeparam>
		/// <typeparam name="TV">The Value type.</typeparam>
		/// <returns>
		///     <c>true</c> if the Dictionary is null or empty; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsNullOrEmpty<TK, TV>(this Dictionary<TK, TV> dictionary)
		{
			if (dictionary == null)
			{
				return true;
			}

			return dictionary.Count < 1;
		}

		[SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
		public static bool ItemsEqual<T>(this IEnumerable<T> collection1, IEnumerable<T> collection2)
		{
			if (collection1 == null && collection2 == null)
			{
				return true;
			}

			if (collection1 == null || collection2 == null)
			{
				return false;
			}

			if (collection1.Count() != collection2.Count())
			{
				return false;
			}

			foreach (T item in collection1)
			{
				if (!collection2.Contains(item))
				{
					return false;
				}
			}

			return true;
		}
	}
}
