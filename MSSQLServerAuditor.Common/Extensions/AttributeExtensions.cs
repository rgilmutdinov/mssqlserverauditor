﻿using System;
using System.Linq;

namespace MSSQLServerAuditor.Common.Extensions
{
	public static class AttributeExtensions
	{
		public static TValue GetAttributeValue<TAttribute, TValue>(
			this Type                type,
			Func<TAttribute, TValue> valueSelector)
			where TAttribute : Attribute
		{
			TAttribute attribute = type
				.GetCustomAttributes(typeof(TAttribute), true)
				.FirstOrDefault() as TAttribute;

			if (attribute != null)
			{
				return valueSelector(attribute);
			}

			return default(TValue);
		}

		public static bool HasAttribute<TAttribute>(this Type type)
			where TAttribute : Attribute
		{
			return type.GetCustomAttributes(typeof(TAttribute), true).Any();
		}
	}
}
