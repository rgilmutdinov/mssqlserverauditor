using System;
using System.Data;
using System.Data.SqlClient;
using NLog;

namespace MSSQLServerAuditor.Common.Extensions
{
	/// <summary>
	/// Helper for SQLite
	/// </summary>
	public static class SqlTypeExtensions
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static DbType ToDbType(this SqlDbType dbType)
		{
			SqlParameter conversionParameter = new SqlParameter();

			try
			{
				conversionParameter.SqlDbType = dbType;
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Can't map DbType:");
			}

			return conversionParameter.DbType;
		}

		public static SqlDbType ToSqlDbType(this DbType dbType)
		{
			SqlParameter conversionParameter = new SqlParameter();

			try
			{
				conversionParameter.DbType = dbType;
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Can't map DbType to:");
			}

			return conversionParameter.SqlDbType;
		}

		private static object GetEmptyInstance(Type type)
		{
			object instance = null;

			if (type == typeof(string))
			{
				instance = string.Empty;
			}

			if ((instance == null) && type.IsArray)
			{
				instance = Activator.CreateInstance(type, 0); //an empty array
			}

			if (instance == null)
			{
				instance = Activator.CreateInstance(type, new object[] { });
			}

			return instance;
		}

		public static SqlDbType ToSqlDbType(this Type type)
		{
			Type srcType = type;

			if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				srcType = Nullable.GetUnderlyingType(type);
			}

			SqlParameter conversionParameter = new SqlParameter();

			object instance = GetEmptyInstance(srcType);

			conversionParameter.Value = instance;

			return conversionParameter.SqlDbType;
		}
	}
}
