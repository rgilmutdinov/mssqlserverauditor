﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace MSSQLServerAuditor.Common
{
	public static class CurrentAssembly
	{
		public static string Title
		{
			get
			{
				string title = GetEntryAssemblyAttributeValue<AssemblyTitleAttribute>(a => a.Title);

				return !string.IsNullOrEmpty(title) 
					? title
					: Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
			}
		}

		public static string ProductName =>
			GetEntryAssemblyAttributeValue<AssemblyProductAttribute>(a => a.Product);

		public static string Copyright =>
			GetEntryAssemblyAttributeValue<AssemblyCopyrightAttribute>(a => a.Copyright);

		public static string Company =>
			GetEntryAssemblyAttributeValue<AssemblyCompanyAttribute>(a => a.Company);

		public static string Version =>
			Assembly.GetEntryAssembly().GetName().Version.ToString();

		public static string Description =>
			GetEntryAssemblyAttributeValue<AssemblyDescriptionAttribute>(a => a.Description);

		public static string ProcessName =>
			Process.GetCurrentProcess().ProcessName;

		private static string GetAttributeValue<T>(Assembly assembly, Func<T, string> mapper) where T : Attribute
		{
			object[] attributes = assembly.GetCustomAttributes(typeof(T), false);

			if (attributes.Length == 0)
			{
				return string.Empty;
			}

			return mapper.Invoke((T) attributes[0]);
		}

		private static string GetEntryAssemblyAttributeValue<T>(Func<T, string> mapper) where T : Attribute
		{
			return GetAttributeValue(Assembly.GetEntryAssembly(), mapper);
		}

		private static string GetExecutingAttributeValue<T>(Func<T, string> mapper) where T : Attribute
		{
			return GetAttributeValue(Assembly.GetExecutingAssembly(), mapper);
		}
	}
}
