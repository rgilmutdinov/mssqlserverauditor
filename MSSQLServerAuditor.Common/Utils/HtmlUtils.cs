using System.Text.RegularExpressions;

namespace MSSQLServerAuditor.Common.Utils
{
	public static class HtmlUtils
	{
		private static string HtmlTitleRegex = @"\<title\b[^>]*\>\s*(?<Title>[\s\S]*?)\</title\>";

		public static string GetTitle(string html, string defaultTitle)
		{
			if (string.IsNullOrWhiteSpace(html))
			{
				return defaultTitle;
			}

			Match match = Regex.Match(
				html, HtmlTitleRegex, RegexOptions.IgnoreCase
			);

			string title = null;

			if (match.Success)
			{
				title = match.Groups["Title"].Value;
			}

			return StringUtils.FirstNonEmpty(title, defaultTitle);
		}
	}
}
