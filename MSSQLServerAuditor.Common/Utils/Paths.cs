﻿using System;
using System.IO;

namespace MSSQLServerAuditor.Common.Utils
{
	public static class Paths
	{
		public static string TrimLastSeparator(string path)
		{
			return path.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
		}

		public static string TrimFirstSeparator(string path)
		{
			return path.TrimStart(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
		}

		public static string[] Split(string path)
		{
			return path.Split(
				new[] { '\\' },
				StringSplitOptions.RemoveEmptyEntries
			);
		}

		public static string GetRelativePath(string rootFolder, string file)
		{
			if (rootFolder == null)
			{
				throw new ArgumentNullException(nameof(rootFolder));
			}

			if (rootFolder.Length == 0 || !Path.IsPathRooted(rootFolder))
			{
				throw new ArgumentException("The root folder must be an absolute (rooted) file path.", nameof(rootFolder));
			}

			if (!Path.IsPathRooted(file))
			{
				return file;
			}

			if (!rootFolder.EndsWith(Path.DirectorySeparatorChar.ToString()))
			{
				rootFolder += Path.DirectorySeparatorChar;
			}

			Uri rootUri = new Uri(rootFolder);
			Uri fileUri = new Uri(file);

			Uri relativeUri = rootUri.MakeRelativeUri(fileUri);

			string relativePath = relativeUri.ToString().Replace('/', Path.DirectorySeparatorChar);

			return Uri.UnescapeDataString(relativePath);
		}
	}
}
