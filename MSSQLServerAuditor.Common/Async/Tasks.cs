﻿using System;
using System.Threading.Tasks;

namespace MSSQLServerAuditor.Common.Async
{
	public static class Tasks
	{
		public static Task<T> FromException<T>(Exception e)
		{
			TaskCompletionSource<T> tcs = new TaskCompletionSource<T>();
			tcs.SetException(e);
			return tcs.Task;
		}

		public static Task FromException(Exception e)
		{
			TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
			tcs.SetException(e);
			return tcs.Task;
		}
	}
}
