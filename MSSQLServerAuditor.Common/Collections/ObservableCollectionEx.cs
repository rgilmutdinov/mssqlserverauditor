﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace MSSQLServerAuditor.Common.Collections
{
	public class ObservableCollectionEx<T> : ObservableCollection<T>
	{
		private const string IndexerName = "Item[]";

		protected enum ProcessRangeAction
		{
			Add,
			Replace,
			Remove
		};

		public ObservableCollectionEx()
		{
		}

		public ObservableCollectionEx(IEnumerable<T> collection)
			: base(collection)
		{
		}

		public ObservableCollectionEx(List<T> list)
			: base(list)
		{
		}

		protected virtual void ProcessRange(IEnumerable<T> collection, ProcessRangeAction action)
		{
			if (collection == null)
			{
				throw new ArgumentNullException(nameof(collection));
			}

			IList<T> items = collection as IList<T> ?? collection.ToList();
			
			this.CheckReentrancy();

			if (action == ProcessRangeAction.Replace)
			{
				this.Items.Clear();
			}

			if (items.Any())
			{
				foreach (T item in items)
				{
					if (action == ProcessRangeAction.Remove)
					{
						this.Items.Remove(item);
					}
					else
					{
						this.Items.Add(item);
					}
				}
			}

			this.OnPropertyChanged(new PropertyChangedEventArgs(nameof(Count)));
			this.OnPropertyChanged(new PropertyChangedEventArgs(IndexerName));
			this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
		}

		public void AddRange(IEnumerable<T> collection)
		{
			this.ProcessRange(collection, ProcessRangeAction.Add);
		}

		public void ReplaceRange(IEnumerable<T> collection)
		{
			this.ProcessRange(collection, ProcessRangeAction.Replace);
		}

		public void RemoveRange(IEnumerable<T> collection)
		{
			this.ProcessRange(collection, ProcessRangeAction.Remove);
		}
	}
}
