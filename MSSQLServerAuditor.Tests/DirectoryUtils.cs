﻿using System.IO;

namespace MSSQLServerAuditor.Tests
{
	public static class DirectoryUtils
	{
		public static void DeleteIfExists(string directory)
		{
			if (Directory.Exists(directory))
			{
				Directory.Delete(directory, true);
			}
		}

		public static void CreateIfNotExists(string directory)
		{
			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
		}
	}
}
