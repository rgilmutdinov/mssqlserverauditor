﻿using System.IO;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Tests.Properties;
using NUnit.Framework;

namespace MSSQLServerAuditor.Tests.Xml
{
	[TestFixture]
	public class LoadTemplateTests
	{
		private readonly string _xmlTemplatePath = Path.Combine(
			Path.GetTempPath(), "TestTemplate.xml");

		[SetUp]
		public void Prepare()
		{
			using (StreamWriter writer = File.CreateText(this._xmlTemplatePath))
			{
				writer.Write(Resources.TestTemplate);
			}
		}

		[Test]
		public void LoadTemplateTest()
		{
			TemplateInfo template = XmlSerialization.Deserialize<TemplateInfo>(
				this._xmlTemplatePath
			);

			Assert.IsNotNull(template);
			Assert.AreEqual("Test", template.Id);
			Assert.AreEqual(ConnectionType.MSSQL.ToString(), template.ModuleType);

			// check template name
			Assert.AreEqual(2, template.TemplateTitle.Count);
			Assert.AreEqual(2, template.TemplateTitle.Count);
			Assert.AreEqual("MSSQL: Test template",   template.TemplateTitle.Find(x => x.Language == "en").Text.TrimmedOrEmpty());
			Assert.AreEqual("MSSQL: Тестовый шаблон", template.TemplateTitle.Find(x => x.Language == "ru").Text.TrimmedOrEmpty());

			// check window title
			Assert.AreEqual(2, template.WindowTitle.Count);
			Assert.AreEqual("Test window header",      template.WindowTitle.Find(x => x.Language == "en").Text.TrimmedOrEmpty());
			Assert.AreEqual("Тестовый заголовок окна", template.WindowTitle.Find(x => x.Language == "ru").Text.TrimmedOrEmpty());

			// check tree title
			Assert.AreEqual(2, template.TreeTitle.Count);
			Assert.AreEqual("Test tree tite",            template.TreeTitle.Find(x => x.Language == "en").Text.TrimmedOrEmpty());
			Assert.AreEqual("Тестовый заголовок дерева", template.TreeTitle.Find(x => x.Language == "ru").Text.TrimmedOrEmpty());

			// check nodes
			Assert.AreEqual(1, template.Nodes.Count);

			TemplateNodeInfo nodeInfo = template.Nodes[0];

			Assert.AreEqual("01",            nodeInfo.Id);
			Assert.AreEqual("TestNode",      nodeInfo.Name);
			Assert.AreEqual("TestNode.xsl",  nodeInfo.ProcessorFile);
			Assert.AreEqual("doc",           nodeInfo.DefaultIcon);
			Assert.AreEqual(true,            nodeInfo.ShowNumberOfRecords);
			Assert.AreEqual("Test node",     nodeInfo.Title.Find(x => x.Language == "en").Text.TrimmedOrEmpty());
			Assert.AreEqual("Тестовый узел", nodeInfo.Title.Find(x => x.Language == "ru").Text.TrimmedOrEmpty());

			Assert.AreEqual(1, nodeInfo.Queries.Count);

			// check query
			TemplateNodeQueryInfo query = nodeInfo.Queries[0];
			Assert.AreEqual("1",             query.Id);
			Assert.AreEqual("TestQuery",     query.QueryName);
			Assert.AreEqual("TestQuery.xml", query.QueryFileName);
		}

		[TearDown]
		public void CleanUp()
		{
			FileUtils.DeleteIfExists(this._xmlTemplatePath);
		}
	}
}
