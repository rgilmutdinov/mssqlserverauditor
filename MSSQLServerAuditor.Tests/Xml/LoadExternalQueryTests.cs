﻿using System.Collections.Generic;
using System.IO;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Tests.Properties;
using NUnit.Framework;

namespace MSSQLServerAuditor.Tests.Xml
{
	[TestFixture]
	public class LoadExternalQueryTests
	{
		private readonly string _tempFolder;
		private readonly string _xmlMssqlQueryPath;
		private readonly string _xmlSqliteQueryPath;
		private readonly string _xmlMssqlScriptPath;
		private readonly string _xmlSqliteScriptPath;

		public LoadExternalQueryTests()
		{
			this._tempFolder          = Path.Combine(Path.GetTempPath(), "QueryTests");

			this._xmlMssqlQueryPath   = Path.Combine(this._tempFolder, "mssql",  "TestExternalMssqlQuery.xml");
			this._xmlSqliteQueryPath  = Path.Combine(this._tempFolder, "sqlite", "TestExternalSqliteQuery.xml");

			this._xmlMssqlScriptPath  = Path.Combine(this._tempFolder, "mssql",  "TestExternalQueryScript.xml");
			this._xmlSqliteScriptPath = Path.Combine(this._tempFolder, "sqlite", "TestExternalQueryScript.xml");
		}

		[SetUp]
		public void Prepare()
		{
			DirectoryUtils.CreateIfNotExists(this._tempFolder);

			DirectoryUtils.CreateIfNotExists(Directory.GetParent(this._xmlMssqlQueryPath).ToString());
			DirectoryUtils.CreateIfNotExists(Directory.GetParent(this._xmlSqliteQueryPath).ToString());

			DirectoryUtils.CreateIfNotExists(Directory.GetParent(this._xmlMssqlScriptPath).ToString());
			DirectoryUtils.CreateIfNotExists(Directory.GetParent(this._xmlSqliteScriptPath).ToString());

			using (StreamWriter writer = File.CreateText(this._xmlMssqlQueryPath))
			{
				writer.Write(Resources.TestExternalMssqlQuery);
			}

			using (StreamWriter writer = File.CreateText(this._xmlMssqlScriptPath))
			{
				writer.Write(Resources.TestExternalMssqlQueryScript);
			}

			using (StreamWriter writer = File.CreateText(this._xmlSqliteQueryPath))
			{
				writer.Write(Resources.TestExternalSqliteQuery);
			}

			using (StreamWriter writer = File.CreateText(this._xmlSqliteScriptPath))
			{
				writer.Write(Resources.TestExternalSqliteQueryScript);
			}
		}

		[Test]
		public void LoadExternalMssqlQueryTest()
		{
			// Arrange
			string    xmlQueryFile    = Path.Combine(this._tempFolder, @"mssql\TestExternalMssqlQuery.xml");
			string    scriptDirectory = Path.Combine(this._tempFolder, $"{StorageType.MSSQL}");
			XmlLoader xmlLoader       = new XmlLoader(scriptDirectory, scriptDirectory);

			// Act
			List<QueryInfo> queries = xmlLoader.LoadQueries(xmlQueryFile);

			// Assert
			Assert.NotNull(queries);
			Assert.AreEqual(1, queries.Count);

			QueryInfo queryInfo = queries[0];
			Assert.AreEqual(ConnectionType.MSSQL, queryInfo.ConnectionType);
			Assert.AreEqual("TestQuery",          queryInfo.Name);
			Assert.AreEqual("1",                  queryInfo.Id);
			Assert.AreEqual(3,                    queryInfo.Items.Count);
			Assert.AreEqual("MSSQL query body 1", queryInfo.Items[0].Text.TrimmedOrEmpty());
			Assert.AreEqual("MSSQL query body 2", queryInfo.Items[1].Text.TrimmedOrEmpty());
			Assert.AreEqual("MSSQL query body 3", queryInfo.Items[2].Text.TrimmedOrEmpty());
		}

		[Test]
		public void LoadExternalSqliteQueryTest()
		{
			// Arrange
			string    xmlQueryFile    = Path.Combine(this._tempFolder, @"sqlite\TestExternalSqliteQuery.xml");
			string    scriptDirectory = Path.Combine(this._tempFolder, $"{StorageType.SQLite}");
			XmlLoader xmlLoader       = new XmlLoader(scriptDirectory, scriptDirectory);

			// Act
			List<QueryInfo> queries = xmlLoader.LoadQueries(xmlQueryFile);

			// Assert
			Assert.NotNull(queries);
			Assert.AreEqual(1, queries.Count);

			QueryInfo queryInfo = queries[0];
			Assert.AreEqual(ConnectionType.SQLite, queryInfo.ConnectionType);
			Assert.AreEqual("TestQuery",           queryInfo.Name);
			Assert.AreEqual("1",                   queryInfo.Id);
			Assert.AreEqual(1,                     queryInfo.Items.Count);
			Assert.AreEqual("SQLite query body 1", queryInfo.Items[0].Text.TrimmedOrEmpty());
		}

		[TearDown]
		public void CleanUp()
		{
			FileUtils.DeleteIfExists(this._xmlMssqlQueryPath);
			FileUtils.DeleteIfExists(this._xmlSqliteQueryPath);

			FileUtils.DeleteIfExists(this._xmlMssqlScriptPath);
			FileUtils.DeleteIfExists(this._xmlSqliteScriptPath);

			DirectoryUtils.DeleteIfExists(Directory.GetParent(this._xmlMssqlQueryPath).ToString());
			DirectoryUtils.DeleteIfExists(Directory.GetParent(this._xmlSqliteQueryPath).ToString());
			DirectoryUtils.DeleteIfExists(this._tempFolder);
		}
	}
}
