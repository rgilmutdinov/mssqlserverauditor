﻿using System.Security.Cryptography;
using MSSQLServerAuditor.Core.Cryptography;
using NUnit.Framework;

namespace MSSQLServerAuditor.Tests.Cryptography
{
	public class SingAndVerifyTests
	{
		private const string XmlDocument = @"<?xml version=""1.0""?>
<example>
<test>some text node</test>
</example>";

		[Test]
		public void TestSignatureRandomKey()
		{
			// Arrange
			using (RSACryptoServiceProvider signRsa = new RSACryptoServiceProvider())
			{
				string privateKey = signRsa.ToXmlString(true);
				string publicKey  = signRsa.ToXmlString(false);

				// Act
				Signer            signer   = new Signer(privateKey);
				SignatureVerifier verifier = new SignatureVerifier(publicKey);

				string signature = signer.Sign(XmlDocument);

				// Assert
				Assert.True(verifier.Verify(XmlDocument, signature));
			}
		}

		[Test]
		public void TestSignatureXmlKey()
		{
			// Arrange
			string xmlPrivateKey = "<RSAKeyValue><Modulus>otDcdHiBMSLWFcKoEz6AdVKiR1BNenPE270BbNXvHfSyc1hj0E9IBgrsARMicfWrSesKIc2Z1JsKsTAyg2vp2mMA2dc7w/JU/3FjhSrAkVRzR9zA8Nu1fbui4piMvkSiFsJ+eAAZayYTfkxNGrZzeKufT8Jgb2t1zhkxTIRaL6U=</Modulus><Exponent>AQAB</Exponent><P>16omk3/4gPwt5+7lUFdbRYfXBKMVVWBWTdBBJurVC/DRmbaz00z/RhBWfih7xtOgtwuknsp8R8BGd48j4pcI2w==</P><Q>wURabEMSB9gy1WnMhfU8OBnjw4694JUj1YC+lFJQMjxnWKAJPz79EL7OYZrY+pziH89RZKa7mxztL1jpfTbRfw==</Q><DP>D34lr0ruKzXyhkrfJlpgMCqGA8tDaRK2oHhpdcZA786pAAXj/TX20K2zyPRXj/z7+qpXdSDOtMjXRxd9WHXJKQ==</DP><DQ>f1SzZmSB+p0cULLVxuQderDD+NGQk+hupXmfFkVtBMv7Is69iOfM/z1W58OHdQXrjR2f+HpJXBagxfMEUjTNrQ==</DQ><InverseQ>srIcD6D+WhGZ3ULqrJdm8/AgtHP/olugRh60TLl9IWPs/7u5GwIN35KY8/TROTOT9Xm7Bw9wESWpIjNzhT0Rfg==</InverseQ><D>Iijb9/vIbzy12f7rMI5ueS8LKRcqCkT7ynaPM/x2nLxYLGchGfVoUQw9k8Ql9+7pLysHByyR189noOFR18zuVTOgQc+DXrNJ7wIrO0d+xa5SbN0hYcEC1j5A/+4a4yo9nsQrccahY9xMpM5m4YeURWuMfp/WpLod+ll8EKlkYAk=</D></RSAKeyValue>";
			string xmlPublicKey  = "<RSAKeyValue><Modulus>otDcdHiBMSLWFcKoEz6AdVKiR1BNenPE270BbNXvHfSyc1hj0E9IBgrsARMicfWrSesKIc2Z1JsKsTAyg2vp2mMA2dc7w/JU/3FjhSrAkVRzR9zA8Nu1fbui4piMvkSiFsJ+eAAZayYTfkxNGrZzeKufT8Jgb2t1zhkxTIRaL6U=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
			
			// Act
			Signer            signer   = new Signer(xmlPrivateKey);
			SignatureVerifier verifier = new SignatureVerifier(xmlPublicKey);

			string signature = signer.Sign(XmlDocument);

			// Assert
			Assert.True(verifier.Verify(XmlDocument, signature));
		}
	}
}
