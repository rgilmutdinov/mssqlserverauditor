﻿using System.IO;

namespace MSSQLServerAuditor.Tests
{
	public static class FileUtils
	{
		public static void DeleteIfExists(string filepath)
		{
			if (File.Exists(filepath))
			{
				File.Delete(filepath);
			}
		}
	}
}
