﻿using System.IO;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Tests.Properties;
using NUnit.Framework;

namespace MSSQLServerAuditor.Tests.Settings
{
	[TestFixture]
	public class AppSettingsTests
	{
		private readonly string _systemSettingsPath = Path.Combine(
			Path.GetTempPath(), "SystemSettings.xml");
		private readonly string _userSettingsPath = Path.Combine(
			Path.GetTempPath(), "UserSettings.xml");
		private readonly string _userDefaultSettingsPath = Path.Combine(
			Path.GetTempPath(), "UserDefaultSettings.xml");

		[SetUp]
		public void Prepare()
		{
			using (StreamWriter writer = File.CreateText(this._systemSettingsPath))
			{
				writer.Write(Resources.SystemSettings);
			}

			using (StreamWriter writer = File.CreateText(this._userSettingsPath))
			{
				writer.Write(Resources.UserDefaultSettings);
			}
		}

		[Test]
		public void DeserializeAppSettings()
		{
			AppSettings appSettings = new AppSettings(
				this._userSettingsPath,
				this._userDefaultSettingsPath,
				this._systemSettingsPath
			);
			
			Assert.NotNull(appSettings);
			Assert.NotNull(appSettings.User);
			Assert.NotNull(appSettings.System);
		}

		[Test]
		public void CreateDefaultAppSettings()
		{
			// Arrange
			string systemSettingsPath      = Path.Combine(Path.GetTempPath(), "SystemSettings_notExists.xml");
			string userDefaultSettingsPath = Path.Combine(Path.GetTempPath(), "UserDefaultSettings_notExists.xml");
			string userSettingsPath        = Path.Combine(Path.GetTempPath(), "UserSettings_notExists.xml");

			FileUtils.DeleteIfExists(systemSettingsPath);
			FileUtils.DeleteIfExists(userSettingsPath);

			// Act
			AppSettings appSettings = new AppSettings(
				userSettingsPath,
				userDefaultSettingsPath,
				systemSettingsPath
			);

			// Assert
			try
			{
				Assert.NotNull(appSettings);
				Assert.NotNull(appSettings.User);
				Assert.NotNull(appSettings.System);
				Assert.NotNull(appSettings.User.NotificationSettings);
				Assert.NotNull(appSettings.User.NotificationSettings.SmtpSettings);
				Assert.NotNull(appSettings.User.NotificationSettings.SmtpSettings.SmtpCredentials);
				Assert.NotNull(appSettings.System.UiLanguages);
				Assert.NotNull(appSettings.System.ReportLanguages);
				Assert.NotNull(appSettings.System.WindowTitles);
			}
			finally
			{
				FileUtils.DeleteIfExists(systemSettingsPath);
				FileUtils.DeleteIfExists(userSettingsPath);
			}
		}

		[Test]
		public void SaveSettings()
		{
			// Arrange
			string systemSettingsPath      = Path.Combine(Path.GetTempPath(), "SystemSettings_notExists.xml");
			string userDefaultSettingsPath = Path.Combine(Path.GetTempPath(), "UserDefaultSettings_notExists.xml");
			string userSettingsPath        = Path.Combine(Path.GetTempPath(), "UserSettings_notExists.xml");

			FileUtils.DeleteIfExists(systemSettingsPath);
			FileUtils.DeleteIfExists(userSettingsPath);

			try
			{
				// Act
				// Load default settings
				AppSettings appSettings = new AppSettings(
					userSettingsPath,
					userDefaultSettingsPath,
					systemSettingsPath
				);

				// Change & Save
				appSettings.User.UiLanguage = "ch";
				appSettings.SaveUserSettings();

				appSettings.System.ScriptsDirectory = "ScriptDir";
				appSettings.SaveSystemSettings();
				
				// Reload settings
				appSettings = new AppSettings(
					userSettingsPath,
					userDefaultSettingsPath,
					systemSettingsPath
				);

				// Assert
				Assert.AreEqual("ch", appSettings.User.UiLanguage);
				Assert.AreEqual("ScriptDir", appSettings.System.ScriptsDirectory);
			}
			finally
			{
				FileUtils.DeleteIfExists(systemSettingsPath);
				FileUtils.DeleteIfExists(userSettingsPath);
				FileUtils.DeleteIfExists(userDefaultSettingsPath);
			}
		}

		[TearDown]
		public void CleanUp()
		{
			FileUtils.DeleteIfExists(this._systemSettingsPath);
			FileUtils.DeleteIfExists(this._userSettingsPath);
			FileUtils.DeleteIfExists(this._userDefaultSettingsPath);
		}
	}
}
