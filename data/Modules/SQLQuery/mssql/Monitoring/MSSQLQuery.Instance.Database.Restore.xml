<?xml version="1.0" encoding="UTF-8"?>
<root xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<sqlquery type="MSSQL">
	<sql-select name="GetInstanceDatabaseRestore">
		<description>
			collect database restore statistics
		</description>
		<reference>
			http://msdn.microsoft.com/en-us/library/ms190284.aspx
		</reference>
		<sql-select-text MinSupportedVersion="7.*" MaxSupportedVersion="8.*" signature="" revision="20170111">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			DECLARE
				@restore_date [DATETIME]
			;

			SELECT
				@restore_date = MAX([restore_date])
			FROM
				[msdb].[dbo].[restorehistory];

			SELECT
				 tSD.[dbid]                   AS [DatabaseId]
				,tSD.[name]                   AS [DatabaseName]
				,tRS.[restore_type]           AS [DatabaseRestoreType]
				,tRS.[restore_date]           AS [DatabaseRestoreDate]
				,tRS.[server_name]            AS [SourceServerName]
				,tRS.[database_name]          AS [SourceDatabaseName]
				,tRS.[backup_finish_date]     AS [SourceDatabaseBackupFinishDate]
				,tRS.[backup_size]            AS [SourceDatabaseBackupSize]
				,tRS.[SourceBackupFilesCount] AS [SourceDatabaseBackupFilesCount]
			FROM
				[master].[dbo].[sysdatabases] tSD
				INNER JOIN (
					SELECT
						 tRH.[destination_database_name]  AS [destination_database_name]
						,tRH.[restore_type]               AS [restore_type]
						,tRH.[restore_date]               AS [restore_date]
						,tBS.[server_name]                AS [server_name]
						,tBS.[database_name]              AS [database_name]
						,tBS.[backup_finish_date]         AS [backup_finish_date]
						,SUM(CASE
							WHEN (tBS.[backup_size] IS NULL) THEN
								0.0
							ELSE
								tBS.[backup_size]
						END)                              AS [backup_size]
						,COUNT(*)                         AS [SourceBackupFilesCount]
					FROM
						[msdb].[dbo].[restorehistory] tRH
						INNER JOIN [msdb].[dbo].[backupset] tBS ON
							tBS.[backup_set_id] = tRH.[backup_set_id]
						INNER JOIN [msdb].[dbo].[backupmediafamily] tBMF ON
							tBMF.[media_set_id] = tBS.[media_set_id]
					WHERE
						tRH.[restore_date] IS NOT NULL
						AND tRH.[restore_date] &gt; DATEADD(dd, -1, @restore_date)
					GROUP BY
						 tRH.[destination_database_name]
						,tRH.[restore_type]
						,tRH.[restore_date]
						,tBS.[server_name]
						,tBS.[database_name]
						,tBS.[backup_finish_date]
				) tRS ON
					tRS.[destination_database_name] = tSD.[name]
			;
		</sql-select-text>
		<sql-select-text MinSupportedVersion="9.*" MaxSupportedVersion="*" signature="" revision="20170111">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			;WITH cte AS
			(
				SELECT
					 tRH.[destination_database_name] AS [destination_database_name]
					,tRH.[restore_type]              AS [restore_type]
					,tBS.[server_name]               AS [server_name]
					,tBS.[database_name]             AS [database_name]
					,tRH.[restore_date]              AS [restore_date]
					,tBS.[backup_finish_date]        AS [backup_finish_date]
					,ROW_NUMBER() OVER
					(
						PARTITION BY
							 tRH.[destination_database_name]
							,tRH.[restore_type]
							,tBS.[server_name]
							,tBS.[database_name]
						ORDER BY
							 tRH.[destination_database_name]
							,tRH.[restore_type]
							,tBS.[server_name]
							,tBS.[database_name]
							,tBS.[backup_finish_date] DESC
							,tRH.[restore_date] DESC
					)                                AS [rn]
				FROM
					[msdb].[dbo].[restorehistory] tRH
					INNER JOIN [msdb].[dbo].[backupset] tBS ON
						tBS.[backup_set_id] = tRH.[backup_set_id]
			)
			SELECT
				 tSD.[database_id]            AS [DatabaseId]
				,tSD.[name]                   AS [DatabaseName]
				,tRS.[restore_type]           AS [DatabaseRestoreType]
				,tRS.[restore_date]           AS [DatabaseRestoreDate]
				,tRS.[server_name]            AS [SourceServerName]
				,tRS.[database_name]          AS [SourceDatabaseName]
				,tRS.[backup_finish_date]     AS [SourceDatabaseBackupFinishDate]
				,tRS.[backup_size]            AS [SourceDatabaseBackupSize]
				,tRS.[SourceBackupFilesCount] AS [SourceDatabaseBackupFilesCount]
			FROM
				[master].[sys].[databases] tSD
				INNER JOIN (
					SELECT
						 tRH.[destination_database_name] AS [destination_database_name]
						,tRH.[restore_type]              AS [restore_type]
						,tRH.[restore_date]              AS [restore_date]
						,tBS.[server_name]               AS [server_name]
						,tBS.[database_name]             AS [database_name]
						,tBS.[backup_finish_date]        AS [backup_finish_date]
						,SUM(CASE
							WHEN (tBS.[backup_size] IS NULL) THEN
								0.0
							ELSE
								tBS.[backup_size]
						END)                             AS [backup_size]
						,COUNT(*)                        AS [SourceBackupFilesCount]
					FROM
						[msdb].[dbo].[restorehistory] tRH
						INNER JOIN [msdb].[dbo].[backupset] tBS ON
							tBS.[backup_set_id] = tRH.[backup_set_id]
						INNER JOIN [msdb].[dbo].[backupmediafamily] tBMF ON
							tBMF.[media_set_id] = tBS.[media_set_id]
						INNER JOIN cte ON
							    cte.[destination_database_name] = tRH.[destination_database_name]
							AND cte.[restore_type]              = tRH.[restore_type]
							AND cte.[server_name]               = tBS.[server_name]
							AND cte.[database_name]             = tBS.[database_name]
							AND cte.[restore_date]              = tRH.[restore_date]
							AND cte.[backup_finish_date]        = tBS.[backup_finish_date]
					WHERE
						tRH.[restore_date] IS NOT NULL
						AND cte.[rn] &lt;= 24
					GROUP BY
						 tRH.[destination_database_name]
						,tRH.[restore_type]
						,tRH.[restore_date]
						,tBS.[server_name]
						,tBS.[database_name]
						,tBS.[backup_finish_date]
				) tRS ON
					tRS.[destination_database_name] = tSD.[name]
			;
		</sql-select-text>
		<sqlite_statements>
			<sqlite_statement id="13" file="hist.h_ServerInstance.xml"                       query="Insert"/>
			<sqlite_statement id="14" file="hist.h_ServerInstance.xml"                       query="Update"/>
			<sqlite_statement id="21" file="mssql\Monitoring\GetInstanceDatabaseRestore.xml" query="ref.h_ref_InstanceDatabase.Insert.1"/>
			<sqlite_statement id="31" file="mssql\Monitoring\GetInstanceDatabaseRestore.xml" query="hist.h_InstanceDatabaseRestore.Insert.1"/>
			<sqlite_statement id="41" file="mssql\Monitoring\GetInstanceDatabaseRestore.xml" query="hist24.h_InstanceDatabaseRestore.Insert.1"/>
		</sqlite_statements>
	</sql-select>
</sqlquery>
</root>
