<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="InstanceDashboard.Graph" columns="33;33;34" rows="33;33;34" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Instance Dashboard</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Instance Dashboard</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Instance Dashboard</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>

	<mssqlauditorpreprocessor id="jInstanceCPUUtilization.Graph" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date, 'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name != 'GetInstanceCPUUtilization']">
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name = 'GetInstanceCPUUtilization' and @hierarchy = '']/RecordSet[@id = '1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>CPU Utilization</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Использование процессора</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>CPU Utilization</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>

						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready( function() {
								var charts  = [];
								var color_1 = 'rgb(224,  64,  10)';
								var color_2 = 'rgb(  5, 100, 146)';
								var color_3 = 'rgb(252, 180,  65)';
								var color_4 = 'rgb( 65, 140, 240)';
								var color_5 = 'rgb(107, 142,  35)';

								<xsl:apply-templates select="//Row[CpuMsSql][1]/CpuMsSql"     mode="plots" />
								<xsl:apply-templates select="//Row[CpuOs][1]/CpuOs"           mode="plots" />

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot( 'chart', charts, {
									stackSeries: false,

									/**
									 * "title" options
									 */
									title: {
										show: true,
										text: 'CPU'
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										shadow:        false,
										drawGridlines: false
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  0,
										bottom: 0,
										left:   0
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: false, //
												showMark:     false, //
												showLabel:    false, //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis.  Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: false,        //
												showMark:     false,        //
												showLabel:    false,        //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer: $.jqplot.EnhancedLegendRenderer,

										show:         true,         // whether to show legend
										location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'insideGrid', //
										xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,         //

										rendererOptions: {
											numberRows:         0,     // maximum number of rows in the legend
											numberColumns:      2,     // maximum number of columns in the legend
											seriesToggle:       false, // false to not enable series on/off toggling on the legend
											seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         false,          // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         2,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not
											shadowAngle:  45,             // angle of the shadow.  Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: false,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             true,
											showDataLabels:     false
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       false,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // CSS spec for the cursor type to change the cursor to when over plot.
										show:                    false,       // whether to show cursor
										showTooltip:             false,       // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       true,        // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									},

									/**
									 * Series options are specified as an array of objects, one object
									 * for each series.
									 */
									series: [
										<xsl:apply-templates select="//Row[CpuMsSql][1]/CpuMsSql"     mode="lines" />,
										<xsl:apply-templates select="//Row[CpuOs][1]/CpuOs"           mode="lines" />
									]
								} );

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height( $('#container').height() * 0.96 );

									plot.replot( {
										resetAxes: true
									} );
								} );
							} );
						</script>
					</body>
				</html>
			</xsl:template>

			<xsl:template match="//Row/CpuMsSql | //Row/CpuOs" mode="plots">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				<xsl:variable name="var_name">
					<xsl:value-of select="translate(concat('chart_', $LineName), ' :()-.[]{}\/', '_____________')"/>
				</xsl:variable>

				var <xsl:value-of select="$var_name"/>=[];

				<xsl:for-each select="ancestor::RecordSet/Row/child::*[local-name() = $LineName]">
					<xsl:value-of select="$var_name"/>.push( [
						'<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="../EventTime"/>
						</xsl:call-template>',

						<xsl:value-of select="."/>
					] );
				</xsl:for-each>

				charts.push(<xsl:value-of select="$var_name"/>);
			</xsl:template>

			<xsl:template match="//Row/CpuMsSql | //Row/CpuOs" mode="lines">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				{
					label: '<xsl:value-of select="$LineName"/>',

					color: color_<xsl:choose>
						<xsl:when test="$LineName = 'CpuMsSql'">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:when test="$LineName = 'CpuOs'">
							<xsl:value-of select="2"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="3"/>
						</xsl:otherwise>
					</xsl:choose>,

					highlighter: {
						formatString: '<xsl:value-of select="$LineName"/>: %s, %s'
					}
				}
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="jInstancePhysicalMemoryUtilization.Graph" column="1" row="2" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date, 'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name != 'GetInstanceMemoryUtilization']">
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name = 'GetInstanceMemoryUtilization' and @hierarchy = '']/RecordSet[@id = '1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>InstancePhysicalMemoryUtilization</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>InstancePhysicalMemoryUtilization</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>InstancePhysicalMemoryUtilization</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready( function() {
								var charts  = [];
								var color_1 = 'rgb(224,  64,  10)';
								var color_2 = 'rgb(  5, 100, 146)';
								var color_3 = 'rgb(252, 180,  65)';
								var color_4 = 'rgb( 65, 140, 240)';
								var color_5 = 'rgb(107, 142,  35)';

								<xsl:apply-templates select="//Row[AvailiblePhysicalMemoryKb][1]/AvailiblePhysicalMemoryKb" mode="plots" />
								<xsl:apply-templates select="//Row[ReservedMemoryKb][1]/ReservedMemoryKb"                   mode="plots" />
								<xsl:apply-templates select="//Row[CommittedMemoryKb][1]/CommittedMemoryKb"                 mode="plots" />

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot( 'chart', charts, {
									stackSeries: false,

									/**
									 * "title" options
									 */
									title: {
										show: true,
										text: 'Memory'
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										shadow:        false,
										drawGridlines: false
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  0,
										bottom: 0,
										left:   0
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: false, //
												showMark:     false, //
												showLabel:    false, //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis.  Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: false,        //
												showMark:     false,        //
												showLabel:    false,        //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer: $.jqplot.EnhancedLegendRenderer,

										show:         true,         // whether to show legend
										location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'insideGrid', //
										xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,         //

										rendererOptions: {
											numberRows:    0,
											numberColumns: 2
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         false,          // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         4,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not.
											shadowAngle:  45,             // angle of the shadow. Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: false,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             true,
											showDataLabels:     false
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       false,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // CSS spec for the cursor type to change the cursor to when over plot.
										show:                    false,       // whether to show cursor
										showTooltip:             false,       // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       true,        // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									},

									/**
									 * Series options are specified as an array of objects, one object
									 * for each series.
									 */
									series: [
										<xsl:apply-templates select="//Row[AvailiblePhysicalMemoryKb][1]/AvailiblePhysicalMemoryKb" mode="lines" />,
										<xsl:apply-templates select="//Row[ReservedMemoryKb][1]/ReservedMemoryKb"                   mode="lines" />,
										<xsl:apply-templates select="//Row[CommittedMemoryKb][1]/CommittedMemoryKb"                 mode="lines" />
									]
								} );

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height( $('#container').height() * 0.96 );

									plot.replot( {
										resetAxes: true
									} );
								} );
							} );
						</script>
					</body>
				</html>
			</xsl:template>

			<xsl:template match="//Row/AvailiblePhysicalMemoryKb | //Row/ReservedMemoryKb | //Row/CommittedMemoryKb" mode="plots">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				<xsl:variable name="var_name">
					<xsl:value-of select="translate(concat('chart_', $LineName), ' :()-.[]{}\/', '_____________')"/>
				</xsl:variable>

				var <xsl:value-of select="$var_name"/>=[];

				<xsl:for-each select="ancestor::RecordSet/Row/child::*[local-name() = $LineName]">
					<xsl:value-of select="$var_name"/>.push( [
						'<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="../EventTime"/>
						</xsl:call-template>',

						<xsl:value-of select="."/>
					] );
				</xsl:for-each>

				charts.push(<xsl:value-of select="$var_name"/>);
			</xsl:template>

			<xsl:template match="//Row/AvailiblePhysicalMemoryKb | //Row/ReservedMemoryKb | //Row/CommittedMemoryKb" mode="lines">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				{
					label: '<xsl:value-of select="$LineName"/>',

					color: color_<xsl:choose>
						<xsl:when test="$LineName = 'AvailiblePhysicalMemoryKb'">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:when test="$LineName = 'ReservedMemoryKb'">
							<xsl:value-of select="2"/>
						</xsl:when>
						<xsl:when test="$LineName = 'CommittedMemoryKb'">
							<xsl:value-of select="3"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="4"/>
						</xsl:otherwise>
					</xsl:choose>,

					highlighter: {
						formatString: '<xsl:value-of select="$LineName"/>: %s, %s'
					}
				}
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="jInstanceIOStatus.Graph" column="1" row="3" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date, 'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name != 'GetInstanceIOStatus']">
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name = 'GetInstanceIOStatus' and @hierarchy = '']/RecordSet[@id = '1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>InstanceIOStatus</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>InstanceIOStatus</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>InstanceIOStatus</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready( function() {
								var charts  = [];
								var color_1 = 'rgb(  5, 100, 146)';
								var color_2 = 'rgb(224,  64,  10)';
								var color_3 = 'rgb(252, 180,  65)';
								var color_4 = 'rgb( 65, 140, 240)';
								var color_5 = 'rgb(107, 142,  35)';

								<xsl:apply-templates select="//Row[num_of_reads][1]/num_of_reads"   mode="plots" />
								<xsl:apply-templates select="//Row[num_of_writes][1]/num_of_writes" mode="plots" />

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot( 'chart', charts, {
									/**
									 * "title" options
									 */
									title: {
										show: true,
										text: 'I/O (read/writes)'
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										shadow:        false,
										drawGridlines: false
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  0,
										bottom: 0,
										left:   0
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: false, //
												showMark:     false, //
												showLabel:    false, //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis.  Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: false,        //
												showMark:     false,        //
												showLabel:    false,        //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer: $.jqplot.EnhancedLegendRenderer,

										show:         true,         // whether to show legend
										location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'insideGrid', //
										xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,         //

										rendererOptions: {
											numberRows:         0,     // maximum number of rows in the legend
											numberColumns:      2,     // maximum number of columns in the legend
											seriesToggle:       false, // false to not enable series on/off toggling on the legend
											seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         false,          // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         4,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not
											shadowAngle:  45,             // angle of the shadow. Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: false,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             true,
											showDataLabels:     false
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       false,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // A CSS spec for the cursor type to change the cursor to when over plot.
										show:                    false,       // whether to show cursor
										showTooltip:             false,       // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       false,       // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									},

									stackSeries: false,

									/**
									 * Series options are specified as an array of objects, one object
									 * for each series.
									 */
									series: [
										<xsl:apply-templates select="//Row[num_of_reads][1]/num_of_reads"   mode="lines" />,
										<xsl:apply-templates select="//Row[num_of_writes][1]/num_of_writes" mode="lines" />
									]
								} );

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height( $('#container').height() * 0.96 );

									plot.replot( {
										resetAxes: true
									} );
								} );
							} );
						</script>
					</body>
				</html>
			</xsl:template>

			<xsl:template match="//Row/num_of_reads | //Row/num_of_writes" mode="plots">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				<xsl:variable name="var_name">
					<xsl:value-of select="translate(concat('chart_', $LineName), ' :()-.[]{}\/,=', '______________')"/>
				</xsl:variable>

				var <xsl:value-of select="$var_name"/>=[];

				<xsl:for-each select="ancestor::RecordSet/Row/child::*[local-name() = $LineName]">
					<xsl:value-of select="$var_name"/>.push( [
						'<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="../EventTime"/>
						</xsl:call-template>',

						<xsl:value-of select="."/>
					] );
				</xsl:for-each>

				charts.push(<xsl:value-of select="$var_name"/>);
			</xsl:template>

			<xsl:template match="//Row/num_of_reads | //Row/num_of_writes" mode="lines">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				{
					label: '<xsl:value-of select="$LineName"/>',

					color: color_<xsl:choose>
						<xsl:when test="$LineName = 'num_of_reads'">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:when test="$LineName = 'num_of_writes'">
							<xsl:value-of select="2"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="3"/>
						</xsl:otherwise>
					</xsl:choose>,

					highlighter: {
						formatString: '<xsl:value-of select="$LineName"/>: %s, %s'
					}
				}
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="jInstanceWaitStatistics.Graph" column="2" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date,'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name != 'GetInstanceSysDmOsWaitStatsSummary']">
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name = 'GetInstanceSysDmOsWaitStatsSummary' and @hierarchy = '']/RecordSet[@id = '1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance Wait Statistics</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Instance Wait Statistics</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance Wait Statistics</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready(function() {
								var charts  = [];
								var color_1 = 'rgb(224,  64,  10)';
								var color_2 = 'rgb(  5, 100, 146)';
								var color_3 = 'rgb(252, 180,  65)';
								var color_4 = 'rgb( 65, 140, 240)';
								var color_5 = 'rgb(107, 142,  35)';

								<xsl:apply-templates select="//Row[WaitTimeMs][1]/WaitTimeMs" mode="plots" />

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot('chart', charts, {
									stackSeries: false,

									/**
									 * "title" options
									 */
									title: {
										show: true,
										text: 'Waits'
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										shadow:        false,
										drawGridlines: false
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  0,
										bottom: 0,
										left:   0
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: false, //
												showMark:     false, //
												showLabel:    false, //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis.  Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: false,        //
												showMark:     false,        //
												showLabel:    false,        //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer:     $.jqplot.EnhancedLegendRenderer,

										show:         true,         // whether to show legend
										location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'insideGrid', //
										xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,         //

										rendererOptions: {
											numberRows:         0,     // maximum number of rows in the legend
											numberColumns:      2,     // maximum number of columns in the legend
											seriesToggle:       false, // false to not enable series on/off toggling on the legend
											seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         false,          // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         4,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not
											shadowAngle:  45,             // angle of the shadow. Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: false,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             true,
											showDataLabels:     false
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       false,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // CSS spec for the cursor type to change the cursor to when over plot.
										show:                    false,       // whether to show cursor
										showTooltip:             false,       // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       true,        // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									},

									/**
									 * Series options are specified as an array of objects, one object
									 * for each series.
									 */
									series: [
										<xsl:apply-templates select="//Row[WaitTimeMs][1]/WaitTimeMs" mode="lines" />
									]
								} );

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height($('#container').height() * 0.96);

									plot.replot( {
										resetAxes: true
									} );
								} );
							});
						</script>
					</body>
				</html>
			</xsl:template>

			<xsl:template match="//Row/WaitTimeMs" mode="plots">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				<xsl:variable name="var_name">
					<xsl:value-of select="translate(concat('chart_', $LineName), ' :()-.[]{}\/', '_____________')"/>
				</xsl:variable>

				var <xsl:value-of select="$var_name"/>=[];

				<xsl:for-each select="ancestor::RecordSet/Row/child::*[local-name() = $LineName]">
					<xsl:value-of select="$var_name"/>.push( [
						'<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="../EventTime"/>
						</xsl:call-template>',

						<xsl:value-of select="."/>
					] );
				</xsl:for-each>

				charts.push(<xsl:value-of select="$var_name"/>);
			</xsl:template>

			<xsl:template match="//Row/WaitTimeMs" mode="lines">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				{
					label: '<xsl:value-of select="$LineName"/>',

					color: color_<xsl:choose>
						<xsl:when test="$LineName = 'WaitTimeMs'">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="2"/>
						</xsl:otherwise>
					</xsl:choose>,

					highlighter: {
						formatString: '<xsl:value-of select="$LineName"/>: %s, %s'
					}
				}
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="jInstanceNetworkStatusSummary.Graph" column="2" row="2" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date, 'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name != 'GetInstanceNetworkStatusSummary']">
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name = 'GetInstanceNetworkStatusSummary' and @hierarchy = '']/RecordSet[@id = '1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>InstanceNetworkStatusSummary</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>InstanceNetworkStatusSummary</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>InstanceNetworkStatusSummary</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready( function() {
								var charts  = [];
								var color_1 = 'rgb(  5, 100, 146)';
								var color_2 = 'rgb(224,  64,  10)';
								var color_3 = 'rgb(252, 180,  65)';
								var color_4 = 'rgb( 65, 140, 240)';
								var color_5 = 'rgb(107, 142,  35)';

								<xsl:apply-templates select="//Row[bytes_reads_second][1]/bytes_reads_second"   mode="plots" />
								<xsl:apply-templates select="//Row[bytes_writes_second][1]/bytes_writes_second" mode="plots" />

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot( 'chart', charts, {
									stackSeries: false,

									/**
									 * "title" options
									 */
									title: {
										show: true,
										text: 'Network (read/writes)'
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										shadow:        false,
										drawGridlines: false
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  0,
										bottom: 0,
										left:   0
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: false, //
												showMark:     false, //
												showLabel:    false, //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis.  Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: false,        //
												showMark:     false,        //
												showLabel:    false,        //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer: $.jqplot.EnhancedLegendRenderer,

										show:         true,         // whether to show legend
										location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'insideGrid', //
										xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,         //

										rendererOptions: {
											numberRows:         0,     // maximum number of rows in the legend
											numberColumns:      2,     // maximum number of columns in the legend
											seriesToggle:       false, // false to not enable series on/off toggling on the legend
											seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         false,          // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         4,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not
											shadowAngle:  45,             // angle of the shadow. Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: false,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             true,
											showDataLabels:     false
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       false,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // A CSS spec for the cursor type to change the cursor to when over plot.
										show:                    false,       // whether to show cursor
										showTooltip:             false,       // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       false,       // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									},

									/**
									 * Series options are specified as an array of objects, one object
									 * for each series.
									 */
									series: [
										<xsl:apply-templates select="//Row[bytes_reads_second][1]/bytes_reads_second"   mode="lines" />,
										<xsl:apply-templates select="//Row[bytes_writes_second][1]/bytes_writes_second" mode="lines" />
									]
								} );

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height( $('#container').height() * 0.96 );

									plot.replot( {
										resetAxes: true
									} );
								} );
							} );
						</script>
					</body>
				</html>
			</xsl:template>

			<xsl:template match="//Row/bytes_reads_second | //Row/bytes_writes_second" mode="plots">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				<xsl:variable name="var_name">
					<xsl:value-of select="translate(concat('chart_', $LineName), ' :()-.[]{}\/,=', '______________')"/>
				</xsl:variable>

				var <xsl:value-of select="$var_name"/>=[];

				<xsl:for-each select="ancestor::RecordSet/Row/child::*[local-name() = $LineName]">
					<xsl:value-of select="$var_name"/>.push( [
						'<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="../EventTime"/>
						</xsl:call-template>',

						<xsl:value-of select="."/>
					] );
				</xsl:for-each>

				charts.push(<xsl:value-of select="$var_name"/>);
			</xsl:template>

			<xsl:template match="//Row/bytes_reads_second | //Row/bytes_writes_second" mode="lines">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				{
					label: '<xsl:value-of select="$LineName"/>',

					color: color_<xsl:choose>
						<xsl:when test="$LineName = 'bytes_reads_second'">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:when test="$LineName = 'bytes_writes_second'">
							<xsl:value-of select="2"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="3"/>
						</xsl:otherwise>
					</xsl:choose>,

					highlighter: {
						formatString: '<xsl:value-of select="$LineName"/>: %s, %s'
					}
				}
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="jInstanceSummaryProcesses.Graph" column="2" row="3" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date, 'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name != 'GetInstanceSummaryProcesses']">
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name = 'GetInstanceSummaryProcesses' and @hierarchy = '']/RecordSet[@id = '1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance Summary Processes</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Instance Summary Processes</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance Summary Processes</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready( function() {
								var charts  = [];
								var color_1 = 'rgb(224,  64,  10)';
								var color_2 = 'rgb(  5, 100, 146)';
								var color_3 = 'rgb(252, 180,  65)';
								var color_4 = 'rgb(107, 142,  35)';
								var color_5 = 'rgb( 65, 140, 240)';

								<xsl:apply-templates select="//Row[Blocked][1]/Blocked"       mode="plots" />
								<xsl:apply-templates select="//Row[UserActive][1]/UserActive" mode="plots" />
								<xsl:apply-templates select="//Row[User][1]/User"             mode="plots" />
								<xsl:apply-templates select="//Row[System][1]/System"         mode="plots" />

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot( 'chart', charts, {
									stackSeries: false,

									/**
									 * "title" options
									 */
									title: {
										show: true,
										text: 'Processes'
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										shadow:        false,
										drawGridlines: false
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  0,
										bottom: 0,
										left:   0
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: false, //
												showMark:     false, //
												showLabel:    false, //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis.  Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: false,        //
												showMark:     false,        //
												showLabel:    false,        //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer:     $.jqplot.EnhancedLegendRenderer,

										show:         true,         // whether to show legend
										location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'insideGrid', //
										xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,         //

										rendererOptions: {
											numberRows:         0,     // maximum number of rows in the legend
											numberColumns:      2,     // maximum number of columns in the legend
											seriesToggle:       false, // false to not enable series on/off toggling on the legend
											seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         false,          // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         2,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not
											shadowAngle:  45,             // angle of the shadow.  Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: false,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             true,
											showDataLabels:     false
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       false,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // CSS spec for the cursor type to change the cursor to when over plot.
										show:                    false,       // whether to show cursor
										showTooltip:             false,       // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       false,       // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									},

									/**
									 * Series options are specified as an array of objects, one object
									 * for each series.
									 */
									series: [
										<xsl:apply-templates select="//Row[Blocked][1]/Blocked"       mode="lines" />,
										<xsl:apply-templates select="//Row[UserActive][1]/UserActive" mode="lines" />,
										<xsl:apply-templates select="//Row[User][1]/User"             mode="lines" />,
										<xsl:apply-templates select="//Row[System][1]/System"         mode="lines" />
									]
								} );

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height( $('#container').height() * 0.96 );

									plot.replot( {
										resetAxes: true
									} );
								} );
							} );
						</script>
					</body>
				</html>
			</xsl:template>

			<xsl:template match="//Row/Blocked | //Row/UserActive | //Row/User | //Row/System" mode="plots">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				<xsl:variable name="var_name">
					<xsl:value-of select="translate(concat('chart_', $LineName), ' :()-.[]{}\/', '_____________')"/>
				</xsl:variable>

				var <xsl:value-of select="$var_name"/>=[];

				<xsl:for-each select="ancestor::RecordSet/Row/child::*[local-name() = $LineName]">
					<xsl:value-of select="$var_name"/>.push( [
						'<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="../EventTime"/>
						</xsl:call-template>',

						<xsl:value-of select="."/>
					] );
				</xsl:for-each>

				charts.push(<xsl:value-of select="$var_name"/>);
			</xsl:template>

			<xsl:template match="//Row/Blocked | //Row/UserActive | //Row/User | //Row/System" mode="lines">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				{
					label: '<xsl:value-of select="$LineName"/>',

					color: color_<xsl:choose>
						<xsl:when test="$LineName = 'Blocked'">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:when test="$LineName = 'UserActive'">
							<xsl:value-of select="2"/>
						</xsl:when>
						<xsl:when test="$LineName = 'User'">
							<xsl:value-of select="3"/>
						</xsl:when>
						<xsl:when test="$LineName = 'System'">
							<xsl:value-of select="4"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="5"/>
						</xsl:otherwise>
					</xsl:choose>,

					highlighter: {
						formatString: '<xsl:value-of select="$LineName"/>: %s, %s'
					}
				}
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="jPageLifeExpectancy.Graph" column="3" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date,'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name != 'GetInstancePageLifeExpectancy']">
			</xsl:template>

			<xsl:key name="CounterName" match="/MSSQLResults/MSSQLResult[@name='GetInstancePageLifeExpectancy' and @hierarchy='']/RecordSet[@id='1']/Row" use="CounterName" />

			<xsl:template match="/MSSQLResults/MSSQLResult[@name = 'GetInstancePageLifeExpectancy' and @hierarchy = '']/RecordSet[@id = '1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>PageLifeExpectancy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>PageLifeExpectancy</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>PageLifeExpectancy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready(function() {
								var charts  = [];
								var color_1 = 'rgb(  5, 100, 146)';
								var color_2 = 'rgb(224,  64,  10)';
								var color_3 = 'rgb(252, 180,  65)';
								var color_4 = 'rgb( 65, 140, 240)';
								var color_5 = 'rgb(107, 142,  35)';

								<xsl:for-each select="//Row[generate-id(.) = generate-id( key( 'CounterName', CounterName ) )]">
									<xsl:sort select="CounterName"/>

									<xsl:variable name="var_name">
										<xsl:value-of select="translate(concat('chart_', CounterName), ' :()-.[]{}\/,=', '______________')"/>
									</xsl:variable>

									var <xsl:value-of select="$var_name"/>=[];

									<xsl:for-each select="key('CounterName', CounterName)">
										<xsl:if test="CounterValue != ''">
											<xsl:value-of select="$var_name"/>.push( [
												'<xsl:call-template name="formatDate">
													<xsl:with-param name="date" select="EventTime"/>
												</xsl:call-template>',

												<xsl:value-of select="CounterValue"/>] );
										</xsl:if>
									</xsl:for-each>

									charts.push(<xsl:value-of select="$var_name"/>);
								</xsl:for-each>

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot('chart', charts, {
									stackSeries: false,

									/**
									 * "title" options
									 */
									title: {
										show: true,
										text: 'Page Life Expectancy'
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										shadow:        false,
										drawGridlines: false
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  0,
										bottom: 0,
										left:   0
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: false, //
												showMark:     false, //
												showLabel:    false, //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis.  Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: false,        //
												showMark:     false,        //
												showLabel:    false,        //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer:     $.jqplot.EnhancedLegendRenderer,

										show:         true,         // whether to show legend
										location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'insideGrid', //
										xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,         //

										rendererOptions: {
											numberRows:         0,     // maximum number of rows in the legend
											numberColumns:      2,     // maximum number of columns in the legend
											seriesToggle:       false, // false to not enable series on/off toggling on the legend
											seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         false,          // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         2,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not
											shadowAngle:  45,             // angle of the shadow.  Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: false,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             true,
											showDataLabels:     false
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       false,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // CSS spec for the cursor type to change the cursor to when over plot.
										show:                    false,       // whether to show cursor
										showTooltip:             false,       // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       true,        // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									},

									/**
									 * Series options are specified as an array of objects, one object
									 * for each series.
									 */
									series: [
										<xsl:for-each select="//Row[generate-id(.) = generate-id (key('CounterName', CounterName))]">

										<xsl:sort select="CounterName"/> {
											label: '<xsl:value-of select="CounterName"/>',

											color: color_<xsl:choose>
												<xsl:when test="position() &lt;= 5">
													<xsl:value-of select="position()"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="5 - position() mod 5"/>
												</xsl:otherwise>
											</xsl:choose>,

											highlighter: {
												formatString: '<xsl:value-of select="CounterName"/>: %s, %s'
											}
										}

										<xsl:if test="position() != last()">,</xsl:if>

										</xsl:for-each>
									]
								} );

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height($('#container').height() * 0.96);

									plot.replot( {
										resetAxes: true
									} );
								} );
							});
						</script>
					</body>
				</html>
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="jBufferCacheHitRatio.Graph" column="3" row="2" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date, 'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name != 'GetInstanceBufferCacheHitRatio']">
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name = 'GetInstanceBufferCacheHitRatio' and @hierarchy = '']/RecordSet[@id = '1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Buffer Cache Hit Ratio</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Статистика попадания в кэш буфера</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Buffer Cache Hit Ratio</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready( function() {
								var charts  = [];
								var color_1 = 'rgb(  5, 100, 146)';
								var color_2 = 'rgb(224,  64,  10)';
								var color_3 = 'rgb(252, 180,  65)';
								var color_4 = 'rgb( 65, 140, 240)';
								var color_5 = 'rgb(107, 142,  35)';

								<xsl:apply-templates select="//Row[BufferCacheHitRatioPercent][1]/BufferCacheHitRatioPercent" mode="plots" />

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot( 'chart', charts, {
									stackSeries: false,

									/**
									 * "title" options
									 */
									title: {
										show: true,
										text: 'Buffer Cache Hit Ratio'
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										shadow:        false,
										drawGridlines: false
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  0,
										bottom: 0,
										left:   0
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: false, //
												showMark:     false, //
												showLabel:    false, //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis.  Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: false,        //
												showMark:     false,        //
												showLabel:    false,        //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer: $.jqplot.EnhancedLegendRenderer,

										show:         true,         // whether to show legend
										location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'insideGrid', //
										xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,         //

										rendererOptions: {
											numberRows:         0,     // maximum number of rows in the legend
											numberColumns:      2,     // maximum number of columns in the legend
											seriesToggle:       false, // false to not enable series on/off toggling on the legend
											seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         false,          // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         2,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not
											shadowAngle:  45,             // angle of the shadow.  Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: false,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             true,
											showDataLabels:     false
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       false,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // A CSS spec for the cursor type to change the cursor to when over plot.
										show:                    false,       // whether to show cursor
										showTooltip:             false,       // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       false,       // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									},

									/**
									 * Series options are specified as an array of objects, one object
									 * for each series.
									 */
									series: [
										<xsl:apply-templates select="//Row[BufferCacheHitRatioPercent][1]/BufferCacheHitRatioPercent" mode="lines" />
									]
								} );

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height( $('#container').height() * 0.96 );

									plot.replot( {
										resetAxes: true
									} );
								} );
							} );
						</script>
					</body>
				</html>
			</xsl:template>

			<xsl:template match="//Row/BufferCacheHitRatioPercent" mode="plots">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				<xsl:variable name="var_name">
					<xsl:value-of select="translate(concat('chart_', $LineName), ' :()-.[]{}\/,=', '______________')"/>
				</xsl:variable>

				var <xsl:value-of select="$var_name"/>=[];

				<xsl:for-each select="ancestor::RecordSet/Row/child::*[local-name() = $LineName]">
					<xsl:value-of select="$var_name"/>.push( [
						'<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="../EventTime"/>
						</xsl:call-template>',

						<xsl:value-of select="."/>
					] );
				</xsl:for-each>

				charts.push(<xsl:value-of select="$var_name"/>);
			</xsl:template>

			<xsl:template match="//Row/BufferCacheHitRatioPercent" mode="lines">
				<xsl:variable name="LineName">
					<xsl:value-of select="local-name()" />
				</xsl:variable>

				{
					label: '<xsl:value-of select="$LineName"/>',

					color: color_<xsl:choose>
						<xsl:when test="$LineName = 'BufferCacheHitRatioPercent'">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="2"/>
						</xsl:otherwise>
					</xsl:choose>,

					highlighter: {
						formatString: '<xsl:value-of select="$LineName"/>: %s, %s'
					}
				}
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="jInstanceBatchRequestsSec.Graph" column="3" row="3" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date,'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name != 'GetInstanceBatchRequestsSec']">
			</xsl:template>

			<xsl:key name="CounterName" match="/MSSQLResults/MSSQLResult[@name = 'GetInstanceBatchRequestsSec' and @hierarchy = '']/RecordSet[@id = '1']/Row" use="CounterName" />

			<xsl:template match="/MSSQLResults/MSSQLResult[@name = 'GetInstanceBatchRequestsSec' and @hierarchy = '']/RecordSet[@id = '1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>InstanceBatchRequestsSec.Graph</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>InstanceBatchRequestsSec.Graph</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>InstanceBatchRequestsSec.Graph</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready(function() {
								var charts  = [];
								var color_1 = 'rgb(  5, 100, 146)';
								var color_2 = 'rgb(224,  64,  10)';
								var color_3 = 'rgb(252, 180,  65)';
								var color_4 = 'rgb( 65, 140, 240)';
								var color_5 = 'rgb(107, 142,  35)';

								<xsl:for-each select="//Row[generate-id(.) = generate-id (key('CounterName', CounterName))]">
									<xsl:sort select="CounterName"/>

									<xsl:variable name="var_name">
										<xsl:value-of select="translate(concat('chart_', CounterName), ' :()-.[]{}\/,=', '______________')"/>
									</xsl:variable>

									var <xsl:value-of select="$var_name"/>=[];

									<xsl:for-each select="key('CounterName', CounterName)">
										<xsl:if test="CounterValue != ''">
											<xsl:value-of select="$var_name"/>.push( [
												'<xsl:call-template name="formatDate">
													<xsl:with-param name="date" select="EventTime"/>
												</xsl:call-template>',

												<xsl:value-of select="CounterValue"/>] );
										</xsl:if>
									</xsl:for-each>

									charts.push(<xsl:value-of select="$var_name"/>);
								</xsl:for-each>

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot('chart', charts, {
									stackSeries: false,

									/**
									 * "title" options
									 */
									title: {
										show: true,
										text: 'Batch Requests'
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										shadow:        false,
										drawGridlines: false
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  0,
										bottom: 0,
										left:   0
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: false, //
												showMark:     false, //
												showLabel:    false, //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          false,    // whether or not to renderer the axis.  Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     false,    //
											labelPosition: 'middle', //
											showTicks:     false,    // whether or not to show the tick labels
											showTickMarks: false,    // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: false,        //
												showMark:     false,        //
												showLabel:    false,        //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer:     $.jqplot.EnhancedLegendRenderer,

										show:         true,         // whether to show legend
										location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'insideGrid', //
										xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,         //

										rendererOptions: {
											numberRows:         0,     // maximum number of rows in the legend
											numberColumns:      2,     // maximum number of columns in the legend
											seriesToggle:       false, // false to not enable series on/off toggling on the legend
											seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         false,          // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         2,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not
											shadowAngle:  45,             // angle of the shadow.  Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: false,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             true,
											showDataLabels:     false
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       false,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // CSS spec for the cursor type to change the cursor to when over plot.
										show:                    false,       // whether to show cursor
										showTooltip:             false,       // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       false,       // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									},

									/**
									 * Series options are specified as an array of objects, one object
									 * for each series.
									 */
									series: [
										<xsl:for-each select="//Row[generate-id(.) = generate-id (key('CounterName', CounterName))]">

										<xsl:sort select="CounterName"/> {
											label: '<xsl:value-of select="CounterName"/>',

											color: color_<xsl:choose>
												<xsl:when test="position() &lt;= 5">
													<xsl:value-of select="position()"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="5 - position() mod 5"/>
												</xsl:otherwise>
											</xsl:choose>,

											highlighter: {
												formatString: '<xsl:value-of select="CounterName"/>: %s, %s'
											}
										}

										<xsl:if test="position() != last()">,</xsl:if>

										</xsl:for-each>
									]
								} );

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height($('#container').height() * 0.96);

									plot.replot( {
										resetAxes: true
									} );
								} );
							});
						</script>
					</body>
				</html>
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
