<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="InstanceIOStatus.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Instance Disks IO status</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Нагрузка дисковой системы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Instance Disks IO status</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="InstanceIOStatus.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Instance Disks IO status</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Нагрузка дисковой системы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Instance Disks IO status</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetInstanceIOStatus' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Экземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Database</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>База данных</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Database</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>File</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Файл</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>File</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Physical File</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Физический файл</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Physical File</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Size (bytes)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Размер (байт)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Size (bytes)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th colspan="3">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Read</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Чтение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Read</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th colspan="3">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Write</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запись</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Write</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Volume (byte)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Количество (байт)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Volume (byte)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Performance (ms)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Скорость (мс)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Performance (ms)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Status</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Статус</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Status</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Volume (byte)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Количество (байт)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Volume (byte)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Performance (ms)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Скорость (мс)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Performance (ms)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Status</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Статус</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Status</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetInstanceIOStatus' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseName != ''">
								<xsl:value-of select="DatabaseName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseFileName != ''">
								<xsl:value-of select="DatabaseFileName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="physical_name != ''">
								<xsl:value-of select="physical_name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="size_on_disk_bytes != ''">
								<xsl:value-of select="format-number(size_on_disk_bytes, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="num_of_bytes_read != ''">
								<xsl:value-of select="format-number(num_of_bytes_read, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="AvgIOReadStallMs != ''">
								<xsl:value-of select="format-number(AvgIOReadStallMs, '###,###,##0.##')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="AvgIOReadStallStatus != ''">
								<xsl:value-of select="AvgIOReadStallStatus"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="num_of_bytes_written != ''">
								<xsl:value-of select="format-number(num_of_bytes_written, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="AvgIOWriteStallMs != ''">
								<xsl:value-of select="format-number(AvgIOWriteStallMs, '###,###,##0.##')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="AvgIOWriteStallStatus != ''">
								<xsl:value-of select="AvgIOWriteStallStatus"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
