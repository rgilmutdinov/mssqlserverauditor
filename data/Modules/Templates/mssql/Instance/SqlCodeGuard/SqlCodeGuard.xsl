<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseErrors.HTML" columns="50;50" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>T-SQL Code issues</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Проблемы T-SQL кода</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>T-SQL Code issues</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseErrors1.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

			<html>
			<head>
				<title>
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>T-SQL Code issues</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Проблемы T-SQL кода</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>T-SQL Code issues</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				</title>

				<script src="$JS_FOLDER$/SyntaxHighlighter/scripts/shCore.js"></script>
				<script src="$JS_FOLDER$/SyntaxHighlighter/scripts/shBrushSql.js"></script>
				<script src="$JS_FOLDER$/SyntaxHighlighter/scripts/shBrushPlain.js"></script>
				<link href="$JS_FOLDER$/SyntaxHighlighter/styles/shCore.css" rel="stylesheet" type="text/css" />
				<link href="$JS_FOLDER$/SyntaxHighlighter/styles/shThemeDefault.css" rel="stylesheet" type="text/css" />

				<script type="text/javascript">SyntaxHighlighter.defaults['toolbar'] = false;</script>
				<script type="text/javascript">SyntaxHighlighter.all()</script>
			</head>
			<body>
				<style>
					body { overflow: auto; padding:0; margin:0; }
				</style>
				<table border="0" width="100%" style="font-size : 12px; font-family : Courier New">
					<xsl:for-each select="MSSQLResults/GetDStoredProceduresCode-SQLCodeGuard[@RecordSet='2']">
						<tr>
							<td>
								<pre class="brush:sql ruler: true; gutter: true; highlight: [{SCGErrorRows}]">
									<xsl:variable name="varSCGObject" select="SCGObject"/>
									<xsl:value-of select="../GetDStoredProceduresCode[ObjectName = $varSCGObject]/Procedure"/>
								</pre>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>

	<mssqlauditorpreprocessor id="DatabaseErrors2.HTML" column="2" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">
			<html>
			<head>
				<title>
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>T-SQL Code issues</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Проблемы T-SQL кода</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>T-SQL Code issues</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				</title>

				<script src="$JS_FOLDER$/SyntaxHighlighter/scripts/shCore.js"></script>
				<script src="$JS_FOLDER$/SyntaxHighlighter/scripts/shBrushSql.js"></script>
				<script src="$JS_FOLDER$/SyntaxHighlighter/scripts/shBrushPlain.js"></script>
				<link href="$JS_FOLDER$/SyntaxHighlighter/styles/shCore.css" rel="stylesheet" type="text/css" />
				<link href="$JS_FOLDER$/SyntaxHighlighter/styles/shThemeDefault.css" rel="stylesheet" type="text/css" />

				<script type="text/javascript">SyntaxHighlighter.defaults['toolbar'] = false;</script>
				<script type="text/javascript">SyntaxHighlighter.all()</script>
			</head>
			<body>
				<style>
					body { overflow: auto; padding:0; margin:0; }
				</style>

				<table border="1" width="100%" style="font-size : 12px; font-family : Courier New">
					<tr bgcolor="#9acd32">
						<th>
							<xsl:choose>
								<xsl:when test="lang('en')">
									<xsl:text>Line</xsl:text>
								</xsl:when>
								<xsl:when test="lang('ru')">
									<xsl:text>Строка</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Line</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</th>
						<th>
							<xsl:choose>
								<xsl:when test="lang('en')">
									<xsl:text>Column</xsl:text>
								</xsl:when>
								<xsl:when test="lang('ru')">
									<xsl:text>Колонка</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Column</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</th>
						<th>
							<xsl:choose>
								<xsl:when test="lang('en')">
									<xsl:text>Object</xsl:text>
								</xsl:when>
								<xsl:when test="lang('ru')">
									<xsl:text>Объект</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Object</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</th>
						<th>
							<xsl:choose>
								<xsl:when test="lang('en')">
									<xsl:text>Code</xsl:text>
								</xsl:when>
								<xsl:when test="lang('ru')">
									<xsl:text>Код</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Code</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</th>
						<th>
							<xsl:choose>
								<xsl:when test="lang('en')">
									<xsl:text>Message</xsl:text>
								</xsl:when>
								<xsl:when test="lang('ru')">
									<xsl:text>Сообщение</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Message</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</th>
					</tr>
					<xsl:for-each select="MSSQLResults/GetDStoredProceduresCode-SQLCodeGuard[@RecordSet='1']">
						<tr>
							<td><xsl:value-of select="SCGRow"/></td>
							<td><xsl:value-of select="SCGColumn"/></td>
							<td><xsl:value-of select="SCGObject"/></td>
							<td><xsl:value-of select="SCGErrorCode"/></td>
							<td><xsl:value-of select="SCGMessage"/></td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
			</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
