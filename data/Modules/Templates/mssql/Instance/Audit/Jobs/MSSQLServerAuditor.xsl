<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="Jobs.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Server Jobs</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Данные о регулярных заданиях</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Server Jobs</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="Jobs.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Server Jobs</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Данные о регулярных заданиях</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Server Jobs</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetJobsList' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Экземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Job</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Имя задания</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Job</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Owner</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Владелец</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Owner</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Enabled</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Включён</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Enabled</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th colspan="5">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Schedule</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Расписание выполнения</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Schedule</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Date Created</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Дата создания</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Date Created</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Date Updated</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Дата изменения</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Date Updated</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Name</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Имя</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Name</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Active</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Активно</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Active</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Start</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Начало</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Start</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>End</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Окончание</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>End</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Description</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Пояснение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Description</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetJobsList' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="JobName != ''">
								<xsl:value-of select="JobName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:if test="IsJobOwnerIssue != 0">
							<xsl:attribute name="style">font-weight: bold; color: blue;</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="JobOwner != ''">
								<xsl:value-of select="JobOwner"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="JobEnabled != ''">
								<xsl:value-of select="JobEnabled"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="JobScheduleName != ''">
								<xsl:value-of select="JobScheduleName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="JobScheduleEnabled != ''">
								<xsl:value-of select="JobScheduleEnabled"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="JobScheduleActiveStartDateTime != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(JobScheduleActiveStartDateTime, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(JobScheduleActiveStartDateTime, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(JobScheduleActiveStartDateTime, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(JobScheduleActiveStartDateTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="JobScheduleActiveEndDateTime != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(JobScheduleActiveEndDateTime, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(JobScheduleActiveEndDateTime, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(JobScheduleActiveEndDateTime, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(JobScheduleActiveEndDateTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="JobScheduleDescription != ''">
								<xsl:value-of select="JobScheduleDescription"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="JobDateCreated != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(JobDateCreated, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(JobDateCreated, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(JobDateCreated, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(JobDateCreated, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="JobDateModified != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(JobDateModified, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(JobDateModified, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(JobDateModified, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(JobDateModified, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>

<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="ServerJobs.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Description</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Описание</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Description</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="ServerJobs.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Description</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Описание</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Description</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Jobs Owned by User Accounts</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Владелец задания - пользователь, а не администратор</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Jobs Owned by User Accounts</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>SQL Server Agent jobs are like hot potatoes: they're owned by whoever
			touched them last. The job owner doesn't really mean much as jobs can be
			owned by anyone and they'll still work the same way.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>SQL Server Agent jobs are like hot potatoes: they're owned by whoever
			touched them last. The job owner doesn't really mean much as jobs can be
			owned by anyone and they'll still work the same way.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>SQL Server Agent jobs are like hot potatoes: they're owned by whoever
			touched them last. The job owner doesn't really mean much as jobs can be
			owned by anyone and they'll still work the same way.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Right up until the job owner's account is dropped.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Right up until the job owner's account is dropped.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Right up until the job owner's account is dropped.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>If the SQL Server was managed by someone who's no longer with the
			company, all their jobs will suddenly stop working when SQL Server can't
			verify the job owner's account. (This can also pop up if there's an Active
			Directory problem when the job needs to run.) The fix is to have the
			built-in SA account own all the jobs.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>If the SQL Server was managed by someone who's no longer with the
			company, all their jobs will suddenly stop working when SQL Server can't
			verify the job owner's account. (This can also pop up if there's an Active
			Directory problem when the job needs to run.) The fix is to have the
			built-in SA account own all the jobs.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>If the SQL Server was managed by someone who's no longer with the
			company, all their jobs will suddenly stop working when SQL Server can't
			verify the job owner's account. (This can also pop up if there's an Active
			Directory problem when the job needs to run.) The fix is to have the
			built-in SA account own all the jobs.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>This part of report lists jobs owned by accounts other than SA. If you've
			changed the name of the SA account, the jobs will show up here, but don't
			pat yourself on the back just yet. Changing the name of the SA account can
			break some SQL Server service packs, so make sure you understand the risks
			there.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>This part of report lists jobs owned by accounts other than SA. If you've
			changed the name of the SA account, the jobs will show up here, but don't
			pat yourself on the back just yet. Changing the name of the SA account can
			break some SQL Server service packs, so make sure you understand the risks
			there.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>This part of report lists jobs owned by accounts other than SA. If you've
			changed the name of the SA account, the jobs will show up here, but don't
			pat yourself on the back just yet. Changing the name of the SA account can
			break some SQL Server service packs, so make sure you understand the risks
			there.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> If the jobs have been failing because
			they're owned by a user whose account has since been disabled, you probably
			don't want to fix this problem right away. The jobs may have been failing
			for months, and when you enable them again, you may run into serious side
			effects. For example, we've seen cases where developers didn't know why the
			job was failing, and they built a separate process to do the same work. When
			the jobs were enabled again, the business processes broke.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> If the jobs have been failing because
			they're owned by a user whose account has since been disabled, you probably
			don't want to fix this problem right away. The jobs may have been failing
			for months, and when you enable them again, you may run into serious side
			effects. For example, we've seen cases where developers didn't know why the
			job was failing, and they built a separate process to do the same work. When
			the jobs were enabled again, the business processes broke.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> If the jobs have been failing because
			they're owned by a user whose account has since been disabled, you probably
			don't want to fix this problem right away. The jobs may have been failing
			for months, and when you enable them again, you may run into serious side
			effects. For example, we've seen cases where developers didn't know why the
			job was failing, and they built a separate process to do the same work. When
			the jobs were enabled again, the business processes broke.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>You also want to understand the contents of any job before you enable it
			again. However, if the job's still running on a regular basis, then it makes
			sense to change that job owner to SA to make sure it continues to run
			if/when the job owner's account is disabled.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>You also want to understand the contents of any job before you enable it
			again. However, if the job's still running on a regular basis, then it makes
			sense to change that job owner to SA to make sure it continues to run
			if/when the job owner's account is disabled.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>You also want to understand the contents of any job before you enable it
			again. However, if the job's still running on a regular basis, then it makes
			sense to change that job owner to SA to make sure it continues to run
			if/when the job owner's account is disabled.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>When you're ready to fix the problem, you can simply right-click on each
			job in SSMS and click Properties. Change the owner to SA and click OK.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>When you're ready to fix the problem, you can simply right-click on each
			job in SSMS and click Properties. Change the owner to SA and click OK.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>When you're ready to fix the problem, you can simply right-click on each
			job in SSMS and click Properties. Change the owner to SA and click OK.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Простой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Невысокий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.brentozar.com/blitz/jobs-owned-by-user-accounts/" target="_blank">Blitz Result: Jobs Owned by User Accounts</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
