<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="JobsExecutionHistory.Graph" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Instance Jobs Execution (graph)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>История выполнения плановых заданий (график)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Instance Jobs Execution (graph)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="jInstanceJobsExecution.Graph" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:key name="JobName" match="Row" use="JobName" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date,'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name='GetJobsExecutionHistory' and @hierarchy='']/RecordSet[@id='1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>InstanceJobsExecution.Graph</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>InstanceJobsExecution.Graph</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>InstanceJobsExecution.Graph</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready(function() {
								var charts  = [];
								var color_1 = 'rgb(224,  64,  10)';
								var color_2 = 'rgb(  5, 100, 146)';
								var color_3 = 'rgb(252, 180,  65)';
								var color_4 = 'rgb( 65, 140, 240)';
								var color_5 = 'rgb(107, 142,  35)';

								<xsl:for-each select="//Row[generate-id(.) = generate-id (key('JobName', JobName))]">
									<xsl:sort select="JobName"/>

									<xsl:variable name="var_name">
										<xsl:value-of select="translate(concat('chart_', JobName), ' :()-.[]{}\/,=', '______________')"/>
									</xsl:variable>

									var <xsl:value-of select="$var_name"/>=[];

									<xsl:for-each select="key('JobName', JobName)">
										<xsl:if test="JobDurationInSeconds != ''">
											<xsl:value-of select="$var_name"/>.push( [
												'<xsl:call-template name="formatDate">
													<xsl:with-param name="date" select="JobRunDateTime"/>
												</xsl:call-template>',

												<xsl:value-of select="JobDurationInSeconds"/>] );
										</xsl:if>
									</xsl:for-each>

									charts.push(<xsl:value-of select="$var_name"/>);
								</xsl:for-each>

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot( 'chart', charts, {
									stackSeries: false,

									/**
									 * "title" options
									 */
									title: {
										show: false,
										text: null
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										shadow:        false,
										drawGridlines: true
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  null,
										bottom: null,
										left:   null
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          true,     // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.1,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     true,     // whether or not to show the tick labels
											showTickMarks: true,     // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: true,  //
												showMark:     true,  //
												showLabel:    true,  //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          true,     // whether or not to renderer the axis.  Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     true,     // whether or not to show the tick labels
											showTickMarks: true,     // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: true,         //
												showMark:     true,         //
												showLabel:    true,         //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer:     $.jqplot.EnhancedLegendRenderer,

										show:         true,          // whether to show legend
										location:     'e',           // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'outsideGrid', //
										xoffset:      12,            // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,            // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,          //

										rendererOptions: {
											numberRows:         0,     // maximum number of rows in the legend
											numberColumns:      0,     // maximum number of columns in the legend
											seriesToggle:       true,  // false to not enable series on/off toggling on the legend
											seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         true,           // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         4,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not
											shadowAngle:  45,             // angle of the shadow. Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: true,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             false,
											showDataLabels:     true
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       true,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // CSS spec for the cursor type to change the cursor to when over plot.
										show:                    true,        // whether to show cursor
										showTooltip:             true,        // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: true,        // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       true,        // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									},

									/**
									 * Series options are specified as an array of objects, one object
									 * for each series.
									 */
									series: [
										<xsl:for-each select="//Row[generate-id(.) = generate-id (key('JobName', JobName))]">

										<xsl:sort select="JobName"/> {
											label: '<xsl:value-of select="JobName"/>',

											color: color_<xsl:choose>
												<xsl:when test="position() &lt;= 5">
													<xsl:value-of select="position()"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="5 - position() mod 5"/>
												</xsl:otherwise>
											</xsl:choose>,

											highlighter: {
												formatString: '<xsl:value-of select="JobName"/>: %s, %s'
											}
										}

										<xsl:if test="position() != last()">,</xsl:if>

										</xsl:for-each>
									]
								} );

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height($('#container').height() * 0.96);

									plot.replot( {
										resetAxes: true
									} );
								} );
							});
						</script>
					</body>
				</html>
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="JobsExecutionHistoryGantt.Graph" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Jobs Execution History Gantt</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Диаграмма Ганта выполнения плановых заданий (график)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Jobs Execution History Gantt</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="jJobsExecutionHistory.Gantt.Graph" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:strip-space elements="*"/>

			<!-- Constants -->
			<xsl:variable name="colorMax">20</xsl:variable>

			<xsl:key name="JobName" match="RecordSet/Row" use="JobName" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template match="MSSQLResults|MSSQLResult">
				<xsl:apply-templates select="node()"/>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date,'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name='GetJobsExecutionHistory' and @hierarchy='']/RecordSet[@id='1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

				<xsl:variable name="GanttName" select="concat('Gantt', RecordSet/@id)" />

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>JobsExecutionHistory.Gantt.Graph</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>JobsExecutionHistory.Gantt.Graph</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>JobsExecutionHistory.Gantt.Graph</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}

							<xsl:apply-templates select="//Row" mode="ganttCss" />

							#chart td.jqplot-table-legend-hide {
								display: none;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready(function() {
								var color_1  = 'rgb(224,  64,  10)';
								var color_2  = 'rgb(  5, 100, 146)';
								var color_3  = 'rgb(252, 180,  65)';
								var color_4  = 'rgb( 65, 140, 240)';
								var color_5  = 'rgb(107, 142,  35)';
								var color_6  = 'rgb(105, 120,  70)';
								var color_7  = 'rgb( 85, 130,  80)';
								var color_8  = 'rgb( 75, 140,  90)';
								var color_9  = 'rgb( 15, 150, 100)';
								var color_10 = 'rgb(  5, 160, 110)';
								var color_11 = 'rgb(224,  64,  10)';
								var color_12 = 'rgb(  5, 100, 146)';
								var color_13 = 'rgb(252, 180,  65)';
								var color_14 = 'rgb( 65, 140, 240)';
								var color_15 = 'rgb(107, 142,  35)';
								var color_16 = 'rgb(105, 120,  70)';
								var color_17 = 'rgb( 85, 130,  80)';
								var color_18 = 'rgb( 75, 140,  90)';
								var color_19 = 'rgb( 15, 150, 100)';
								var color_20 = 'rgb(  5, 160, 110)';

								var ganttData1 = [
									<xsl:apply-templates select="//ancestor::RecordSet/Row" mode="gantt" />
								];

								var seriesConf = [
									<xsl:apply-templates select="//ancestor::RecordSet/Row" mode="ganttConf" />
								];

								plot = $.jqplot('chart', ganttData1, {
									series:       seriesConf,
									seriesColors: ["#888888"],
									stackSeries:  false,

									/**
									 * "title" options
									 */
									title: {
										show: false,
										text: null
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										drawGridlines: true,
										shadow:        false,
										shadowAngle:   -90
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    null,
										right:  null,
										bottom: null,
										left:   null
									},

									/**
									 * "axes" options
									 */
									axes: {
										xaxis: {
											renderer:     $.jqplot.DateAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          true,     // whether or not to renderer the axis. Determined automatically.
											min:           null,     // minimum numerical value of the axis. Determined automatically.
											max:           null,     // maximum numverical value of the axis. Determined automatically.
											pad:           1.1,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     true,     // whether or not to show the tick labels
											showTickMarks: true,     // whether or not to show the tick marks

											tickOptions: {
												angle:        0,     //
												formatString: '',    // format string to use with the axis tick formatter
												showGridline: true,  //
												showMark:     true,  //
												showLabel:    true,  //
												shadow:       false  //
											}
										},

										yaxis: {
											renderer:     $.jqplot.LinearAxisRenderer,
											tickRenderer: $.jqplot.CanvasAxisTickRenderer,

											show:          true,     // whether or not to renderer the axis.  Determined automatically.
											min: <xsl:value-of select="1 + count(
												//ancestor::RecordSet/Row
												[generate-id(.) = generate-id(key('JobName', JobName)[1])]
											)" />,
											max:           0,        // maximum numverical value of the axis.
											pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
											autoscale:     true,     //
											labelPosition: 'middle', //
											showTicks:     true,     // whether or not to show the tick labels
											showTickMarks: true,     // whether or not to show the tick marks

											tickOptions: {
												angle:        -90,          //
												formatString: "%'d",        // format string to use with the axis tick formatter
												showGridline: true,         //
												showMark:     true,         //
												showLabel:    true,         //
												shadow:       false,        //
												formatter:    tickFormatter //
											}
										}
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer:     $.jqplot.EnhancedLegendRenderer,

										show:         true,          // whether to show legend
										location:     'e',           // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'outsideGrid', //
										xoffset:      12,            // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,            // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,          //

										rendererOptions: {
											numberRows:    0,
											numberColumns: 1,
											seriesToggle:  true
										},
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults: {
										renderer:       $.jqplot.LinearRenderer,
										markerRenderer: $.jqplot.MarkerRenderer,

										showLine:  true,
										lineWidth: 0.75,
										fill:      false,
										shadow:    false,

										markerOptions: {
											show:         true,           // whether to show data point markers
											style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
											lineWidth:    2,              // width of the stroke drawing the marker.
											size:         4,              // size (diameter, edge length, etc.) of the marker.
											color:        '#666666',      // color of marker, set to color of line by default.
											shadow:       false,          // whether to draw shadow on marker or not
											shadowAngle:  45,             // angle of the shadow. Clockwise from x axis.
											shadowOffset: 1,              // offset from the line of the shadow,
											shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
											shadowAlpha:  0.07            // Opacity of the shadow
										},

										rendererOptions: {
											highlightMouseOver: true,
											highlightMouseDown: false,
											highlightColor:     'rgb(0, 0, 0)',
											smooth:             false,
											showDataLabels:     true
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       true,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // CSS spec for the cursor type to change the cursor to when over plot.
										show:                    true,        // whether to show cursor
										showTooltip:             true,        // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: true,        // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       true,        // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									}
								});

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height($('#container').height() * 0.96);

									plot.replot ( {
										resetAxes: false
									} );
								});
							});
						</script>
					</body>
				</html>
			</xsl:template>

			<xsl:template match="//Row" mode="gantt">
				[
					[
						'<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="JobRunDateTime"/>
						</xsl:call-template>',

						<xsl:call-template name="jobOrder" />
					],

					[
						'<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="JobFinishDateTime"/>
						</xsl:call-template>',

						<xsl:call-template name="jobOrder" />
					]
				]

				<xsl:if test="position() != last()">,</xsl:if>
			</xsl:template>

			<xsl:template match="//Row" mode="ganttConf">
				{
					label: '<xsl:value-of select="JobName" />',

					color: color_<xsl:call-template name="jobOrderModule" />,

					highlighter: {
						formatString: '<xsl:value-of select="JobName"/>: %s, %s'
					},

					lineWidth: 6
				}

				<xsl:if test="position() != last()">,</xsl:if>
			</xsl:template>

			<xsl:template match="//Row" mode="ganttLegend">
				<xsl:text>'</xsl:text>
					<xsl:value-of select="JobName" />
				<xsl:text>'</xsl:text>

				<xsl:if test="position() != last()">,</xsl:if>
			</xsl:template>

			<xsl:template match="//Row" mode="ganttCss">
				<xsl:if test="JobName[text() = ../preceding-sibling::Row/JobName/text()]">
					#chart td.jqplot-table-legend:nth-of-type(<xsl:value-of select="position() * 2 - 1" />),
					#chart td.jqplot-table-legend:nth-of-type(<xsl:value-of select="position() * 2" />),
				</xsl:if>
			</xsl:template>

			<xsl:template name="jobOrder">
				<xsl:variable name="firstRowWithJobName" select="ancestor::RecordSet
					/Row[JobName/text() = current()/JobName/text()][1]"/>
				<xsl:value-of select="1 +
					count($firstRowWithJobName/preceding-sibling::Row[not(JobName = preceding-sibling::Row/JobName)])
				" />
			</xsl:template>

			<xsl:template name="jobOrderModule">
				<xsl:variable name="jobOrder">
					<xsl:call-template name="jobOrder" />
				</xsl:variable>
				<xsl:value-of select="1 + (($jobOrder - 1) mod $colorMax)" />
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
