<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="AuditInstanceConfiguration.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>MS SQL server configuration</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Данные о конфигурации MS SQL сервера</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>MS SQL server configuration</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="AuditInstanceConfiguration.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>MS SQL server configuration</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Данные о конфигурации MS SQL сервера</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>MS SQL server configuration</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetInstanceConfiguration' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Name</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Название</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Name</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Parameter</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Параметр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Parameter</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Description</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Описание параметра</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Description</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Current value</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Текущее значение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Current value</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Recommended value</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Рекомендуемое значение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Recommended value</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetInstanceConfiguration' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<xsl:if test="ConfigurationValueRecommended != '' and ConfigurationValueInUse != ConfigurationValueRecommended">
						<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
					</xsl:if>
					<xsl:if test="ConfigurationValueNotRecommended != '' and ConfigurationValueInUse = ConfigurationValueNotRecommended">
						<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
					</xsl:if>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="ConfigurationOptionName != ''">
								<xsl:value-of select="ConfigurationOptionName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="ConfigurationOptionDescription != ''">
								<xsl:value-of select="ConfigurationOptionDescription"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="ConfigurationValueInUse != ''">
								<xsl:value-of select="ConfigurationValueInUse"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="ConfigurationValueRecommended != ''">
								<xsl:value-of select="ConfigurationValueRecommended"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>

<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="AuditInstanceConfiguration.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Description</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Описание</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Description</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="AuditInstanceConfiguration.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Description</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Описание</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Description</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Informational checks</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Информационные проверки</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Informational checks</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>The checks on this page are for informational purposes only. The check
		will just tell you if a feature is enabled on not, once you click on the
		item it will bring you to the sub-section of this page where you will get a
		little more info why we checks for this.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Проверки на данной странице выполняются лишь в информационных целях.
			Проверки покажут, включена ли данная опция.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>The checks on this page are for informational purposes only. The check
		will just tell you if a feature is enabled on not, once you click on the
		item it will bring you to the sub-section of this page where you will get a
		little more info why we checks for this.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>"CLR enabled":</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>"CLR включен":</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>"CLR enabled":</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Some shops won't allow the CLR to run
		because it brings some securty implications with it. By default the CLR is
		disabled.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Некоторые приложения не позволят
			запустить CLR, поскольку он может нести угрозу безопасности. По умолчанию
			CLR отключен. У Вас должна быть веская причина чтобы разрешить выполение
			внешних утилит CLR.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Some shops won't allow the CLR to run
		because it brings some securty implications with it. By default the CLR is
		disabled.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>"Backup compression default":</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>"Резервное сжатие данных по умолчанию":</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>"Backup compression default":</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>

			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> This check will tell you
		if by default backup compression is turned on, if this is the case then the
		backup will be compressed if you issue a regular BACKUP DATABASE statement.
		For a given backup, you can use either WITH NO_COMPRESSION or WITH
		COMPRESSION in a BACKUP statement so you can still create uncompressed
		backups. Backup compression was introduced in SQL Server 2008 Enterprise.
		Beginning in SQL Server 2008 R2, backup compression is supported by SQL
		Server 2008 R2 Standard and all higher editions. Every edition of SQL Server
		2008 and later can restore a compressed backup.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Эта проверка
			покажет, включено ли резервное сжатие данных по умолчанию. Если оно
			включено, резервные данные будут сохранятся, если Вы зададите команду
			регулярного резервного копирования базы данных. Для заданного резервного
			копирования, Вы можете указать команды WITH NO_COMPRESSION (без сжатия) и
			WITH COMPRESSION (с применением компрессии) в поле BACKUP (резервное копирование). Так Вы
			можете создавать резервные копии без сжатия данных. Впервые резервное сжатие
			данных было представлено в SQL Server 2008 Enterprise. Начиная с SQL Server
			2008 R2, резервное сжатие данных поддерживается SQL Server 2008 R2 Standard
			и всеми более поздними версиями. Каждая версия SQL Server 2008 и последующие
			может восстановить сжатые резервные данные.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> This check will tell you
		if by default backup compression is turned on, if this is the case then the
		backup will be compressed if you issue a regular BACKUP DATABASE statement.
		For a given backup, you can use either WITH NO_COMPRESSION or WITH
		COMPRESSION in a BACKUP statement so you can still create uncompressed
		backups. Backup compression was introduced in SQL Server 2008 Enterprise.
		Beginning in SQL Server 2008 R2, backup compression is supported by SQL
		Server 2008 R2 Standard and all higher editions. Every edition of SQL Server
		2008 and later can restore a compressed backup.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>"Database Mail XPs":</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>"Database Mail XPs":</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>"Database Mail XPs":</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>

			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> This check tells you if database
		mail is enabled, database mail replaces the old SQL Mail procedures. Instead
		of xp_sendmail, you will now use sp_send_dbmail, this new proc now can also use smtp
		mailservers. For more information about database mail visit the Books On Line.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Эта провека покажет, используется
			ли отправка по электронной почте через использование нового протокола или
			используется устаревший метод. Всесто использования функции xp_sendmail рекомендуется использование sp_send_dbmail. Подробности в БОЛ.
			</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> This check tells you if database
		mail is enabled, database mail replaces the old SQL Mail procedures. Instead
		of xp_sendmail, you will now use sp_send_dbmail, this new proc now can also use smtp
		mailservers. For more information about database mail visit the Books On Line.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Review the configuration options and
		if you don't have the strong understanding why the oprion set to the
		specific, not recomended value, think to change that option.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Проверьте параметры настройки и, если
			некоторых из них имеют отличные от рекомендуемых значения, и Вы не совсем
			понимаете, почему, установите рекомендованные значения.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Review the configuration options and
		if you don't have the strong understanding why the oprion set to the
		specific, not recomended value, think to change that option.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://wiki.lessthandot.com/index.php/SQLCop_informational_checks" target="_blank">Server Informational Checks</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
