<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseUsers.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Database Users</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Пользователи баз данных</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Database Users</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseUsers.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Database Users</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Пользователи баз данных</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Database Users</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Database Users</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Пользователи баз данных</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Database Users</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Attaching and restoring databases from one server instance to another are common
			tasks executed by a DBA. After attaching or restoring of a database, previously created
			and configured logins in that database do not provide access. The most common symptoms
			of this problem are that the application may face login failed errors or you may get a
			message like the user already exists in the current database when you try to add the
			login to the database. This is a common scenario when performing an attach or a restore,
			so how do you resolve this?</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Добавление и восстановление баз данных одного экземпляра сервера на другой является задачей, часто выполняемой администратором
			базы данных. После присоединения или восстановления базы данных, вход с ранее созданными и настроенныит логинами данной базы данных невозможен.
			Наиболее распространенный симптом этой проблемы – приложение выдает ошибки входа, либо при попытке добавить
			имя пользователя в базу данных, сообщает, что данный пользователь уже существует в текущей базе данных. Это часто происходит
			при добавлении или восстановлении базы данных, как решить эту проблему? </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Attaching and restoring databases from one server instance to another are common
			tasks executed by a DBA. After attaching or restoring of a database, previously created
			and configured logins in that database do not provide access. The most common symptoms
			of this problem are that the application may face login failed errors or you may get a
			message like the user already exists in the current database when you try to add the
			login to the database. This is a common scenario when performing an attach or a restore,
			so how do you resolve this?</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> The stored procedure sp_change_users_login can be
			used.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> The stored procedure sp_change_users_login can be
			used.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> The stored procedure sp_change_users_login can be
			used.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> High</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Высокий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> High</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> High</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Высокий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> High</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<a href="http://www.mssqltips.com/sqlservertip/1590/understanding-and-dealing-with-orphaned-users-in-a-sql-server-database/"
			target="_blank">Orphaned users.</a>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
