<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseUsersMembersOfGroupsWithElevatedPermissions.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>User with Elevated Database Permissions</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Привелигированные пользователи баз данных</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>User with Elevated Database Permissions</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseUsersMembersOfGroupsWithElevatedPermissions.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>User with Elevated Database Permissions</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Привелигированные пользователи баз данных</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>User with Elevated Database Permissions</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>User with Elevated Database Permissions</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Привелигированные пользователи баз данных</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>User with Elevated Database Permissions</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Inside a database, users can be set up in the roles
				db_owner, db_accessadmin, db_securityadmin, and db_ddladmin. The
				Books Online page on database-level roles explains the permissions for these
				roles, all of which involve more than just reading and writing data. Some of
				these can get you fired if somebody even just clicks around too much in SSMS
				– it's way too easy to rename an object accidentally.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В каждой базе данных есть особые, привелигированные роли баз данных,
			такие как </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Inside a database, users can be set up in the roles
				db_owner, db_accessadmin, db_securityadmin, and db_ddladmin. The
				Books Online page on database-level roles explains the permissions for these
				roles, all of which involve more than just reading and writing data. Some of
				these can get you fired if somebody even just clicks around too much in SSMS
				– it's way too easy to rename an object accidentally.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Talk to the users listed and find out
				if they really need to change database objects or security. If not, remove
				them from the roles. In SQL Server Management Studio, you can go into
				Security, Logins, and right-click on a login. Go to User Mapping and you can
				see their roles for each database. Unchecking them from the roles for each
				database will take them out immediately – but just make sure they've got
				SOME kind of access so they can still do their queries. We're a big fan of
				db_datareader and db_datawriter – those
				two roles give people rights to read and write to any table in the database.
				(It's not nearly as good as doing fine-grained permissions for just the
				tables they need, but it's way better than making them
				db_owner).</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Внимательно пересмотрите схему
			безопасности баз данных, и, возможно, используйте специально созданных роли
			вместо привелигированного доступа.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Talk to the users listed and find out
				if they really need to change database objects or security. If not, remove
				them from the roles. In SQL Server Management Studio, you can go into
				Security, Logins, and right-click on a login. Go to User Mapping and you can
				see their roles for each database. Unchecking them from the roles for each
				database will take them out immediately – but just make sure they've got
				SOME kind of access so they can still do their queries. We're a big fan of
				db_datareader and db_datawriter – those
				two roles give people rights to read and write to any table in the database.
				(It's not nearly as good as doing fine-grained permissions for just the
				tables they need, but it's way better than making them
				db_owner).</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> High</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Высокий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> High</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> High</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Высокий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> High</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://msdn.microsoft.com/en-us/library/ms189121.aspx" target="_blank">Database-Level Roles</a></p>

			<p><a href="http://BrentOzar.com/go/elevated" target="_blank">User with Elevated Database Permissions</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
