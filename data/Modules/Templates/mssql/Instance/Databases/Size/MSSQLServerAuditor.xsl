<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabasesSize.ColumnDiagramm" columns="100" rows="80;20" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Bar</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Столбчатая диаграмма</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Bar</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="jDatabasesSize.ColumnDiagramm" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name != 'GetDatabaseSize']">
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name='GetDatabaseSize' and @hierarchy='']/RecordSet[@id='1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Bar</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>DatabasesSize.ColumnDiagramm.ru</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Bar</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.barRenderer.js"></script>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.categoryAxisRenderer.js"></script>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.pieRenderer.js"></script>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.pointLabels.js"></script>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							var chartData = [],

							labels = [
								'DatabaseFileSizeUsedMB',
								'DatabaseFileSizeFreeMB'
							];

							<xsl:for-each select="//MSSQLResult[@name='GetDatabaseSize']/RecordSet/Row">
								chartData.push({
									name: '<xsl:value-of select="translate(DatabaseFileName, ' :()-.[]{}\/,=', '______________')"/>',

									values: [
										<xsl:value-of select="DatabaseFileSizeUsedMB"/>,
										<xsl:value-of select="DatabaseFileSizeFreeMB"/>
									]
								});
							</xsl:for-each>

							<xsl:text disable-output-escaping="yes">
								$(function() {
									stackedColumnBar.draw();

									$(window).resize($.proxy(stackedColumnBar.resize, stackedColumnBar));
								});

								var stackedColumnBar = (function ($, data) {
								/**
								 *
								 * @param {String} name
								 * @param {String} container
								 * @param {Object} data
								 * @returns {stacked-columns_L1.StackedColumnBar}
								 */
								var StackedColumnBar = function (name, container, data, labels) {
									this.chartName  = name;
									this.data       = data;
									this.labels     = labels;
									this.$target    = $('#' + this.chartName);
									this.$container = $('#' + container);
									this.plot       = null;
								};

								$.extend(StackedColumnBar.prototype, {
									/**
									 *
									 * @returns {undefined}
									 */
									draw: function () {
										var charts = this.getCharts();

										/**
										 * jqPlot settings
										 *
										 * read more
										 * @link http://www.jqplot.com/examples/barTest.php
										 */
										this.plot = $.jqplot(this.chartName, charts, {
											stackSeries: true,

											/**
											 * "title" options
											 */
											title: {
												show: false,
												text: null
											},

											/**
											 * "grid" options
											 */
											grid: {
												drawBorder:    false,
												shadow:        false,
												drawGridlines: true
											},

											/**
											 * "gridPadding" options
											 */
											gridPadding: {
												top:    0,
												right:  0,
												bottom: null,
												left:   null
											},

											/**
											 * "axes" options
											 */
											axes: {
												xaxis: {
													renderer:     $.jqplot.CategoryAxisRenderer,
													tickRenderer: $.jqplot.AxisTickRenderer,

													show:          true,     // whether or not to renderer the axis. Determined automatically.
													min:           null,     // minimum numerical value of the axis. Determined automatically.
													max:           null,     // maximum numverical value of the axis. Determined automatically.
													pad:           1.1,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
													autoscale:     true,     //
													labelPosition: 'middle', //
													showTicks:     true,     // whether or not to show the tick labels
													showTickMarks: true,     // whether or not to show the tick marks

													tickOptions: {
														angle:        0,     //
														formatString: '',    // format string to use with the axis tick formatter
														showGridline: true,  //
														showMark:     true,  //
														showLabel:    true,  //
														shadow:       false  //
													},

													ticks: this.getTicks()
												},

												yaxis: {
													renderer:     $.jqplot.LinearAxisRenderer,
													tickRenderer: $.jqplot.CanvasAxisTickRenderer,

													show:          true,     // whether or not to renderer the axis.  Determined automatically.
													min:           null,     // minimum numerical value of the axis. Determined automatically.
													max:           null,     // maximum numverical value of the axis. Determined automatically.
													pad:           1.0,      // a factor multiplied by the data range on the axis to give the axis range so that data points don't fall on the edges of the axis.
													padMin:        0,        // don't pad out the bottom of the data range. By default, axes scaled as if data extended 10% above and below the actual range to prevent data points right on grid boundaries. Don't want to do that here.
													autoscale:     true,     //
													labelPosition: 'middle', //
													showTicks:     true,     // whether or not to show the tick labels
													showTickMarks: true,     // whether or not to show the tick marks

													tickOptions: {
														angle:        -90,          //
														formatString: "%'d",        // format string to use with the axis tick formatter
														showGridline: true,         //
														showMark:     true,         //
														showLabel:    true,         //
														shadow:       false,        //
														formatter:    tickFormatter //
													}
												}
											},

											/**
											 * "legend" options
											 */
											legend: {
												renderer:     $.jqplot.EnhancedLegendRenderer,

												show:         true,         // whether to show legend
												location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
												placement:    'insideGrid', //
												xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
												yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
												showSwatches: true,         //

												rendererOptions: {
													numberRows:         1,     // maximum number of rows in the legend
													numberColumns:      2,     // maximum number of columns in the legend
													seriesToggle:       true,  // false to not enable series on/off toggling on the legend
													seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
												}
											},

											/**
											 * set "default options" for all graph series
											 */
											seriesDefaults: {
												renderer:       $.jqplot.BarRenderer,
												markerRenderer: $.jqplot.MarkerRenderer,

												showLine:  true,
												lineWidth: 0.75,
												fill:      false,
												shadow:    false,

												markerOptions: {
													show:         true,           // whether to show data point markers
													style:        'filledCircle', // circle, diamond, square, filledCircle, filledDiamond or filledSquare.
													lineWidth:    2,              // width of the stroke drawing the marker.
													size:         2,              // size (diameter, edge length, etc.) of the marker.
													color:        '#666666',      // color of marker, set to color of line by default.
													shadow:       false,          // whether to draw shadow on marker or not
													shadowAngle:  45,             // angle of the shadow.  Clockwise from x axis.
													shadowOffset: 1,              // offset from the line of the shadow,
													shadowDepth:  3,              // Number of strokes to make when drawing shadow. Each stroke offset by shadowOffset from the last.
													shadowAlpha:  0.07            // Opacity of the shadow
												},

												rendererOptions: {
													highlightMouseOver: false,
													highlightMouseDown: false,
													highlightColor:     'rgb(0, 0, 0)',
													smooth:             false,
													showDataLabels:     true,
													barDirection:       'vertical',
													fillToZero:         false
												},

												pointLabels: {
													show: true
												}
											},

											/**
											 * The Highlighter plugin will highlight data points near the mouse
											 * and display an optional tooltip with the data point value.
											 * By default, the tooltip values will be formatted with the
											 * same formatter as used to display the axes tick values.
											 * The text format can be customized with an optional
											 * sprintf style format string.
											 */
											highlighter: {
												show:       true,
												sizeAdjust: 7.5
											},

											/**
											 * "Cursor"
											 * Options are passed to the cursor plugin through the "cursor" object at the top
											 * level of the options object.
											 */
											cursor: {
												style:                   'crosshair', // CSS spec for the cursor type to change the cursor to when over plot.
												show:                    false,       // whether to show cursor
												showTooltip:             false,       // show a tooltip showing cursor position.
												followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
												tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
												tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
												showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
												showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
												tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
												useAxesFormatters:       true,        // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
												tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
												zoom:                    false
											},

											/**
											 * Series options are specified as an array of objects, one object
											 * for each series.
											 */
											series: this.getLabels()
										});

										this.resize();
									},

									getLabels: function () {
										var series = [];

										for (var i = 0; i &lt; this.labels.length; i++) {
											series.push({
												label: this.labels[i]
											});
										}

										return series;
									},

									getTicks: function () {
										var ticks = [];

										for (var i = 0; i &lt; this.data.length; i++) {
											ticks.push(this.data[i].name);
										}

										return ticks;
									},

									/**
									 * Resize plot
									 *
									 * @returns {undefined}
									 */
									resize: function () {
										this.$target.height(this.$container.height() * 0.98);

										if (this.plot) {
											this.plot.replot({
												resetAxes: true
											});
										}
									},

									/**
									 * Get charts data
									 * Extract chart data values
									 *
									 * @returns {Array}
									 */
									getCharts: function () {
										var charts = [];

										for (var i = 0; i &lt; this.data.length; i++) {
											for (var k = 0; k &lt; data[i].values.length; k++) {
												charts[k] = charts[k] || [];
												charts[k].push(data[i].values[k]);
											}
										}

										return charts;
									}
								});

								return new StackedColumnBar('chart', 'container', data, labels);

								})($, chartData);
							</xsl:text>
						</script>
					</body>
				</html>
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="DatabaseFilesSize.HTML" column="1" row="2" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Database Files (MB)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Файлы для базы данных (МБайт)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Database Files (MB)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetDatabaseSize' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('ru')">
								<xsl:text>Файл</xsl:text>
							</xsl:when>
							<xsl:when test="lang('en')">
								<xsl:text>File</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>File</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Physical FileName</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Путь</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Physical FileName</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Type</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Type</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Reserved</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зарезервировано</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Reserved</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Used</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Используется</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Used</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetDatabaseSize' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseFileName != ''">
								<xsl:value-of select="DatabaseFileName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseFilePhysicalName != ''">
								<xsl:value-of select="DatabaseFilePhysicalName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseFileStatusDesc != ''">
								<xsl:value-of select="DatabaseFileStatusDesc"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseFileSizeMB != ''">
								<xsl:value-of select="format-number(DatabaseFileSizeMB, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseFileSizeUsedMB != ''">
								<xsl:value-of select="format-number(DatabaseFileSizeUsedMB, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>

<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabasesSize.PieDiagramm" columns="100" rows="80;20" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Pie</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Круговая диаграмма</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Pie</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="jDatabasesSize.PieDiagramm" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
			<xsl:output method="html" encoding="utf-8" indent="yes" />

			<xsl:template match="@*|node()">
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:template>

			<xsl:template name="formatDate">
				<xsl:param name="date"/>
				<xsl:value-of select="concat(substring-before($date, 'T'), ' ', substring-after($date, 'T'))"/>
			</xsl:template>

			<xsl:template match="/MSSQLResults/MSSQLResult[@name='GetDatabaseSize' and @hierarchy='']/RecordSet[@id='1']">
				<xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>

				<xsl:variable name="PieName" select="concat('Pie', RecordSet/@id)" />

					<html>
					<head>
						<title>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Pie</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>DatabasesSize.PieDiagramm</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Pie</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						</title>
						<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
						<meta content="utf-8"                   http-equiv="encoding"/>

						<script type="text/javascript" src="$JS_FOLDER$/excanvas.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jquery-1.12.4.min.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/tickFormatter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/jquery.jqplot.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.canvasTextRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.cursor.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.dateAxisRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.enhancedLegendRenderer.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.highlighter.js"/>
						<script type="text/javascript" src="$JS_FOLDER$/jqplot/plugins/jqplot.donutRenderer.js"/>

						<link rel="stylesheet" type="text/css" href="$JS_FOLDER$/jqplot/jquery.jqplot.css" />
					</head>
					<body>
						<style type="text/css">
							html, body {
								height:   100%;
								overflow: hidden;
							}

							#container {
								width:  100%;
								height: 100%;
							}
						</style>

						<div id="container">
							<div id="chart" style="height:100%; width:100%"/>
						</div>

						<script type="text/javascript">
							$(document).ready(function() {
								var pieData1 = [<xsl:apply-templates select="//Row" mode="pie" />
								];

								$('#chart').height($('#container').height() * 0.96);

								var plot = $.jqplot('chart', [pieData1], {
									stackSeries: false,

									/**
									 * "title" options
									 */
									title: {
										show: false,
										text: null
									},

									/**
									 * "grid" options
									 */
									grid: {
										drawBorder:    false,
										drawGridlines: true,
										background:    '#ffffff',
										shadow:        false
									},

									/**
									 * "gridPadding" options
									 */
									gridPadding: {
										top:    0,
										right:  0,
										bottom: 0,
										left:   0
									},

									/**
									 * "axes" options
									 */
									axesDefaults: {
									},

									/**
									 * "legend" options
									 */
									legend: {
										renderer:     $.jqplot.EnhancedLegendRenderer,

										show:         true,         // whether to show legend
										location:     'n',          // compass direction, nw, n, ne, e, se, s, sw, w
										placement:    'insideGrid', //
										xoffset:      12,           // pixel offset of the legend box from the x (or x2) axis
										yoffset:      12,           // pixel offset of the legend box from the y (or y2) axis
										showSwatches: true,         //

										rendererOptions: {
											numberRows:         0,     // maximum number of rows in the legend
											numberColumns:      3,     // maximum number of columns in the legend
											seriesToggle:       true,  // false to not enable series on/off toggling on the legend
											seriesToggleReplot: false  // true to replot the chart after toggling series on/off.
										}
									},

									/**
									 * set "default options" for all graph series
									 */
									seriesDefaults:{
										renderer: $.jqplot.DonutRenderer,

										rendererOptions: {
											highlightMouseOver: true,           //
											highlightMouseDown: false,          //
											highlightColor:     'rgb(0, 0, 0)', //
											smooth:             false,          //
											showDataLabels:     true,           // true to show data labels on slices
											dataLabels:         'label'         // Either 'label', 'value', 'percent' or an array of labels to place on the pie slices
										}
									},

									/**
									 * The Highlighter plugin will highlight data points near the mouse
									 * and display an optional tooltip with the data point value.
									 * By default, the tooltip values will be formatted with the
									 * same formatter as used to display the axes tick values.
									 * The text format can be customized with an optional
									 * sprintf style format string.
									 */
									highlighter: {
										show:       true,
										sizeAdjust: 7.5
									},

									/**
									 * "Cursor"
									 * Options are passed to the cursor plugin through the "cursor" object at the top
									 * level of the options object.
									 */
									cursor: {
										style:                   'crosshair', // CSS spec for the cursor type to change the cursor to when over plot.
										show:                    false,       // whether to show cursor
										showTooltip:             false,       // show a tooltip showing cursor position.
										followMouse:             false,       // whether tooltip should follow the mouse or be stationary.
										tooltipLocation:         's',         // location of the tooltip either relative to the mouse (followMouse=true) or relative to the plot.  One of the compass directions, n, ne, e, se, etc.
										tooltipOffset:           6,           // pixel offset of the tooltip from the mouse or the axes.
										showTooltipGridPosition: false,       // show the grid pixel coordinates of the mouse in the tooltip.
										showTooltipUnitPosition: false,       // show the coordinates in data units of the mouse in the tooltip.
										tooltipFormatString:     '%.4P',      // sprintf style format string for tooltip values.
										useAxesFormatters:       true,        // whether to use the same formatter and formatStrings as used by the axes, or to use the formatString specified on the cursor with sprintf.
										tooltipAxesGroups:       [],          // show only specified axes groups in tooltip.  Would specify like: [['xaxis', 'yaxis'], ['xaxis', 'y2axis']].  By default, all axes combinations with for the series in the plot are shown.
										zoom:                    false
									}
								});

								plot.replot( {
									resetAxes: true
								} );

								$(window).resize(function() {
									$('#chart').height( $('#container').height() * 0.96 );

									plot.replot( {
										resetAxes: true
									} );
								} );
							});
						</script>
					</body>
				</html>
			</xsl:template>

			<xsl:template match="//Row" mode="pie">
				[
					'<xsl:value-of select="DatabaseFileName" />',
					<xsl:value-of select="DatabaseFileSizeMB" />
				]
				<xsl:if test="position() != last()">,</xsl:if>
			</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="DatabaseFilesSize.HTML" column="1" row="2" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Database Files (MB)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Файлы для базы данных (МБайт)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Database Files (MB)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetDatabaseSize' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('ru')">
								<xsl:text>Файл</xsl:text>
							</xsl:when>
							<xsl:when test="lang('en')">
								<xsl:text>File</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>File</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>PhysicalName</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Путь</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>PhysicalName</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Type</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Type</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Reserved</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зарезервированное</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Reserved</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Used</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Используется</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Used</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetDatabaseSize' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseFileName != ''">
								<xsl:value-of select="DatabaseFileName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseFilePhysicalName != ''">
								<xsl:value-of select="DatabaseFilePhysicalName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseFileStatusDesc != ''">
								<xsl:value-of select="DatabaseFileStatusDesc"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseFileSizeMB != ''">
								<xsl:value-of select="format-number(DatabaseFileSizeMB, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseFileSizeUsedMB != ''">
								<xsl:value-of select="format-number(DatabaseFileSizeUsedMB, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
