<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithPrefix.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables With a Prefix</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с tbl префиксами</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables With a Prefix</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithPrefix.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables With a Prefix</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с tbl префиксами</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables With a Prefix</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Don't prefix your table names with tbl</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Не начинайте имя таблицы с «tbl»</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Don't prefix your table names with tbl</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>This is a naming convention issue. Tables should not be prefaced with tbl because it
				does nothing to add to the clarity of the code (self-documenting). It actually has the
				opposite effect because it takes longer to read it and causes you to perform an
				interpretation of the three letter abbreviation.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Эта проблема связана с именами. Нельзя использовать приставку «tbl» в таблицах, потому что это не добавляет ясности коду
			(самодокументирует). В действительности такая приставка дает обратный эффект, так как на чтение кода требуется больше
			времени и пользователю приходится расшифровывать аббревиатуру.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>This is a naming convention issue. Tables should not be prefaced with tbl because it
				does nothing to add to the clarity of the code (self-documenting). It actually has the
				opposite effect because it takes longer to read it and causes you to perform an
				interpretation of the three letter abbreviation.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Rename the table to remove the prefix. This is
				not as simple as it seems because this table could be referenced from a number of
				places, including views, stored procedures, user defined functions, index creation
				scripts, in-line SQL (embedded within front end applications), etc...</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Переименуйте таблицу, убрав приставку. Это не так просто, как кажется, потому что
			таблица может быть связана с другими элементами, такими как виды, хранимые процедуры, заданные пользователем функции, скрипты
			для создания индекса, встроенный SQL (в интерфейсных приложениях) и т.д.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Rename the table to remove the prefix. This is
				not as simple as it seems because this table could be referenced from a number of
				places, including views, stored procedures, user defined functions, index creation
				scripts, in-line SQL (embedded within front end applications), etc...</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> moderate to high</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> От среднего до высокого</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> moderate to high</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Mild</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Mild</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/MSSQLServer/don-t-prefix-your-table-names-with-tbl" target="_blank">Don't prefix your table names with tbl</a>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/MSSQLServer/don-t-prefix-your-table-names-with-tbl" target="_blank">Не начинайте имя таблицы с "tbl"</a>
				</xsl:when>
				<xsl:otherwise>
					<a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/MSSQLServer/don-t-prefix-your-table-names-with-tbl" target="_blank">Don't prefix your table names with tbl</a>
				</xsl:otherwise>
			</xsl:choose>
			</p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
