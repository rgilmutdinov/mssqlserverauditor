<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="WideTables.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Wide table check</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Широкие таблицы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Wide table check</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="WideTables.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Wide table check</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Широкие таблицы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Wide table check</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Wide table check</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Что такое широкие таблицы?</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Wide table check</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>The wide table check will check if the sum of all the max values of the columns in
				your table exceeds 8,060 bytes.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Проверка широких таблиц покажет, превышает ли сумма максимальных значений в столбцах 8,060 байт.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>The wide table check will check if the sum of all the max values of the columns in
				your table exceeds 8,060 bytes.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>In SQL Server 2008, the restriction that a table can contain a maximum of 8,060 bytes
				per row is relaxed for tables that contain varchar, nvarchar, varbinary, sql_variant, or
				CLR user-defined type columns. The length of each one of these columns must still fall
				within the limit of 8,000 bytes; however, their combined widths can exceed the
				8,060-byte limit. Here is what Books On Line says you have to consider:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В SQL Server 2008 ограничение в 8,060 байт на табличный ряд "ослаблено" для таблиц с заданными
			пользователем колонками, имеющие данные следующих типов: varchar, nvarchar, varbinary, sql_variant или CLR.
			Длина каждой колонки не должна превышать 8000 байт, однако их общая ширина может быть больше 8060 байт.
			Вот на что советует обратить внимание Books On Line:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>In SQL Server 2008, the restriction that a table can contain a maximum of 8,060 bytes
				per row is relaxed for tables that contain varchar, nvarchar, varbinary, sql_variant, or
				CLR user-defined type columns. The length of each one of these columns must still fall
				within the limit of 8,000 bytes; however, their combined widths can exceed the
				8,060-byte limit. Here is what Books On Line says you have to consider:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Surpassing the 8,060-byte row-size limit might affect performance because SQL Server
				still maintains a limit of 8 KB per page. When a combination of varchar, nvarchar,
				varbinary, sql_variant, or CLR user-defined type columns exceeds this limit, the SQL
				Server Database Engine moves the record column with the largest width to another page in
				the ROW_OVERFLOW_DATA allocation unit, while maintaining a 24-byte pointer on the
				original page.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Превышение ограничения ряда в 8060 байт может сказаться на производительности, поскольку в SQL Server
			действует ограничение 8 КБ на страницу. Если сочетание заданных пользователем колонок с типами VARCHAR,
			NVARCHAR, VARBINARY, sql_variant или CLR превышает это значение, ядро системы управления базой данных SQL Server
			перемещает самую широкую колонку на другую страницу в единичный блок ROW_OVERFLOW_DATA, оставляя на оригинальной
			странице 24байтовый указатель.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Surpassing the 8,060-byte row-size limit might affect performance because SQL Server
				still maintains a limit of 8 KB per page. When a combination of varchar, nvarchar,
				varbinary, sql_variant, or CLR user-defined type columns exceeds this limit, the SQL
				Server Database Engine moves the record column with the largest width to another page in
				the ROW_OVERFLOW_DATA allocation unit, while maintaining a 24-byte pointer on the
				original page.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Moving large records to another page occurs dynamically as records are lengthened
				based on update operations. Update operations that shorten records may cause records to
				be moved back to the original page in the IN_ROW_DATA allocation unit. Also, querying
				and performing other select operations, such as sorts or joins on large records that
				contain row-overflow data slows processing time, because these records are processed
				synchronously instead of asynchronously.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Перемещение больших записей на другую страницу происходит динамически, так как длина записей увеличивается из-за операций обновления.
			Если операции обновления уменьшают длину записи, они могут вызвать возвращение записей на оригинальную страницу в единичный блок IN_ROW_DATA.
			Запросы и выполнение других операций типа SELECT, сортировка или объединение больших записей с данными, превышающими
			максимальный размер ряда, также замедляет время обработки, поскольку данные записи обрабатываются одновременно, а не последовательно.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Moving large records to another page occurs dynamically as records are lengthened
				based on update operations. Update operations that shorten records may cause records to
				be moved back to the original page in the IN_ROW_DATA allocation unit. Also, querying
				and performing other select operations, such as sorts or joins on large records that
				contain row-overflow data slows processing time, because these records are processed
				synchronously instead of asynchronously.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> So be aware of the implications of having wide
				tables, if you can, move these columns tho their own tables, this will also help when
				your developers specify SELECT * when they really only want 2 columns out of the
				table</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Не следует забывать о "подводных камнях" использования широких таблиц и, если это возможно,
			следует перенести такие колонки в их собственные таблицы. Разработчики могут воспользоваться SELECT *, если им нужно убрать 2 колонки из таблицы.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> So be aware of the implications of having wide
				tables, if you can, move these columns tho their own tables, this will also help when
				your developers specify SELECT * when they really only want 2 columns out of the
				table</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Mild</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Mild</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://wiki.lessthandot.com/index.php/SQLCop_wide_table_check" target="_blank">Wide table check</a></p>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
