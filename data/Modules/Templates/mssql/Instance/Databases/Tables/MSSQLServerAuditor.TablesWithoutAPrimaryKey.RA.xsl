<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithoutAPrimaryKey.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables Without a Primary Key</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы без первичного ключа</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables Without a Primary Key</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithoutAPrimaryKey.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables Without a Primary Key</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы без первичного ключа</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables Without a Primary Key</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Best Practice: Every table should have a primary key</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Лучше всего, чтобы у каждой таблицы был первичный ключ</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Best Practice: Every table should have a primary key</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>By definition, primary keys must contain unique data. They are implemented in the
				database through the use of a unique index. If there is not already a clustered index on
				the table, then the primary key's index will be clustered. It's not always true, but
				most of the time, you want your primary keys to be clustered because it is usually the
				key criteria in your requests to the data. This includes join conditions and where
				clause criteria. Clustered indexes give you exceptional performance because it allows
				SQL Server to create optimal execution plans.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Согласно своему определению, первичные ключи должны содержать уникальные данные. Они выполняются в базе данных через уникальный индекс.
			Если в таблице нет кластерного индекса, будет кластеризован индекс первичного ключа. Это не всегда так, но в большинстве случаев мы хотим,
			чтобы первичные ключи были клатеризованы, потому что они, как правило, являются ключевым критерием в запросах к данным (включая условия
			объединения и критерий "where"). Кластерные индексы значительно повышают быстродействие системы, так как позволяют SQL Server создавать
			оптимальные планы выполнения.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>By definition, primary keys must contain unique data. They are implemented in the
				database through the use of a unique index. If there is not already a clustered index on
				the table, then the primary key's index will be clustered. It's not always true, but
				most of the time, you want your primary keys to be clustered because it is usually the
				key criteria in your requests to the data. This includes join conditions and where
				clause criteria. Clustered indexes give you exceptional performance because it allows
				SQL Server to create optimal execution plans.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Identify tables without a primary key using the
				query above. Examine each table and identify what makes each row in the table unique.
				Modify the table to include a primary key.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Найдите таблицы без первичного ключа, используя запрос выше. Проанализируйте каждую таблицу и
			определите, что именно делает каждый ряд уникальным. Измените таблицу, добавив в нее первичный ключ.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Identify tables without a primary key using the
				query above. Examine each table and identify what makes each row in the table unique.
				Modify the table to include a primary key.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/best-practice-every-table-should-have-a" target="_blank">Best Practice: Every table should have a primary key</a></p>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
