<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithMissingForeignKeys.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Description</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Пропущенные ключи</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Description</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithMissingForeignKeys.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Description</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Пропущенные ключи</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Description</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Missing foreign key constraints</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Отсутствие внешних ключей</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Missing foreign key constraints</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>References are at the heart of a database. It is possible to create a beautiful
				database with perfectly working front end code that always, 100% of the time, does the
				right thing with your data. But, writing code is hard. Very hard! Your data is often the
				most important asset you own. You need to protect it with every bit of technology you
				can find. At the heart of protecting your data is referential integrity. What does this
				mean? It means that you shouldn't be missing data, ever!</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Ссылки являются сердцем базы данных. Можно создать красивую базу данных с идеально работающим
			интерфейсным кодом, который всегда, в 100% случаев, делает с данными именно то, что нужно. Но писать код сложно.
			Очень сложно. Часто ваши данные – самое ценное имущество. И нужно защитить его каждым битом имеющихся технологий.
			В сердце защиты Ваших данных лежит целостность на уровне ссылок. Что это значит? Это значит, что Вы никогда не должны
			терять свои данные!</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>References are at the heart of a database. It is possible to create a beautiful
				database with perfectly working front end code that always, 100% of the time, does the
				right thing with your data. But, writing code is hard. Very hard! Your data is often the
				most important asset you own. You need to protect it with every bit of technology you
				can find. At the heart of protecting your data is referential integrity. What does this
				mean? It means that you shouldn't be missing data, ever!</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>The code below will check for columns that have ID in the name of the column where
				that column is not part of a primary key or foreign key constraint. Often times, this
				represents a missing constraint, but not always. The code presented below exists to
				highlight potential problems. You must still determine if this potential problem is
				real, and then act accordingly.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Приведенный ниже код найдет колонки с ID в имени, где колонка не является частью первичного ключа или ограничением внешнего ключа.
			В большинстве случаев он находит отсутствующий ограничитель и отображает потенциальную проблему. Вам нужно будет определить, существует ли
			данная проблема на самом деле и действовать соответственно.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>The code below will check for columns that have ID in the name of the column where
				that column is not part of a primary key or foreign key constraint. Often times, this
				represents a missing constraint, but not always. The code presented below exists to
				highlight potential problems. You must still determine if this potential problem is
				real, and then act accordingly.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Correcting this problem seems simple at first.
				Just declare your foreign keys, right? Well, it's not so simple. You see, there could be
				code running that deletes all the necessary data from the related tables. If you have
				code that deletes data in related tables in the wrong order, you will get referential
				constraint errors. Similar problems can occur with updates and inserts. The order in
				which you do things is important when you have referential constraints.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> На первый взгляд кажется, что исправить проблему просто. Нужно лишь задать внешние ключи, верно?
			На самом деле это не так просто. В этот момент может выполняться код, удаляющий все необходимые данные из связанных с ним таблиц.
			Если у Вас есть код, удаляющий данные в родственных таблицах в неправильном порядке, возникнут ошибки ограничителей, связанные
			со ссылками. Аналогичнвя проблема может возникнуть с обновлениями и вставками. Если у Вас есть ограничения по ссылкам, важен порядок
			выполнения операций.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Correcting this problem seems simple at first.
				Just declare your foreign keys, right? Well, it's not so simple. You see, there could be
				code running that deletes all the necessary data from the related tables. If you have
				code that deletes data in related tables in the wrong order, you will get referential
				constraint errors. Similar problems can occur with updates and inserts. The order in
				which you do things is important when you have referential constraints.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> High</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Высокий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> High</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> High</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Высокий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> High</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DataDesign/missing-foreign-key-constraints" target="_blank">Missing foreign key constraints</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
