<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithCollationColumns.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with columns with different collation</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с колонками у которых collation другой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with columns with different collation</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithCollationColumns.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with columns with different collation</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с колонками у которых collation другой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with columns with different collation</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>SQL Server collation conflicts</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Конфликты collation в SQL Server</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>SQL Server collation conflicts</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Collations control how strings are sorted and compared. Sorting is not usually a
				problem because it does not cause collation conflicts. It may not sort the way you want
				it to, but it won't cause errors. The real problem here is when you compare data.
				Comparisons can occur several different ways. This can be a simple comparison in a where
				clause, or a comparison in a join condition. By having columns in your database that do
				not match the default collation of the database, you have a problem just waiting to
				happen.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Collation контролирует сортировку и сравнение строк. Сортировка, как правило,не
			является проблемой, так как не вызывает конфликтов между разными collation. Она может
			быть выполнена не совсем так, как Вы планировали, но не вызовет ошибок. Проблема
			возникнет при сравнении данных. Существует несколько видов сравнений. Например, простое
			сравнение, где выражение или сравнение имеет условие объединения. Если в базе данных
			есть колонки, имеющие collation отличную от collation базы данных по умолчанию, проблема
			не заставит себя долго ждать.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Collations control how strings are sorted and compared. Sorting is not usually a
				problem because it does not cause collation conflicts. It may not sort the way you want
				it to, but it won't cause errors. The real problem here is when you compare data.
				Comparisons can occur several different ways. This can be a simple comparison in a where
				clause, or a comparison in a join condition. By having columns in your database that do
				not match the default collation of the database, you have a problem just waiting to
				happen.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>When you add a new column to an existing table or create a new table with string
				column(s), and you do NOT specify the collation, it will use the default collation of
				the database. If you then write queries that join with existing columns (that has a
				different collation) you will get collation conflict errors.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Если Вы НЕ задаете collation при добавлении новой колонки к существующей таблице со
			строковыми колонками, будет использована collation базы данных по умолчанию. Если после
			этого Вы создадите запрос для присоединения к существующим колонкам (с другой
			collation), может произойти ошибка конфликта collation.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>When you add a new column to an existing table or create a new table with string
				column(s), and you do NOT specify the collation, it will use the default collation of
				the database. If you then write queries that join with existing columns (that has a
				different collation) you will get collation conflict errors.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Just to be clear here, I am NOT suggesting that every string column should have a
				collation that matches the default collation for the database. Instead, I am suggesting
				that when it is different, there should be a good reason for it. There are many
				successful databases out there where the developers never give any thought to the
				collation. In this circumstance, it's best for the collations for each string column
				match the default collation for the database.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хочу пояснить, я НЕ предлагаю, чтобы collation каждой строковой колонки совпадала с
			collation по умолчанию базы данных. Я советую, чтобы в случае, если она отличается от
			collation по умолчанию, у этого была веская причина. Есть немало баз данных, где
			разработчики не задумывались о collation. В данной ситуации лучше, чтобы collation
			каждой строковая колонки совпадала с collation по умолчанию базы данных.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Just to be clear here, I am NOT suggesting that every string column should have a
				collation that matches the default collation for the database. Instead, I am suggesting
				that when it is different, there should be a good reason for it. There are many
				successful databases out there where the developers never give any thought to the
				collation. In this circumstance, it's best for the collations for each string column
				match the default collation for the database.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> To correct this problem, you can modify the
				collation for your existing string columns.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Чтобы исправить эту проблему, измените collation
			существующих строковых колонок.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> To correct this problem, you can modify the
				collation for your existing string columns.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> High</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Высокий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> High</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/sql-server-collation-conflicts" target="_blank">SQL Server Collation Conflicts</a></p>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
