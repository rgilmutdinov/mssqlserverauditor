<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithUniqueIdentifierColumn.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with NewID keys</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с ключами на основе NewID</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with NewID keys</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithUniqueIdentifierColumn.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with NewID keys</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с ключами на основе NewID</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with NewID keys</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Best Practice: Do not cluster on UniqueIdentifier when you use NewId</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Совет: не кластеризуйте тип данных UniqueIdentifierUniqueIdentifier при использовании NewId</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Best Practice: Do not cluster on UniqueIdentifier when you use NewId</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>There can only be one clustered index per table because SQL Server stores the data in
				the table in the order of the clustered index . When you use a UniqueIdentifier as the
				first column in a clustered index, every time you insert a row in the table, it is
				almost guaranteed to be inserted in to the middle of the table. SQL server stores data
				in 8K pages. If a page is full, SQL Server will do a page split, which causes another 8k
				page to be allocated and half the data from the previous page to be moved to the new
				page. Individually, each page split is fast but does take a little bit of time. In a
				high transaction environment, there could be many page splits happening frequently,
				which ultimately result in slower performance.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>SQL Server хранит данные в порядке кластерного индекса, поэтому в таблице может быть только один кластерный индекс.
			Если Вы используете UniqueIdentifier в качестве первой колонки кластерного индекса, каждый раз при вставлении ряда в таблицу
			он будет помещен в ее центр. SQL сервер хранит данные на страницах размером 8Kилобайт. Если страница заполнена,
			SQL сервер выполнит разрыв страницы, таким образом создавая новую 8 Kилобайтовую странцу и половина данных с предыдущей
			страницы будет перемещена на новую. Разрыв каждой страницы происходит относительно быстро, но иногда может занять некоторое время.
			В рабочей среде с большим количеством транзакций разрывы страниц могут происходит довольно часто, замедляя этим работу программы.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>There can only be one clustered index per table because SQL Server stores the data in
				the table in the order of the clustered index . When you use a UniqueIdentifier as the
				first column in a clustered index, every time you insert a row in the table, it is
				almost guaranteed to be inserted in to the middle of the table. SQL server stores data
				in 8K pages. If a page is full, SQL Server will do a page split, which causes another 8k
				page to be allocated and half the data from the previous page to be moved to the new
				page. Individually, each page split is fast but does take a little bit of time. In a
				high transaction environment, there could be many page splits happening frequently,
				which ultimately result in slower performance.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>When you use an Identity column for a clustered index, the next value inserted is
				guaranteed to be higher than the previous value. This means that new rows will always be
				added to the end of the table and you will not get unnecessary page splits for table
				fragmentation.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Когда Вы используете колонку Identity (Идентичность) для кластеризованного индекса, следующее вставленное значение почти всегда будет
			больше предыдущего. Это означает, что новые ряды всегда будут добавляться в конец таблицы, что избавит Вас от ненужных
			разрывов страниц для фрагментации таблицы.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>When you use an Identity column for a clustered index, the next value inserted is
				guaranteed to be higher than the previous value. This means that new rows will always be
				added to the end of the table and you will not get unnecessary page splits for table
				fragmentation.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>SQL Server 2005 introduced a new function called NewSequentialId(). This function can
				only be used as a default for a column of type UniqueIdentifier. The benefit of
				NewSequentialId is that it always generates a value greater than any other value already
				in the table. This causes the new row to be inserted at the end of the table and
				therefore no page splits.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В SQL Server 2005 представлена новая функция NewSequentialId().Она может быть использована лишь по умолчанию для колонки типа
			UniqueIdentifier. Преимущество функции NewSequentialId в том, что она всегда генерирует значение больше,
			чем уже имеющееся в таблице. В результате чего, новый ряд вставляется в конец таблицы и не вызывает разрывов страницы.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>SQL Server 2005 introduced a new function called NewSequentialId(). This function can
				only be used as a default for a column of type UniqueIdentifier. The benefit of
				NewSequentialId is that it always generates a value greater than any other value already
				in the table. This causes the new row to be inserted at the end of the table and
				therefore no page splits.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> There are several ways to prevent this problem.
				The best method is to use NewSequentialId() instead of NewId. Alternatively (if you are
				using SQL 2000), you could set the fill factor for the index to be less than 100%. Fill
				factor identifies how "full" the data pages are when you recreate the index. With a 100%
				fill factor there is no room in the index to accommodate new rows. If you need to use a
				UniqueIdentifier, and it must be clustered and you cannot use NewSequentialId, then you
				should modify the Fill Factor to minimize page splits. If you do this, it's important to
				rebuild the index periodically.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Существует несколько способ избавиться от данной проблемы.
			Лучший способ – использование функции NewSequentialId() вместо NewId. Либо (если Вы используете SQL 2000), следует установить значение индекса фактора
			заполнения меньше, чем 100%. Фактор заполнения определяет, насколько заполнены страницы при повторном создании индекса. Если фактор заполнения – 100%,
			в индексе не остается места для размещения новых рядов. Если необходимо использовать UniqueIdentifier, индекс должен быть кластеризован.
			При этом нельзя использовать функцию NewSequentialId. Следует изменить Фактор Заполнения, чтобы уменьшить количество разрывов страниц.
			В этом случае также важно периодически перестраивать индекс.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> There are several ways to prevent this problem.
				The best method is to use NewSequentialId() instead of NewId. Alternatively (if you are
				using SQL 2000), you could set the fill factor for the index to be less than 100%. Fill
				factor identifies how "full" the data pages are when you recreate the index. With a 100%
				fill factor there is no room in the index to accommodate new rows. If you need to use a
				UniqueIdentifier, and it must be clustered and you cannot use NewSequentialId, then you
				should modify the Fill Factor to minimize page splits. If you do this, it's important to
				rebuild the index periodically.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Простой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/best-practice-don-t-not-cluster-on-uniqu" target="_blank">Best Practice: Do not cluster on UniqueIdentifier when you use NewId</a></p>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
