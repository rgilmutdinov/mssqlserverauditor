<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TableColumnsWithInconsistency.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with columns type inconsistency</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с проблемами в типах колонок</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with columns type inconsistency</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TableColumnsWithInconsistency.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with columns type inconsistency</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с проблемами в типах колонок</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with columns type inconsistency</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Columns Inconsintency Check</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Проверка типов колонок таблиц</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Columns Inconsintency Check</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Frequently data types, length and other field attributes change during
				development, so the report that highlights possible mistakes (same column
				name but different definition) was created.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В течение разработки схемы базы данных из-за различных факторов может
			случится, что одинаковые по сути и смыслу колонки (имеющие одинаковые
			названия) получат различные типы данных. Данный отчёт и призван сообщить об
			этом. Данный отчёт не более чем рекомендация к размышлению о структуре базы
			данных и решение об изменении типов любых колонок должно приниматься
			архитектором базы данных в каждом конкретном случае.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Frequently data types, length and other field attributes change during
				development, so the report that highlights possible mistakes (same column
				name but different definition) was created.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> To correct this problem, you can
				modify the types for some of your columns.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Подумайте об изменении типов колонок
			таблиц, возможно в базе даннах есть скрытая ошибка или неточность.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> To correct this problem, you can
				modify the types for some of your columns.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Небольшой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://sqlapproach.blogspot.com.au/2013/02/columns-inconsintency-check.html" target="_blank">Columns Inconsintency Check</a></p>		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
