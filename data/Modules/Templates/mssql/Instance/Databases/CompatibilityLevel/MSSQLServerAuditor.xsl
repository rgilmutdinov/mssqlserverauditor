<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseInfo.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Full list of instance databases (info)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Полный список доступных баз данных экземпляра (информация)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Full list of instance databases (info)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseInfo.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Full list of instance databases (info)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Полный список доступных баз данных экземпляра (информация)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Full list of instance databases (info)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='DatabaseInfo' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Database</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>База данных</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Database</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>DatabaseSource</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Имя базы источника</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>DatabaseSource</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Version</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Версия</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Version</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Owner</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Владелец</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Owner</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Date created</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Дата создания</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Date created</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Collation</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Параметр сортировки</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Collation</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Status</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Статус</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Status</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>DatabaseRecovery</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип восстановления</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>DatabaseRecovery</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>DatabaseUpdateability</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип доступа</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>DatabaseUpdateability</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>StandbyMode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Обновление лога</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>StandbyMode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IsAutoClose</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Автозакрытие</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IsAutoClose</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IsAutoShrink</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Автоматическое сжатие</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IsAutoShrink</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IsAutoCreateStatistics</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Создание статистики</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IsAutoCreateStatistics</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IsAutoUpdateStatistics</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Обновление статистики</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IsAutoUpdateStatistics</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IsPublished</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Репликатор</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IsPublished</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IsSubscribed</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Подписчик</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IsSubscribed</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IsEncrypted</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Используется шифрование</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IsEncrypted</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='DatabaseInfo' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseName != ''">
								<xsl:value-of select="DatabaseName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseSourceName != ''">
								<xsl:value-of select="DatabaseSourceName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:if test="DatabaseCompatibilityLevel != MasterCompatibilityLevel">
							<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="DatabaseCompatibilityLevel != ''">
								<xsl:value-of select="DatabaseCompatibilityLevel"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:if test="DatabaseOwner != 'sa'">
							<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="DatabaseOwner != ''">
								<xsl:value-of select="DatabaseOwner"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseCreationDate != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(DatabaseCreationDate, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(DatabaseCreationDate, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(DatabaseCreationDate, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(DatabaseCreationDate, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:if test="DatabaseCollation != MasterCollation">
							<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="DatabaseCollation != ''">
								<xsl:value-of select="DatabaseCollation"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseStatus != ''">
								<xsl:value-of select="DatabaseStatus"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseRecovery != ''">
								<xsl:value-of select="DatabaseRecovery"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseUpdateability != ''">
								<xsl:value-of select="DatabaseUpdateability"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseIsInStandBy != ''">
								<xsl:value-of select="DatabaseIsInStandBy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:if test="DatabaseIsAutoClose != '0'">
							<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="DatabaseIsAutoClose != ''">
								<xsl:value-of select="DatabaseIsAutoClose"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseIsAutoShrink != ''">
								<xsl:value-of select="DatabaseIsAutoShrink"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseIsAutoCreateStatistics != ''">
								<xsl:value-of select="DatabaseIsAutoCreateStatistics"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseIsAutoUpdateStatistics != ''">
								<xsl:value-of select="DatabaseIsAutoUpdateStatistics"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseIsPublished != ''">
								<xsl:value-of select="DatabaseIsPublished"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseIsSubscribed != ''">
								<xsl:value-of select="DatabaseIsSubscribed"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseIsEncrypted != ''">
								<xsl:value-of select="DatabaseIsEncrypted"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>

<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseInfo.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Description</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Описание</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Description</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseInfo.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Description</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Описание</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Description</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>SQL Server and the Auto Close Setting</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Настройки параметров баз данных</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>SQL Server and the Auto Close Setting</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<h2 class="secondHeading">Настройки SQL Server и автозакрытия</h2>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>If you have a database that was recently created and you find "Auto
			Close" set to True, change it to False. It is a best practice, won't hurt
			anything and will prevent things like this situation from coming up. Now
			lets see if I can restart this Saturday morning.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Если Вы недавно создали базу данных, а параметр автозакрытия установлен как True,
			измените его на False. Это наилучший вариант, он ничему не повредит и избавит Вам от
			проблем вроде описанной ниже. Давайте проверим, получится ли у меня перезапустить то
			субботнее утро.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>If you have a database that was recently created and you find "Auto
			Close" set to True, change it to False. It is a best practice, won't hurt
			anything and will prevent things like this situation from coming up. Now
			lets see if I can restart this Saturday morning.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Set the database option "Auto Close"
			to false.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Установите параметр автозакрытия false.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Set the database option "Auto Close"
			to false.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.sql-server-performance.com/tips/optimizing_indexes_general_p1.aspx" target="_blank">General Tips on Optimizing SQL Server Indexes</a></p>
			<h2 class="secondHeading">Конфликты collation с временными таблицами и табличными переменными</h2>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>When the collation of your user database does not match the collation of
			TempDB, you have a potential problem. Temp tables and table variables are
			created in TempDB. When you do not specify the collation for string columns
			in your table variables and temp tables, they will inherit the default
			collation for TempDB. Whenever you compare and/or join to the temp table or
			table variable, you may get a collation conflict.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Если collation Вашей базы данных не совпадает с collation временной базы данных,
			может возникнуть конфликт. Временные таблицы и табличные переменные создаются во
			временной базе данных. Если Вы не задаете collation строковых колонок в переменных Вашей
			таблицы и временных таблицах, они унаследуют collation временной базы данных по
			умолчанию. При сравнении и/или добавлении временной таблицы или табличной переменной,
			может возникнуть конфликт разных collation.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>When the collation of your user database does not match the collation of
			TempDB, you have a potential problem. Temp tables and table variables are
			created in TempDB. When you do not specify the collation for string columns
			in your table variables and temp tables, they will inherit the default
			collation for TempDB. Whenever you compare and/or join to the temp table or
			table variable, you may get a collation conflict.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Under normal circumstances, it is best if all your collations match. This
			includes TempDB, Model (used for creating a new database), your user
			database, and all your string columns (VARCHAR, NVARCHAR, CHAR, NCHAR, TEXT,
			NTEXT).</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Обычно лучше всего, чтобы все элементы имели одинаковую collation, включая временную
			бау данных, модели (используется для создания новой базы данных), Вашу базу данных и все
			строковые колонки (varchar, nvarchar, char, nchar, text, ntext).</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Under normal circumstances, it is best if all your collations match. This
			includes TempDB, Model (used for creating a new database), your user
			database, and all your string columns (VARCHAR, NVARCHAR, CHAR, NCHAR, TEXT,
			NTEXT).</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> There are several ways to correct
			this problem. The long term solution is to change the default collation for
			your database (affecting new string columns) and then change the collation
			for your existing columns. Alternatively, you could modify any code that
			creates a temp table or table variable so that it specifies a collation on
			your string columns. You can hard code the collation or use the default
			database collation.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Существует несколько способов решения этой проблемы. Самое лучшее решение –
			изменить collation базы данных по умолчанию (это повлияет на строковые колонки), затем
			изменить collation существующих колонок. Другой способ – изменить код, создающий
			временную таблицу или табличные переменные,чтобы он задавал collation строковых колонок.
			Вы можете прописать collation самостоятельно или использовать collation базы данных по
			умолчанию.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> There are several ways to correct
			this problem. The long term solution is to change the default collation for
			your database (affecting new string columns) and then change the collation
			for your existing columns. Alternatively, you could modify any code that
			creates a temp table or table variable so that it specifies a collation on
			your string columns. You can hard code the collation or use the default
			database collation.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> High. This is a hidden, hard to find
			bug, just waiting to happen.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Высокий. Это скрытая ошибка, которую сложно
			найти. Она может произойти в любой момент.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> High. This is a hidden, hard to find
			bug, just waiting to happen.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/collation-conflicts-with-temp-tables-and" target="_blank">Collation conflicts with temp tables and table variables</a></p>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>

<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseInfo.Description2.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Description2</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Описание2</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Description2</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseInfo.Description2.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Description2</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Описание2</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Description2</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Old Compatibility Level</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Old Compatibility Level</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Old Compatibility Level</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>

						Each SQL Server database has a setting called compatibility level that
						kinda-sorta influences how T-SQL commands are interpreted. There's a lot of
						misconceptions around what this does - some people think setting a database
						at an old compatibility level will let an out-of-date application work just
						fine with a brand spankin' new SQL Server. That's not usually the case,
						though.

					</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>

						Каждая база данных SQL Server имеет параметр, называемый уровень
						совместимости, своего рода тип того, как интерпретируются команды T-SQL. И
						для достижения оптимального уровня производительности лучше использовать
						последний (максимальный) уровень совместимости для Вашей версии MSSQL
						сервера.

					</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>

						Each SQL Server database has a setting called compatibility level that
						kinda-sorta influences how T-SQL commands are interpreted. There's a lot of
						misconceptions around what this does - some people think setting a database
						at an old compatibility level will let an out-of-date application work just
						fine with a brand spankin' new SQL Server. That's not usually the case,
						though.

					</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>This report checks the compatibility_level field for each database in
			sys.databases and compares it to the compatibility_level of the master
			database.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>This report checks the compatibility_level field for each database in
			sys.databases and compares it to the compatibility_level of the master
			database.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>This report checks the compatibility_level field for each database in
			sys.databases and compares it to the compatibility_level of the master
			database.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Check out the Books Online
			explanation of how to change compatibility levels of a database.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Check out the Books Online
			explanation of how to change compatibility levels of a database.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Check out the Books Online
			explanation of how to change compatibility levels of a database.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Лёгкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.brentozar.com/blitz/old-compatibility-level/" target="_blank">Old Compatibility Level</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
