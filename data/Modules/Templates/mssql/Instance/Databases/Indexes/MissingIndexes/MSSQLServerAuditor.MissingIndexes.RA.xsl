<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="MissingIndexes.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Missing indexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Пропущенные индексы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Missing indexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="MissingIndexes.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Missing indexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Пропущенные индексы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Missing indexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Missing indexes</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Пропущенные индексы</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Missing indexes</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>

						Performance Tuning is quite interesting and Index plays a vital role in it.
						A proper index can improve the performance and a bad index can hamper the
						performance. Please note, if you should not create all the missing indexes
						this script suggest. This is just for guidance. You should not create more
						than 5-10 indexes per table. Additionally, this script sometime does not
						give accurate information so use your common sense. Any way, the scripts is
						good starting point. You should pay attention to Avg_Estimated_Impact when
						you are going to create index. The index creation script is also provided in
						the last column. The suggested indexes have an extremely narrow view - they
						only look at a single query, or a single operation within a single query.
						They don't take into account what already exists or your other query
						patterns. You still need a thinking human being to analyze the overall
						indexing strategy and make sure that you index structure is efficient and
						cohesive.

					</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>

						Данный отчёт укажет рекомендации по добавлению индексов. Кроме параметров
						индекса также представлен DDL скрипт для создания индексов. Отчёт
						основывается на динамических представлениях. Отчёт не более чем
						рекомендации, поэтому будьте внимательны и руководствуйтесь здравым смыслом
						при создании индексов. Кроме ускорения выполнения операций по выборке данных
						наличие большого количества индексов на таблице может привести к проблемам с
						производительностью при вставки или обновлении данных. Кроме того, каждый
						индекс требует ресурсов сервера по поддержанию индекса в актуальном
						состоянии, поэтому на реальной системе важен баланс по количеству индексов.

					</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>

						Performance Tuning is quite interesting and Index plays a vital role in it.
						A proper index can improve the performance and a bad index can hamper the
						performance. Please note, if you should not create all the missing indexes
						this script suggest. This is just for guidance. You should not create more
						than 5-10 indexes per table. Additionally, this script sometime does not
						give accurate information so use your common sense. Any way, the scripts is
						good starting point. You should pay attention to Avg_Estimated_Impact when
						you are going to create index. The index creation script is also provided in
						the last column. The suggested indexes have an extremely narrow view - they
						only look at a single query, or a single operation within a single query.
						They don't take into account what already exists or your other query
						patterns. You still need a thinking human being to analyze the overall
						indexing strategy and make sure that you index structure is efficient and
						cohesive.

					</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Незначительный</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://technet.microsoft.com/en-us/library/ms345524.aspx" target="_blank">About the Missing Indexes Feature</a></p>

			<p><a href="http://blog.sqlauthority.com/2011/01/03/sql-server-2008-missing-index-script-download/" target="_blank">SQL SERVER – 2008 – Missing Index Script – Download</a></p>

			<p><a href="http://basitaalishan.com/2013/03/13/find-missing-indexes-using-sql-servers-index-related-dmvs/" target="_blank">Find missing indexes using SQL Servers index related DMVs</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
