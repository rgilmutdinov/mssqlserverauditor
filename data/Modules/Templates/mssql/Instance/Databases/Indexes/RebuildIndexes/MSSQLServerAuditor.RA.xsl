<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="RebuildIndexes.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Indexes for Rebuild</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Индексы для выполнения операции Rebuild</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Indexes for Rebuild</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="RebuildIndexes.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Indexes for Rebuild</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Индексы для выполнения операции Rebuild</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Indexes for Rebuild</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Finding Fragmentation Of An Index And Fixing It</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Поиск фрагментации индекса и ее исправление</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Finding Fragmentation Of An Index And Fixing It</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>A lof of time your index will get framented over time if you do a lot of updates or
				insert and deletes. We will look at an example by creating a table, fragmenting the heck
				out of it and then doing a reorganize and rebuild on the index.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>При частом обновлении, вставках и удалениях индекс становится фрагментированным.
			Далее я приведу пример создания таблицы, полного ее фрагментирования, последующей
			реорганизации и перестройки индекса.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>A lof of time your index will get framented over time if you do a lot of updates or
				insert and deletes. We will look at an example by creating a table, fragmenting the heck
				out of it and then doing a reorganize and rebuild on the index.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>There are two ways to fix fragmentation, one is to
				reorganize the index and the other is to rebuild the index. Reorganize is an online
				operation while rebuild is not unless you specify ONLINE = ON, ONLINE = ON will only
				work on Enterprise editions of SQL Server.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Есть два способа избавиться от фрагментации –
			заново организовать индекс и перестроить его. Реорганизация индекса выполняется в
			обычном режиме работы сервера, перестройка - вне ее (в специальном режиме регламентного
			обслуживания) за исключением случая, если Вы укажете опцию ONLINE = ON. Эта функция
			поддерживается лишь в Enterprise версиях SQL Server.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>There are two ways to fix fragmentation, one is to
				reorganize the index and the other is to rebuild the index. Reorganize is an online
				operation while rebuild is not unless you specify ONLINE = ON, ONLINE = ON will only
				work on Enterprise editions of SQL Server.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Here are two differences between REBUILD ONLINE = ON and REBUILD ONLINE = OFF</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Между REBUILD ONLINE = ON и REBUILD ONLINE = OFF есть два отличия</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Here are two differences between REBUILD ONLINE = ON and REBUILD ONLINE = OFF</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>ON:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>ON:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>ON:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Long-term table locks are not held for the duration of the index
				operation. During the main phase of the index operation, only an Intent Share (IS) lock
				is held on the source table. This allows queries or updates to the underlying table and
				indexes to continue. At the start of the operation, a Shared (S) lock is very briefly
				held on the source object. At the end of the operation, an S lock is very briefly held
				on the source if a nonclustered index is being created, or an SCH-M (Schema
				Modification) lock is acquired when a clustered index is created or dropped online, or
				when a clustered or nonclustered index is being rebuilt. ONLINE cannot be set to ON when
				an index is being created on a local temporary table.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Долгосрочные блокировки таблицы не применяются во время операций
			индекса. В течение основной фазы операции индекса на исходной таблице удерживается
			только блокировка Intent Share (IS). Это позволяет продолжать выполнение запросов или
			обновлений для базовой таблицы и индексов. В начале операции на исходном объекте
			недолгое время удерживается блокировка Shared (S). В конце операции при создании
			некластеризованного индекса на исходном объекте некоторое время действует блокировка
			Shared (S). Блокировка SCH-M (Schema Modification) применяется при создании кластерного
			индекса или его «выброса» в сеть, а также перестройке как кластеризованного, так и не
			кластеризованного индексов. Фукция ONLINE не может быть включена (ON) при создании
			индекса на местной временной таблице. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Long-term table locks are not held for the duration of the index
				operation. During the main phase of the index operation, only an Intent Share (IS) lock
				is held on the source table. This allows queries or updates to the underlying table and
				indexes to continue. At the start of the operation, a Shared (S) lock is very briefly
				held on the source object. At the end of the operation, an S lock is very briefly held
				on the source if a nonclustered index is being created, or an SCH-M (Schema
				Modification) lock is acquired when a clustered index is created or dropped online, or
				when a clustered or nonclustered index is being rebuilt. ONLINE cannot be set to ON when
				an index is being created on a local temporary table.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>OFF:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>OFF:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>OFF:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Table locks are applied for the duration of the index operation.
				An offline index operation that creates, rebuilds, or drops a clustered, spatial, or XML
				index, or rebuilds or drops a nonclustered index, acquires a Schema modification (Sch-M)
				lock on the table. This prevents all user access to the underlying table for the
				duration of the operation. An offline index operation that creates a nonclustered index
				acquires a Shared (S) lock on the table. This prevents updates to the underlying table
				but allows read operations, such as SELECT statements.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Блокировки таблицы применяются на время выполнения операции
			индекса. К внесетевым операциям создания, перестройки или удаления из памяти
			кластерного, пространственного или XML индекса, также перестройки и удаления из памяти
			некластерного индекса применяется блокировка таблицы Schema modification (Sch-M). Она
			блокирует доступ всех пользователей к базовой таблице на время проведения операции. Для
			внесетевой операции по созданию некластериного индекса применяется блокировка Shared
			(S). Она блокирует обновления базовой таблицы, но позволяет выполнять операции чтения,
			например SELECT.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Table locks are applied for the duration of the index operation.
				An offline index operation that creates, rebuilds, or drops a clustered, spatial, or XML
				index, or rebuilds or drops a nonclustered index, acquires a Schema modification (Sch-M)
				lock on the table. This prevents all user access to the underlying table for the
				duration of the operation. An offline index operation that creates a nonclustered index
				acquires a Shared (S) lock on the table. This prevents updates to the underlying table
				but allows read operations, such as SELECT statements.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Indexes for "Rebuild"</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Индексы, для которых нужно провести операцию "Rebuild"</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Indexes for "Rebuild"</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Report provide the list of indexes, which are required to perform operation "Rebuild".</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В отчёте представлены индексы, для которых рекомендуется провести операцию "Rebuild".</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Report provide the list of indexes, which are required to perform operation "Rebuild".</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://technet.microsoft.com/en-us/library/ms189858.aspx" target="_blank">Reorganize and Rebuild Indexes</a></p>
			<p><a href="http://wiki.lessthandot.com/index.php/Finding_Fragmentation_Of_An_Index_And_Fixing_It" target="_blank">Finding Fragmentation Of An Index And Fixing It</a></p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text><a href="http://blog.sqlauthority.com/2010/01/12/sql-server-fragmentation-detect-fragmentation-and-eliminate-fragmentation/" target="_blank">SQL SERVER – Fragmentation – Detect Fragmentation and Eliminate Fragmentation</a></xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text><a href="http://blog.sqlauthority.com/2010/01/12/sql-server-fragmentation-detect-fragmentation-and-eliminate-fragmentation/" target="_blank">SQL SERVER – Fragmentation – Detect Fragmentation and Eliminate Fragmentation</a></xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text><a href="http://blog.sqlauthority.com/2010/01/12/sql-server-fragmentation-detect-fragmentation-and-eliminate-fragmentation/" target="_blank">SQL SERVER – Fragmentation – Detect Fragmentation and Eliminate Fragmentation</a></xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.oszone.net/7326/SQL_Server" target="_blank">SQL Server: Лучшие советы по эффективному обслуживанию баз данных</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
