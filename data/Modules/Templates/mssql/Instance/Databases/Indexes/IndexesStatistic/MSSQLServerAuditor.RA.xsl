<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="IndexesStatisticStatus.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Index statistics</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Статистика для индексов</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Index statistics</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="IndexesStatisticStatus.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Index statistics</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Статистика для индексов</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Index statistics</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>SQL Server: How Important Are Index Statistics</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>SQL Server: Важность статистики индекса</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>SQL Server: How Important Are Index Statistics</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Statistics play a vital role in performance. Accurate statistics about the data held
				in tables are used to provide the best execution strategy for SQL queries. but if the
				statistics don't accurately reflect the current contents of the table you'll get a
				poorly-performing query. How do you find out if statistics are correct, and what can you
				do if the automatic update of statistics isn't right for the way a table is used?</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Статистика играет очень важную роль в производительности. Точная
			статистика данных, хранящихся в таблицах, используется для вычисления
			наилучшей стратегии выполнения запросов SQL. Если статистика неправильно
			отражает текущее содержимое таблицы, запрос будет плохо выполнен. Как
			узнать, верна ли статистика, и что сделать, если автоматическое обновление
			статистики не подходит для способа использования таблицы?</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Statistics play a vital role in performance. Accurate statistics about the data held
				in tables are used to provide the best execution strategy for SQL queries. but if the
				statistics don't accurately reflect the current contents of the table you'll get a
				poorly-performing query. How do you find out if statistics are correct, and what can you
				do if the automatic update of statistics isn't right for the way a table is used?</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text><a href="http://www.databasejournal.com/features/mssql/sql-server-how-important-are-index-statistics.html" target="_blank">SQL Server: How Important Are Index Statistics</a></xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>SQL сервер использует статистику для выбора соответствующего плана
			запроса, так что, если у Вас проблемы с производительностью во время
			запросов, стоит ее исправить. Устаревшая статистика может заставить SQL
			сервер выбрать неправильный план. Выполните следующий запрос для нахождения
			устаревшей статистики. Он использует функцию STATS_DATE()</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text><a href="http://www.databasejournal.com/features/mssql/sql-server-how-important-are-index-statistics.html" target="_blank">SQL Server: How Important Are Index Statistics</a></xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>Если Вы обнаружили, что статистика устарела, воспользуйтесь фукцией
			"UPDATE STATISTICS()" для ее обновления. Для этой цели Вы даже можете
			написать курсор.</p>

			<p><a href="http://www.databasejournal.com/features/mssql/sql-server-how-important-are-index-statistics.html" target="_blank">SQL Server: How Important Are Index Statistics</a></p>

			<p><a href="http://sequelserver.blogspot.com.au/search/label/find%20outdated%20Statistics" target="_blank">How to find outdated statistics in sql.</a></p>

			<p><a href="http://www.mssqltips.com/sqlservertip/2734/what-are-the-sql-server-wasys-statistics/" target="_blank">Что такое статистика SQL Server _WA_Sys... ?</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
