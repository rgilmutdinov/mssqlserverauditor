<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseProceduresWithoutSetNocountOn.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures Without Set Nocount On</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры без SET NOCOUNT ON</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures Without Set Nocount On</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseProceduresWithoutSetNocountOn.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures Without Set Nocount On</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры без SET NOCOUNT ON</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures Without Set Nocount On</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Identify procedures without SET NOCOUNT ON</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Поиск процедур без SET NOCOUNT ON</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Identify procedures without SET NOCOUNT ON</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>When you have a lot of updates and inserts or deletes in a stored procedure it is
				good to set nocount to on. SQL server can become really chatty when this setting is off;
				this will waste bandwith and CPU power. If a proc is called 30 times a second and you do
				3 DML operations then that will result in 90 n row(s) affected) messages.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Если Вы выполняете много обновлений, вставок или удалений в хранимой процедуре,
			следует установить NOCOUNT ON. SQL server может стать очень "болтливым", если этот режим
			выключен. Это будет лишь засорять канал и расходовать системные ресурсы. Если процедура
			вызывается 30 раз в секунду, и Вы выполняете 3 DML операции, пострадает 90 рядов
			сообщений.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>When you have a lot of updates and inserts or deletes in a stored procedure it is
				good to set nocount to on. SQL server can become really chatty when this setting is off;
				this will waste bandwith and CPU power. If a proc is called 30 times a second and you do
				3 DML operations then that will result in 90 n row(s) affected) messages.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>set nocount on should be the first statement in the proc after the parameters and the
				AS.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Команда "set nocount on" должна быть первой в процедуре после параметров и AS.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>set nocount on should be the first statement in the proc after the parameters and the
				AS.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Rewrite your functionality and add 'SET NOCOUNT
				ON'.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Добавьте 'SET NOCOUNT ON' в начале хранимой процедуры.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Rewrite your functionality and add 'SET NOCOUNT
				ON'.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.mssqltips.com/sqlservertip/1226/set-nocount-on-improves-sql-server-stored-procedure-performance/" target="_blank">SET NOCOUNT ON Improves SQL Server Stored Procedure Performance</a></p>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
