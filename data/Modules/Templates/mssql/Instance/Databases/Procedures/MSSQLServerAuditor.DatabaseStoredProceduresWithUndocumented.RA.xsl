<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseProceduresWithundocumented.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Stored procedures calling to undocumented procedures</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры с вызовом недокументированных функций</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Stored procedures calling to undocumented procedures</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseProceduresWithundocumented.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Stored procedures calling to undocumented procedures</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры с вызовом недокументированных функций</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Stored procedures calling to undocumented procedures</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Identify procedures that call SQL Server undocumented procedures</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Поиск процедур, вызывающих недокументированные процедуры в SQL Server</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Identify procedures that call SQL Server undocumented procedures</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>When you use an undocumented stored procedure, you run the risk of not being able to
				upgrade your database to a new version. What's worse... you could have broken
				functionality and not even know it. With undocumented stored procedures, Microsoft may
				not document when they decide to deprecate it, so you may not know about your broken
				functionality until it's too late.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>При использовании хранимыхнедокументированных процедур, Вы рискуете не обновитьбазу данных до новой версии. Или еще хуже…
			У Вас может быть нарушена функциональность, а Вы даже узнаете об этом. В случае с хранимыми недокументированными процедурами,
			Microsoft может не объявить заранее, если онит решат сделать их устаревшими, поэтому Вы не узнаете, что функциональность
			нарушена до тех пор, пока не станет слишком поздно.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>When you use an undocumented stored procedure, you run the risk of not being able to
				upgrade your database to a new version. What's worse... you could have broken
				functionality and not even know it. With undocumented stored procedures, Microsoft may
				not document when they decide to deprecate it, so you may not know about your broken
				functionality until it's too late.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Presented below is a hard coded list of undocumented stored procedures. By their very
				nature, it is hard to find documentation on undocumented procedures. Therefore, the
				procedures in the list below is likely to be incomplete.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Ниже представлен запрограммированный список недокументированных хранимых процедур. В соответствии с их типом,
			для недокументированных процедур сложно найти документацию. Поэтому, скорее всего, в данном списке представлены не все процедуры. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Presented below is a hard coded list of undocumented stored procedures. By their very
				nature, it is hard to find documentation on undocumented procedures. Therefore, the
				procedures in the list below is likely to be incomplete.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Rewrite your functionality so that is does not
				rely upon undocumented procedures.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Перепишите функциональность, чтобы она не опиралась на недокументированные процедуры.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Rewrite your functionality so that is does not
				rely upon undocumented procedures.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> moderate to high. Undocumented stored
				procedures are often used because it's easy. Replacing it is usually NOT easy.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> От среднего до высокого. Хранимые недокументированные процедуры часто используются ввиду легкости,
			Но их заменить не так просто.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> moderate to high. Undocumented stored
				procedures are often used because it's easy. Replacing it is usually NOT easy.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate to high</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> От средней до высокой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate to high</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DataDesign/identify-procedures-that-call-sql-server" target="_blank">Identify procedures that call SQL Server undocumented procedures</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
