<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseProceduresWithCharIssues.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures With Char Issues</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры без указания размера текстовых переменных</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures With Char Issues</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseProceduresWithCharIssues.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures With Char Issues</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры без указания размера текстовых переменных</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures With Char Issues</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Always include size when using varchar, nvarchar, char and nchar</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Всегда указывайте размер при помощи varchar, nvarchar, char или nchar</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Always include size when using varchar, nvarchar, char and nchar</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>There are several string data types in SQL Server. There are varchar, nvarchar, char,
				and nchar. Most front end languages do not require you to identify the length of string
				variables, and SQL Server is no exception. When you do not specify the length of your
				string objects in SQL Server, it applies its own defaults. For most things, the default
				length is one character. This applies to columns, parameters, and locally declared
				variables. The notable exception is with the cast and convert functions which default to
				30 characters.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В SQL Server несколько строковых типов данных. Это varchar, nvarchar, char и nchar. Многие интерфейсные
			языки не требуют указания длины строковых переменных и SQL Server не исключение. Если Вы не укажете длину
			строковых объектов в SQL Server, он применит значения по умолчанию. Наиболее часто длина по умолчанию
			составляет 1 символ. Они применяется для столбцов, параметров и локальных переменных. Исключение - функции CAST и CONVERT,
			длина которых по умолчанию – 30 символов. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>There are several string data types in SQL Server. There are varchar, nvarchar, char,
				and nchar. Most front end languages do not require you to identify the length of string
				variables, and SQL Server is no exception. When you do not specify the length of your
				string objects in SQL Server, it applies its own defaults. For most things, the default
				length is one character. This applies to columns, parameters, and locally declared
				variables. The notable exception is with the cast and convert functions which default to
				30 characters.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> To correct this problem, identify where it exists
				(using the SQL shown above). Then, for each occurrence, identify the size. If the
				problem occurred when declaring a column in a table and you WANT the size to be 1
				character, then specify (1). Other places, you will need to determine the size that it
				should be. Sometimes this involves looking up the size in the table definition.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Чтобы исправить эту проблему, найдите ее местоположение (используя код SQL выше).
			Затем укажите размер для каждого случая.  Если проблема появилась во время объявления столбца в таблице и Вы ХОТИТЕ, чтобы
			его размер был равен 1 символу, укажите (1). В других местах нужно будет определить необходимый размер. Иногда для этого
			требуется найти размер в таблице.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> To correct this problem, identify where it exists
				(using the SQL shown above). Then, for each occurrence, identify the size. If the
				problem occurred when declaring a column in a table and you WANT the size to be 1
				character, then specify (1). Other places, you will need to determine the size that it
				should be. Sometimes this involves looking up the size in the table definition.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Простой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> High, because this problem can corrupt your data</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Высокий, так как данная проблема может привести к повреждению данных</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> High, because this problem can corrupt your data</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/MSSQLServer/always-include-size-when-using-varchar-n" target="_blank">Always include size when using varchar, nvarchar, char and nchar</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
