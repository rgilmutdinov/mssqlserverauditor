<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseProceduresWithSPName.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures With SP Name</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Список хранимых процедур с sp_</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures With SP Name</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseProceduresWithSPName.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures With SP Name</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Список хранимых процедур с sp_</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures With SP Name</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Don't start your procedures with SP_</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Не начинайте процедуры с SP_</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Don't start your procedures with SP_</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>When SQL Server executes a stored procedure, it first checks to see if it is a
				built-in stored procedure (system supplied). It checks the master database for the
				existence of this procedure. If the procedure is not found, it will search the user
				database. It doesn't sound like much, but in a high transaction environment, the slight
				performance hit will add up.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>При выполнении хранимой процедуры, SQL Server сначала проверяет, является ли она встроенной (системной).
			Он проверяет главную базу данных на наличие данной процедуры. Если процедура не найдена, SQL Server ищет в базе данных пользователя.
			В рабочей среде с большим количеством запущенных процессов, это может замедлить время обработки данных.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>When SQL Server executes a stored procedure, it first checks to see if it is a
				built-in stored procedure (system supplied). It checks the master database for the
				existence of this procedure. If the procedure is not found, it will search the user
				database. It doesn't sound like much, but in a high transaction environment, the slight
				performance hit will add up.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Also, consider what would happen if Microsoft decides to ship a system stored
				procedure with the same name as the procedure you wrote. Suddenly, your procedure will
				stop working and the one supplied by Microsoft will be executed instead. To see what I
				mean, try creating a stored procedure in your database named sp_help. When you execute
				this stored procedure, SQL will actually execute the one in the master database
				instead.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Представьте, что будет, если Microsoft решит загрузить хранимую системную процедуру с именем
			созданной Вами процедуры. Ваша процедура неожиданно прекратит работу и вместо нее будет выполняться процедура Microsoft.
			Чтобы понять, о чем я, создайте в базе данных хранимую процедуру с именем sp_help. Когда Вы попробуете ее запустить,
			SQL запустит одноименную процедуру из главной базы данных.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Also, consider what would happen if Microsoft decides to ship a system stored
				procedure with the same name as the procedure you wrote. Suddenly, your procedure will
				stop working and the one supplied by Microsoft will be executed instead. To see what I
				mean, try creating a stored procedure in your database named sp_help. When you execute
				this stored procedure, SQL will actually execute the one in the master database
				instead.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>One possible strategy you could use to help resolve this problem would be to rename
				the procedure, and then create a procedure with the original name. This procedure could
				write to a log file, and then call the original procedure. This strategy allows your
				application to continue working (albeit a little slower because of the logging). You can
				then determine which application ran the procedure and change the name of the call.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Один из возможных вариантов решения проблемы – переименовать процедуру, затем создать новую процедуру с прежним именем.
			Данная процедура удет вести файл журнала и вызывать оригинальную процедуру. Этот способ сделает Вашу программу снова работоспособной
			(хоть она и будет работать немного медленнее из-за журнальных операций). После этого Вы можете определить, какое именно приложение
			запустило процедуру и переименовать запрос.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>One possible strategy you could use to help resolve this problem would be to rename
				the procedure, and then create a procedure with the original name. This procedure could
				write to a log file, and then call the original procedure. This strategy allows your
				application to continue working (albeit a little slower because of the logging). You can
				then determine which application ran the procedure and change the name of the call.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> To correct this problem, you will need to
				identify all procedures named this way, and then change the name of the procedure. There
				are far greater implications though. Some stored procedures are called by other stored
				procedures. In cases like this, you will need to change those stored procedures too.
				Additionally, you will also need to change your front end code to call the procedure
				with the new name.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Чтобы исправить эту проблему, найдите все аналогичные процедуры и переименуйте их. Но могут возникнуть небольшие сложности.
			Некоторые хранимые процедуры вызываются другими хранимыми процедурами. В таких случаях, их тоже нужно переименовать.
			Следует также изменить интерфейсный код для вызова процедуры с новым именем.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> To correct this problem, you will need to
				identify all procedures named this way, and then change the name of the procedure. There
				are far greater implications though. Some stored procedures are called by other stored
				procedures. In cases like this, you will need to change those stored procedures too.
				Additionally, you will also need to change your front end code to call the procedure
				with the new name.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Medium to high. The level of effort required to
				correct this problem can range from medium to high, depending on how many procedures you
				have than require a name change.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средне-высокий. Уровень усилий для исправления данной проблемы – от среднего до высокого,
			в зависимости от того, сколько процедур необходимо переименовать</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Medium to high. The level of effort required to
				correct this problem can range from medium to high, depending on how many procedures you
				have than require a name change.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/MSSQLServer/don-t-start-your-procedures-with-sp_" target="_blank">Don't start your procedures with SP_</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
