<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseProceduresWithSetRowCount.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures With Set RowCount</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры с SET ROWCOUNT</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures With Set RowCount</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseProceduresWithSetRowCount.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures With Set RowCount</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры с SET ROWCOUNT</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures With Set RowCount</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>SET ROWCOUNT will not be supported in future version of SQL Server</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Следующая версия SQL Server не будет поддерживать функцию SET ROWCOUNT</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>SET ROWCOUNT will not be supported in future version of SQL Server</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Things are changing very rapidly in SQL Server future versions. Some of the features
				which were once treated as standard solutions to some typical scenarios of TSQL are now
				scheduled to be deprecated in future versions of SQL Server. One of these feature was
				the SET ROWCOUNT statement. This statement is also scheduled for being deprecated in
				future versions of SQL Server. The only way to limit your results will be by using the
				TOP keyword.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В новых версиях SQL Server изменения происходят очень быстро. Некоторые функции, считавшиеся стандартными решениями
			типичных сценариев TSQL, в следующих версиях будут объявлены устаревшими. Одной из таких функций является параметр SET ROWCOUNT.
			Данный параметр также будет объявлен устаревшим в следующих версиях SQL Server. Единственным способом ограничить результаты
			будет использование ключевого слова TOP.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Things are changing very rapidly in SQL Server future versions. Some of the features
				which were once treated as standard solutions to some typical scenarios of TSQL are now
				scheduled to be deprecated in future versions of SQL Server. One of these feature was
				the SET ROWCOUNT statement. This statement is also scheduled for being deprecated in
				future versions of SQL Server. The only way to limit your results will be by using the
				TOP keyword.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Similarly some other SET options will no longer be supported like</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Некоторые другие функции SET тоже не будут поддерживаться, например</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Similarly some other SET options will no longer be supported like</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			SET ANSI_NULLS<br />
			SET ANSI_PADDING<br />
			SET CONCAT_NULL_YIELDS_NULL<br />

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>I will suggest you to study the complete list of deprecated database features here
				and make sure you are not relying too much on these features, if you want to make sure
				that your database is compatible with future versions of SQL Server. </xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Я советую Вам ознакомиться с полным списком устаревших функций базы данных и не слишком на них опираться, если Вы хотите,
			чтобы Ваша база данных была совместима с новыми версиями SQL Server. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>I will suggest you to study the complete list of deprecated database features here
				and make sure you are not relying too much on these features, if you want to make sure
				that your database is compatible with future versions of SQL Server. </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Rewrite your functionality without using 'SET ROWCOUNT'.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Перепишите функциональность без использования 'SET ROWCOUNT'.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Rewrite your functionality without using 'SET ROWCOUNT'.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Простой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="https://sqltips.wordpress.com/2007/08/19/set-rowcount-will-not-be-supported-in-future-version-of-sql-server/" target="_blank">SET ROWCOUNT will not be supported in future version of SQL Server</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
