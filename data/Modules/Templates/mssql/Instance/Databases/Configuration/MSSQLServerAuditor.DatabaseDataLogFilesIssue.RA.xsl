<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseDataLogFilesIssue.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Data and Log files on the same physical drive</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Файлы данных и журнала транзакций на одном физическом диске</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Data and Log files on the same physical drive</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseDataLogFilesIssue.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Data and Log files on the same physical drive</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Файлы данных и журнала транзакций на одном физическом диске</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Data and Log files on the same physical drive</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Database and Log files on the same physical disk</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Файлы данных и журнала транзакций на одном физическом диске</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Database and Log files on the same physical disk</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>A reader emailed me with a question: if you only have two drive arrays in a server (C
				and D, let's say), and you also have an application on that server, how should you
				configure SQL Server? Best practices say to keep your data and log files on separate
				drive arrays, but is it OK to put them on the same drive and put the application files
				on the other drive?</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Один читатель написал мне письмо с вопросом: "Если в сервере только два
			дисковых массива (к примеру, C и D) и на этом же сервере находится
			программа, как следует настроить SQL Server? Чаще всего советуют хранить
			данные и файлы журнала в массивах разных дисков, но можно ли хранить их на
			одном диске, а файлы приложения на другом?"</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>A reader emailed me with a question: if you only have two drive arrays in a server (C
				and D, let's say), and you also have an application on that server, how should you
				configure SQL Server? Best practices say to keep your data and log files on separate
				drive arrays, but is it OK to put them on the same drive and put the application files
				on the other drive?</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>The reason the data file (mdf) and log file (ldf) should normally be on separate
				drives is that when you're doing inserts/updates/deletes, they both have to write at the
				exact same time. When the SQL Server engine starts to write data, it essentially:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Причина, по которой файлы данных (mdf) и файлы журнала (ldf) должны
			находиться на разных дисках, в том, что, когда вы делаете вставки
			/обновления/удаления, им приходится выполнять запись одновременно. Когда
			движок SQL Server начинает записывать данные, он обязательно:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>The reason the data file (mdf) and log file (ldf) should normally be on separate
				drives is that when you're doing inserts/updates/deletes, they both have to write at the
				exact same time. When the SQL Server engine starts to write data, it essentially:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<ul>

				<li>Записывает в файл журнала, что он собирается изменить данные</li>

				<li>Записывает в файл данных</li>

				<li>Записывает в файл журнала, что операция выполнена</li>

			</ul>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>That means putting those on the same set of drives is going to make writes much
				slower, because the writes will always be random: the disks will jump from the log file
				location to the data file location and back to the log file location. Random activity is
				slower than sequential activity, so the penalty is even worse.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Это значит, что размещение файлов на одном диске замедляет процесс
			записи, потому что записи всегда будут случайными: диски будут "прыгать" от
			местоположения файла журнала к местоположению файла данных и обратно к
			месторасположению файла журнала. Такой процесс медленнее, чем
			последовательный, поэтому он сказывается на производительности. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>That means putting those on the same set of drives is going to make writes much
				slower, because the writes will always be random: the disks will jump from the log file
				location to the data file location and back to the log file location. Random activity is
				slower than sequential activity, so the penalty is even worse.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>So when is it appropriate to put the data and log files on the same drive, and use
				the leftover drive for something else?</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В таком случае, когда следует расположить данные и файлы программы на
			одном диске, и использовать другой диск для чего-то еще?</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>So when is it appropriate to put the data and log files on the same drive, and use
				the leftover drive for something else?</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<ul>

				<li><strong>Когда база данных редко обновляется</strong>. Если она
				используется в основном для чтения, заметной нагрузки при записи не
				возникнет.</li>

				<li><strong>Если один диск значительно быстрее, чем другой</strong>.
				Предположим, у вас есть массив RAID 10 с 8 жесткими дисками и массив
				RAID 5 с 3 жесткими дисками. В этом случае лучше разместить все на
				более быстром диске.</li>

				<li><strong>В этом случае приложение чаще всего обращается к TempDB
				</strong>. Однажды я был на собеседовании, где компания, сообщила,
				что TempDB более активна, чем диски с данными или файлами журнала. В
				этом случае, стоит расположить TempDB на собственном массиве, а базу
				данных пользователя и файлы журнала на другом.</li>

				<li><strong>Когда дисковая активность приложения больше, чем у SQL
				Server</strong>. Заметьте, что я сказал "больше дисковая
				активность": не достаточно того, что приложение активно используется
				или много работает. Я имел ввиду именно активность диска. Некоторые
				приложения просто используют диск при запуске, в этом случае, им не
				нужен отдельный дисковый массив.</li>

			</ul>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Regardless of your decision, though, use Perfmon after the system goes live and track
				the drive activity. If one of the two arrays is being overwhelmed with load while the
				other one sits idle, then it's time to rethink the decision. Moving data or log files is
				as easy as detaching the database, copying the physical file (don't move it something
				could go wrong) and reattaching the database. Granted, it involves downtime, but it's
				better than being stuck with your decision for life.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В любом случае следует использовать Perfmon после включения системы и
			отслеживать активность диска. Если один из двух массивов перегружен, а
			другой неактивен, стоит изменить свое решение. Переместить данные или файлы
			журнала также легко, как отсоединить базу данных и скопировать физический
			файл (не стоит его переносить, что-нибудь может пойти не так) и присоединить
			базу данных. Конечно, стоит учитывать время простоя, но это лучше, чем
			застрять с вашим решением на всю жизнь.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Regardless of your decision, though, use Perfmon after the system goes live and track
				the drive activity. If one of the two arrays is being overwhelmed with load while the
				other one sits idle, then it's time to rethink the decision. Moving data or log files is
				as easy as detaching the database, copying the physical file (don't move it something
				could go wrong) and reattaching the database. Granted, it involves downtime, but it's
				better than being stuck with your decision for life.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Update on moving database files: JMKehayias, KBrianKelley and SQLDBA on Twitter all
				chimed in that users would be better off using ALTER DATABASE rather than a
				detach/reattach. At first I was suspicious, not seeing an advantage, but SQLDBA sold me
				when he said they'd moved a log shipped database that way: alter database to change the
				file paths, take it offline, copy the files to their new locations, and bring it back
				online. It kept log shipping intact. That's a win.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В Twitter советуют использовать функцию ALTER DATABASE, вместо
			detach/reattach. Сначала я сомневался, не видя преимуществ, но SQLDBA
			изменил мое решение, сказав, что они перенесли таким способом базу данных
			журнала и журнал транзакций: изменили базу данных, чтобы изменить пути к
			файлам, вынесли ее автономный режим, скопировали файлы в новые
			местоположения и снова включили в сеть. Так они сохранили доставку журналов
			нетронутой. Это победа.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Update on moving database files: JMKehayias, KBrianKelley and SQLDBA on Twitter all
				chimed in that users would be better off using ALTER DATABASE rather than a
				detach/reattach. At first I was suspicious, not seeing an advantage, but SQLDBA sold me
				when he said they'd moved a log shipped database that way: alter database to change the
				file paths, take it offline, copy the files to their new locations, and bring it back
				online. It kept log shipping intact. That's a win.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.brentozar.com/archive/2009/02/when-should-you-put-data-and-logs-on-the-same-drive" target="_blank">Database and Log files on the same physical disk</a></p>

			<p><a href="http://www.sql.ru/articles/mssql/01122801optimizingperformancebyusingfilegroups.shtml" target="_blank">Оптимизация работы SQL Server за счёт грамотного использования файлов и filegroups</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
