<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="Monitoring.WMI.LogicalDisk.Performance.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Logical Disk Performance</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Производительность локальных дисков</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Logical Disk Performance</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="Monitoring.WMI.LogicalDisk.Performance.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Logical Disk Performance</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Производительность локальных дисков</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Logical Disk Performance</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function() {
					$("#myErrorTable").tablesorter({
						theme : 'MSSQLServerAuditorError',

						widgets: [ "zebra", "resizable", "stickyHeaders" ],

						widgetOptions : {
							zebra : ["even", "odd"]
						}
					});

					$("#myTable").tablesorter({
						theme : 'MSSQLServerAuditor',

						widgets: [ "zebra", "resizable", "stickyHeaders" ],

						widgetOptions : {
							zebra : ["even", "odd"]
						}
					});
				});
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:apply-templates select="child::node()"/>
		</body>
		</html>
		</xsl:template>

		<xsl:template match="MSSQLResult[@name='Get_Win32_PerfRawData_PerfDisk_LogicalDisk' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']">
			<table id="myTable">
			<caption>
			</caption>
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Экземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Drive</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Диск</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Drive</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>AvgDisksecPerRead</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Чтение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>AvgDisksecPerRead</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>AvgDisksecPerTransfer</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Передача</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>AvgDisksecPerTransfer</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>AvgDisksecPerWrite</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запись</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>AvgDisksecPerWrite</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="Name != ''">
								<xsl:value-of select="Name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="AvgDisksecPerRead != ''">
								<xsl:value-of select="format-number(AvgDisksecPerRead, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="AvgDisksecPerTransfer != ''">
								<xsl:value-of select="format-number(AvgDisksecPerTransfer, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="AvgDisksecPerWrite != ''">
								<xsl:value-of select="format-number(AvgDisksecPerWrite, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
