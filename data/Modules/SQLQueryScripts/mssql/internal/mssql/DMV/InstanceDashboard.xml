<?xml version="1.0" encoding="UTF-8"?>
<sql-select-content xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<sql-select name="GetDashboardMonth">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 LEFT(CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112), 6)                                            AS [NodeUName]
				,LEFT(CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112), 6)                                            AS [NodeUId]
				,1                                                                                                            AS [NodeEnabled]
				,N'NodeIcon'                                                                                                  AS [NodeUIcon]
				,N'#080808'                                                                                                   AS [NodeFontColor]
				,N'normal'                                                                                                    AS [NodeFontStyle]
				,CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112)                                                     AS [EventDayMin]
				,CONVERT([NVARCHAR](128), DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, tM.[EventDayStartOfMonth]) + 1, 0)), 112) AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					DATEADD(month, DATEDIFF(month, 0, tD.[EventDay]), 0) AS [EventDayStartOfMonth]
				FROM (
					SELECT DISTINCT
						hICU.[EventDay] AS [EventDay]
					FROM
						[${hist}$].[dbo].[h_InstanceCPUUtilization] hICU
						INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
							    hSI.[id]                  = hICU.[h_ServerInstance_id]
							AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
							AND hSI.[d_Login_id]          = @d_Login_id
					WHERE
						hICU.[EventTime] &gt; (
							SELECT
								DATEADD(mm, -12, MAX(hICU.[EventTime]))
							FROM
								[${hist}$].[dbo].[h_InstanceCPUUtilization] hICU
								INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
									    hSI.[id]                  = hICU.[h_ServerInstance_id]
									AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
									AND hSI.[d_Login_id]          = @d_Login_id
						)

					UNION ALL

					SELECT DISTINCT
						hISP.[EventDay] AS [EventDay]
					FROM
						[${hist}$].[dbo].[h_InstanceSummaryProcesses] hISP
						INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
							    hSI.[id]                  = hISP.[h_ServerInstance_id]
							AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
							AND hSI.[d_Login_id]          = @d_Login_id
					WHERE
						hISP.[EventTime] &gt; (
							SELECT
								DATEADD(mm, -12, MAX(hISP.[EventTime]))
							FROM
								[${hist}$].[dbo].[h_InstanceSummaryProcesses] hISP
								INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
									    hSI.[id]                  = hISP.[h_ServerInstance_id]
									AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
									AND hSI.[d_Login_id]          = @d_Login_id
						)
					) tD
				) tM
			ORDER BY
				tM.[EventDayStartOfMonth] DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@strDateTimeModifier" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetDashboardMonthWeeks">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 CONVERT([NVARCHAR](128), DATEPART(WEEK, tW.[EventDayMonday])) AS [NodeUName]
				,CONVERT([NVARCHAR](128), DATEPART(WEEK, tW.[EventDayMonday])) AS [NodeUId]
				,1                                                             AS [NodeEnabled]
				,N'NodeIcon'                                                   AS [NodeUIcon]
				,N'#080808'                                                    AS [NodeFontColor]
				,N'normal'                                                     AS [NodeFontStyle]
				,CASE
					WHEN (tW.[EventDayMonday] &lt; CONVERT([DATE], @EventDayMin, 112)) THEN
						CONVERT([NVARCHAR](128), @EventDayMin, 112)
					ELSE
						CONVERT([NVARCHAR](128), tW.[EventDayMonday], 112)
				END                                                            AS [EventDayMin]
				,CASE
					WHEN (tW.[EventDaySunday] &gt; CONVERT([DATE], @EventDayMax, 112)) THEN
						CONVERT([NVARCHAR](128), @EventDayMax, 112)
					ELSE
						CONVERT([NVARCHAR](128), tW.[EventDaySunday], 112)
				END                                                            AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					 DATEADD(wk, DATEDIFF(d, 0, tD.[EventDay]) / 7, 0)                  AS [EventDayMonday]
					,DATEADD(day, 6, DATEADD(wk, DATEDIFF(d, 0, tD.[EventDay]) / 7, 0)) AS [EventDaySunday]
				FROM (
					SELECT DISTINCT
						hICU.[EventDay] AS [EventDay]
					FROM
						[${hist}$].[dbo].[h_InstanceCPUUtilization] hICU
						INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
							    hSI.[id]                  = hICU.[h_ServerInstance_id]
							AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
							AND hSI.[d_Login_id]          = @d_Login_id
					WHERE
						hICU.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)

					UNION ALL

					SELECT DISTINCT
						hISP.[EventDay] AS [EventDay]
					FROM
						[${hist}$].[dbo].[h_InstanceSummaryProcesses] hISP
						INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
							    hSI.[id]                  = hISP.[h_ServerInstance_id]
							AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
							AND hSI.[d_Login_id]          = @d_Login_id
					WHERE
						hISP.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)
				) tD
			) tW
			ORDER BY
				DATEPART(WEEK, tW.[EventDayMonday]) DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin" type="NVarChar" />
			<sql-select-parameter name="@EventDayMax" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetDashboardDays">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 CONVERT([NVARCHAR](128), DATEPART(DAY, tD.[EventDay])) AS [NodeUName]
				,CONVERT([NVARCHAR](128), DATEPART(DAY, tD.[EventDay])) AS [NodeUId]
				,1                                                      AS [NodeEnabled]
				,N'NodeIcon'                                            AS [NodeUIcon]
				,CASE
					WHEN (DATENAME(weekday, tD.[EventDay]) = N'Sunday') THEN
						N'#CC0000'
					ELSE
						N'#080808'
				END                                                     AS [NodeFontColor]
				,N'normal'                                              AS [NodeFontStyle]
				,CONVERT([NVARCHAR](128), tD.[EventDay], 112)           AS [EventDayMin]
				,CONVERT([NVARCHAR](128), tD.[EventDay], 112)           AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					tD1.[EventDay] AS [EventDay]
				FROM (
					SELECT DISTINCT
						hICU.[EventDay] AS [EventDay]
					FROM
						[${hist}$].[dbo].[h_InstanceCPUUtilization] hICU
						INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
							    hSI.[id]                  = hICU.[h_ServerInstance_id]
							AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
							AND hSI.[d_Login_id]          = @d_Login_id
					WHERE
						hICU.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)

					UNION ALL

					SELECT DISTINCT
						hISP.[EventDay] AS [EventDay]
					FROM
						[${hist}$].[dbo].[h_InstanceSummaryProcesses] hISP
						INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
							    hSI.[id]                  = hISP.[h_ServerInstance_id]
							AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
							AND hSI.[d_Login_id]          = @d_Login_id
					WHERE
						hISP.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)
				) tD1
			) tD
			ORDER BY
				tD.[EventDay] DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin" type="NVarChar" />
			<sql-select-parameter name="@EventDayMax" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>
</sql-select-content>
