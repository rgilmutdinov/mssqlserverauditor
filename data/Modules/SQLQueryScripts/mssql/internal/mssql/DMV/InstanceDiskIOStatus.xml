<?xml version="1.0" encoding="UTF-8"?>
<sql-select-content xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<sql-select name="InstanceDiskIOStatus.Month">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 LEFT(CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112), 6)                                            AS [NodeUName]
				,LEFT(CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112), 6)                                            AS [NodeUId]
				,1                                                                                                            AS [NodeEnabled]
				,N'NodeIcon'                                                                                                  AS [NodeUIcon]
				,N'#080808'                                                                                                   AS [NodeFontColor]
				,N'normal'                                                                                                    AS [NodeFontStyle]
				,CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112)                                                     AS [EventDayMin]
				,CONVERT([NVARCHAR](128), DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, tM.[EventDayStartOfMonth]) + 1, 0)), 112) AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					DATEADD(month, DATEDIFF(month, 0, hIIS.[EventDay]), 0) AS [EventDayStartOfMonth]
				FROM
					[${hist}$].[dbo].[h_InstanceIOStatus] hIIS
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hIIS.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id
				WHERE
					hIIS.[EventDay] &gt; (
						SELECT
							DATEADD(mm, -12, MAX(hIIS.[EventDay]))
						FROM
							[${hist}$].[dbo].[h_InstanceIOStatus] hIIS
							INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
								    hSI.[id]                  = hIIS.[h_ServerInstance_id]
								AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
								AND hSI.[d_Login_id]          = @d_Login_id
					)
				) tM
			ORDER BY
				tM.[EventDayStartOfMonth] DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@strDateTimeModifier" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="InstanceDiskIOStatus.Weeks">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 CONVERT([NVARCHAR](128), DATEPART(WEEK, tW.[EventDayMonday])) AS [NodeUName]
				,CONVERT([NVARCHAR](128), DATEPART(WEEK, tW.[EventDayMonday])) AS [NodeUId]
				,1                                                             AS [NodeEnabled]
				,N'NodeIcon'                                                   AS [NodeUIcon]
				,N'#080808'                                                    AS [NodeFontColor]
				,N'normal'                                                     AS [NodeFontStyle]
				,CASE
					WHEN (tW.[EventDayMonday] &lt; CONVERT([DATE], @EventDayMin, 112)) THEN
						CONVERT([NVARCHAR](128), @EventDayMin, 112)
					ELSE
						CONVERT([NVARCHAR](128), tW.[EventDayMonday], 112)
				END                                                            AS [EventDayMin]
				,CASE
					WHEN (tW.[EventDaySunday] &gt; CONVERT([DATE], @EventDayMax, 112)) THEN
						CONVERT([NVARCHAR](128), @EventDayMax, 112)
					ELSE
						CONVERT([NVARCHAR](128), tW.[EventDaySunday], 112)
				END                                                            AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					 DATEADD(wk, DATEDIFF(d, 0, tD.[EventDay]) / 7, 0)                  AS [EventDayMonday]
					,DATEADD(day, 6, DATEADD(wk, DATEDIFF(d, 0, tD.[EventDay]) / 7, 0)) AS [EventDaySunday]
				FROM (
					SELECT DISTINCT
						hIIS.[EventDay] AS [EventDay]
					FROM
						[${hist}$].[dbo].[h_InstanceIOStatus] hIIS
						INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
							    hSI.[id]                  = hIIS.[h_ServerInstance_id]
							AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
							AND hSI.[d_Login_id]          = @d_Login_id
					WHERE
						hIIS.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)
				) tD
			) tW
			ORDER BY
				DATEPART(WEEK, tW.[EventDayMonday]) DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin" type="NVarChar" />
			<sql-select-parameter name="@EventDayMax" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="InstanceDiskIOStatus.Days">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 CONVERT([NVARCHAR](128), DATEPART(DAY, tD.[EventDay])) AS [NodeUName]
				,CONVERT([NVARCHAR](128), DATEPART(DAY, tD.[EventDay])) AS [NodeUId]
				,1                                                      AS [NodeEnabled]
				,N'NodeIcon'                                            AS [NodeUIcon]
				,CASE
					WHEN (DATENAME(weekday, tD.[EventDay]) = N'Sunday') THEN
						N'#CC0000'
					ELSE
						N'#080808'
				END                                                     AS [NodeFontColor]
				,N'normal'                                              AS [NodeFontStyle]
				,CONVERT([NVARCHAR](128), tD.[EventDay], 112)           AS [EventDayMin]
				,CONVERT([NVARCHAR](128), tD.[EventDay], 112)           AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					hIIS.[EventDay] AS [EventDay]
				FROM
					[${hist}$].[dbo].[h_InstanceIOStatus] hIIS
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hIIS.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id
				WHERE
					hIIS.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)
			) tD
			ORDER BY
				tD.[EventDay] DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin" type="NVarChar" />
			<sql-select-parameter name="@EventDayMax" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceDiskIOStatus">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			DECLARE
				 @EventTimeMin [DATETIME]
				,@EventTimeMax [DATETIME]
			;

			SET @EventTimeMin = GETDATE();
			SET @EventTimeMax = GETDATE();

			IF (@EventDayMin IS NOT NULL)
			BEGIN
				SET @EventTimeMin = CONVERT([DATETIME], @EventDayMin, 112);
			END
			IF (@EventDayMax IS NOT NULL)
			BEGIN
				SET @EventTimeMax = DATEADD(dd, 1, CONVERT([DATETIME], @EventDayMax, 112));
			END
			IF (@strDateTimeModifier IS NOT NULL)
			BEGIN
				SELECT
					@EventTimeMax = MAX(hIIS.[EventTime])
				FROM
					[${hist}$].[dbo].[h_InstanceIOStatus] hIIS
					INNER JOIN [${ref}$].[dbo].[h_ref_InstanceDisk] hRSIDisk ON
						hRSIDisk.[h_ServerInstance_id] = hIIS.[h_ServerInstance_id]
						AND hRSIDisk.[h_ref_InstanceDisk_id] = hIIS.[h_ref_InstanceDisk_id]
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hIIS.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id
				WHERE
					hRSIDisk.[Disk] = ISNULL(@Disk, hRSIDisk.[Disk])
					AND hRSIDisk.[Disk] IS NOT NULL;

				IF (@strDateTimeModifier = N'-1 day')
				BEGIN
					SET @EventTimeMin = DATEADD(dd, -1, @EventTimeMax);
				END
				ELSE
				BEGIN
					SET @EventTimeMin = @EventTimeMax;
				END
			END

			SELECT
				 hIIS.[EventTime]                              AS [EventTime]
				,hRSIDisk.[Disk]                               AS [Disk]
				,MAX(hIIS.[sample_ms] - hIIS.[sample_ms_last]) AS [sample_ms]
				,SUM(CASE
					WHEN ((hIIS.[num_of_reads] &gt; hIIS.[num_of_reads_last]) AND (DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) &gt; 0)) THEN
						CAST((hIIS.[num_of_reads] - hIIS.[num_of_reads_last]) AS [REAL]) / CAST(DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) AS [REAL])
					ELSE
						0.0
				END)                                           AS [num_of_reads]
				,SUM(CASE
					WHEN ((hIIS.[num_of_bytes_read] &gt; hIIS.[num_of_bytes_read_last]) AND (DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) &gt; 0)) THEN
						CAST((hIIS.[num_of_bytes_read] - hIIS.[num_of_bytes_read_last]) AS [REAL]) / CAST(DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) AS [REAL])
					ELSE
						0.0
				END)                                           AS [num_of_bytes_read]
				,SUM(CASE
					WHEN ((hIIS.[io_stall_read_ms] &gt; hIIS.[io_stall_read_ms_last]) AND (DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) &gt; 0)) THEN
						CAST((hIIS.[io_stall_read_ms] - hIIS.[io_stall_read_ms_last]) AS [REAL]) / CAST(DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) AS [REAL])
					ELSE
						0.0
				END)                                           AS [io_stall_read_ms]
				,SUM(CASE
					WHEN ((hIIS.[num_of_writes] &gt; hIIS.[num_of_writes_last]) AND (DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) &gt; 0)) THEN
						CAST((hIIS.[num_of_writes] - hIIS.[num_of_writes_last]) AS [REAL]) / CAST(DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) AS [REAL])
					ELSE
						0.0
				END)                                           AS [num_of_writes]
				,SUM(CASE
					WHEN ((hIIS.[num_of_bytes_written] &gt; hIIS.[num_of_bytes_written_last]) AND (DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) &gt; 0)) THEN
						CAST((hIIS.[num_of_bytes_written] - hIIS.[num_of_bytes_written_last]) AS [REAL]) / CAST(DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) AS [REAL])
					ELSE
						0.0
				END)                                           AS [num_of_bytes_written]
				,SUM(CASE
					WHEN ((hIIS.[io_stall_write_ms] &gt; hIIS.[io_stall_write_ms_last]) AND (DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) &gt; 0)) THEN
						CAST((hIIS.[io_stall_write_ms] - hIIS.[io_stall_write_ms_last]) AS [REAL]) / CAST(DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) AS [REAL])
					ELSE
						0.0
				END)                                           AS [io_stall_write_ms]
				,SUM(CASE
					WHEN ((hIIS.[io_stall] &gt; hIIS.[io_stall_last]) AND (DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) &gt; 0)) THEN
						CAST((hIIS.[io_stall] - hIIS.[io_stall_last]) AS [REAL]) / CAST(DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) AS [REAL])
					ELSE
						0.0
				END)                                           AS [io_stall]
			FROM
				[${hist}$].[dbo].[h_InstanceIOStatus] hIIS
				INNER JOIN [${ref}$].[dbo].[h_ref_InstanceDisk] hRSIDisk ON
					    hRSIDisk.[h_ServerInstance_id]   = hIIS.[h_ServerInstance_id]
					AND hRSIDisk.[h_ref_InstanceDisk_id] = hIIS.[h_ref_InstanceDisk_id]
				INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
					    hSI.[id]                  = hIIS.[h_ServerInstance_id]
					AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
					AND hSI.[d_Login_id]          = @d_Login_id
			WHERE
				hIIS.[sample_ms] &gt; hIIS.[sample_ms_last]
				AND DATEDIFF(ss, hIIS.[EventTimeLast], hIIS.[EventTime]) &gt; 0
				AND hRSIDisk.[Disk] IS NOT NULL
				AND hRSIDisk.[Disk] = ISNULL(@Disk, hRSIDisk.[Disk])
				AND hIIS.[EventTime] BETWEEN @EventTimeMin AND @EventTimeMax
			GROUP BY
				 hIIS.[EventTime]
				,hRSIDisk.[Disk]
			ORDER BY
				hIIS.[EventTime] ASC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin"         type="NVarChar" />
			<sql-select-parameter name="@EventDayMax"         type="NVarChar" />
			<sql-select-parameter name="@strDateTimeModifier" type="NVarChar" />
			<sql-select-parameter name="@Disk"                type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceDiskIOPerformanceStatus">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			DECLARE
				 @EventTimeMin [DATETIME]
				,@EventTimeMax [DATETIME]
			;

			SET @EventTimeMin = GETDATE();
			SET @EventTimeMax = GETDATE();

			IF (@EventDayMin IS NOT NULL)
			BEGIN
				SET @EventTimeMin = CONVERT([DATETIME], @EventDayMin, 112);
			END
			IF (@EventDayMax IS NOT NULL)
			BEGIN
				SET @EventTimeMax = DATEADD(dd, 1, CONVERT([DATETIME], @EventDayMax, 112));
			END
			IF (@strDateTimeModifier IS NOT NULL)
			BEGIN
				SELECT
					@EventTimeMax = MAX(hIIS.[EventTime])
				FROM
					[${hist}$].[dbo].[h_InstanceIOStatus] hIIS
					INNER JOIN [${ref}$].[dbo].[h_ref_InstanceDisk] hRSIDisk ON
						    hRSIDisk.[h_ServerInstance_id]   = hIIS.[h_ServerInstance_id]
						AND hRSIDisk.[h_ref_InstanceDisk_id] = hIIS.[h_ref_InstanceDisk_id]
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hIIS.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id
				WHERE
					hRSIDisk.[Disk] = ISNULL(@Disk, hRSIDisk.[Disk])
					AND hRSIDisk.[Disk] IS NOT NULL;

				IF (@strDateTimeModifier = N'-1 day')
				BEGIN
					SET @EventTimeMin = DATEADD(dd, -1, @EventTimeMax);
				END
				ELSE
				BEGIN
					SET @EventTimeMin = @EventTimeMax;
				END
			END

			SELECT
				 hIIS.[EventTime]                              AS [EventTime]
				,hRSIDisk.[Disk]                               AS [Disk]
				,CASE
					WHEN (SUM(CAST(hIIS.[num_of_reads] AS [REAL])) &gt; SUM(CAST(hIIS.[num_of_reads_last] AS [REAL]))) THEN
						SUM(CAST((hIIS.[io_stall_read_ms] - hIIS.[io_stall_read_ms_last]) AS [REAL])) / (SUM(CAST(hIIS.[num_of_reads] AS [REAL])) - SUM(CAST(hIIS.[num_of_reads_last] AS [REAL])))
					ELSE
						0.0
				END                                            AS [io_stall_read_per_page]
				,CASE
					WHEN (SUM(CAST(hIIS.[num_of_writes] AS [REAL])) &gt; SUM(CAST(hIIS.[num_of_writes_last] AS [REAL]))) THEN
						SUM(CAST((hIIS.[io_stall_write_ms] - hIIS.[io_stall_write_ms_last]) AS [REAL])) / (SUM(CAST(hIIS.[num_of_writes] AS [REAL])) - SUM(CAST(hIIS.[num_of_writes_last] AS [REAL])))
					ELSE
						0.0
				END                                            AS [io_stall_write_per_page]
				,CASE
					WHEN (SUM(CAST((hIIS.[num_of_reads] + hIIS.[num_of_writes]) AS [REAL])) &gt; SUM(CAST((hIIS.[num_of_reads_last] + hIIS.[num_of_writes_last]) AS [REAL]))) THEN
						SUM(CAST((hIIS.[io_stall] - hIIS.[io_stall_last]) AS [REAL])) / (SUM(CAST((hIIS.[num_of_reads] + hIIS.[num_of_writes]) AS [REAL])) - SUM(CAST((hIIS.[num_of_reads_last] + hIIS.[num_of_writes_last]) AS [REAL])))
					ELSE
						0.0
				END                                            AS [io_stall_per_page]
			FROM
				[${hist}$].[dbo].[h_InstanceIOStatus] hIIS
				INNER JOIN [${ref}$].[dbo].[h_ref_InstanceDisk] hRSIDisk ON
					    hRSIDisk.[h_ServerInstance_id]   = hIIS.[h_ServerInstance_id]
					AND hRSIDisk.[h_ref_InstanceDisk_id] = hIIS.[h_ref_InstanceDisk_id]
				INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
					    hSI.[id]                  = hIIS.[h_ServerInstance_id]
					AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
					AND hSI.[d_Login_id]          = @d_Login_id
			WHERE
				hIIS.[sample_ms] &gt; hIIS.[sample_ms_last]
				AND hIIS.[io_stall_read_ms] &gt; hIIS.[io_stall_read_ms_last]
				AND hIIS.[num_of_reads] &gt; hIIS.[num_of_reads_last]
				AND hIIS.[io_stall_write_ms] &gt; hIIS.[io_stall_write_ms_last]
				AND hIIS.[num_of_writes] &gt; hIIS.[num_of_writes_last]
				AND hRSIDisk.[Disk] IS NOT NULL
				AND hRSIDisk.[Disk] = ISNULL(@Disk, hRSIDisk.[Disk])
				AND hIIS.[EventDay] BETWEEN CONVERT([DATE], @EventTimeMin) AND CONVERT([DATE], @EventTimeMax)
				AND hIIS.[EventTime] BETWEEN @EventTimeMin AND @EventTimeMax
			GROUP BY
				 hIIS.[EventTime]
				,hRSIDisk.[Disk]
			ORDER BY
				hIIS.[EventTime] ASC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin"         type="NVarChar" />
			<sql-select-parameter name="@EventDayMax"         type="NVarChar" />
			<sql-select-parameter name="@strDateTimeModifier" type="NVarChar" />
			<sql-select-parameter name="@Disk"                type="NVarChar" />
		</sql-select-parameters>
	</sql-select>
</sql-select-content>
