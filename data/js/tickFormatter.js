tickFormatter = function (format, val) {
	if (val >= 1000000000000 || val < -1000000000000) {
		val = val / 1000000000000;

		return val.toFixed(1) + "T";
	}

	if (val >= 1000000000 || val < -1000000000) {
		val = val / 1000000000;

		return val.toFixed(1) + "G";
	}

	if (val >= 1000000 || val < -1000000) {
		val = val / 1000000;

		return val.toFixed(1) + "M";
	}

	if (val >= 1000 || val < -1000) {
		val = val / 1000;

		if (val < 10) {
			return val.toFixed(1) + "K";
		}

		return val.toFixed(0) + "K";
	}

	return $.jqplot.sprintf(format, val);
}
