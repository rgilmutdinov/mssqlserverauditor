﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MSSQLServerAuditor")]
[assembly: InternalsVisibleTo("MSSQLServerAuditor.Core")]
namespace MSSQLServerAuditor.Metadata
{
	internal static class EnvironmentProperties
	{
		internal static string PublicKey  = string.Empty;
		internal static string PrivateKey = string.Empty;
	}
}